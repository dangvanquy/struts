/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.DAL.ChecksumDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * ChecksumService
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class ChecksumService extends BaseService implements Serializable {

    private ChecksumDAL checksumDAL = new ChecksumDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addChecksum(RSAKeysBO keys) throws Exception {
        getChecksumDAL().addChecksum(keys);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateChecksum(RSAKeysBO keys) throws Exception {
        getChecksumDAL().updateChecksum(keys);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public RSAKeysBO getChecksumById(Integer keyId) throws Exception {
        return getChecksumDAL().getChecksumById(keyId);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public RSAKeysBO getChecksumByIdIndex(Integer keyId, Integer index) throws Exception {
        return getChecksumDAL().getChecksumByIdIndex(keyId, index);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getChecksumByIndex(Integer rsaIndex) throws Exception {
        return getChecksumDAL().getChecksumByIndex(rsaIndex);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getChecksum(HashMap hashMap) throws Exception {
        return getChecksumDAL().getChecksum(hashMap);
    }
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getCheckExChecksum(HashMap hashMap) throws Exception {
        return getChecksumDAL().getCheckExChecksum(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getAllChecksum() throws Exception {
        return getChecksumDAL().getAllChecksum();
    }

    public ChecksumDAL getChecksumDAL() {
        if (checksumDAL == null) {
            checksumDAL = new ChecksumDAL();
        }
        return checksumDAL;
    }

    public void setChecksumDAL(ChecksumDAL checkSumDAL) {
        this.checksumDAL = checkSumDAL;
    }
}
