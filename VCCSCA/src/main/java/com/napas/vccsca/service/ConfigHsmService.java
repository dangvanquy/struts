/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.ConfigHsmBO;
import com.napas.vccsca.DAL.ConfigHsmDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * ConfigHsmService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ConfigHsmService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(ConfigHsmService.class);
    private static ConfigHsmDAL paramDAL = new ConfigHsmDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addConfigHsm(ConfigHsmBO paramBO) throws Exception {
        getConfigHsmDAL().addConfigHsm(paramBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateConfigHsm(ConfigHsmBO paramBO) throws Exception {
        getConfigHsmDAL().updateConfigHsm(paramBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteConfigHsm(ConfigHsmBO paramBO) throws Exception {
        getConfigHsmDAL().deleteConfigHsm(paramBO);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public ConfigHsmBO getConfigHsmById(HashMap hashMap) {
        return getConfigHsmDAL().getConfigHsmById(hashMap);
    }
    
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public ConfigHsmBO getConfigHsmBySlot(int slot) {
        return getConfigHsmDAL().getConfigHsmBySlot(slot);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Long getConfigHsm(HashMap hashMap) {
        return getConfigHsmDAL().getConfigHsm(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<ConfigHsmBO> findConfigHsm() {
        return getConfigHsmDAL().findConfigHsm();
    }
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<ConfigHsmBO> findConfigHsmDESC() {
        return getConfigHsmDAL().findConfigHsmDESC();
    }

    public static ConfigHsmDAL getConfigHsmDAL() {
        return paramDAL;
    }

    public static void setConfigHsmDAL(ConfigHsmDAL paramDAL) {
        ConfigHsmService.paramDAL = paramDAL;
    }

}
