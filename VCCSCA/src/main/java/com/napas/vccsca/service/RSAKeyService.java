/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.DAL.RSAKeyDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * RSAKeyService
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class RSAKeyService extends BaseService implements Serializable {

    private RSAKeyDAL keyDAL = new RSAKeyDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addKeys(RSAKeysBO keys) throws Exception {
        getRSAKeyDAL().addKeys(keys);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateKeys(RSAKeysBO keys) throws Exception {
        getRSAKeyDAL().updateKeys(keys);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public RSAKeysBO getKeyById(Integer keyId) throws Exception {
        return getRSAKeyDAL().getKeyById(keyId);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public RSAKeysBO getKeyByIndex(Integer index) throws Exception {
        return getRSAKeyDAL().getKeyByIndex(index);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getKeys(HashMap hashMap) throws Exception {
        return getRSAKeyDAL().getKeys(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getAllKeys() throws Exception {
        return getRSAKeyDAL().getAllKeys();
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RSAKeysBO> getKeysByRsaStatus() throws Exception {
        return getRSAKeyDAL().getKeysByRsaStatus();
    }

    public RSAKeyDAL getRSAKeyDAL() {
        if (keyDAL == null) {
            keyDAL = new RSAKeyDAL();
        }
        return keyDAL;
    }

    public void setRSAKeyDAL(RSAKeyDAL keyDAL) {
        this.keyDAL = keyDAL;
    }
}
