/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.GroupsPermissionBO;
import com.napas.vccsca.DAL.GroupPemissionDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * GroupsPermissionService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class GroupPemissionService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(GroupPemissionService.class);
    private static GroupPemissionDAL groupPemissionDAL = new GroupPemissionDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addGroupsPermission(GroupsPermissionBO groupsPermissionBO) throws Exception {
        getGroupPemissionDAL().addGroupsPermission(groupsPermissionBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteGroupsPermission(GroupsPermissionBO groupsPermissionBO) throws Exception {
        getGroupPemissionDAL().deleteGroupsPermission(groupsPermissionBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateGroupsPermission(GroupsPermissionBO groupsPermissionBO) throws Exception {
        getGroupPemissionDAL().updateGroupsPermission(groupsPermissionBO);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List getAllGroupsPermission() throws Exception {
        return getGroupPemissionDAL().getAllGroupsPermission();
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<GroupsPermissionBO> findGroupsPermission(HashMap hashMap) throws Exception {
        return getGroupPemissionDAL().findGroupsPermission(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<GroupsPermissionBO> chkGroupsPermission(HashMap hashMap) throws Exception {
        return getGroupPemissionDAL().chkGroupsPermission(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public GroupsPermissionBO getgroupPemissionById(Integer idRequest) {
        return getGroupPemissionDAL().getgroupPemissionById(idRequest);
    }

    public static GroupPemissionDAL getGroupPemissionDAL() {
        return groupPemissionDAL;
    }

    public static void setGroupPemissionDAL(GroupPemissionDAL groupPemissionDAL) {
        GroupPemissionService.groupPemissionDAL = groupPemissionDAL;
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public long checkPermisionName(String groupName) {
        return getGroupPemissionDAL().checkPermisionName(groupName);
    }

}
