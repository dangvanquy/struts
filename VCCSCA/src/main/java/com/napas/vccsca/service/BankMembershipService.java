/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.DAL.MembershipBankingDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * BankMembershipService
 *
 * @author CuongTV
 * @since Aug 23, 2018
 * @version 1.0-SNAPSHOT
 */
public class BankMembershipService extends BaseService implements Serializable {

    private static MembershipBankingDAL bankingDAL = new MembershipBankingDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addBanking(BankMembershipBO banking) throws Exception {
        getBankingDAL().addBanking(banking);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateBanking(BankMembershipBO banking) throws Exception {
        getBankingDAL().updateBanking(banking);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteBanking(BankMembershipBO banking) throws Exception {
        getBankingDAL().deleteBanking(banking);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public BankMembershipBO getMembershipBankingById(int bankId) throws Exception {
        return getBankingDAL().getMembershipBankById(bankId);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<BankMembershipBO> findAll() throws Exception {
        return getBankingDAL().getAllBanking();
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<BankMembershipBO> findAllBankActive() throws Exception {
        return getBankingDAL().getAllBankingActive();
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<BankMembershipBO> getMembershipBanking(HashMap hashMap, Integer[] arrBin) throws Exception {
        return getBankingDAL().getMembershipBanking(hashMap, arrBin);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<BankMembershipBO> chkBankMembership(HashMap hashMap) throws Exception {
        return getBankingDAL().chkBankMembership(hashMap);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public BankMembershipBO getMembershipBankByBin(Integer bin) throws Exception {
        return getBankingDAL().getMembershipBankByBin(bin);
    }

    public static MembershipBankingDAL getBankingDAL() {
        if (bankingDAL == null) {
            bankingDAL = new MembershipBankingDAL();
        }
        return bankingDAL;
    }

    public static void setBankingDAL(MembershipBankingDAL bankingDAL) {
        BankMembershipService.bankingDAL = bankingDAL;
    }

}
