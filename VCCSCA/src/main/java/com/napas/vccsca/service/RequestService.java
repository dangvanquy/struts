/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.DAL.RequestDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * RequestService
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class RequestService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(RequestService.class);
    private static RequestDAL requestDAL = new RequestDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void createRequest(RequestBO requestBO) throws Exception {
        getRequestDAL().createRequest(requestBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<RequestBO> getRequestList() throws Exception {
        return getRequestDAL().getRequestList();
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<RequestBO> getRequestListDesc() throws Exception {
        return getRequestDAL().getRequestListDesc();
    }
    
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RequestBO> findRequest(HashMap hashMap) {
        return getRequestDAL().findRequest(hashMap);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<RequestBO> getRequestFileList() throws Exception {
        return getRequestDAL().getRequestFileList();
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RequestBO> findRequestFile(HashMap hashMap) {
        return getRequestDAL().findRequestFile(hashMap);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<RequestBO> findAllRequestRemove() throws Exception {
        return getRequestDAL().findAllRequestRemove();
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<RequestBO> findRequestRemove(HashMap hashMap) {
        return getRequestDAL().findRequestRemove(hashMap);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public RequestBO getRequestById(Integer requestId) throws Exception {
        return getRequestDAL().getRequestById(requestId);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public RequestBO getRequestByRegId(Integer registerId) throws Exception {
        return getRequestDAL().getRequestByRegId(registerId);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<RequestBO> getReportRequests(HashMap hashMap, Integer[] arrBin) throws Exception {
        return getRequestDAL().getReportRequests(hashMap, arrBin);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateRequest(RequestBO requestBO) throws Exception {
        getRequestDAL().updateRequest(requestBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteRequest(RequestBO requestBO) throws Exception {
        getRequestDAL().deleteRequest(requestBO);
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteRequestByBin(int bin) throws Exception {
        getRequestDAL().deleteRequestByBin(bin);
    }

    public RequestDAL getRequestDAL() {
        if (requestDAL == null) {
            requestDAL = new RequestDAL();
        }
        return requestDAL;
    }

    public void setRequestDAL(RequestDAL requestDAL) {
        this.requestDAL = requestDAL;
    }

}
