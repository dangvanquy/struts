/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.PermissionRightBO;
import com.napas.vccsca.DAL.PermissionRightDAL;
import java.io.Serializable;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * PermissionRightService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class PermissionRightService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(PermissionRightService.class);
    private static PermissionRightDAL pemissionRightDAL = new PermissionRightDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addPermissionRight(PermissionRightBO permissionRightBO) throws Exception {
        getPermissionRightDAL().addPermissionRight(permissionRightBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addListPermissionRight(List<PermissionRightBO> lstpermissionRightBO) throws Exception {
        getPermissionRightDAL().addListPermissionRight(lstpermissionRightBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deletePermissionRight(PermissionRightBO permissionRightBO) throws Exception {
        getPermissionRightDAL().deletePermissionRight(permissionRightBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updatePermissionRight(PermissionRightBO permissionRightBO) throws Exception {
        getPermissionRightDAL().updatePermissionRight(permissionRightBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateListPermissionRight(Integer groupId, List<PermissionRightBO> lstPermissionRightBO) {
        getPermissionRightDAL().updatePermissionRight(groupId, lstPermissionRightBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deletePermissionRightByGroupId(Integer groupId) {
        getPermissionRightDAL().deletePermissionRightByGroupId(groupId);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<PermissionRightBO> getMenuPermission(int groupId) {
        return getPermissionRightDAL().getMenuPermission(groupId);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<PermissionRightBO> getPermissionById(Integer groupId) {
        return getPermissionRightDAL().getPermissionById(groupId);
    }

    public static PermissionRightDAL getPermissionRightDAL() {
        return pemissionRightDAL;
    }

    public static void setPermissionRightDAL(PermissionRightDAL pemissionRightDAL) {
        PermissionRightService.pemissionRightDAL = pemissionRightDAL;
    }

    public boolean checkPermision(String userName, String usrID, String code) {
        return getPermissionRightDAL().checkPermision(userName, usrID, code);
    }

}
