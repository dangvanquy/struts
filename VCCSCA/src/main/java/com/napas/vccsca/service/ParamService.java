/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.DAL.ParamDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * ParamService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ParamService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(ParamService.class);
    private static ParamDAL paramDAL = new ParamDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addParam(ParamBO paramBO) throws Exception {
        getParamDAL().addParam(paramBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateParam(ParamBO paramBO) throws Exception {
        getParamDAL().updateParam(paramBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteParam(ParamBO paramBO) throws Exception {
        getParamDAL().deleteParam(paramBO);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public ParamBO getParamById(HashMap hashMap) {
        return getParamDAL().getParamById(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Long getParams(HashMap hashMap) {
        return getParamDAL().getParams(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<ParamBO> findParam() {
        return getParamDAL().findParam();
    }

    public static ParamDAL getParamDAL() {
        return paramDAL;
    }

    public static void setParamDAL(ParamDAL paramDAL) {
        ParamService.paramDAL = paramDAL;
    }

}
