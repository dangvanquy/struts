/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.DAL.ConfigWarningSystemDAL;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * ConfigWarningSystemService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ConfigWarningSystemService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(ConfigWarningSystemService.class);
    private static ConfigWarningSystemDAL configWarningSystemDAL = new ConfigWarningSystemDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addConfigWarningSystem(ConfigWarningSystemBO configWarningSystemBO) throws Exception {
        getConfigWarningSystemDAL().addConfigWarningSystem(configWarningSystemBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateConfigWarningSystem(ConfigWarningSystemBO configWarningSystemBO) throws Exception {
        getConfigWarningSystemDAL().updateConfigWarningSystem(configWarningSystemBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteConfigWarningSystem(ConfigWarningSystemBO configWarningSystemBO) throws Exception {
        getConfigWarningSystemDAL().deleteConfigWarningSystem(configWarningSystemBO);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public ConfigWarningSystemBO getConfigWarningSystemById(HashMap hashMap) {
        return getConfigWarningSystemDAL().getConfigWarningSystemById(hashMap);
    }
    
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List getAllCreateUserConfigWarningSystem() {
        return getConfigWarningSystemDAL().getAllCreateUserConfigWarningSystem();
    }
    
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<ConfigWarningSystemBO> findConfigWarningSystem(HashMap searchMap) {
        return getConfigWarningSystemDAL().findConfigWarningSystem(searchMap);
    }

    public static ConfigWarningSystemDAL getConfigWarningSystemDAL() {
        return configWarningSystemDAL;
    }

    public static void setConfigWarningSystemDAL(ConfigWarningSystemDAL configWarningSystemDAL) {
        ConfigWarningSystemService.configWarningSystemDAL = configWarningSystemDAL;
    }

}
