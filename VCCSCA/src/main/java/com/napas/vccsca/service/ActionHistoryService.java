/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.ActionLogBO;
import com.napas.vccsca.DAL.ActionHistoryDAL;
import com.napas.vccsca.form.ActionHistoryForm;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * ActionHistoryService
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class ActionHistoryService extends BaseService implements Serializable {

    private ActionHistoryDAL actionHistoryDAL = null;

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addActionHistory(ActionLogBO actionLogBO) throws Exception {
        getActionHistoryDAL().addActionLog(actionLogBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addListActionHistory(List<ActionLogBO> actionLogBOs) throws Exception {
        getActionHistoryDAL().addListActionLog(actionLogBOs);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<ActionLogBO> getActionHistory(HashMap hashMap, Integer offset, Integer maxResults) throws Exception {
        return getActionHistoryDAL().getActionLog(hashMap, offset, maxResults);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Long getCount(HashMap hashMap) throws Exception {
        return getActionHistoryDAL().getCount(hashMap);
    }

    public ActionHistoryDAL getActionHistoryDAL() {
        if (actionHistoryDAL == null) {
            actionHistoryDAL = new ActionHistoryDAL();
        }
        return actionHistoryDAL;
    }

    public void setActionHistoryDAL(ActionHistoryDAL actionHistoryDAL) {
        this.actionHistoryDAL = actionHistoryDAL;
    }
}
