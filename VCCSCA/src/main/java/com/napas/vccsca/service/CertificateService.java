/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.DAL.CertificateDAL;
import com.napas.vccsca.form.CertificateForm;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * CertificateService
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
public class CertificateService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(CertificateService.class);
    private CertificateDAL certificateDAL = new CertificateDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addCertificate(DigitalCertificateBO certificateBO) throws Exception {
        getCertificateDAL().addCertificate(certificateBO);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateCertificate(DigitalCertificateBO certificateBO) throws Exception {
        getCertificateDAL().updateCertificate(certificateBO);
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateRQCertificate() throws Exception {
        getCertificateDAL().updateRqCertificate();
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<DigitalCertificateBO> findAll() throws Exception {
        return getCertificateDAL().getAllCertificates();
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public List<DigitalCertificateBO> findAllDesc() throws Exception {
        return getCertificateDAL().getAllCertificatesDesc();
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<DigitalCertificateBO> getCertificates(HashMap hashMap) throws Exception {
        return getCertificateDAL().getCertificates(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<CertificateForm> getCertificateReport(HashMap hashMap) throws Exception {
        return getCertificateDAL().getCertificateReport(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public DigitalCertificateBO getCertificateById(Integer id) throws Exception {
        return getCertificateDAL().getCertificateById(id);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Long getCountBySeria(String seria) throws Exception {
        return getCertificateDAL().getCountBySeria(seria);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public String getNewSeria() throws Exception {
        return getCertificateDAL().getNewSeria();
    }

    public CertificateDAL getCertificateDAL() {
        if (certificateDAL == null) {
            certificateDAL = new CertificateDAL();
        }
        return certificateDAL;
    }

    public void setCertificateDAL(CertificateDAL certificateDAL) {
        this.certificateDAL = certificateDAL;
    }

    public void getCertExpired() {
        getCertificateDAL().getCertExpired();
    }

    public void autoThuhoichungthu() {
        getCertificateDAL().autoThuhoichungthu();
    }

}
