/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.DAL.EmailConfigDAL;
import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * EmailConfigService
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class EmailConfigService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(EmailConfigService.class);
    private static EmailConfigDAL emailConfigDAL = new EmailConfigDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateEmailConfig(ConfigEmailServerBO emailConfigBO) throws Exception {
        getEmailConfigDAL().updateEmailConfig(emailConfigBO);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public ConfigEmailServerBO getEmailConfig() throws Exception {
        return getEmailConfigDAL().getEmailConfig();
    }

    public EmailConfigDAL getEmailConfigDAL() {
        if (emailConfigDAL == null) {
            emailConfigDAL = new EmailConfigDAL();
        }
        return emailConfigDAL;
    }

    public void setEmailConfigDAL(EmailConfigDAL emailConfigDAL) {
        this.emailConfigDAL = emailConfigDAL;
    }

}
