/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.DAL.UsersDAL;
import com.napas.vccsca.common.OutputCommon;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * UsersService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class UsersService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(UsersService.class);
    private UsersDAL usersDAL = new UsersDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addUsers(UsersBO users) throws Exception {
        getUsersDAL().addUsers(users);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteUsers(UsersBO users) throws Exception {
        getUsersDAL().deleteUsers(users);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateUsers(UsersBO users) throws Exception {
        logger.info("update user-id :" + users.getUserId());
        getUsersDAL().updateUsers(users);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public UsersBO getUsersById(Object id) throws Exception {
        return getUsersDAL().getUsersById(id);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public UsersBO getUserById(Integer userId) throws Exception {
        return getUsersDAL().getUserById(userId);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public UsersBO getUserByEmail(String email) throws Exception {
        return getUsersDAL().getUserByEmail(email);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<UsersBO> getUser(HashMap hashMap) throws Exception {
        return getUsersDAL().getUser(hashMap);
    }
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<UsersBO> chkUser(HashMap hashMap) throws Exception {
        return getUsersDAL().chkUser(hashMap);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public boolean checkEmailExist(String email, Long userID) throws Exception {
        return getUsersDAL().checkEmailExist(email, userID);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public OutputCommon getUser(String userName, String password) throws Exception {
        return getUsersDAL().getUser(userName, password);
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<UsersBO> getAllUsers() throws Exception {
        return getUsersDAL().getAllUsers();
    }

    public UsersDAL getUsersDAL() {
        if (usersDAL == null) {
            usersDAL = new UsersDAL();
        }
        return usersDAL;
    }

    public void setUsersDAL(UsersDAL usersDAL) {
        this.usersDAL = usersDAL;
    }

}
