/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.service;

import com.napas.vccsca.BO.SendMailHistoryBO;
import com.napas.vccsca.DAL.SendMailHistoryDAL;
import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * SendMailHistoryService
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendMailHistoryService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(SendMailHistoryService.class);
    private SendMailHistoryDAL sendMailHisDAL = new SendMailHistoryDAL();

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void addNewHistory(SendMailHistoryBO sendMailHistoryBO) throws Exception {
        getSendMailHistoryDAL().addNewHistory(sendMailHistoryBO);
    }

    public SendMailHistoryDAL getSendMailHistoryDAL() {
        if (sendMailHisDAL == null) {
            sendMailHisDAL = new SendMailHistoryDAL();
        }
        return sendMailHisDAL;
    }

    public void setSendMailHistoryDAL(SendMailHistoryDAL sendMailHisDAL) {
        this.sendMailHisDAL = sendMailHisDAL;
    }

}
