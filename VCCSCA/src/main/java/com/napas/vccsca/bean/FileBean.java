/*
 * Copyright 2014 Viettel Software. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.napas.vccsca.bean;

/**
 * @version 1.0
 * @since Aug 31, 2015
 * @author kientt6
 */
public class FileBean {
    private String filePath; 
    private String fileName;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileBean(String filePath, String fileName) {
        this.filePath = filePath;
        this.fileName = fileName;
    }
    
    
}
