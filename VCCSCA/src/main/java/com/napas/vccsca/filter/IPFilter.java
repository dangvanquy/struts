/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.filter;

import com.napas.vccsca.utils.StringUtils;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * IPFilter
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class IPFilter implements Filter {
    
    private static final Logger logger = LogManager.getLogger(IPFilter.class);
    
    private static ConcurrentHashMap<String,String> blacklistIp = new ConcurrentHashMap<String, String>();
    
    private static void addIpToBlacklist(String ipAddress){
        blacklistIp.put(ipAddress, "1");
    }
    
    static{
        //addIpToBlacklist("xxx.xxx.xxx.xxx");
    }
    
    private String getClientIP(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-Forwarded-For");;
        if (StringUtils.isNullOrEmpty(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }

    private FilterConfig config;
    // the regex must define whole string to match - for example a substring without .* will not match
    // note the double backslashes that need to be present in Java code but not in web.xml
    //private String IP_REGEX = "172\\.20\\.\\d+\\.\\d+.*";
    // private String IP_REGEX = "172\\.20\\..*";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //TODO xay dung san fillter IP de nang cap neu can thiet

    }

    public void destroy() {
    }
}
