/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.filter;

import com.napas.vccsca.constants.Constants;
import com.napas.vccsca.service.PermissionRightService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.CookieHelper;
import com.napas.vccsca.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.faces.application.ResourceHandler;
import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.Filter;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;

/**
 *
 * AuthenticateFilter
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@WebFilter(urlPatterns = {"*"}, dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class AuthenticateFilter implements Filter {

    private FilterConfig filterConfig;
    private static final Logger logger = LogManager.getLogger(AuthenticateFilter.class);
    private static final HashSet<String> casAllowedURL = new HashSet();
    public final static ResourceBundle rb = ResourceBundle.getBundle("cas");//cas
    private static String[] allowedUrls;
    private static String[] allowTemplate;
    private static String[] allowAction;

    private static String defaultURL;
    private static String loginURL;
    private static String recoverPasswordUrl;
    private static String homeURL;
    private static String page404URL;
    private static boolean goPage404 = false;

    static {
        try {
            allowedUrls = rb.getString("AllowUrl").split(",");
            allowTemplate = rb.getString("AllowTemplate").split(",");
            allowAction = rb.getString("AllowAction").split(",");

            defaultURL = rb.getString("defaultUrl");
            loginURL = rb.getString("loginUrl");
            recoverPasswordUrl = rb.getString("recoverPasswordUrl");
            page404URL = rb.getString("page404URL");
            homeURL = rb.getString("homeUrl");
        } catch (MissingResourceException e) {
            logger.error("loi xay ra khi lay accessUrl", e);
        }
    }

    @Override
    public void init(FilterConfig config)
            throws ServletException {
        try {
            logger.info("Lay danh sach AllowUrl tu file config 'cas.properties'");
            if (allowedUrls != null && allowedUrls.length > 0) {
                getCasAllowedURL().addAll(Arrays.asList(allowedUrls));
            }
        } catch (Exception ex) {
            logger.error("Loi lay danh sach AllowUrl tu file config:'cas_en_US.properties'", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private Boolean isAuthenticate(HttpServletRequest request) {
        return request != null && request.getSession() != null
                && request.getSession().getAttribute("tokenLogin") != null;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = null;
        HttpServletResponse res = null;
        if ((request instanceof HttpServletRequest)) {
            req = (HttpServletRequest) request;
        }
        if ((response instanceof HttpServletResponse)) {
            res = (HttpServletResponse) response;
        }
        if (req == null) {
            return;
        }
        request.setCharacterEncoding("UTF-8");

        String linkFull = req.getRequestURI();
        linkFull = linkFull.replace(defaultURL, "");
        if ("".equals(linkFull)) {
            linkFull = defaultURL;
        }

        boolean facesResourceRequest = req.getRequestURI().startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER + "/");
        if (facesResourceRequest) {
            if (logger.isDebugEnabled()) {
                logger.debug("facesResourceRequest");
            }
            chain.doFilter(req, response); // Skip JSF resources.
            return;
        }

        //cho phep request vao resource
        if (alowPrefixURL(linkFull, allowTemplate) || alowTempalteURL(linkFull, allowTemplate)) {
            if (logger.isDebugEnabled()) {
                logger.debug("allowURL :" + true);
            }
            chain.doFilter(req, response); // Skip JSF resources.
            return;
        }

        if ("GET".equals(req.getMethod())) {

            //cho phep request vao trang login
            if (linkFull.endsWith(loginURL)
                    || linkFull.endsWith(recoverPasswordUrl)
                    || linkFull.endsWith(page404URL)) {
                if (checkAccount(req, res)) {
                    if (linkFull.endsWith(page404URL)) {
                        chain.doFilter(req, response);
                        return;
                    } else {
                        gotoHomePage(req, res);
                        return;
                    }
                } else {
                    chain.doFilter(req, response); // Skip JSF resources.
                    return;
                }
            }

            if (checkAccount(req, res)) {
                if (!alowURL(linkFull, allowedUrls)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("allowURL :" + true);
                    }
                    goto404Page(req, res);
                    return;
                }
                if ("".equalsIgnoreCase(linkFull) || defaultURL.equals(linkFull)) {
                    gotoHomePage(req, res);
                    return;
                }
                chain.doFilter(req, response);
                return;
            } else {
                if (goPage404) {
                    goto404Page(req, res);
                    return;
                } else {
                    gotoLoginPage(req, res);
                    return;
                }

            }
        }

        if ("POST".equals(req.getMethod())) {

            if (linkFull.endsWith("login.do")
                    || linkFull.endsWith("recoverPassword.do")) {
                if (checkAccount(req, res)) {
                    gotoHomePage(req, res);
                    return;
                } else {
                    chain.doFilter(req, response); // Skip JSF resources.
                    return;
                }
            }

            if (checkAccount(req, res)) {
                //cho phep request vao action controller
                if (alowURL(linkFull, allowAction) || alowURL(linkFull, allowedUrls)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("allowURL :" + true);
                    }
                    chain.doFilter(req, response); // Skip JSF resources.
                    return;
                } else {
                    goto404Page(req, res);
                    return;
                }
            } else {
                if (goPage404) {
                    goto404Page(req, res);
                    return;
                } else {
                    gotoLoginPage(req, res);
                    return;
                }
            }

        }

    }

    private String getcode(String url) {
        try {
            Field[] fields = Constants.PERMISSION.class.getFields();
            for (int i = 0; i < fields.length; i++) {
                try {
                    String[] ret = (String[]) fields[i].get(null);
                    if (ret[1].equals(url)) {
                        return ret[0];
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }

    private boolean checkAccount(HttpServletRequest req, HttpServletResponse res) {
        goPage404 = false;
        if (isAuthenticate(req)) {
            if (logger.isDebugEnabled()) {
                logger.debug("isAuthenticate(req): false");
            }
            Cookie tokenLogin = CookieHelper.getCookie("tokenLogin", req);
            Cookie userName = CookieHelper.getCookie("userName", req);
            if (tokenLogin == null || userName == null) {
                logger.info("go to login page case I");
                return false;
            } else {
                Cookie jSessionId = CookieHelper.getCookie("JSESSIONID", req);
                Object jSessionIdReal = req.getSession().getAttribute("jSessionId-ip");
                if (jSessionId == null || jSessionIdReal == null
                        || !(jSessionId.getValue() + req.getRemoteAddr()).equals(jSessionIdReal.toString())) {
                    logger.info("go to login page case II");
                    return false;
                } else {
                    //check quyen va khoi tao menu
                    if ("POST".equals(req.getMethod())) {
                        String linkFull = req.getRequestURI();
                        linkFull = linkFull.replace(defaultURL, "");//createIndex.do

                        if (linkFull.endsWith("login.do")
                                || linkFull.endsWith("logout.do")
                                || linkFull.endsWith("recoverPassword.do")
                                || linkFull.endsWith("changePass.do")) {
                            return true;
                        }

                        String[] usr = AESUtil.gettoken((String) req.getSession().getAttribute("tokenLogin"));
//                        return new PermissionRightService().checkPermision(usr[1], usr[2], getcode(linkFull));
                        String code = getcode(linkFull);
                        if (new PermissionRightService().checkPermision(usr[1], usr[2], code)) {
                            return true;
                        } else {
                            goPage404 = true;
                            return false;
                        }

                    } else if ("GET".equals(req.getMethod())) {
                        String linkFull = req.getRequestURI();
                        linkFull = linkFull.replace(defaultURL, "");//createIndex.do

                        if (linkFull.endsWith("dang-nhap.html")
                                || linkFull.endsWith("trang-chu.html")
                                || linkFull.endsWith("quen-mat-khau.html")
                                || linkFull.endsWith("doi-mat-khau.html")
                                || linkFull.endsWith("404.html")) {
                            return true;
                        }

                        String[] usr = AESUtil.gettoken((String) req.getSession().getAttribute("tokenLogin"));
                        String code = getcode(linkFull);
                        if ("".equals(code)) {
                            return true;
                        }
                        if (new PermissionRightService().checkPermision(usr[1], usr[2], code)) {
                            return true;
                        } else {
                            goPage404 = true;
                            return false;
                        }
                    }
                    return true;
                }
            }
        } else {
            //bo sung kiem tra neu tokenLogin cua user bi xoa thi bat dang nhap lai
            if (logger.isDebugEnabled()) {
                logger.debug("Check tokenLogin of user was removed");
            }
            return false;
        }
    }

    private boolean isAJAXRequest(HttpServletRequest request) {
        boolean check = false;
        String facesRequest = request.getHeader("Faces-Request");
        if (facesRequest != null && "partial/ajax".equals(facesRequest)) {
            check = true;
        }
        return check;
    }

    private void gotoHomePage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (isAJAXRequest(request)) {
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><partial-response><redirect url=\"").append(defaultURL + homeURL).append("\"></redirect></partial-response>");
            response.setHeader("Cache-Control", "no-cache");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/xml");
            PrintWriter pw = response.getWriter();
            pw.println(sb.toString());
            pw.flush();
        } else {
            response.sendRedirect(defaultURL + homeURL);
        }
    }

    public void gotoLoginPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        CookieHelper.deleteAllCookies(request, response);
        if (isAJAXRequest(request)) {
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><partial-response><redirect url=\"").append(defaultURL + loginURL).append("\"></redirect></partial-response>");
            response.setHeader("Cache-Control", "no-cache");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/xml");
            PrintWriter pw = response.getWriter();
            pw.println(sb.toString());
            pw.flush();
        } else {
            response.sendRedirect(defaultURL + loginURL);
        }
    }

    private void goto404Page(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (isAJAXRequest(request)) {
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><partial-response><redirect url=\"").append(defaultURL + page404URL).append("\"></redirect></partial-response>");
            response.setHeader("Cache-Control", "no-cache");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/xml");
            PrintWriter pw = response.getWriter();
            pw.println(sb.toString());
            pw.flush();
        } else {
            response.sendRedirect(defaultURL + page404URL);
        }
    }

    @Override
    public void destroy() {
    }

    private Boolean alowPrefixURL(String url, String[] listPrefixAlowUrl) {
        if (listPrefixAlowUrl != null && listPrefixAlowUrl.length > 0) {
            for (String str : listPrefixAlowUrl) {
                if (!StringUtils.isNullOrEmpty(str) && url.contains(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    private Boolean alowTempalteURL(String url, String[] listPrefixAlowUrl) {
        if (listPrefixAlowUrl != null && listPrefixAlowUrl.length > 0) {
            for (String str : listPrefixAlowUrl) {
                if (str.startsWith("*.") && url.endsWith(str.substring(1))) {
                    return true;
                }
            }
        }
        return false;
    }

    private Boolean alowURL(String url, String[] listAlowUrl) {
        if (defaultURL.equals(url) || loginURL.equals(url) || page404URL.equals(url) || homeURL.equals(url)) {
            return true;
        }
        for (String str : listAlowUrl) {
            if (!StringUtils.isNullOrEmpty(str) && str.equalsIgnoreCase(url)) {
                return true;
            }
        }
        return false;
    }

    private Boolean alowURLWhenLogin(String url, String[] listAlowUrlWhenLogin) {
        for (String str : listAlowUrlWhenLogin) {
            if (!StringUtils.isNullOrEmpty(str) && str.equalsIgnoreCase(url)) {
                return true;
            }
        }
        return false;
    }

    public static HashSet<String> getCasAllowedURL() {
        return casAllowedURL;
    }

    public FilterConfig getFilterConfig() {
        return this.filterConfig;
    }

    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

}
