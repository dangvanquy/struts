/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.HSM;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;


/**
 *
 * @author TRUNGLUONG
 */
public class testp12 {

    public static void main(String[] args) throws Exception {
        String buff = null;
        /* Some Code that gets 'buff' etc. */
        byte[] byteBuff = hexStringToByteArray(buff);

        char[] passwordChars = "password".toCharArray();
        String fileOne = "output_1.p12";
        String fileTwo = "output_2.p12";

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(null, null);
        keyStore.store(new FileOutputStream(fileOne), passwordChars);

        keyStore = KeyStore.getInstance("PKCS12");

        Path wiki_path = Paths.get("", fileOne);

        byte[] fileContent = Files.readAllBytes(wiki_path);
        
//        Path wiki_path = Paths.get("", fileOne);
//        byte[] byteBuff = Files.readAllBytes(wiki_path);
//        byte[] byteBuff = Files.readAllBytes(wiki_path);
        InputStream inputStream = new ByteArrayInputStream(fileContent);
        keyStore.load(inputStream, passwordChars);
        keyStore.store(new FileOutputStream(fileTwo), passwordChars);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
