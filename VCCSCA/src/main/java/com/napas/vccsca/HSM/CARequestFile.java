/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.HSM;

import static com.napas.vccsca.HSM.testpublickey.hexStringToByteArray;
import static com.napas.vccsca.controller.BaseBean.byteToHex;
import static com.napas.vccsca.controller.BaseBean.byteToHexArr;
import static com.napas.vccsca.controller.BaseBean.hexStringToByteArray;
import static com.napas.vccsca.controller.CertificateController.getHexbyte;
import static com.napas.vccsca.controller.CertificateController.getHexbyteStart;
import com.napas.vccsca.utils.StringUtils;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * CARequestFile
 *
 * @author LuongNK
 * @since Oct 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class CARequestFile {

    // Unsigned IPK Input Extension
    private byte UIPKIEheader = 0x22;
    private byte ipkmLen = 0;
    private byte[] ipkm;
    private byte ipkeLen = 0;
    private byte[] ipke;
    private byte[] regNumb;
    // Self-Signed IPK Data
    private byte SSIPKDEheader = 0x23;

    /**
     * getHexbyte
     *
     * @param byteData
     * @param pos
     * @param bytelen
     * @return
     */
    private static String getHexbyte(String[] byteData, int bytelen) {

        String ret = "";
        try {
            for (int jdx = 0; jdx < bytelen; jdx++) {
                ret += byteData[countIndex + jdx];
            }
        } catch (Exception e) {
            System.out.println("ret:" + ret);
            System.exit(0);
        }
        countIndex = countIndex + bytelen;
        return ret;
    }

    /**
     * getHexbyte
     *
     * @param byteData
     * @param pos
     * @param bytelen
     * @return
     */
    private static String getHexbyteStart(String[] byteData, int bytelen) {
        countIndex = 0;
        String ret = "";
        try {
            for (int jdx = 0; jdx < bytelen; jdx++) {
                ret += byteData[countIndex + jdx];
            }
        } catch (Exception e) {
            System.out.println("ret:" + ret);
            System.exit(0);
        }
        countIndex = countIndex + bytelen;
        return ret;
    }

    static int countIndex = 0;

    public static void main(String[] args) throws Exception {
//        requestfile();
        certfile();
//System.out.println(StringUtils.paddingLeft("10001", 6));
//        BigInteger bb = new BigInteger(hexStringToByteArray("010001"));
//        BigInteger bb2 = new BigInteger("65537");
//        if (bb.compareTo(bb2) == 0) {
//            System.out.println("ok");
//        }
    }

    public static void certfile() {
        //        Path wiki_path = Paths.get("D:/Napas/vccs_ca/01. TAILIEU/temp file/", "NP123456.inp");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file", "456789.i99.abb");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file\\cert", "112233.i99.VCB");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file\\cert", "456789.i99.ABB");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file\\cert", "000002.i99.VTB");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file\\cert", "123789.i99.AGR");
//        Path wiki_path = Paths.get("D:\\Napas\\vccs_ca\\01. TAILIEU\\temp file\\cert", "123987.i00");
        Path wiki_path = Paths.get("C:\\Users\\trung\\Desktop\\rq", "123987.i00");
//        new CARequestFile().readfile(wiki_path);

        try {

            String CAPK_modulus = "B5A2D7130183614F9461E7D120D5FE7EEE627524EB972FDA552484B9FC11AA96F36FB0080F28AB375209E33C4215B897766BD3EF16CDF03AA9496AEBF5E1FEF61446BE47E30B671880B04E66487EBC74E84B87F0FC026257A97E069DB54666368D9D8355BF37545ED8620C44BA6C7CADB7262A8C6A34394ABAAF60C43684669B7862221967CFF8B5C384C9E71B6D02FB5BC315CC36BEE673A3596F20CC7E49CE3899AC42577515B847E425BCF691CF8164141B2ADC06F0CDF9DFB1483728A8BD2F033FF442957E5D97BE731CC2B8C64B5AD3804BE3D5BE47BC3C355908DB8F4D0FA2ACFFE9E703777CB5D45F92AB87D9D9ED1A27FD2E00DB";
            String CAPK_ex = "03";

//            String hexx = "2410100000789456FFFFFF0102E4000103016C43F9591148390F9E3520F2A4AB1CB816C3466A54DF67AACE0625F2C708F87AE0F2D64E344CBCE631740D52634575EF162B8B95D7F6998DEADC2682365952F5908A30215331C4762B7C139F1AAE7A00865989DA28B22209E74B80D318D31750BB0F2AE47DD3A4B56AD1F0521B54752143E04C2873FF7A09D6C66AE4DF4E9A05";
            byte[] fileContent = Files.readAllBytes(wiki_path);
//            byte[] fileContent = hexStringToByteArray(hexx);

            System.out.println("hex:" + byteToHex(fileContent));
//            System.out.println(byteToHex2(fileContent));
            String[] list = byteToHexArr(fileContent);

            HashMap<String, String> hexDataCert = new HashMap<String, String>();

            //5.1.1)Unsigned IPK Output Extension
            String usHeader = getHexbyteStart(list, 1);
            hexDataCert.put("usHeader", usHeader);

            //Service Identifier 
            String usServiceIdentifier = getHexbyte(list, 4);
            hexDataCert.put("usServiceIdentifier", usServiceIdentifier);
            System.out.println("usServiceIdentifier : " + usServiceIdentifier);
            //Issuer Identification Number (IIN) 
            String usIssuerIdentificationNumber = getHexbyte(list, 4);
            hexDataCert.put("usIssuerIdentificationNumber", usIssuerIdentificationNumber);
            System.out.println("usIssuerIdentificationNumber : " + usIssuerIdentificationNumber);

            //Certificate Serial Number
            String usCertificateSerialNumber = getHexbyte(list, 3);
            hexDataCert.put("usCertificateSerialNumber", usCertificateSerialNumber);
            System.out.println("usCertificateSerialNumber : " + usCertificateSerialNumber);

            //Certificate Expiration Date 
            String usCertificateExpirationDate = getHexbyte(list, 2);
            hexDataCert.put("usCertificateExpirationDate", usCertificateExpirationDate);
            System.out.println("usCertificateExpirationDate : " + usCertificateExpirationDate);

            //IPK Modulus Remainder Length 
            String usIPKModulusRemainderLength = getHexbyte(list, 1);
            hexDataCert.put("usIPKModulusRemainderLength", usIPKModulusRemainderLength);
            System.out.println("usIPKModulusRemainderLength" + usIPKModulusRemainderLength);
            System.out.println("Integer.parseInt(usIPKModulusRemainderLength, 16)" + Integer.parseInt(usIPKModulusRemainderLength, 16));

            if (Integer.parseInt(usIPKModulusRemainderLength, 16) > 0) {
                //IPK Modulus Remainder 
                String usIPKModulusRemainder = getHexbyte(list, Integer.parseInt(usIPKModulusRemainderLength, 16));//Phần còn lại của IPK Modulus. Xuất hiện trong trường hợp NI > NCA – 36. Gồm (NI - NCA + 36) byte đầu tiên (bên trái) của IPK Modulus 
                hexDataCert.put("usIPKModulusRemainder", usIPKModulusRemainder);
            }

            //IPK Exponent Length 
            String usIPKExponentLength = getHexbyte(list, 1);
            hexDataCert.put("usIPKExponentLength", usIPKExponentLength);
            System.out.println("usIPKExponentLength : " + usIPKExponentLength);
            //IPK Exponent 
            String usIPKExponent = getHexbyte(list, Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3);
            hexDataCert.put("usIPKExponent", usIPKExponent);
            System.out.println("usIPKExponent : " + usIPKExponent);
            //CA Public Key Index  
            String usCAPublicKeyIndex = getHexbyte(list, 1);
            hexDataCert.put("usCAPublicKeyIndex", usCAPublicKeyIndex);
            System.out.println("usCAPublicKeyIndex : " + usCAPublicKeyIndex);

            //----------------------------------------------phan ky
            String allSign = getHexbyte(list, list.length - countIndex);
            byte[] allSignbyte = hexStringToByteArray(allSign);

            //Trong đó e, NI , and NCA lần lượt là độ dài tính bằng byte của IPK Exponent, IPK Modulus, và CA Public Key dùng để tạo chứng thư số. 
            int e = Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3;
            int Nca = allSignbyte.length;//NCA Độ dài khóa của CA (tính bằng byte) 
            hexDataCert.put("Nca", Nca + "");
            RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(CAPK_modulus, 16), new BigInteger("03", 16));
            PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(spec);

            Cipher cipher = Cipher.getInstance("RSA/ECB/NOPADDING");
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] allSignbyteDecode = cipher.doFinal(allSignbyte);

            System.out.println(">>>>" + byteToHex(allSignbyteDecode));
            list = byteToHexArr(allSignbyteDecode);

            //5.1.2)IPK Certificate 
            //Recovered Data Header
            String recoveredDataHeader = getHexbyteStart(list, 1);
            hexDataCert.put("recoveredDataHeader", recoveredDataHeader);
            //Certificate Format
            String certificateFormat = getHexbyte(list, 1);
            hexDataCert.put("certificateFormat", certificateFormat);
            //Issuer Identification Number (IIN) 
            String issuerIdentificationNumber = getHexbyte(list, 4);
            hexDataCert.put("issuerIdentificationNumber", issuerIdentificationNumber);
            //Certificate Expiration Date
            String certificateExpirationDate = getHexbyte(list, 2);
            hexDataCert.put("certificateExpirationDate", certificateExpirationDate);
            //Certificate Serial Number 
            String certificateSerialNumber = getHexbyte(list, 3);
            hexDataCert.put("certificateSerialNumber", certificateSerialNumber);

            //Hash Algorithm Indicator 1
            String hashAlgorithmIndicator = getHexbyte(list, 1);
            hexDataCert.put("hashAlgorithmIndicator", hashAlgorithmIndicator);
            //IPK Algorithm Indicator 1
            String iPKAlgorithmIndicator = getHexbyte(list, 1);
            hexDataCert.put("iPKAlgorithmIndicator", iPKAlgorithmIndicator);
            //IPK Length 1
            String iPKLength = getHexbyte(list, 1);
            hexDataCert.put("iPKLength", iPKLength);
            //IPK Exponent Length 1
            String iPKExponentLength = getHexbyte(list, 1);
            hexDataCert.put("iPKExponentLength", iPKExponentLength);

            //IPK Modulus or Leftmost part of the IPK Modulus NCA – 36 
            String iPKModulus = getHexbyte(list, Nca - 36);
            hexDataCert.put("iPKModulus", iPKModulus);
            System.out.println("iPKModulus: " + iPKModulus);

            //Hash Result 20
            String hashResult = getHexbyte(list, 20);
            hexDataCert.put("hashResult", hashResult);
            //Recovered Data Trailer 1
            String recoveredDataTrailer = getHexbyte(list, 1);
            hexDataCert.put("recoveredDataTrailer", recoveredDataTrailer);

            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");

            String hexsha = hexDataCert.get("certificateFormat")
                    + hexDataCert.get("issuerIdentificationNumber")
                    + hexDataCert.get("certificateExpirationDate")
                    + hexDataCert.get("certificateSerialNumber")
                    + hexDataCert.get("hashAlgorithmIndicator")
                    + hexDataCert.get("iPKAlgorithmIndicator")
                    + hexDataCert.get("iPKLength")
                    + hexDataCert.get("iPKExponentLength")
                    + hexDataCert.get("iPKModulus");
            System.out.println(">>" + hexsha);
            byte[] dataToSign = hexStringToByteArray(hexsha);
//            byte[] dataToSign = new byte[]{};
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("certificateFormat")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("issuerIdentificationNumber")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("certificateExpirationDate")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("certificateSerialNumber")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("hashAlgorithmIndicator")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("iPKAlgorithmIndicator")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("iPKLength")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("iPKExponentLength")));
//            dataToSign = appendArray(dataToSign, hexStringToByteArray(hexDataCert.get("iPKExponentLength")));

            dataToSign = hashSHA1(dataToSign);
            hexsha = byteToHex(dataToSign);
            //g) So sánh trường Hash Result đã được tính từ bước trước với Hash Result đã được khôi phục, nếu chúng không giống nhau, việc xác thực không thành công. 

            System.out.println(">>" + hexsha);
            System.out.println(">>" + hexDataCert.get("hashResult"));
//            if (!checksha.equalsIgnoreCase(hexDataCert.get("hashResult"))) {
////            return false;
//            }

        } catch (Exception e) {
            System.out.print("Exception");
            e.printStackTrace();
        }
    }

    private static byte[] hashSHA1(byte[] Message) throws NoSuchAlgorithmException {
        MessageDigest md = null;
        md = MessageDigest.getInstance("SHA-1");
        return md.digest(Message);
    }

    private static byte[] appendArray(byte[] arr, byte newObj) {
        byte[] result = Arrays.copyOf(arr, arr.length + 1);
        result[result.length - 1] = newObj;
        return result;
    }

    private static byte[] appendArray(byte[] arr1, byte[] arr2) {
        byte[] result = new byte[(arr1 != null ? arr1.length : 0) + (arr2 != null ? arr2.length : 0)];
        System.arraycopy(arr1, 0, result, 0, arr1.length);
        if (arr2 != null) {
            System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
        }
        return result;
    }

//    private static byte[] hashSHA1(byte[] Message) throws NoSuchAlgorithmException {
//        MessageDigest md = null;
//        md = MessageDigest.getInstance("SHA-1");
//        return md.digest(Message);
//    }
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    static HashMap<String, String> hexDataRequest = null;

    public static void requestfile() throws Exception {

//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file", "NP123456.inp");//loi
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file", "NP000001.inp");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file", "CC040301.inp");
//        Path wiki_path = Paths.get("D:\\Napas\\01. TAILIEU\\temp file", "CC970425_000002_99.inp");
        Path wiki_path = Paths.get("C:\\Users\\trung\\Desktop\\rq", "NP000001.inp");

        byte[] fileContent = Files.readAllBytes(wiki_path);

        System.out.println(byteToHex(fileContent));
        System.out.println(byteToHex2(fileContent));
        String[] list = byteToHexArr(fileContent);

        hexDataRequest = new HashMap<String, String>();
        countIndex = 0;

        //4.1.1 Unsigned IPK Input Extension
        //Header 
        String usHeader = getHexbyte(list, 1);
        hexDataRequest.put("usHeader", usHeader);
        //Length of IPK Modulus  
        String usLengthofIPKModulus = getHexbyte(list, 1);
        hexDataRequest.put("usLengthofIPKModulus", usLengthofIPKModulus);

        int decimal = Integer.parseInt(usLengthofIPKModulus, 16);
        //IPK Modulus 
        String usIPKModulus = getHexbyte(list, decimal);//NI: là độ dài của IPK Modulus (đơn vị: byte). 
        hexDataRequest.put("usIPKModulus", usIPKModulus);
        System.out.println("usIPKModulus:" + usIPKModulus);
        //IPK Exponent Length  
        String usIPKExponentLength = getHexbyte(list, 1);
        hexDataRequest.put("usIPKExponentLength", usIPKExponentLength);
        System.out.println("usIPKExponentLength: " + usIPKExponentLength);
        //IPK Exponent 
        String usIPKExponent = getHexbyte(list, Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3);//1 or 3 IPK Exponent. Định dạng hex, chỉ nhận một trong hai giá trị 03 (3) hoặc 01 00 01 (65537) 
        hexDataRequest.put("usIPKExponent", usIPKExponent);
        System.out.println("usIPKExponent:" + usIPKExponent);
        //Registered Number  
        String usRegisteredNumber = getHexbyte(list, 3);
        hexDataRequest.put("usRegisteredNumber", usRegisteredNumber);
        System.out.println("usRegisteredNumber:" + usRegisteredNumber);

        String allSign = getHexbyte(list, list.length - countIndex);
        byte[] allSignbyte = hexStringToByteArray(allSign);

        int Ni = Integer.parseInt(usLengthofIPKModulus, 16);

        RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(usIPKModulus, 16), new BigInteger(usIPKExponent, 16));
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(spec);

        Cipher cipher = Cipher.getInstance("RSA/ECB/NOPADDING");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] allSignbyteDecode = cipher.doFinal(allSignbyte);

        System.out.println(">>>>" + byteToHex(allSignbyteDecode));
        list = byteToHexArr(allSignbyteDecode);
        countIndex = 0;

        //4.1.2 Se lf-Signed IPK Data
        //Header 
        String seHeader = getHexbyte(list, 1);
        hexDataRequest.put("seHeader", seHeader);
        //Service Identifier  
        String seServiceIdentifier = getHexbyte(list, 4);
        hexDataRequest.put("seServiceIdentifier", seServiceIdentifier);
        //Certificate Format  
        String seCertificateFormat = getHexbyte(list, 1);
        hexDataRequest.put("seCertificateFormat", seCertificateFormat);
        //Issuer Identification Number (IIN) 
        String seIssuerIdentificationNumber = getHexbyte(list, 4);
        hexDataRequest.put("seIssuerIdentificationNumber", seIssuerIdentificationNumber);
        //Certificate Expiration Date 
        String seCertificateExpirationDate = getHexbyte(list, 2);
        hexDataRequest.put("seCertificateExpirationDate", seCertificateExpirationDate);
        //Registered Number  
        String seRegisteredNumber = getHexbyte(list, 3);
        hexDataRequest.put("seRegisteredNumber", seRegisteredNumber);
        //Hash Algorithm Indicator  
        String seHashAlgorithmIndicator = getHexbyte(list, 1);
        hexDataRequest.put("seHashAlgorithmIndicator", seHashAlgorithmIndicator);
        //Issuer’s Public Key Algorithm Indicator  
        String seIssuersPublicKeyAlgorithmIndicator = getHexbyte(list, 1);
        hexDataRequest.put("seIssuersPublicKeyAlgorithmIndicator", seIssuersPublicKeyAlgorithmIndicator);
        //IPK Modulus Length  
        String seIPKModulusLength = getHexbyte(list, 1);
        hexDataRequest.put("seIPKModulusLength", seIPKModulusLength);
        //IPK Exponent Length 
        String seIPKExponentLength = getHexbyte(list, 1);
        hexDataRequest.put("seIPKExponentLength", seIPKExponentLength);

        int e = Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3;
        int leftmostLen = Ni - (39 + e);
        //Leftmost part of IPK Modulus  
        String seLeftmostpartofIPKModulus = getHexbyte(list, leftmostLen);//NI − (39+ e) Phần tử bên trái của IPK Modulus, tính bằng cách lấy [NI − (39 + e)] byte đầu tiên của IPK Modulus
        hexDataRequest.put("seLeftmostpartofIPKModulus", seLeftmostpartofIPKModulus);

        //IPK Exponent 
        String seIPKExponent = getHexbyte(list, e);//e IPK Exponent, định dạng hex. Giá trị bằng 03 (3) hoặc 01 00 01 (65537)
        hexDataRequest.put("seIPKExponent", seIPKExponent);
        //Hash Result 
        String seHashResult = getHexbyte(list, 20);
        hexDataRequest.put("seHashResult", seHashResult);

        for (Map.Entry<String, String> entry : hexDataRequest.entrySet()) {
            System.out.println(">" + entry.getKey() + ":" + entry.getValue() + " --- ");
//            byte[] bb = hexStringToByteArray(entry.getValue());
//            for (Byte b : bb) {
//                System.out.print("" + b);
//            }
//            System.out.println("");
        }
    }

//    public static byte[] hexStringToByteArray(String s) {
//        int len = s.length();
//        byte[] data = new byte[len / 2];
//        for (int i = 0; i < len; i += 2) {
//            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
//                    + Character.digit(s.charAt(i + 1), 16));
//        }
//        return data;
//    }
    public static String byteToHex2(byte[] bytes) {
        String result = "";
        for (int i = 0; i < bytes.length; i++) {
            result
                    += Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static String byteToHex(byte[] bytes) {
        String reString = "";
        for (byte b : bytes) {
            reString += String.format("%02X", b);
        }
        return reString;
    }

    public static String[] byteToHexArr(byte[] bytes) {
        String[] hexArr = new String[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            hexArr[i] = String.format("%02X", bytes[i]);
        }
        return hexArr;
    }

    public void readfile(Path inpFilePath) {
        try {
            byte[] fileContent = Files.readAllBytes(inpFilePath);
            ByteBuffer buffer = ByteBuffer.wrap(fileContent);

            // read Unsigned IPK Input Extension
            int index = 0;
            if (buffer.get(index) != UIPKIEheader) {
                throw new Exception("Wrong Unsigned IPK Input Extension header");
            }
            index += 1;

            ipkmLen = buffer.get(index);
            index += 1;

            buffer.get(ipkm, index, ipkmLen);
            index += ipkmLen;

            ipkeLen = buffer.get(index);
            index += 1;

            buffer.get(ipke, index, ipkeLen);
            index += ipkeLen;

            buffer.get(regNumb, index, 3);
            index += 3;

            // read Self-Signed IPK Data
            if (buffer.get(index) != SSIPKDEheader) {
                throw new Exception("Wrong Self-Signed IPK Data header");
            }
            index += 1;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
