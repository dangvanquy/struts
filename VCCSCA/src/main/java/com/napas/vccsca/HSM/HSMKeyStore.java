package com.napas.vccsca.HSM;

import javax.crypto.*;
import java.security.*;
import java.util.*;

import au.com.safenet.crypto.provider.SAFENETProvider;
import java.math.BigInteger;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class HSMKeyStore {

    /**
     * genPublicKeySAFENET
     *
     * @param strModulus
     * @param strExponent
     * @return
     * @throws Exception
     */
    public static PublicKey genPublicKeySAFENET(String strModulus, String strExponent) throws Exception {
        try {
            Provider p = new SAFENETProvider();
            Security.addProvider(p);

            BigInteger modulus = new BigInteger(hexStringToByteArray(strModulus));
            BigInteger exponent = new BigInteger(hexStringToByteArray(strExponent));

            RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
            KeyFactory factory = KeyFactory.getInstance("RSA", p);

            PublicKey pub = factory.generatePublic(spec);
            return pub;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * addKeyToHSM ADD a random des key to the store
     */
    public static boolean addKeyToHSM(String password, String addKeyName, byte[] privateKeyBytes) throws Exception {
        try {

            /* make sure that we have access to the safenet provider */
            Provider p = new SAFENETProvider();
            Security.addProvider(p);

            /* get the safenet keystore - access to the adapter */
            KeyStore keyStore = KeyStore.getInstance("CRYPTOKI", p.getName());

            /* LOAD the keystore from the adapter - presenting the password if required */
            if (password == null) {
                keyStore.load(null, null);
            } else {
                keyStore.load(null, password.toCharArray());
            }

            /* This key cannot be added to the keystore if it already exists */
            if (keyStore.containsAlias(addKeyName)) {
                System.out.println("Key name already exists");
                return false;
            }

//            PublicKey publicKey
//                    = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyBytes));
//            PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
//            KeyPair keyPair = new KeyPair(publicKey, privateKey);
            SecretKey skey = new SecretKeySpec(privateKeyBytes, addKeyName);

            keyStore.setKeyEntry(addKeyName, skey, null, null);

        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    /**
     * deleteKeyToHSM DELETE a key from the keystore
     */
    public static void deleteKeyToHSM(String password, String delKeyName) throws Exception {
        try {

            /* make sure that we have access to the safenet provider */
            Provider p = new SAFENETProvider();
            Security.addProvider(p);

            /* get the safenet keystore - access to the adapter */
            KeyStore keyStore = KeyStore.getInstance("CRYPTOKI", p.getName());

            /* LOAD the keystore from the adapter - presenting the password if required */
            if (password == null) {
                keyStore.load(null, null);
            } else {
                keyStore.load(null, password.toCharArray());
            }
            /*
				 * Validate that the specified key exists
             */
            if (!keyStore.isKeyEntry(delKeyName)) {
                System.out.println(delKeyName + " is not a recognised key");
            }
            /* the key exists so delete it */
            keyStore.deleteEntry(delKeyName);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * ListKeyHSM
     *
     * LIST all the aliases of keys in the store
     */
    public static void ListKeyHSM(String password) throws Exception {
        try {

            /* make sure that we have access to the safenet provider */
            Provider p = new SAFENETProvider();
            Security.addProvider(p);

            /* get the safenet keystore - access to the adapter */
            KeyStore keyStore = KeyStore.getInstance("CRYPTOKI", p.getName());

            /* LOAD the keystore from the adapter - presenting the password if required */
            if (password == null) {
                keyStore.load(null, null);
            } else {
                keyStore.load(null, password.toCharArray());
            }
            try {
                /* get an enumeration of all the key names (aliases), print each one */
                for (Enumeration enumKeys = keyStore.aliases(); enumKeys.hasMoreElements();) {
                    System.out.println("enumKeys: " + enumKeys.nextElement().toString());
                }
            } catch (KeyStoreException kse) {
                throw kse;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * hexStringToByteArray
     *
     * @param s
     * @return
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
