/*
 * $Id: prod/jprov_sfnt/samples/safenet/ptkj/samples/keypair/GenKeyPair.java 1.1 2009/11/05 10:29:36GMT-05:00 Sorokine, Joseph (jsorokine) Exp  $
 * $Author: Sorokine, Joseph (jsorokine) $
 *
 * Copyright (c) 2002 Safenet Technologies
 * All Rights Reserved - Proprietary Information of Safenet Technologies
 * Not to be Construed as a Published Work.
 *
 * $Source: prod/jprov_sfnt/samples/safenet/ptkj/samples/keypair/GenKeyPair.java $
 * $Revision: 1.1 $
 * $Date: 2009/11/05 10:29:36GMT-05:00 $
 * $State: Exp $
 *
 * Created on 3 October 2002, 16:20
 */
package com.napas.vccsca.HSM;

import SafenetPL.PCIPL_Adap;
import java.security.*;
import java.security.interfaces.*;
import au.com.safenet.crypto.provider.SAFENETProvider;
import com.napas.vccsca.utils.AESUtil;

/**
 *
 * GenKeyPair
 *
 * @author LuongNK
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class GenKeyPair {

    static public Provider provider = null;

    /**
     * createRSA
     *
     * @param keySize
     */
    public RSAPublicKey createRSA(String privateKeyName, int keySize) throws Exception {
        try {
            /* safenet provider */
            provider = new SAFENETProvider();
            Security.addProvider(provider);
            /* RSA key key-size */
            if (keySize <= 0) {
                keySize = 1024;//default 1024 byte
            }
            KeyPair keyPair = null;

            /* Instantiate Key Pair Generator */
            KeyPairGenerator keyPairGenerator
                    = KeyPairGenerator.getInstance("RSA", provider.getName());

            /* Initialise Key Pair Generator */
            keyPairGenerator.initialize(keySize);

            /* generate the key pair 
            Create alice's key pair */
            keyPair = keyPairGenerator.generateKeyPair();

//            /* get the safenet keystore - access to the adapter */
//            KeyStore keyStore = KeyStore.getInstance("CRYPTOKI", provider.getName());
//            /* This key cannot be added to the keystore if it already exists */
//            if (keyStore.containsAlias(privateKeyName)) {
//                System.out.println("Key name already exists");
//                return null;
//            }
//            keyStore.setKeyEntry(privateKeyName, keyPair, null, null);
            RSAPublicKey pubKey = (RSAPublicKey) keyPair.getPublic();
            return pubKey;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * createRSA
     *
     * @param keySize
     */
    public KeyPair createRSASUN(int keySize) throws Exception {
        try {
            /* RSA key key-size */
            if (keySize <= 0) {
                keySize = 1024;//default 1024 byte
            }
            KeyPair keyPair = null;

            /* Instantiate Key Pair Generator */
            KeyPairGenerator keyPairGenerator
                    = KeyPairGenerator.getInstance("RSA");

            /* Initialise Key Pair Generator */
            keyPairGenerator.initialize(keySize);

            /* generate the key pair 
            Create alice's key pair */
            keyPair = keyPairGenerator.generateKeyPair();
            return keyPair;
//            RSAPublicKey pubKey = (RSAPublicKey) keyPair.getPublic();
//            return pubKey;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    static String sha256(byte[] input) throws Exception {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] result = mDigest.digest(input);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
