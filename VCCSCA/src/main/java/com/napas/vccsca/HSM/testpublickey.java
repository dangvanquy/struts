/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.HSM;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

/**
 *
 * testpublickey
 *
 * @author LuongNK
 * @since Oct 13, 2018
 * @version 1.0-SNAPSHOT
 */
public class testpublickey {

     public static String getBankIdentity(String bankIdentity) {
        for (int i = 0; bankIdentity.length() < 8; i++) {
            bankIdentity = bankIdentity + "F";
        }
        return bankIdentity;
    }
     
    public static void main(String[] args) throws Exception {
//        System.out.println(getBankIdentity("5564"));
        byte b = 0x03;
//        String strExponent = "010001";
        String strExponent = "03";
        String strModulus = "B5A2D7130183614F9461E7D120D5FE7EEE627524EB972FDA552484B9FC11AA96F36FB0080F28AB375209E33C4215B897766BD3EF16CDF03AA9496AEBF5E1FEF61446BE47E30B671880B04E66487EBC74E84B87F0FC026257A97E069DB54666368D9D8355BF37545ED8620C44BA6C7CADB7262A8C6A34394ABAAF60C43684669B7862221967CFF8B5C384C9E71B6D02FB5BC315CC36BEE673A3596F20CC7E49CE3899AC42577515B847E425BCF691CF8164141B2ADC06F0CDF9DFB1483728A8BD2F033FF442957E5D97BE731CC2B8C64B5AD3804BE3D5BE47BC3C355908DB8F4D0FA2ACFFE9E703777CB5D45F92AB87D9D9ED1A27FD2E00DB";
        System.out.println("strExponent>>>>>>>" + new BigInteger(hexStringToByteArray(strExponent)));
        System.out.println("strModulus>>>>>>>" + new BigInteger(hexStringToByteArray(strModulus)));

        BigInteger modulus = new BigInteger(strModulus,16);
        BigInteger exponent = new BigInteger(strExponent,16);

        RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PublicKey pub = factory.generatePublic(spec);
//        RSAPublicKey r= actory.generatePublic(spec);
//
        System.out.println("pub>>" + pub);
        System.out.println(sha256("79173A0CABACEB8A62EBEFE0C08EA9A9F441A36DF264CA918E18587BFD611C64A24A755AB4C5C77A36B142282C0E7B0FA447E29F64894AD1C630F1F2A3EBFF4EB82F298542079A1055CADEEEDAFF284DF0325AA0A80196E51BA959BE78D99979B3BE578E7F7A383F3AEC082DD19DA873CF6EC70846CD7B87271F95D703288588770AE234665D1BA7CBC657D74DC05CAE6A1C37B96029B188C7F0A09DF80BE7A59D9E9C9408929BBFCC930BBA6BF6F147F0A9D4714B68FC91117407074E55FA8985E0C10D7560B7406925EFF3FF03559AC34357BCA62F07784578B751C7292E1E3657F5E9B4EA89C008F2A71B759393F27093AB63306AF23B"));
//        System.out.println("-->" + r.);

//        Signature verifier = Signature.getInstance("SHA1withRSA");
//        verifier.initVerify(pub);
//        verifier.update(url.getBytes("UTF-8")); // Or whatever interface specifies.
//        boolean okay = verifier.verify(signature);
    }
static String sha256(String input) throws Exception {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    /**
     * hexStringToByteArray
     *
     * @param s
     * @return
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
