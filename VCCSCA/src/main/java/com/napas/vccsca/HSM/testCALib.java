
import ISO9796_2.EMV_Cert_Request;
import ISO9796_2.EMVcert;
import ISO9796_2.VCCS_CA_FileFormat;
import SafenetPL.PCIPL_Adap;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class testCALib {
    public static void main(String[] agrs) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, NoSuchProviderException, KeyStoreException, IOException, CertificateException
    {
        //import PrivateKey to HSM from Modulus&PrivateExponent
        String CAPK_modulus = "B5A2D7130183614F9461E7D120D5FE7EEE627524EB972FDA552484B9FC11AA96F36FB0080F28AB375209E33C4215B897766BD3EF16CDF03AA9496AEBF5E1FEF61446BE47E30B671880B04E66487EBC74E84B87F0FC026257A97E069DB54666368D9D8355BF37545ED8620C44BA6C7CADB7262A8C6A34394ABAAF60C43684669B7862221967CFF8B5C384C9E71B6D02FB5BC315CC36BEE673A3596F20CC7E49CE3899AC42577515B847E425BCF691CF8164141B2ADC06F0CDF9DFB1483728A8BD2F033FF442957E5D97BE731CC2B8C64B5AD3804BE3D5BE47BC3C355908DB8F4D0FA2ACFFE9E703777CB5D45F92AB87D9D9ED1A27FD2E00DB";
        String CAPriK_exponent = "79173A0CABACEB8A62EBEFE0C08EA9A9F441A36DF264CA918E18587BFD611C64A24A755AB4C5C77A36B142282C0E7B0FA447E29F64894AD1C630F1F2A3EBFF4EB82F298542079A1055CADEEEDAFF284DF0325AA0A80196E51BA959BE78D99979B3BE578E7F7A383F3AEC082DD19DA873CF6EC70846CD7B87271F95D703288588770AE234665D1BA7CBC657D74DC05CAE6A1C37B96029B188C7F0A09DF80BE7A59D9E9C9408929BBFCC930BBA6BF6F147F0A9D4714B68FC91117407074E55FA8985E0C10D7560B7406925EFF3FF03559AC34357BCA62F07784578B751C7292E1E3657F5E9B4EA89C008F2A71B759393F27093AB63306AF23B";    
//        PCIPL_Adap.importPrivateKey(0, "Canapas123", "VCCS_POC", CAPK_modulus, CAPriK_exponent, null);
        
        //Generate IssCert using imported PrivateKey
        EMV_Cert_Request certReq = new EMV_Cert_Request();
        EMVcert tool = new EMVcert();
        certReq.setCAPublicKeyIndex((byte)0x99);
        certReq.setIssuerIdentifier(new byte[]{(byte)0x97,(byte)0x04,(byte)0x36,(byte)0xFF});
        certReq.setCertificateExpirationDate(new byte[]{(byte)0x12,(byte)0x24});
        certReq.setCertificateSerialNumber(new byte[]{(byte)0x36,(byte)0x36,(byte)0x36});
        certReq.setIssuerPublicKeyModulus("BFA4D4233C60B9B5CE687632FC925A77A7FAFAD467785173BCB1D3BF683AD9FD112071C20777E6F7E51B2174722E172390C8A47608D63ECEF197DD958190F6595A8BE4A48290BF323C86892B48E9FD0C77B8DBADD6718ABD9BBFD84A330500FC7365F0BD66EC61D5646B47E06A1F425F280C5DB0BA052A7F46A3349BFC13B8FEFA477EA22BF7D9EB49F108D4AF8603CD5EF0412752E0F0107B5B76304C397C2E34A13052E04F7C6F2E7284CA7DE77FA695713168E2E24C46E24745B168E8A3167AB97E9F22CF57A70ED2C9034AA5212F82E1009C626200747EEB885407434E31834C49D8CE9C32857D7B9FA242A330632792F4CB029679");        

        EMVcert certGen = new EMVcert();
        byte[] cert = certGen.genEMV_IssuerCert(0, "Canapas123", "VCCS_POC", "", certReq);
        System.out.println("cert: "+DatatypeConverter.printHexBinary(cert));
        byte[] certFile = certGen.genVCCS_IssuerFile(0, "Canapas123", "VCCS_POC", "", certReq);
        System.out.println("cert File: "+DatatypeConverter.printHexBinary(certFile));

        VCCS_CA_FileFormat format = new VCCS_CA_FileFormat();
        format.setCAPKindex((byte)0x99);
        format.setCAPKexponent(new byte[]{(byte)0x03});
        format.setCAPKExpirationDate(new byte[]{(byte)0x12,(byte)0x26});
        byte[] CAFile = certGen.genVCCS_CAPKFile(0, "Canapas123", "VCCS_POC", "", format);
        System.out.println("CA File: "+DatatypeConverter.printHexBinary(CAFile));
    }
}
