/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.email;

import java.util.Properties;

/**
 *
 * EmailUtils
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class EmailUtils {

    public static void setProperties(Properties props, String server, int port) {
        props.put("mail.transport.protocol.rfc822", "smtps");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", server);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "*");
    }
}
