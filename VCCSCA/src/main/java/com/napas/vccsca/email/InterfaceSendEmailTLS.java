/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.email;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.bean.FileBean;
import com.napas.vccsca.utils.StringUtils;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * InterfaceSendEmailTLS
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class InterfaceSendEmailTLS extends SendEmail {

    @Override
    protected Properties setProperties() throws Exception {
        Properties props = System.getProperties();
        EmailUtils.setProperties(props, server, port);
        return props;
    }

    protected Properties setProperties(String passwordDecrypt) throws Exception {
        //Set mail properties
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", server);
        props.put("mail.smtp.user", username);
        props.put("mail.smtp.password", passwordDecrypt);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        return props;
    }

    public InterfaceSendEmailTLS(Logger logger) {
        super(logger);
    }

    @Override
    public String sendEmail(List<String> toList, List<String> ccList, List<String> bccList, String subject, String body, List<FileBean> attachments) throws MessagingException, NoSuchAlgorithmException, KeyManagementException, Exception {
        logger.info("Start send email secure TLS...");
        String to = null;
        try {
            to = getlistTo(toList);
            String passwordDecrypt = password;
            String mail_subject = subject;
            Session session = Session.getDefaultInstance(setProperties(passwordDecrypt));
            MimeMessage message = new MimeMessage(session);

            try {
                //Set email data 
                message.setFrom(new InternetAddress(emailAddress));
                this.doAddToList(message, toList);
                this.doAddCcList(message, ccList);
                this.doAddBccList(message, bccList);

                message.setSubject(mail_subject, "UTF-8");
                BodyPart messageBodyPart1 = new MimeBodyPart();
                messageBodyPart1.setContent(body, "text/html; charset=utf-8");
                Multipart multipart = new MimeMultipart();
                multipart.addBodyPart(messageBodyPart1);

                if ((attachments != null) && (attachments.size() > 0)) {
                    for (FileBean fileBean : attachments) {
                        MimeBodyPart messageBodyPart = new MimeBodyPart();
                        if (!StringUtils.isNullOrEmpty(fileBean.getFilePath()) && !StringUtils.isNullOrEmpty(fileBean.getFileName())) {
                            DataSource source = new FileDataSource(fileBean.getFilePath());
                            messageBodyPart.setDataHandler(new DataHandler(source));
                            messageBodyPart.setFileName(fileBean.getFileName());
                            multipart.addBodyPart(messageBodyPart);
                        }
                    }
                }
                message.setContent(multipart);
                //Conect to smtp server and send Email
                Transport transport = session.getTransport("smtp");
                transport.connect(server, emailAddress, passwordDecrypt);
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();

                logger.info("Mail sent successfully...");
                return "";
            } catch (MessagingException ex) {
                logger.error("loi xay ra khi gui mail", ex);
                return "error when send email to: " + to + ",exception:" + ex.getMessage();
            } catch (Exception ae) {
                logger.error("loi xay ra khi gui mail", ae);
                return "error when send email to: " + to + ",exception:" + ae.getMessage();
            }
        } catch (Exception e) {
            logger.error("loi xay ra khi gui mail", e);
            return "error when send email to: " + to + ",exception:" + e.getMessage();
        }
    }

    @Override
    public boolean checkEmail() throws Exception {
        logger.info("Start check email secure TLS...");
        try {
            String passwordDecrypt = password;
            Session session = Session.getDefaultInstance(setProperties(passwordDecrypt));
            MimeMessage message = new MimeMessage(session);

            //Set email data 
            message.setFrom(new InternetAddress(emailAddress));
            //Conect to smtp server and send Email
            Transport transport = session.getTransport("smtp");
            transport.connect(server, emailAddress, passwordDecrypt);
            transport.close();

            logger.info("Mail sent successfully...");
            return true;

        } catch (Exception e) {
            logger.error("loi xay ra khi gui mail", e);
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
//        Logger logger = Logger.getLogger(InterfaceSendEmailTLS.class);
//        InterfaceSendEmailTLS sendEmail = new InterfaceSendEmailTLS(logger);
//
//        ConfigEmailServerBO emailConfigBO = new ConfigEmailServerBO(1, "mail.eprtech.com", 25, "luongnk@eprtech.com", "luongnk", "Luong@123", 0, "SSLTLS");
//        sendEmail.loadEmailConfig(emailConfigBO);
//        String to = "eprtest01@gmail.com";
//        sendEmail.sendEmail(to, "Subject new TLS", "body yyyyyyyyyyyy", null);
    }
}
