/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.email;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.napas.vccsca.constants.Constants;

/**
 *
 * SendEmailFactory
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendEmailFactory {

    int count = 0;

    /**
     * getInstance
     *
     * @param className
     * @param logger
     * @return
     */
    public static SendEmail getInstance(String className, Logger logger) {
        if (Constants.SEND_EMAL_CLASS.SEND_MAIL_SSLTLS.equalsIgnoreCase(className)) {
            return new InterfaceSendEmaiSSLTLS(logger);
        } else if (Constants.SEND_EMAL_CLASS.SEND_MAIL_SSL.equalsIgnoreCase(className)) {
            return new InterfaceSendEmailSSL(logger);
        } else if (Constants.SEND_EMAL_CLASS.SEND_MAIL_TLS.equalsIgnoreCase(className)) {
            return new InterfaceSendEmailTLS(logger);
        }
        return new InterfaceSendEmaiSSLTLS(logger);
    }

    /**
     * functionTimeOut
     */
    public void functionTimeOut() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                count = 10;
            }
        });
        thread.start();
        long endTimeMillis = System.currentTimeMillis() + 2000;
        while (thread.isAlive()) {
            if (System.currentTimeMillis() > endTimeMillis) {
                // set an error flag
                thread.destroy();
                break;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException t) {
            }
        }
    }

    /**
     * main
     *
     * @param args
     */
    public static void main(String[] args) {
        SendEmailFactory sendEmailFactory = new SendEmailFactory();
        sendEmailFactory.functionTimeOut();
        //System.out.println("End Count : " + sendEmailFactory.count);
    }
}
