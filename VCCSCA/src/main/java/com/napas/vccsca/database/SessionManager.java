/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.database;

import com.napas.vccsca.utils.AESUtil;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * SessionManager
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class SessionManager {

    private static final Logger logger = LogManager.getLogger(SessionManager.class);
    protected static Configuration cfg;
    protected static SessionFactory factory;

    static {
        initSS();
    }

    private static void initSS() {
        try {
            cfg = new Configuration();
            cfg.configure();
            String privateKey = ResourceBundle.getBundle("cas").getString("securitykey");

            if (privateKey != null && !"".equals(privateKey)) {
                String url = AESUtil.decryption(privateKey, cfg.getProperty("hibernate.connection.url"));
                String usr = AESUtil.decryption(privateKey, cfg.getProperty("hibernate.connection.username"));
                String pass = AESUtil.decryption(privateKey, cfg.getProperty("hibernate.connection.password"));

                cfg.setProperty("hibernate.dialect", cfg.getProperty("hibernate.dialect"));
                cfg.setProperty("hibernate.connection.driver_class", cfg.getProperty("hibernate.connection.driver_class"));
                cfg.setProperty("hibernate.connection.url", url);
                cfg.setProperty("hibernate.connection.username", usr);
                cfg.setProperty("hibernate.connection.password", pass);
            }
            factory = cfg.buildSessionFactory();
        } catch (Exception e) {
            logger.error("ERROR WHEN LOAD CONFIG DB");
            System.exit(0);
        }
    }

    private static SessionFactory buildSessionFactory() {
        try {
            if (factory == null || factory.isClosed()) {
                initSS();
            }
            return factory;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static Session getSession() {
        try {
            return buildSessionFactory().openSession();
        } catch (Exception e) {
            return null;
        }
    }

    public static void rollbackSession(Session session) {
        try {
            if (session != null && session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
        } catch (Exception e) {
            logger.error("Exception when rollback", e);
        }
    }

    public static void closeSession(Session session) {
        try {
            if (session != null) {
                session.close();
            }
            session = null;
        } catch (Exception e) {
            try {
                session = null;
            } catch (Exception ex) {
                session = null;
            }
        }
    }

    public static void commit(Session session) {
        try {
            if (session != null && session.getTransaction() != null) {
                session.getTransaction().commit();
                session.getTransaction().begin();
            }
        } catch (Exception e) {
            logger.error("Exception when rollback", e);
        }
    }
}
