/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author LuongNK
 */
@Entity
@Table(name = "TBL_GROUP_PERMISSION")
public class GroupsPermissionBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "GROUP_PERMISSION_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "GROUP_ID", unique = true, nullable = false)
    private Integer groupId;
    
    @Column(name = "GROUP_NAME", nullable = false)
    private String groupName;
    
    @Column(name = "DESCRIPTION")
    private String description;
    
    @Column(name = "CREATE_USER")
    private String createUser;
    
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
    @Column(name = "UPDATE_USER")
    private String updateUser;
    
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "STATUS")
    private Integer status;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UsersGroupsBO{" + "groupId=" + groupId + ", groupName=" + groupName + ", description=" + description + ", createUser=" + createUser + ", createDate=" + createDate + ", status=" + status + '}';
    }

}
