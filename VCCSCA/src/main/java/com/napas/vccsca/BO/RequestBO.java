/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_REQUEST")
public class RequestBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "REQUEST_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "REQUEST_ID", unique = true, nullable = false)
    private Integer requestId;
    
    @Column(name = "REGISTER_ID", unique = true, nullable = false)
    private Integer registerId;
    
    @Column(name = "BIN", nullable = false)
    private Integer bin;
    
    @Column(name = "BANK_NAME", nullable = false)
    private String bankName;
    
    @Column(name = "IPK")
    private String ipk;
    
    @Column(name = "SERIAL")
    private String serial;
    
    @Column(name = "BANK_IDENTITY")
    private String bankIdentity;
    
    @Column(name = "IPK_LENGH")
    private Integer ipkLengh;
    
    @Column(name = "CREATE_USER")
    private String createUser;
    
    @Column(name = "ACCEPT_USER")
    private String acceptUser;
    
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
    @Column(name = "SIGN_USER")
    private String signUser;
    
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    
    @Column(name = "UPDATE_USER")
    private String updateUser;
    
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "ACCEPT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acceptDate;
    
    @Column(name = "EXP_DATE")
    private String expDate;
    
    @Column(name = "APP_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date appDate;
    
    @Column(name = "CONTENT")
    private String content;
    
    @Column(name = "STATUS")
    private Integer status;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
    
    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public Integer getBin() {
        return bin;
    }

    public void setBin(Integer bin) {
        this.bin = bin;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIpk() {
        return ipk;
    }

    public void setIpk(String ipk) {
        this.ipk = ipk;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getBankIdentity() {
        return bankIdentity;
    }

    public void setBankIdentity(String bankIdentity) {
        this.bankIdentity = bankIdentity;
    }

    public Integer getIpkLengh() {
        return ipkLengh;
    }

    public void setIpkLengh(Integer ipkLengh) {
        this.ipkLengh = ipkLengh;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAcceptUser() {
        return acceptUser;
    }

    public void setAcceptUser(String acceptUser) {
        this.acceptUser = acceptUser;
    }

    public String getSignUser() {
        return signUser;
    }

    public void setSignUser(String signUser) {
        this.signUser = signUser;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public Date getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(Date acceptDate) {
        this.acceptDate = acceptDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "requestBO{" + "requestId=" + requestId + ", registerId=" + registerId + ", bin=" + bin + ", bankName=" + bankName + ", ipk=" + ipk + ", serial=" + serial + ", bankIdentity=" + bankIdentity + ", ipkLengh=" + ipkLengh + ", createUser=" + createUser + ", acceptUser=" + acceptUser + ", createDate=" + createDate + ", signUser=" + signUser + ", signDate=" + signDate + ", updateUser=" + updateUser + ", updateDate=" + updateDate + ", acceptDate=" + acceptDate + ", expDate=" + expDate + ", appDate=" + appDate + ", status=" + status + '}';
    }

}
