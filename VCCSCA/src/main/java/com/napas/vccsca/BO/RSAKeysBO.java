/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_RSA_KEYS")
public class RSAKeysBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "RSA_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "RSA_ID", unique = true, nullable = false)
    private Integer rsaId;

    @Column(name = "RSA_INDEX", nullable = false)
    private Integer rsaIndex;

    @Column(name = "TYPE_KEY")
    private String typeKey;

    @Lob
    @Column(name = "PUBLIC_KEY_MODULUS_BYTE")
    private byte[] publicKeyModulusByte;

    @Column(name = "PUBLIC_KEY_MODULUS")
    private String publicKeyModulus;

    @Column(name = "PRIVATE_KEY_NAME")
    private String privateKeyName;

    @Column(name = "PUBLIC_KEY_EXPONENT", unique = true, nullable = false)
    private String publicKeyExponent;

    @Column(name = "RSA_LENGTH")
    private Integer rsaLength;

    @Column(name = "RSA_CONTENT")
    private String rsaContent;

    @Column(name = "CHECKSUM_NAME", unique = true, nullable = false)
    private String checksumName;

    @Column(name = "CHECKSUM_CONTENT")
    private String checksumContent;

    @Column(name = "EXPIRATION_DATE")
    private String expirationDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "CREATE_CS_USER")
    private String createCsUser;

    @Column(name = "UPDATE_CS_USER")
    private String updateCsUser;

    @Column(name = "CREATE_CS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createCsDate;

    @Column(name = "UPDATE_CS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateCsDate;

    @Column(name = "CHECKSUM_STATUS")
    private Integer checksumStatus;

    @Column(name = "RSA_STATUS")
    private Integer rsaStatus;

    @Column(name = "SLOT_HSM")
    private Integer slotHsm;

    public Integer getSlotHsm() {
        return slotHsm;
    }

    public void setSlotHsm(Integer slotHsm) {
        this.slotHsm = slotHsm;
    }

    public Integer getRsaId() {
        return rsaId;
    }

    public void setRsaId(Integer rsaId) {
        this.rsaId = rsaId;
    }

    public Integer getRsaIndex() {
        return rsaIndex;
    }

    public void setRsaIndex(Integer rsaIndex) {
        this.rsaIndex = rsaIndex;
    }

    public String getTypeKey() {
        return typeKey;
    }

    public void setTypeKey(String typeKey) {
        this.typeKey = typeKey;
    }

    public byte[] getPublicKeyModulusByte() {
        return publicKeyModulusByte;
    }

    public void setPublicKeyModulusByte(byte[] publicKeyModulusByte) {
        this.publicKeyModulusByte = publicKeyModulusByte;
    }

    public String getPublicKeyModulus() {
        return publicKeyModulus;
    }

    public void setPublicKeyModulus(String publicKeyModulus) {
        this.publicKeyModulus = publicKeyModulus;
    }

    public String getPublicKeyExponent() {
        return publicKeyExponent;
    }

    public void setPublicKeyExponent(String publicKeyExponent) {
        this.publicKeyExponent = publicKeyExponent;
    }

    public Integer getRsaLength() {
        return rsaLength;
    }

    public void setRsaLength(Integer rsaLength) {
        this.rsaLength = rsaLength;
    }

    public String getRsaContent() {
        return rsaContent;
    }

    public void setRsaContent(String rsaContent) {
        this.rsaContent = rsaContent;
    }

    public String getChecksumName() {
        return checksumName;
    }

    public void setChecksumName(String checksumName) {
        this.checksumName = checksumName;
    }

    public String getChecksumContent() {
        return checksumContent;
    }

    public void setChecksumContent(String checksumContent) {
        this.checksumContent = checksumContent;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateCsUser() {
        return createCsUser;
    }

    public void setCreateCsUser(String createCsUser) {
        this.createCsUser = createCsUser;
    }

    public String getUpdateCsUser() {
        return updateCsUser;
    }

    public void setUpdateCsUser(String updateCsUser) {
        this.updateCsUser = updateCsUser;
    }

    public Date getCreateCsDate() {
        return createCsDate;
    }

    public void setCreateCsDate(Date createCsDate) {
        this.createCsDate = createCsDate;
    }

    public Date getUpdateCsDate() {
        return updateCsDate;
    }

    public void setUpdateCsDate(Date updateCsDate) {
        this.updateCsDate = updateCsDate;
    }

    public Integer getChecksumStatus() {
        return checksumStatus;
    }

    public void setChecksumStatus(Integer checksumStatus) {
        this.checksumStatus = checksumStatus;
    }

    public Integer getRsaStatus() {
        return rsaStatus;
    }

    public void setRsaStatus(Integer rsaStatus) {
        this.rsaStatus = rsaStatus;
    }

    public String getPrivateKeyName() {
        return privateKeyName;
    }

    public void setPrivateKeyName(String privateKeyName) {
        this.privateKeyName = privateKeyName;
    }

    @Override
    public String toString() {
        return "RSAKeysBO{" + "rsaId=" + rsaId + ", rsaIndex=" + rsaIndex + ", typeKey=" + typeKey + ", publicKeyModulus=" + publicKeyModulus + ", publicKeyExponent=" + publicKeyExponent + ", lengh=" + rsaLength + ", rsaContent=" + rsaContent + ", checksumName=" + checksumName + ", checksumContent=" + checksumContent + ", expirationDate=" + expirationDate + ", createUser=" + createUser + ", updateUser=" + updateUser + ", createDate=" + createDate + ", updateDate=" + updateDate + ", createCsUser=" + createCsUser + ", updateCsUser=" + updateCsUser + ", createCsDate=" + createCsDate + ", updateCsDate=" + updateCsDate + '}';
    }

}
