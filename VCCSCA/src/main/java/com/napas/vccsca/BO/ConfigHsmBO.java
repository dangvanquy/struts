/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_CONFIG_HSM")
public class ConfigHsmBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "CONFIG_HSM_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "HSM_ID", unique = true, nullable = false)
    private Integer hsmId;

    @Column(name = "SLOT", nullable = false)
    private int slot;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "CREATEDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "UPDATEDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getHsmId() {
        return hsmId;
    }

    public void setHsmId(Integer hsmId) {
        this.hsmId = hsmId;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "ConfigHsmBO{" + "hsmId=" + hsmId + ", slot=" + slot + ", password=" + password + '}';
    }

}
