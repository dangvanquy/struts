/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LuongNK
 */
@Entity
@Table(name = "TBL_PERMISSION")
public class PermissionBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PERMISSION_CODE", unique = true, nullable = false)
    private String permissionCode;

    @Column(name = "PERMISSION_NAME", nullable = false)
    private String permissionName;

    @Column(name = "PERMISSION_TYPE", nullable = false)
    private Integer permissionType;

    @Column(name = "PERMISSION_URL", nullable = false)
    private String permissionUrl;

    public PermissionBO(String permissionCode, String permissionName, Integer permissionType, String permissionUrl) {
        this.permissionCode = permissionCode;
        this.permissionName = permissionName;
        this.permissionType = permissionType;
        this.permissionUrl = permissionUrl;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public Integer getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Integer permissionType) {
        this.permissionType = permissionType;
    }

    public String getPermissionUrl() {
        return permissionUrl;
    }

    public void setPermissionUrl(String permissionUrl) {
        this.permissionUrl = permissionUrl;
    }

    @Override
    public String toString() {
        return "GroupPermissionBO{" + "permissionCode=" + permissionCode + ", permissionName=" + permissionName + '}';
    }

}
