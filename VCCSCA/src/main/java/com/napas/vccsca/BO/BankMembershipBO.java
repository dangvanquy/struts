/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_BANK_MEMBERSHIP")
public class BankMembershipBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "BANK_MEMBERSHIP_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "BANK_ID", unique = true, nullable = false)
    private Integer bankId;

    @Column(name = "BIN", unique = true, nullable = false)
    private Integer bin;

    @Column(name = "BANK_FULL_NAME", nullable = false)
    private String bankFullName;

    @Column(name = "BANK_SHORT_NAME")
    private String bankShortName;

    @Column(name = "APPELLATION1")
    private String appellation1;

    @Column(name = "REPRESENT1")
    private String represent1;

    @Column(name = "EMAIL1")
    private String email1;

    @Column(name = "PHONE_NUMBER1")
    private String phoneNumber1;

    @Column(name = "APPELLATION2")
    private String appellation2;

    @Column(name = "REPRESENT2")
    private String represent2;

    @Column(name = "EMAIL2")
    private String email2;

    @Column(name = "PHONE_NUMBER2")
    private String phoneNumber2;

    @Column(name = "APPELLATION3")
    private String appellation3;

    @Column(name = "REPRESENT3")
    private String represent3;

    @Column(name = "EMAIL3")
    private String email3;

    @Column(name = "PHONE_NUMBER3")
    private String phoneNumber3;

    @Column(name = "APPELLATION4")
    private String appellation4;

    @Column(name = "REPRESENT4")
    private String represent4;

    @Column(name = "EMAIL4")
    private String email4;

    @Column(name = "PHONE_NUMBER4")
    private String phoneNumber4;

    @Column(name = "APPELLATION5")
    private String appellation5;

    @Column(name = "REPRESENT5")
    private String represent5;

    @Column(name = "EMAIL5")
    private String email5;

    @Column(name = "PHONE_NUMBER5")
    private String phoneNumber5;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "STATUS")
    private Integer status;

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getBin() {
        return bin;
    }

    public String getBin2() {
        return "," + bin + ",";
    }

    public void setBin(Integer bin) {
        if (!StringUtils.isNumber(bin + "")) {
            bin = -1;
        }
        this.bin = bin;
    }

    public String getBankFullName() {
        return bankFullName;
    }

    public void setBankFullName(String bankFullName) {
        this.bankFullName = bankFullName;
    }

    public String getBankShortName() {
        return bankShortName;
    }

    public void setBankShortName(String bankShortName) {
        this.bankShortName = bankShortName;
    }

    public String getAppellation1() {
        return appellation1;
    }

    public void setAppellation1(String appellation1) {
        this.appellation1 = appellation1;
    }

    public String getRepresent1() {
        return represent1;
    }

    public void setRepresent1(String represent1) {
        this.represent1 = represent1;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getAppellation2() {
        return appellation2;
    }

    public void setAppellation2(String appellation2) {
        this.appellation2 = appellation2;
    }

    public String getRepresent2() {
        return represent2;
    }

    public void setRepresent2(String represent2) {
        this.represent2 = represent2;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getAppellation3() {
        return appellation3;
    }

    public void setAppellation3(String appellation3) {
        this.appellation3 = appellation3;
    }

    public String getRepresent3() {
        return represent3;
    }

    public void setRepresent3(String represent3) {
        this.represent3 = represent3;
    }

    public String getEmail3() {
        return email3;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    public String getPhoneNumber3() {
        return phoneNumber3;
    }

    public void setPhoneNumber3(String phoneNumber3) {
        this.phoneNumber3 = phoneNumber3;
    }

    public String getAppellation4() {
        return appellation4;
    }

    public void setAppellation4(String appellation4) {
        this.appellation4 = appellation4;
    }

    public String getRepresent4() {
        return represent4;
    }

    public void setRepresent4(String represent4) {
        this.represent4 = represent4;
    }

    public String getEmail4() {
        return email4;
    }

    public void setEmail4(String email4) {
        this.email4 = email4;
    }

    public String getPhoneNumber4() {
        return phoneNumber4;
    }

    public void setPhoneNumber4(String phoneNumber4) {
        this.phoneNumber4 = phoneNumber4;
    }

    public String getAppellation5() {
        return appellation5;
    }

    public void setAppellation5(String appellation5) {
        this.appellation5 = appellation5;
    }

    public String getRepresent5() {
        return represent5;
    }

    public void setRepresent5(String represent5) {
        this.represent5 = represent5;
    }

    public String getEmail5() {
        return email5;
    }

    public void setEmail5(String email5) {
        this.email5 = email5;
    }

    public String getPhoneNumber5() {
        return phoneNumber5;
    }

    public void setPhoneNumber5(String phoneNumber5) {
        this.phoneNumber5 = phoneNumber5;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BankMembershipBO{" + "bin=" + bin + ", bankFullName=" + bankFullName + ", bankShortName=" + bankShortName + ", appellation1=" + appellation1 + ", represent1=" + represent1 + ", email1=" + email1 + ", phoneNumber1=" + phoneNumber1 + ", appellation2=" + appellation2 + ", represent2=" + represent2 + ", email2=" + email2 + ", phoneNumber2=" + phoneNumber2 + ", appellation3=" + appellation3 + ", represent3=" + represent3 + ", email3=" + email3 + ", phoneNumber3=" + phoneNumber3 + ", appellation4=" + appellation4 + ", represent4=" + represent4 + ", email4=" + email4 + ", phoneNumber4=" + phoneNumber4 + ", appellation5=" + appellation5 + ", represent5=" + represent5 + ", email5=" + email5 + ", phoneNumber5=" + phoneNumber5 + ", createUser=" + createUser + ", updateUser=" + updateUser + ", createDate=" + createDate + ", updateDate=" + updateDate + ", status=" + status + '}';
    }

}
