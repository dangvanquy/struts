/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author LuongNK
 */
@Entity
@Table(name = "TBL_PERMISSION_RIGHT")
public class PermissionRightBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "PERMISSION_RIGHT_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "PERMISSION_RIGHT_ID", nullable = false)
    private Integer permissionRightId;

    @Column(name = "GROUP_ID", nullable = false)
    private Integer groupId;

    @Column(name = "PERMISSION_CODE", unique = true, nullable = false)
    private String permissionCode;

    public Integer getPermissionRightId() {
        return permissionRightId;
    }

    public void setPermissionRightId(Integer permissionRightId) {
        this.permissionRightId = permissionRightId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    @Override
    public String toString() {
        return "GroupPermissionBO{" + "permissionRightId=" + permissionRightId + "groupId=" + groupId + ", permissionCode=" + permissionCode + '}';
    }

}
