/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_CONFIG_WARNING_SYSTEM")
public class ConfigWarningSystemBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "CONFIG_WARNING_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "WARNING_ID", unique = true, nullable = false)
    private Integer warningId;

    @Column(name = "WARNING_NAME", nullable = false)
    private String warningName;

    @Column(name = "MAIL_TO", nullable = false)
    private String mailTo;

    @Column(name = "MAIL_CC")
    private String mailCc;

    @Column(name = "MAIL_BCC")
    private String mailBcc;

    @Column(name = "TYPE")
    private Integer type;

    @Column(name = "SEND_NUMBER")
    private Integer sendNumber;

    @Column(name = "SEND_TIME")
    private String sendTime;

    @Column(name = "DAY_CONFIG", nullable = false)
    private Integer dayConfig;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "PARAM")
    private String param;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "CREATE_USER", nullable = false)
    private String createUser;

    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "UPDATE_USER")
    private String updateUser;

    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "LAST_SCAN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastScan;

    public Date getLastScan() {
        return lastScan;
    }

    public void setLastScan(Date lastScan) {
        this.lastScan = lastScan;
    }

    public Integer getWarningId() {
        return warningId;
    }

    public void setWarningId(Integer warningId) {
        this.warningId = warningId;
    }

    public String getWarningName() {
        return warningName;
    }

    public void setWarningName(String warningName) {
        this.warningName = warningName;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getMailCc() {
        return mailCc;
    }

    public void setMailCc(String mailCc) {
        this.mailCc = mailCc;
    }

    public String getMailBcc() {
        return mailBcc;
    }

    public void setMailBcc(String mailBcc) {
        this.mailBcc = mailBcc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }

    public Integer getDayConfig() {
        return dayConfig;
    }

    public void setDayConfig(Integer dayConfig) {
        this.dayConfig = dayConfig;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    @Override
    public String toString() {
        return "ConfigWarningSystemBO{" + "warningId=" + warningId + ", mailTo=" + mailTo + ", mailCC=" + mailCc + ", mailBCC=" + mailBcc + ", type=" + type + ", sendNumber=" + sendNumber + ", dayConfig=" + dayConfig + ", content=" + content + ", subject=" + subject + ", param=" + param + ", status=" + status + '}';
    }

}
