/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_DIGITAL_CERTIFICATE")
public class DigitalCertificateBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "CERTIFICATE_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "CERTIFICATE_ID", unique = true, nullable = false)
    private Integer certificateId;

    @Column(name = "RSA_ID", unique = true, nullable = false)
    private Integer rsaId;

    @Column(name = "REGISTER_ID", unique = true, nullable = false)
    private Integer registerId;

    @Lob
    @Column(name = "CERTIFICATE")
    private byte[] certificate;

    @Column(name = "BIN", nullable = false)
    private Integer bin;

    @Column(name = "BANK_NAME", unique = true, nullable = false)
    private String bankName;

    @Column(name = "IPK", unique = true, nullable = false)
    private String ipk;

    @Column(name = "IPK_LENGH", unique = true, nullable = false)
    private Integer ipkLengh;

    @Column(name = "EXP_DATE", nullable = false)
    private String expDate;

    @Column(name = "SERIAL", unique = true, nullable = false)
    private String serial;

    @Column(name = "BANK_IDENTITY", unique = true, nullable = false)
    private String bankIdentity;

    @Column(name = "RSA_LENGH", nullable = false)
    private Integer rsaLengh;

    @Column(name = "EVICTION_USER")
    private String evictionUser;

    @Column(name = "EVICTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evictionDate;

    @Column(name = "SIGN_USER")
    private String signUser;

    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "RESTORE_USER")
    private String restoreUser;

    @Column(name = "RESTORE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date restoreDate;

    public Integer getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Integer certificateId) {
        this.certificateId = certificateId;
    }

    public Integer getRsaId() {
        return rsaId;
    }

    public void setRsaId(Integer rsaId) {
        this.rsaId = rsaId;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public byte[] getCertificate() {
        return certificate;
    }

    public void setCertificate(byte[] certificate) {
        this.certificate = certificate;
    }
  
    public Integer getBin() {
        return bin;
    }

    public void setBin(Integer bin) {
        this.bin = bin;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIpk() {
        return ipk;
    }

    public void setIpk(String ipk) {
        this.ipk = ipk;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getBankIdentity() {
        return bankIdentity;
    }

    public void setBankIdentity(String bankIdentity) {
        this.bankIdentity = bankIdentity;
    }

    public Integer getRsaLengh() {
        return rsaLengh;
    }

    public void setRsaLengh(Integer rsaLengh) {
        this.rsaLengh = rsaLengh;
    }

    public String getEvictionUser() {
        return evictionUser;
    }

    public void setEvictionUser(String evictionUser) {
        this.evictionUser = evictionUser;
    }

    public Date getEvictionDate() {
        return evictionDate;
    }

    public void setEvictionDate(Date evictionDate) {
        this.evictionDate = evictionDate;
    }

    public String getSignUser() {
        return signUser;
    }

    public void setSignUser(String sign) {
        this.signUser = sign;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getIpkLengh() {
        return ipkLengh;
    }

    public void setIpkLengh(Integer ipkLengh) {
        this.ipkLengh = ipkLengh;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRestoreUser() {
        return restoreUser;
    }

    public void setRestoreUser(String restoreUser) {
        this.restoreUser = restoreUser;
    }

    public Date getRestoreDate() {
        return restoreDate;
    }

    public void setRestoreDate(Date restoreDate) {
        this.restoreDate = restoreDate;
    }

    @Override
    public String toString() {
        return "DigitalCertificateBO{" + "certificateId=" + certificateId + ", rsaId=" + rsaId + ", registerId=" + registerId + ", certificate=" + certificate + ", bin=" + bin + ", bankName=" + bankName + ", ipk=" + ipk + ", ipkLengh=" + ipkLengh + ", expDate=" + expDate + ", serial=" + serial + ", bankIdentity=" + bankIdentity + ", rsaLengh=" + rsaLengh + ", evictionUser=" + evictionUser + ", evictionDate=" + evictionDate + ", signUser=" + signUser + ", signDate=" + signDate + ", createUser=" + createUser + ", createDate=" + createDate + ", content=" + content + ", status=" + status + ", restoreUser=" + restoreUser + ", restoreDate=" + restoreDate + '}';
    }

}
