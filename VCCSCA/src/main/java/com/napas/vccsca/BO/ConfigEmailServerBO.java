/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_CONFIG_EMAIL_SERVER")
public class ConfigEmailServerBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "EMAIL_CONFIG_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "EMAIL_CONFIG_ID", unique = true, nullable = false)
    private Integer emailConfigId;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "MAIL_SERVER", nullable = false)
    private String mailServer;

    @Column(name = "PORT", nullable = false)
    private Integer port;

    @Column(name = "SECURITY_TYPE", nullable = false)
    private String securityType;

    @Column(name = "USERNAME", unique = true, nullable = false)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "STATUS", nullable = false)
    private Integer status;

    public Integer getEmailConfigId() {
        return emailConfigId;
    }

    public void setEmailConfigId(Integer emailConfigId) {
        this.emailConfigId = emailConfigId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMailServer() {
        return mailServer;
    }

    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ConfigEmailServerBO{" + "emailConfigId=" + emailConfigId + ", email=" + email + ", mailServer=" + mailServer + ", port=" + port + ", securityType=" + securityType + ", accountType=" + ", username=" + username + ", password=" + password + ", description=" + description + ", status=" + status + '}';
    }

}
