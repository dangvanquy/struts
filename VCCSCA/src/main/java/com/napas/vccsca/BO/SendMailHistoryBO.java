/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tuanna
 */
@Entity
@Table(name = "TBL_SEND_MAIL_HISTORY")
public class SendMailHistoryBO implements Serializable {

    /**
     * serial version id.
     */
    private static final long serialVersionUID = 1L;

    @SequenceGenerator(name = "generator", sequenceName = "SEND_MAIL_HIS_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "SEND_EMAIL_HISTORY_ID", unique = true, nullable = false)
    private Integer sendEmailHistoryId;

    @Column(name = "PROCESS_TIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date processTime;

    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    @Column(name = "MAIL_TO")
    private String mailTo;

    @Column(name = "MAIL_CC")
    private String mailCc;

    @Column(name = "MAIL_BCC")
    private String mailBcc;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "SEND_NUMBER")
    private Integer sendNumber;

    @Column(name = "SUCCESS_COUNT")
    private Integer successCount;

    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "LAST_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    @Column(name = "TEM_ID")
    private Integer tem_id;

    public Integer getTem_id() {
        return tem_id;
    }

    public void setTem_id(Integer tem_id) {
        this.tem_id = tem_id;
    }

    public Integer getSendEmailHistoryId() {
        return sendEmailHistoryId;
    }

    public void setSendEmailHistoryId(Integer sendEmailHistoryId) {
        this.sendEmailHistoryId = sendEmailHistoryId;
    }

    public Date getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Date processTime) {
        this.processTime = processTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getMailCc() {
        return mailCc;
    }

    public void setMailCc(String mailCc) {
        this.mailCc = mailCc;
    }

    public String getMailBcc() {
        return mailBcc;
    }

    public void setMailBcc(String mailBcc) {
        this.mailBcc = mailBcc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "SendMailHistoryBO{" + "sendEmailHistoryId=" + sendEmailHistoryId + ", processTime=" + processTime + ", subject=" + subject + ", mailTo=" + mailTo + ", mailCc=" + mailCc + ", mailBcc=" + mailBcc + ", content=" + content + ", sendNumber=" + sendNumber + ", successCount=" + successCount + ", createDate=" + createDate + ", lastUpdate=" + lastUpdate + '}';
    }

}
