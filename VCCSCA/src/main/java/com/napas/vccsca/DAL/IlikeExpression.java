/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import org.hibernate.criterion.MatchMode;
import org.hibernate.dialect.Dialect;

/**
 *
 * IlikeExpression
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class IlikeExpression extends LikeExpression {

    protected IlikeExpression(
            String propertyName,
            Object value) {
        super(propertyName, value);
    }

    protected IlikeExpression(
            String propertyName,
            String value,
            MatchMode matchMode) {
        super(propertyName, value, matchMode);
    }

    protected IlikeExpression(String propertyName, String value, Character escapeChar) {
        super(propertyName, value, escapeChar);
    }

    @Override
    protected String lhs(Dialect dialect, String column) {
        return dialect.getLowercaseFunction() + '(' + column + ')';

    }

    @Override
    protected String typedValue(String value) {
        return super.typedValue(value).toLowerCase();
    }
}
