/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * ChecksumDAL
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class ChecksumDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ChecksumDAL.class);

    public void addChecksum(RSAKeysBO keysBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(keysBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public void updateChecksum(RSAKeysBO keysBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(keysBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getAllChecksum() throws Exception {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(RSAKeysBO.class)
                    .addOrder(Order.desc("rsaId"))
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getChecksum(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaIndex"))) {
                    cri.add(Restrictions.eq("rsaIndex", NumberUtil.toNumber(hashMap.get("rsaIndex").toString())));
                }

                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaStatusor"))) {
                    Criterion rest1 = Restrictions.and(Restrictions.eq("rsaStatus", NumberUtil.toNumber(hashMap.get("rsaStatusor").toString())));
                    Criterion rest2 = Restrictions.and(Restrictions.eq("checksumStatus", NumberUtil.toNumber(hashMap.get("checksumStatus").toString())));
                    cri.add(Restrictions.or(rest1, rest2));
                } else if (!StringUtils.isNullOrEmpty(hashMap.get("checksumStatus"))) {
                    cri.add(Restrictions.eq("checksumStatus", NumberUtil.toNumber(hashMap.get("checksumStatus").toString())));

                }
                cri.add(Restrictions.isNotNull("checksumStatus"));
            }
            cri.addOrder(Order.desc("updateCsDate"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getCheckExChecksum(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (hashMap != null) {
                cri.add(Restrictions.eq("rsaIndex", NumberUtil.toNumber(hashMap.get("rsaIndex").toString())));
                Criterion rest1 = Restrictions.and(Restrictions.eq("rsaStatus", NumberUtil.toNumber(hashMap.get("rsaStatusor").toString())));
                Criterion rest2 = Restrictions.and(Restrictions.eq("checksumStatus", NumberUtil.toNumber(hashMap.get("checksumStatus").toString())));
                cri.add(Restrictions.or(rest1, rest2));
            }
            cri.addOrder(Order.desc("updateCsDate"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getChecksumById
     *
     * @param rsaId
     * @return
     * @throws Exception
     */
    public RSAKeysBO getChecksumById(Integer rsaId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaId)) {
                cri.add(Restrictions.eq("rsaId", rsaId));
            }
            return (RSAKeysBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getChecksumById
     *
     * @param rsaId
     * @return
     * @throws Exception
     */
    public RSAKeysBO getChecksumByIdIndex(Integer rsaId, Integer rsaIndex) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaId)) {
                cri.add(Restrictions.eq("rsaId", rsaId));
            }
            if (!StringUtils.isNullOrEmpty(rsaIndex)) {
                cri.add(Restrictions.eq("rsaIndex", rsaIndex));
            }
            return (RSAKeysBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getChecksumByIndex(Integer rsaIndex) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaIndex)) {
                cri.add(Restrictions.eq("rsaIndex", rsaIndex));
            }
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }
}
