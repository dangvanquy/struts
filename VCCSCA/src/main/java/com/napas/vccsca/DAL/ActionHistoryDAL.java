/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.ActionLogBO;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * ActionHistoryDAL
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class ActionHistoryDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ActionHistoryDAL.class);

    /**
     * addActionLog
     *
     * @param actionLogBO
     * @throws Exception
     */
    public void addActionLog(ActionLogBO actionLogBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(actionLogBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * addListActionLog
     *
     * @param actionLogBOs
     * @throws Exception
     */
    public void addListActionLog(List<ActionLogBO> actionLogBOs) throws Exception {
        Session session = null;
        try {
            session = getSession();
            for (ActionLogBO actionLogBO : actionLogBOs) {
                session.save(actionLogBO);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getActionLog
     *
     * @param hashMap
     * @param offset
     * @param maxResults
     * @return
     * @throws Exception
     */
    public List<ActionLogBO> getActionLog(HashMap hashMap, Integer offset, Integer maxResults) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ActionLogBO.class);
            if (hashMap != null) {

                if (!StringUtils.isNullOrEmpty(hashMap.get("fullName"))) {
                    cri.add(Restrictions.like("userName", hashMap.get("fullName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("fromDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(DATE_TIME) >= TO_DATE('" + hashMap.get("fromDate") + "','dd/MM/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("toDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(DATE_TIME) <= TO_DATE('" + hashMap.get("toDate") + "','dd/MM/yyyy') "));
                }
            }
            addOrder(cri, offset != null ? offset : 0, maxResults != null ? maxResults : 10, "dateTime", "DESCENDING");

            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getCount
     *
     * @param hashMap
     * @return
     * @throws Exception
     */
    public Long getCount(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ActionLogBO.class).setProjection(Projections.rowCount());
            if (hashMap != null) {

                if (!StringUtils.isNullOrEmpty(hashMap.get("fullName"))) {
                    cri.add(Restrictions.like("userName", hashMap.get("fullName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("fromDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(DATE_TIME) >= TO_DATE('" + hashMap.get("fromDate") + "','dd/MM/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("toDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(DATE_TIME) <= TO_DATE('" + hashMap.get("toDate") + "','dd/MM/yyyy') "));
                }
            }

            Object obj = cri.uniqueResult();
            if (obj != null) {
                return (Long.valueOf(obj.toString()));
            }
            return 0l;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return 0l;
        } finally {
            closeSession(session);
        }
    }

}
