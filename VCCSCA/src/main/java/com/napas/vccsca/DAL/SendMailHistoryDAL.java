/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.SendMailHistoryBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * SendMailHistoryDAL
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendMailHistoryDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(SendMailHistoryDAL.class);

    public void addNewHistory(SendMailHistoryBO sendMailHistoryBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(sendMailHistoryBO);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }
}
