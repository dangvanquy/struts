/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;

/**
 *
 * EmailConfigDAL
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class EmailConfigDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(EmailConfigDAL.class);

    public void updateEmailConfig(ConfigEmailServerBO emailConfigBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(emailConfigBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public ConfigEmailServerBO getEmailConfig() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ConfigEmailServerBO.class);
            return (ConfigEmailServerBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }
}
