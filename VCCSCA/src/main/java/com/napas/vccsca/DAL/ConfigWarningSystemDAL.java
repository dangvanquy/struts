/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * ConfigWarningSystemDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ConfigWarningSystemDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ConfigWarningSystemDAL.class);

    public void addConfigWarningSystem(ConfigWarningSystemBO configWarningBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(configWarningBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public void deleteConfigWarningSystem(ConfigWarningSystemBO configWarningBO) {
        Session session = null;
        try {
            session = getSession();
            session.delete(configWarningBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void updateConfigWarningSystem(ConfigWarningSystemBO configWarningBO) {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(configWarningBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    /**
     * findConfigWarningSystem
     *
     * @param searchMap
     * @param maxResults
     * @return
     */
    public List<ConfigWarningSystemBO> findConfigWarningSystem(HashMap searchMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ConfigWarningSystemBO.class);
            if (searchMap != null) {
                if (!StringUtils.isNullOrEmpty(searchMap.get("createUser"))) {
                    cri.add(Restrictions.eq("createUser", searchMap.get("createUser")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("warningId"))) {
                    cri.add(Restrictions.eq("warningId", searchMap.get("warningId")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("warningName"))) {
                    cri.add(Restrictions.ilike("warningName", (String) searchMap.get("warningName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("warningNameCheck"))) {
                    cri.add(Restrictions.eq("warningName", (String) searchMap.get("warningNameCheck")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("status"))) {
                    cri.add(Restrictions.eq("status", searchMap.get("status")));
                }
            }
            cri.addOrder(Order.desc("createDate"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public ConfigWarningSystemBO getConfigWarningSystemById(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ConfigWarningSystemBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("warningId"))) {
                    cri.add(Restrictions.eq("warningId", hashMap.get("warningId")));
                }
            }
            return (ConfigWarningSystemBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }

    }

    public List getAllCreateUserConfigWarningSystem() {
        Session session = null;
        try {
            session = getSession();
            return session.createCriteria(ConfigWarningSystemBO.class)
                    .setProjection(Projections.property("createUser"))
                    .setProjection(Projections.distinct(Projections.property("createUser"))).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }
}
