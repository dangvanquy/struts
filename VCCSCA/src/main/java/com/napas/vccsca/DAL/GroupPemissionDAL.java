/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.BO.GroupsPermissionBO;
import static com.napas.vccsca.DAL.BaseDAL.getSession;
import com.napas.vccsca.constants.Constants;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * GroupsPermissionDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class GroupPemissionDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(GroupPemissionDAL.class);

    public void addGroupsPermission(GroupsPermissionBO groupsPermissionBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(groupsPermissionBO);
            session.getTransaction().commit();
        } catch (Exception e) {
        }

    }

    public void deleteGroupsPermission(GroupsPermissionBO groupsPermissionBO) {
        Session session = null;
        try {
            session = getSession();
            session.delete(groupsPermissionBO);
            session.getTransaction().commit();
        } catch (Exception e) {
        }

    }

    public void updateGroupsPermission(GroupsPermissionBO groupsPermissionBO) {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(groupsPermissionBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public List<GroupsPermissionBO> getAllGroupsPermission() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(GroupsPermissionBO.class);
            cri.add(Restrictions.eq("status", 0));
            cri.addOrder(Order.desc("groupId"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public List<GroupsPermissionBO> findGroupsPermission(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(GroupsPermissionBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("groupName"))) {
                    cri.add(Restrictions.ilike("groupName", (String) hashMap.get("groupName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status")) && !"-1".equals(hashMap.get("status") + "")) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
            }
            addOrder(cri, 0, 0, "groupId", Constants.ORDER.DESCENDING);
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    /**
     * chkGroupsPermission
     *
     * @param hashMap
     * @return
     * @throws Exception
     */
    public List<GroupsPermissionBO> chkGroupsPermission(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(GroupsPermissionBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("groupName"))) {
                    cri.add(Restrictions.eq("groupName", (String) hashMap.get("groupName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status")) && !"-1".equals(hashMap.get("status") + "")) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
            }
            addOrder(cri, 0, 0, "groupId", Constants.ORDER.DESCENDING);
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public Long count(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(GroupsPermissionBO.class);
            if (hashMap != null) {
                if (hashMap.get("groupName") != null) {
                    cri.add(Restrictions.ilike("groupName", (String) hashMap.get("groupName"), MatchMode.ANYWHERE));
                }
                if (hashMap.get("status") != null) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
            }
            cri.setProjection(Projections.rowCount());
            return (Long) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }

    }

    public GroupsPermissionBO getgroupPemissionById(Integer idRequest) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(GroupsPermissionBO.class);
            if (!StringUtils.isNullOrEmpty(idRequest)) {
                cri.add(Restrictions.eq("groupId", idRequest));
            }
            return (GroupsPermissionBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }

    }

    public long checkPermisionName(String groupName) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(GroupsPermissionBO.class).setProjection(Projections.rowCount());
            cri.add(Restrictions.eq("groupName", groupName));

            Object obj = cri.uniqueResult();
            if (obj != null) {
                return (Long.valueOf(obj.toString()));
            }
            return 0l;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return 0l;
        } finally {
            closeSession(session);
        }
    }
}
