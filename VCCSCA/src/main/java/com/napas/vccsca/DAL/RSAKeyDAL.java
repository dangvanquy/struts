/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.RSAKeysBO;
import static com.napas.vccsca.DAL.BaseDAL.getSession;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * RSAKeyDAL
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class RSAKeyDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(RSAKeyDAL.class);

    public void addKeys(RSAKeysBO keysBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(keysBO);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void updateKeys(RSAKeysBO keysBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(keysBO);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getAllKeys() throws Exception {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(RSAKeysBO.class)
                    .addOrder(Order.asc("rsaIndex"))
                    .list();
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getKeysByRsaStatus() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            cri.add(Restrictions.isNull("rsaStatus"));
            cri.addOrder(Order.asc("rsaIndex"));
            return cri.list();
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RSAKeysBO> getKeys(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("listId"))) {
                    cri.add(Restrictions.in("rsaId", (Integer[])hashMap.get("listId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaIndex"))) {
                    cri.add(Restrictions.eq("rsaIndex", NumberUtil.toNumber(hashMap.get("rsaIndex").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaStatus"))) {
                    cri.add(Restrictions.eq("rsaStatus", NumberUtil.toNumber(hashMap.get("rsaStatus").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expirationDate"))) {
                    cri.add(Restrictions.eq("expirationDate", hashMap.get("expirationDate")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("privateKeyNameCheck"))) {
                    cri.add(Restrictions.isNotNull("privateKeyName"));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("slotHsmCheck"))) {
                    cri.add(Restrictions.isNotNull("slotHsm"));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("oderdesc"))) {
                    cri.addOrder(Order.desc(hashMap.get("oderdesc") + ""));
                }
                cri.add(Restrictions.isNotNull("rsaStatus"));
            }
            cri.addOrder(Order.desc("updateDate"));
//            cri.addOrder(Order.desc("createDate"));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex);
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public RSAKeysBO getKeyById(Integer rsaId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaId)) {
                cri.add(Restrictions.eq("rsaId", rsaId));
            }
            return (RSAKeysBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public RSAKeysBO getKeyByIndex(Integer rsaIndex) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaIndex)) {
                cri.add(Restrictions.eq("rsaIndex", rsaIndex));
            }
            cri.add(Restrictions.eq("rsaStatus", 0));
            return (RSAKeysBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

}
