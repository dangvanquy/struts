/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * MembershipBankingDAL
 *
 * @author CuongTV
 * @since Aug 23, 2018
 * @version 1.0-SNAPSHOT
 */
public class MembershipBankingDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(MembershipBankingDAL.class);

    public void addBanking(BankMembershipBO banking) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(banking);
            session.getTransaction().commit();
        } catch (Exception e) {
//            e.printStackTrace();
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public void deleteBanking(BankMembershipBO banking) throws Exception {
        Session session = null;
        try {

            session = getSession();
            session.delete(banking);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public void updateBanking(BankMembershipBO banking) throws Exception {
        Session session = null;
        try {

            session = getSession();
            session.saveOrUpdate(banking);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<BankMembershipBO> getAllBanking() throws Exception {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(BankMembershipBO.class)
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<BankMembershipBO> getAllBankingActive() throws Exception {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(BankMembershipBO.class)
                    .add(Restrictions.eq("status", 0))
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<BankMembershipBO> getMembershipBanking(HashMap hashMap, Integer[] arrBin) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankId"))) {
                    cri.add(Restrictions.eq("bankId", NumberUtil.toNumber(hashMap.get("bankId").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", NumberUtil.toNumber(hashMap.get("bin").toString())));
                }
                // cho phep chon nhieu ngan hang
                if (!StringUtils.isNullOrEmpty(arrBin) && arrBin.length > 0) {
                    cri.add(Restrictions.in("bin", arrBin));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankFullName"))) {
                    cri.add(Restrictions.ilike("bankFullName", (String) hashMap.get("bankFullName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankShortName"))) {
                    cri.add(Restrictions.ilike("bankShortName", (String) hashMap.get("bankShortName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", NumberUtil.toNumber(hashMap.get("status").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("oderdesc"))) {
                    cri.addOrder(Order.desc(hashMap.get("oderdesc") + ""));
                }
            }
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<BankMembershipBO> chkBankMembership(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankId"))) {
                    cri.add(Restrictions.eq("bankId", NumberUtil.toNumber(hashMap.get("bankId").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", NumberUtil.toNumber(hashMap.get("bin").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankFullName"))) {
                    cri.add(Restrictions.eq("bankFullName", (String) hashMap.get("bankFullName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankShortName"))) {
                    cri.add(Restrictions.eq("bankShortName", (String) hashMap.get("bankShortName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", NumberUtil.toNumber(hashMap.get("status").toString())));
                }
            }
            cri.addOrder(Order.desc("createDate"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public BankMembershipBO getMembershipBankById(int bankId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (!StringUtils.isNullOrEmpty(bankId)) {
                cri.add(Restrictions.eq("bankId", bankId));
            }
            return (BankMembershipBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public BankMembershipBO getMembershipBankByBin(int bin) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (!StringUtils.isNullOrEmpty(bin)) {
                cri.add(Restrictions.eq("bin", bin));
            }
            return (BankMembershipBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

}
