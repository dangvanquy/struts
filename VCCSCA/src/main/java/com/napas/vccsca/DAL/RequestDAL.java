/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * RequestDAL
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class RequestDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(RequestDAL.class);

    public void createRequest(RequestBO requestBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(requestBO);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void updateRequest(RequestBO requestBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(requestBO);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void deleteRequest(RequestBO requestBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.delete(requestBO);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void deleteRequestByBin(int bin) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.createSQLQuery("DELETE TBL_REQUEST  where BIN = :BIN ").setParameter("BIN", bin).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> getRequestList() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.ne("status", 6));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> getRequestListDesc() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.ne("status", 6));//6: Tạo mới
            cri.add(Restrictions.ne("status", 40));//4: Xóa
            cri.add(Restrictions.ne("status", 41));//4: Xóa
            cri.add(Restrictions.ne("status", 42));//4: Xóa
            cri.add(Restrictions.ne("status", 43));//4: Xóa
            cri.add(Restrictions.ne("status", 44));//4: Xóa
            cri.add(Restrictions.ne("status", 45));//4: Xóa
            cri.add(Restrictions.ne("status", 46));//4: Xóa
            cri.add(Restrictions.ne("status", 5));//5: Thu hồi
            cri.add(Restrictions.ne("status", 2));//2: Duyệt
            cri.addOrder(Order.desc("createDate"));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> getRequestFileList() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("status", 6));
            cri.addOrder(Order.desc("createDate"));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> findRequest(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", hashMap.get("bin")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("ipk"))) {
                    cri.add(Restrictions.eq("ipk", hashMap.get("ipk")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                    cri.add(Restrictions.eq("serial", hashMap.get("serial")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankIdentity"))) {
                    cri.add(Restrictions.eq("bankIdentity", StringUtils.getBankIdentity((String) hashMap.get("bankIdentity"))));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expDate"))) {
                    cri.add(Restrictions.eq("expDate", hashMap.get("expDate")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("createDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) = TO_DATE('" + hashMap.get("createDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("fromDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) >= TO_DATE('" + hashMap.get("fromDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("toDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) <= TO_DATE('" + hashMap.get("toDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("statusne"))) {
                    if (!StringUtils.isNullOrEmpty(hashMap.get("status")) && !hashMap.get("status").equals(hashMap.get("statusne"))) {
                        cri.add(Restrictions.ne("status", hashMap.get("statusne")));
                    } else {
                        cri.add(Restrictions.ne("status", hashMap.get("statusne")));
                    }
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("statusne4")) && "delete".equals(hashMap.get("statusne4"))) {
                    cri.add(Restrictions.ne("status", 40));//4: Xóa
                    cri.add(Restrictions.ne("status", 41));//4: Xóa
                    cri.add(Restrictions.ne("status", 42));//4: Xóa
                    cri.add(Restrictions.ne("status", 43));//4: Xóa
                    cri.add(Restrictions.ne("status", 44));//4: Xóa
                    cri.add(Restrictions.ne("status", 45));//4: Xóa
                    cri.add(Restrictions.ne("status", 46));//4: Xóa
                }
            }
//            
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> findRequestFile(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", hashMap.get("bin")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("createDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) = TO_DATE('" + hashMap.get("createDate") + "','dd/mm/yyyy') "));
                }
            }
            cri.add(Restrictions.eq("status", 6));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public RequestBO getRequestById(Integer requestId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("requestId", requestId));
            return (RequestBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public RequestBO getRequestByRegId(Integer registerId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("registerId", registerId));
            return (RequestBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> findAllRequestRemove() throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            Integer[] deleteStatus = {4, 40, 41, 42, 43, 44, 45, 46};
            cri.add(Restrictions.in("status", deleteStatus));
            cri.addOrder(Order.desc("updateDate"));

            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> findRequestRemove(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("ipk"))) {
                    cri.add(Restrictions.eq("ipk", hashMap.get("ipk")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                    cri.add(Restrictions.eq("serial", hashMap.get("serial")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankIdentity"))) {
                    cri.add(Restrictions.eq("bankIdentity", hashMap.get("bankIdentity")));
                }
            }
            Integer[] deleteStatus = {4, 40, 41, 42, 43, 44, 45, 46};
            cri.add(Restrictions.in("status", deleteStatus));
            cri.addOrder(Order.desc("updateDate"));
//            cri.add(Restrictions.eq("status", 4));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<RequestBO> getReportRequests(HashMap hashMap, Integer[] arrBin) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(RequestBO.class);
            for (int i = 0; i < 5; i++) {
                cri.add(Restrictions.ne("status", Integer.parseInt("4" + i)));
            }
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(arrBin) && arrBin.length > 0) {
                    cri.add(Restrictions.in("bin", arrBin));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("appDate"))) {
                    cri.add(Restrictions.eq("appDate", DateTimeUtils.convertStringToDate(hashMap.get("appDate").toString(), "dd/MM/yyyy")));
                }
                cri.addOrder(Order.asc("bankName"));
            }
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }
}
