/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.GroupsPermissionBO;
import com.napas.vccsca.BO.PermissionRightBO;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * PermissionRightDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class PermissionRightDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(PermissionRightDAL.class);

    public void addPermissionRight(PermissionRightBO permissionRightBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(permissionRightBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void addListPermissionRight(List<PermissionRightBO> lstPermissionRightBO) throws Exception {

        Session session = null;
        try {
            session = getSession();
            for (PermissionRightBO permissionRightBO : lstPermissionRightBO) {
                session.save(permissionRightBO);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void deletePermissionRight(PermissionRightBO permissionRightBO) {
        Session session = null;
        try {
            session = getSession();
            session.delete(permissionRightBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void updatePermissionRight(PermissionRightBO permissionRightBO) {
        Session session = null;
        try {
            session = getSession();
            session.update(permissionRightBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<PermissionRightBO> getMenuPermission(int groupId) {
        Session session = null;
        try {
            session = getSession();
            if (StringUtils.isNullOrEmpty(session.createCriteria(GroupsPermissionBO.class)
                    .add(Restrictions.eq("groupId", groupId))
                    .add(Restrictions.eq("status", 0)).uniqueResult())) {
                return null;
            } else {
                return session.createCriteria(PermissionRightBO.class).add(Restrictions.eq("groupId", groupId)).list();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public List<PermissionRightBO> getPermissionById(Integer groupId) {
        Session session = null;
        try {
            session = getSession();
            return session.createCriteria(PermissionRightBO.class).add(Restrictions.eq("groupId", groupId)).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void updatePermissionRight(Integer groupId, List<PermissionRightBO> lstPermissionRightBO) {

        Session session = null;
        try {
            session = getSession();
            session.createQuery("delete from PermissionRightBO where groupId = :groupId")
                    .setParameter("groupId", groupId)
                    .executeUpdate();
            for (PermissionRightBO permissionRightBO : lstPermissionRightBO) {
                session.save(permissionRightBO);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void deletePermissionRightByGroupId(Integer groupId) {
        Session session = null;
        try {
            session = getSession();
            session.createQuery("delete from PermissionRightBO where groupId = :groupId")
                    .setParameter("groupId", groupId)
                    .executeUpdate();
            session.getTransaction().commit();

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public boolean checkPermision(String userName, String usrID, String code) {
        Session session = null;
        try {
            session = getSession();
            SQLQuery lQuery = session.createSQLQuery(" select count(1) from "
                    + " TBL_PERMISSION_RIGHT where "
                    + " GROUP_ID= (select GROUP_ID from TBL_USERS "
                    + " where USER_ID = :USER_ID and "
                    + " USERNAME = :USERNAME ) and "
                    + " PERMISSION_CODE= :PERMISSION_CODE ");

            lQuery.setParameter("USER_ID", NumberUtil.toNumber(usrID));
            lQuery.setParameter("USERNAME", userName);
            lQuery.setParameter("PERMISSION_CODE", code);

            Object obj = lQuery.uniqueResult();
            if (obj != null && (Integer.valueOf(obj.toString()) == 1)) {
                return true;
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        } finally {
            closeSession(session);
        }
        return false;
    }
}
