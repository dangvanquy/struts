/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.form.CertificateForm;
import com.napas.vccsca.utils.StringUtils;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

/**
 *
 * CertificateDAL
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class CertificateDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(CertificateDAL.class);

    /**
     * addCertificate
     *
     * @param certificateBO
     * @throws Exception
     */
    public void addCertificate(DigitalCertificateBO certificateBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(certificateBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * updateCertificate
     *
     * @param certificateBO
     */
    public void updateCertificate(DigitalCertificateBO certificateBO) {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(certificateBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getAllCertificates
     *
     * @return
     */
    public List<DigitalCertificateBO> getAllCertificates() {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(DigitalCertificateBO.class)
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getAllCertificates
     *
     * @return
     */
    public List<DigitalCertificateBO> getAllCertificatesDesc() {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(DigitalCertificateBO.class).addOrder(Order.desc("createDate"))
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getCertificates
     *
     * @param hashMap
     * @return
     */
    public List<DigitalCertificateBO> getCertificates(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(DigitalCertificateBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", hashMap.get("bin")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaId"))) {
                    cri.add(Restrictions.eq("rsaId", hashMap.get("rsaId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                    cri.add(Restrictions.eq("serial", hashMap.get("serial")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankIdentity"))) {
                    cri.add(Restrictions.eq("bankIdentity", hashMap.get("bankIdentity")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expDate"))) {
                    cri.add(Restrictions.like("expDate", hashMap.get("expDate")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("fromDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) >= TO_DATE('" + hashMap.get("fromDate") + "','dd/MM/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("toDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) <= TO_DATE('" + hashMap.get("toDate") + "','dd/MM/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("binSearch"))) {
                    cri.add(Restrictions.eq("bin", hashMap.get("binSearch")));
                }
            }
            cri.addOrder(Order.asc("certificateId"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    /**
     * getCertificateById
     *
     * @param certificateId
     * @return
     */
    public DigitalCertificateBO getCertificateById(Integer certificateId) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(DigitalCertificateBO.class);
            if (!StringUtils.isNullOrEmpty(certificateId)) {
                cri.add(Restrictions.eq("certificateId", certificateId));
            }
            return (DigitalCertificateBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public String getNewSeria() {
        Session session = null;
        try {
            session = getSession();
            String sql = "SELECT SERIAL FROM TBL_DIGITAL_CERTIFICATE ORDER BY TO_NUMBER( SERIAL, 'xxxxxx' ) DESC";
            SQLQuery query = session.createSQLQuery(sql).addScalar("SERIAL", StringType.INSTANCE);
            List serial = query.list();
            if (serial.size() > 0) {
                return StringUtils.paddingLeft(new BigInteger(serial.get(0) + "", 16).add(BigInteger.ONE).toString(16), 6);
            } else {
                return "000001";
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            return "000001";
        } finally {
            closeSession(session);
        }
    }

    /**
     * getCertificateReport
     *
     * @param hashMap
     * @return
     */
    public List<CertificateForm> getCertificateReport(HashMap hashMap) {
        Session session = null;
        try {
            StringBuffer sql = new StringBuffer();
            sql.append("select B.RSA_INDEX as rsaIndex ");
            sql.append(",A.SERIAL as serial ");
            sql.append(",A.EXP_DATE as expDate ");
            sql.append(",A.BANK_NAME as bankName ");
            sql.append(",A.REGISTER_ID as registerId ");
            sql.append(",A.STATUS as status ");
            sql.append(",A.CREATE_USER as createUser ");
            sql.append(",A.CREATE_DATE as createDate ");
            sql.append(" from TBL_DIGITAL_CERTIFICATE A, TBL_RSA_KEYS B ");
            sql.append(" where A.RSA_ID = B.RSA_ID ");
            if (!StringUtils.isNullOrEmpty(hashMap.get("rsaIndex"))) {
                sql.append(" and B.RSA_INDEX = " + hashMap.get("rsaIndex"));
            }
            if (!StringUtils.isNullOrEmpty(hashMap.get("expDate"))) {
                sql.append(" and A.EXP_DATE = '" + hashMap.get("expDate") + "'");
            }
            if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                sql.append(" and A.SERIAL = " + hashMap.get("serial"));
            }
            if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                sql.append(" and A.BIN = " + hashMap.get("bin"));
            }
            if (!StringUtils.isNullOrEmpty(hashMap.get("arrBin"))) {
                Integer[] arr = (Integer[]) hashMap.get("arrBin");
                String search = "";
                for (Integer integer : arr) {
                    search += "" + integer + ",";
                }
                sql.append(" and A.BIN in (" + search.substring(0, search.length() - 1) + ") ");
            }
            if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                sql.append(" and A.REGISTER_ID = " + hashMap.get("registerId"));
            }
            if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                sql.append(" and A.STATUS = " + hashMap.get("status"));
            }
            sql.append(" ORDER BY A.CREATE_DATE");
            session = getSession();
            Query q = session.createSQLQuery(sql.toString())
                    .addScalar("rsaIndex", IntegerType.INSTANCE)
                    .addScalar("serial", StringType.INSTANCE)
                    .addScalar("expDate", StringType.INSTANCE)
                    .addScalar("bankName", StringType.INSTANCE)
                    .addScalar("registerId", IntegerType.INSTANCE)
                    .addScalar("status", IntegerType.INSTANCE)
                    .addScalar("createUser", StringType.INSTANCE)
                    .addScalar("createDate", DateType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(CertificateForm.class));

            List<CertificateForm> list = q.list();
            return list;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public Long getCountBySeria(String seria) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(DigitalCertificateBO.class).setProjection(Projections.rowCount());
            if (!StringUtils.isNullOrEmpty(seria)) {
                cri.add(Restrictions.eq("serial", seria));
            }
            Object obj = cri.uniqueResult();
            if (obj != null) {
                return (Long.valueOf(obj.toString()));
            }
            return 0l;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return 0l;
        } finally {
            closeSession(session);
        }
    }

    public void getCertExpired() {
        Session session = null;
        try {
            session = getSession();
            session.createSQLQuery("update TBL_DIGITAL_CERTIFICATE set STATUS=2 where last_day(TO_DATE(EXP_DATE, 'MM/YY')) < sysdate").executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            closeSession(session);
        }
    }

    public void autoThuhoichungthu() {
        Session session = null;
        try {
            session = getSession();
            session.createSQLQuery("update TBL_DIGITAL_CERTIFICATE set STATUS=1,CONTENT='Ngân hàng thành viên dừng hoạt động' where STATUS = 0 AND BIN in (select BIN from TBL_BANK_MEMBERSHIP where STATUS=1) ").executeUpdate();
            session.createSQLQuery("update TBL_REQUEST set STATUS=5  where REGISTER_ID in (select REGISTER_ID from TBL_DIGITAL_CERTIFICATE where STATUS=1)  ").executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            closeSession(session);
        }
    }
    public void updateRqCertificate() {
        Session session = null;
        try {
            session = getSession();
            session.createSQLQuery("update TBL_REQUEST set STATUS=2  where REGISTER_ID in (select REGISTER_ID from TBL_DIGITAL_CERTIFICATE where STATUS=0)  ").executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            closeSession(session);
        }
    }
}
