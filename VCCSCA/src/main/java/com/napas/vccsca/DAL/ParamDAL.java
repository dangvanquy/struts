/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * ParamDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ParamDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ParamDAL.class);

    public void addParam(ParamBO paramBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(paramBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void deleteParam(ParamBO paramBO) {
        Session session = null;
        try {
            session = getSession();
            session.delete(paramBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void updateParam(ParamBO paramBO) {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(paramBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public List<ParamBO> findParam() {
        Session session = null;
        try {
            session = getSession();
            return session.createCriteria(ParamBO.class).addOrder(Order.desc("paramId")).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public ParamBO getParamById(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ParamBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("paramId"))) {
                    cri.add(Restrictions.eq("paramId", hashMap.get("paramId")));
                }
            }
            return (ParamBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    /**
     *
     * getParams
     *
     * @param hashMap
     * @return
     */
    public long getParams(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ParamBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("paramName"))) {
                    cri.add(Restrictions.eq("paramName", hashMap.get("paramName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("tableName"))) {
                    cri.add(Restrictions.eq("tableName", hashMap.get("tableName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("columnValue"))) {
                    cri.add(Restrictions.eq("columnValue", hashMap.get("columnValue")));
                }
            }
            cri.setProjection(Projections.rowCount());
            return (long) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return 0L;
        } finally {
            closeSession(session);
        }
    }

}
