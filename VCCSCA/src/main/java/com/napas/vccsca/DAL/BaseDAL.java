/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.database.SessionManager;
import com.napas.vccsca.constants.Constants;
import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * BaseDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class BaseDAL implements Serializable {

    private static final Logger logger = LogManager.getLogger(BaseDAL.class);

    /**
     * getSession
     *
     * @return
     */
    public static Session getSession() {
        Session session = null;
        try {
            session = new SessionManager().getSession();
            session.beginTransaction();
        } catch (Exception e) {
            logger.error("HibernateException, cannot get CurrenSession, openSession in Factory", e);
            session = null;
        }
        return session;
    }

    /**
     * closeSession
     *
     * @param session
     */
    public void closeSession(Session session) {
        try {
            if (session != null) {
                new SessionManager().closeSession(session);
            }
        } catch (Exception e) {
            logger.error("HibernateException, cannot get CurrenSession, openSession in Factory", e);
            session = null;
        }
    }

    /**
     * addOrder
     *
     * @param cri
     * @param begin
     * @param rowPerPage
     * @param fieldOrder
     * @param orderValue
     * @throws Exception
     */
    void addOrder(Criteria cri, int begin, int rowPerPage, String fieldOrder, String orderValue) throws Exception {
        if (fieldOrder != null) {
            switch (orderValue) {
                case Constants.ORDER.ASCENDING:
                    cri.addOrder(Order.asc(fieldOrder));
                    break;
                case Constants.ORDER.DESCENDING:
                    cri.addOrder(Order.desc(fieldOrder));
                    break;
            }
        }
        if (begin > 0 || rowPerPage > 0) {
            cri.setFirstResult(begin);
            cri.setMaxResults(rowPerPage);
        }

    }
}
