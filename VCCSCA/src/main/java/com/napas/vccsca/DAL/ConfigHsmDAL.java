/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.ConfigHsmBO;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * configHsmDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ConfigHsmDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ConfigHsmDAL.class);

    public void addConfigHsm(ConfigHsmBO hsmBO) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(hsmBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void deleteConfigHsm(ConfigHsmBO hsmBO) {
        Session session = null;
        try {
            session = getSession();
            session.delete(hsmBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public void updateConfigHsm(ConfigHsmBO hsmBO) {
        Session session = null;
        try {
            session = getSession();
            session.saveOrUpdate(hsmBO);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }

    }

    public List<ConfigHsmBO> findConfigHsm() {
        Session session = null;
        try {
            session = getSession();
            return session.createCriteria(ConfigHsmBO.class).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }
    
    public List<ConfigHsmBO> findConfigHsmDESC() {
        Session session = null;
        try {
            session = getSession();
            return session.createCriteria(ConfigHsmBO.class).addOrder(Order.desc("createDate")).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            closeSession(session);
        }
    }

    public ConfigHsmBO getConfigHsmById(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ConfigHsmBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("hsmId"))) {
                    cri.add(Restrictions.eq("hsmId", hashMap.get("hsmId")));
                }
            }
            return (ConfigHsmBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    /**
     *
     * getconfigHsm
     *
     * @param hashMap
     * @return
     */
    public long getConfigHsm(HashMap hashMap) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ConfigHsmBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("slot"))) {
                    cri.add(Restrictions.eq("slot", NumberUtil.toNumber(hashMap.get("slot") + "")));
                }
            }
            cri.setProjection(Projections.rowCount());
            return (long) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return 0L;
        } finally {
            closeSession(session);
        }
    }

    public ConfigHsmBO getConfigHsmBySlot(int slot) {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(ConfigHsmBO.class);
            cri.add(Restrictions.eq("slot", slot));
            return (ConfigHsmBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

}
