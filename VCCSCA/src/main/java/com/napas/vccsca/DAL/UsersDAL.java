/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.DAL;

import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.common.OutputCommon;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * UsersDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class UsersDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(UsersDAL.class);

    public void addUsers(UsersBO users) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.save(users);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void deleteUsers(UsersBO users) throws Exception {
        Session session = null;
        try {
            session = getSession();
            session.delete(users);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void updateUsers(UsersBO users) throws Exception {
        Session session = null;
        try {
            logger.info("update user-id :" + users.getUserId());
            session = getSession();
            session.saveOrUpdate(users);
            session.getTransaction().commit();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<UsersBO> getAllUsers() throws Exception {
        Session session = null;
        try {
            session = getSession();
            return session
                    .createCriteria(UsersBO.class)
                    .list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public List<UsersBO> getUser(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(UsersBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("fullName"))) {
                    cri.add(Restrictions.ilike("fullName", (String) hashMap.get("fullName"), MatchMode.ANYWHERE));
                }

                if (!StringUtils.isNullOrEmpty(hashMap.get("oremail"))) {
                    Criterion rest1 = Restrictions.and(Restrictions.eq("username", (String) hashMap.get("username")));
                    Criterion rest2 = Restrictions.and(Restrictions.eq("email", (String) hashMap.get("oremail")));
                    cri.add(Restrictions.or(rest1, rest2));
                } else if (!StringUtils.isNullOrEmpty(hashMap.get("username"))) {
                    cri.add(Restrictions.ilike("username", (String) hashMap.get("username"), MatchMode.ANYWHERE));
                }

                if (!StringUtils.isNullOrEmpty(hashMap.get("phoneNumber"))) {
                    cri.add(Restrictions.ilike("phoneNumber", (String) hashMap.get("phoneNumber"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("email"))) {
                    cri.add(Restrictions.ilike("email", (String) hashMap.get("email"), MatchMode.ANYWHERE));
                }

                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", NumberUtil.toNumber(hashMap.get("status").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("groupId"))) {
                    cri.add(Restrictions.eq("groupId", NumberUtil.toNumber(hashMap.get("groupId").toString())));
                }
            }
//            cri.addOrder(Order.desc("updateDate"));
            cri.addOrder(Order.desc("createDate"));
            List list = cri.list();
            return list;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw null;
        } finally {
            closeSession(session);
        }

    }

    /**
     * chkUser
     *
     * @param hashMap
     * @return
     * @throws Exception
     */
    public List<UsersBO> chkUser(HashMap hashMap) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(UsersBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("fullName"))) {
                    cri.add(Restrictions.eq("fullName", (String) hashMap.get("fullName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("username"))) {
                    cri.add(Restrictions.eq("username", (String) hashMap.get("username")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("phoneNumber"))) {
                    cri.add(Restrictions.eq("phoneNumber", (String) hashMap.get("phoneNumber")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("email"))) {
                    cri.add(Restrictions.eq("email", (String) hashMap.get("email")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", NumberUtil.toNumber(hashMap.get("status").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("groupId"))) {
                    cri.add(Restrictions.eq("groupId", NumberUtil.toNumber(hashMap.get("groupId").toString())));
                }
            }
            cri.addOrder(Order.desc("updateDate"));
//            cri.addOrder(Order.desc("createDate"));
            List list = cri.list();
            return list;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }

    }

    public boolean checkEmailExist(String email, Long userId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            StringBuilder strQueryBuilder = new StringBuilder("select 1 from UsersBO where lower(emailAddress) = :emailAddress and rownum <=1");
            if (userId != null) {
                strQueryBuilder.append(" and userId <> :userId ");
            }
            Object obj;
            if (userId == null) {
                obj = session
                        .createQuery(strQueryBuilder.toString())
                        .setParameter("emailAddress", email.toLowerCase())
                        .uniqueResult();
            } else {
                obj = session
                        .createQuery(strQueryBuilder.toString())
                        .setParameter("emailAddress", email.toLowerCase())
                        .setParameter("userId", userId)
                        .uniqueResult();
            }
            return obj != null && (Long.valueOf(obj.toString())).intValue() > 0;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public UsersBO getUsersById(Object id) throws Exception {
        Session session = null;
        try {
            session = getSession();
            List list = session.createQuery("from UsersBO where userId = :userId").setParameter("userId", id).list();
            if (list.isEmpty()) {
                return null;
            }
            return (UsersBO) list.get(0);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public UsersBO getUserById(Integer userId) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(UsersBO.class);
            if (!StringUtils.isNullOrEmpty(userId)) {
                cri.add(Restrictions.eq("userId", userId));
            }
            return (UsersBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public UsersBO getUserByEmail(String email) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(UsersBO.class);
            if (!StringUtils.isNullOrEmpty(email)) {
                cri.add(Restrictions.eq("email", email));
            }
            return (UsersBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
            closeSession(session);
        }
    }

    public OutputCommon getUser(String userName, String password) throws Exception {
        Session session = null;
        try {
            session = getSession();
            Criteria cri = session.createCriteria(UsersBO.class);
            cri.add(Restrictions.eq("username", userName));
            cri.add(Restrictions.eq("status", 0));
            List<UsersBO> list = cri.list();
            if (list == null || list.size() == 0) {
                return new OutputCommon("login.user.notexist", "", null);
            } else {
                UsersBO usersBO = list.get(0);
                if (usersBO != null && password.equals(usersBO.getPassword())) {
                    return new OutputCommon("", "", usersBO);
                } else {
                    return new OutputCommon("login.pass.error", "", null);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }
}
