/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import java.security.Key;
import java.security.MessageDigest;
import java.util.ResourceBundle;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * AESUtil
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class AESUtil {

    static String privateKey;

    public AESUtil() {
        privateKey = ResourceBundle.getBundle("cas").getString("securitykey");
    }

    public static void main(String[] args) throws Exception {
        System.out.println(encryptionPassword("12345678"));
//        System.out.println(encryption("jdbc:oracle:thin:@222.254.21.0:1521:vccsca"));
//        System.out.println(decryption("/Iwp6XnwsUh8CNHzAKwhGQ=="));
    }

    public static String decryption(String securitykey, String value) {
        try {
            Key key = new SecretKeySpec(securitykey.getBytes(), "AES");
            try {
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, key);
                return new String(cipher.doFinal(Base64.decodeBase64(value)));
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static String encryption(String value) {
        try {
            if (StringUtils.isNullOrEmpty(privateKey)) {
                privateKey = ResourceBundle.getBundle("cas").getString("securitykey");
            }
            if (!StringUtils.isNullOrEmpty(privateKey)) {
                // SALT is your secret key
                Key key = new SecretKeySpec(privateKey.getBytes(), "AES");
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, key);
                return Base64.encodeBase64String(cipher.doFinal(value.getBytes()));
            } else {
                return value;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static String encryptionPassword(String input) throws Exception {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static String decryption(String value) {
        try {
            if (StringUtils.isNullOrEmpty(privateKey)) {
                privateKey = ResourceBundle.getBundle("cas").getString("securitykey");
            }
            if (!StringUtils.isNullOrEmpty(privateKey)) {
                Key key = new SecretKeySpec(privateKey.getBytes(), "AES");
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, key);
                return new String(cipher.doFinal(Base64.decodeBase64(value)));
            } else {
                return value;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     *
     * @param value
     * @param pos 1:userName 2:userId
     * @return
     */
    public static String gettokenLogin(String value, int pos) {
//         "napasAES_" + usersBO.getUsername() + "_" + usersBO.getUserId() + "_KEY"
        try {
            String[] token = decryption(value).split("_");
            if (pos == 1) {
                return token[1];
            }
            if (pos == 2) {
                return token[2];
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }

    /**
     *
     * @param value
     * @param pos 1:userName 2:userId
     * @return
     */
    public static String[] gettoken(String value) {
//         "napasAES_" + usersBO.getUsername() + "_" + usersBO.getUserId() + "_KEY"
        try {
            String[] token = decryption(value).split("_");
            return token;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param value
     * @param pos 1:userName 2:userId
     * @return
     */
    public static String getUserLoginFromToken(String value) {
//         "napasAES_" + usersBO.getUsername() + "_" + usersBO.getUserId() + "_KEY"
        try {
            String[] token = decryption(value).split("_");
            return token[1];
        } catch (Exception e) {
            return "";
        }
    }

    public static String getUserIdFromToken(String value) {
//         "napasAES_" + usersBO.getUsername() + "_" + usersBO.getUserId() + "_KEY"
        try {
            String[] token = decryption(value).split("_");
            return token[2];
        } catch (Exception e) {
            return "";
        }
    }

    /**
     *
     * @param value
     * @param pos 1:userName 2:userId
     * @return
     */
    public static String getUserLoginFromToken(String value, Integer pos) {
//         "napasAES_" + usersBO.getUsername() + "_" + usersBO.getUserId() + "_KEY"
        try {
            String[] token = decryption(value).split("_");
            return token[pos];
        } catch (Exception e) {
            return "";
        }
    }

    public static String showpass(String passs) {
        String ret = "";
        for (int i = 0; i < passs.length(); i++) {
            ret += "*";
        }
        return ret;
    }
}
