/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import com.napas.vccsca.constants.Constants;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * DateTimeUtils
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class DateTimeUtils {

    private static final Logger logger = LogManager.getLogger(DateTimeUtils.class);
    private static final String PATTERN_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    private static final String PATTERN_DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";
    private static final String PATTERN_DDMMYYYY = "dd/MM/yyyy";

    private static ConcurrentHashMap<String, SimpleDateFormat> dateFormatHashMap = new ConcurrentHashMap<>();

    private static SimpleDateFormat getSimpleDateFormat(String pattern) {
        if (!dateFormatHashMap.contains(pattern)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            dateFormatHashMap.put(pattern, dateFormat);
        }
        return dateFormatHashMap.get(pattern);
    }

    public static Date convertStringToTime(String date, String pattern) {
        //SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            if ("dd/MM/yyyy".equals(pattern)) {
                if (!StringUtils.isNullOrEmpty(date)) {
                    String[] arr = date.split("/");
                    arr[0] = StringUtils.paddingLeft(arr[0], 2);
                    arr[1] = StringUtils.paddingLeft(arr[1], 2);
                    arr[2] = StringUtils.paddingLeft(arr[2], 4);
                    date = arr[0] + "/" + arr[1] + "/" + arr[2];
                }
                if (!date.matches("^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$")) {
                    return null;
                }
            }
            //return dateFormat.parse(date);
            return getSimpleDateFormat(pattern).parse(date);
        } catch (Exception e) {
            logger.error("Error when convertStringToTime", e);
            return null;
        }
    }

    public static Date convertStringToDate(String date) {
        try {
            String pattern = "yyyy-MM-dd";
            return convertStringToTime(date, pattern);
        } catch (Exception e) {
            logger.error("", e);
        }
        return null;
        //String pattern = "dd/MM/yyyy";
    }

    public static Date convertStringToDate(String date, String pattern) {
        try {
            return convertStringToTime(date, pattern);
        } catch (Exception e) {
            logger.error("", e);
        }
        return null;
        //String pattern = "dd/MM/yyyy";
    }

    public static String convertDateToString(Date date, String pattern) {
        try {
            //SimpleDateFormat dateFormat = new SimpleDateFormat(pattern); //"dd/MM/yyyy"
            if (date == null) {
                return "";
            }
            //return dateFormat.format(date);
            return getSimpleDateFormat(pattern).format(date);
        } catch (Exception e) {
            logger.error("Exception ", e);
            return "";
        }
    }

    public static String convertDateToString(Date date) throws Exception {
        return convertDateToString(date, "yyyy-MM-dd");
    }

    /*
     *  @todo: get sysdate
     *  @return: String sysdate
     */
    public static String getSysdate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        return convertDateToString(calendar.getTime());
    }

    /*
     *  @todo: get sysdate detail
     *  @return: String sysdate
     */
    public static String getSysDateTime() throws Exception {
        Calendar calendar = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            //return dateFormat.format(calendar.getTime());
            return getSimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(calendar.getTime());
        } catch (Exception e) {
            throw e;
        }
    }

    /*
     *  @todo: get sysdate detail formated in pattern
     *  @return: String sysdate
     */
    public static String getSysDateTime(String pattern) throws Exception {
        Calendar calendar = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            //return dateFormat.format(calendar.getTime());
            return getSimpleDateFormat(pattern).format(calendar.getTime());
        } catch (Exception e) {
            throw e;
        }
    }

    /*
     *  @todo: convert from String to DateTime detail
     *  @param: String date
     *  @return: Date
     */
    public static Date convertStringToDateTime(String date) throws Exception {
        String pattern = "dd/MM/yyyy HH:mm:ss";
        return convertStringToTime(date, pattern);
    }

    /**
     * <p>
     * Convert date to string with format dd/MM/yyyy
     * </p>
     * <p>
     * </p>
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static String convertDateTimeToString(Date date) throws Exception {
        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return getSimpleDateFormat("dd/MM/yyyy").format(date);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @param utilDate
     * @return
     * @todo: convert from java.util.Date to java.sql.Date
     */
    public static java.sql.Date convertToSqlDate(java.util.Date utilDate) {
        return new java.sql.Date(utilDate.getTime());
    }

    /**
     * @param monthInput
     * @anhlt - Get the first day on month selected.
     * @return
     */
    public static String parseDate(int monthInput) {
        String dateReturn = "01/01/";
        Calendar cal = Calendar.getInstance();
        switch (monthInput) {
            case 1:
                dateReturn = "01/01/";
                break;
            case 2:
                dateReturn = "01/02/";
                break;
            case 3:
                dateReturn = "01/03/";
                break;
            case 4:
                dateReturn = "01/04/";
                break;
            case 5:
                dateReturn = "01/05/";
                break;
            case 6:
                dateReturn = "01/06/";
                break;
            case 7:
                dateReturn = "01/07/";
                break;
            case 8:
                dateReturn = "01/08/";
                break;
            case 9:
                dateReturn = "01/09/";
                break;
            case 10:
                dateReturn = "01/10/";
                break;
            case 11:
                dateReturn = "01/11/";
                break;
            case 12:
                dateReturn = "01/12/";
                break;
        }
        return dateReturn + cal.get(Calendar.YEAR);
    }

    public static Date getFirstDayOfCurrentMonth() {
        //Using for get first day of the current month
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getFirstDayOfMonthBefore() {
        //Using for get first day of the month before
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 1, cal.get(Calendar.DATE));
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getMaxDate(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            if (date1.before(date2)) {
                return date2;
            } else {
                return date1;
            }
        } else if (date1 == null && date2 == null) {
            return date1;
        } else {
            return null;
        }
    }

    public static Date getMinDate(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            if (date1.before(date2)) {
                return date1;
            } else {
                return date2;
            }
        } else if (date1 == null && date2 == null) {
            return date1;
        } else {
            return null;
        }
    }

    /**
     * @param time
     * @return true if timePattern is correct other return false
     * @description timePattern is formated: HH:mm
     */
    public static boolean validateTime(String time) {
        try {
            if (time.length() != 5) {
                return false;
            }

            String[] dateSplit = time.split(":");
            //validate hour
            int numTemp = Integer.parseInt(dateSplit[0]);
            if (numTemp < 0 || numTemp > 23) {
                return false;
            }

            //validate minute
            numTemp = Integer.parseInt(dateSplit[1]);
            return !(numTemp < 0 || numTemp > 59);
        } catch (Exception e) {
            logger.error("Exception ", e);
        }
        return false;
    }

    /**
     * true if current time is between from time and to time other return fail
     *
     * @param fromTimeS
     * @param toTimeSet
     * @return
     * @throws Exception
     */
    public static boolean checkCurrentTimeBetween(String fromTimeS, String toTimeSet) throws Exception {
        return checkCurrentTimeBetween(fromTimeS, toTimeSet, new Date());
    }

    public static boolean checkCurrentTimeBetween(String fromTimeS, String toTimeSet, Date timeNow) throws Exception {

        //validate from time and to time
        if (!validateTime(fromTimeS)) {
            return false;
        }
        if (!validateTime(toTimeSet)) {
            return false;
        }

        String today = DateTimeUtils.convertDateToString(timeNow, "yyyyMMdd");
        /*String today = calendar.get(Calendar.YEAR) + ""
         + (calendar.get(Calendar.MONTH) + 1) + ""
         + calendar.get(Calendar.DAY_OF_MONTH);*/
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHH:mm");

        Date fromTimeTemp = simpleDateFormat.parse(today + fromTimeS);  //date convert from param table
        Date toTimeTemp = simpleDateFormat.parse(today + toTimeSet);      //date convert from param table

        //check time fromDate and time toDate
        if (fromTimeTemp.before(toTimeTemp)) { // if from time < to time download in a day
            if (timeNow.before(toTimeTemp) && timeNow.after(fromTimeTemp)) {
                return true;
            }
        } else if (fromTimeTemp.after(toTimeTemp)) {

//            if (timeNow.before(new Date(toTimeTemp.getTime()+24*60*60*1000)) && timeNow.after(fromTimeTemp)) {
//                return true;
//            }
            Date toTime1 = simpleDateFormat.parse(today + "24:00");
            Date fromTime1 = simpleDateFormat.parse(today + "00:00");
            if ((timeNow.before(toTime1) && timeNow.after(fromTimeTemp)) || (timeNow.before(toTimeTemp) && timeNow.after(fromTime1))) {
                return true;
            }
        } else {
            String temp = simpleDateFormat.format(timeNow);
            timeNow = simpleDateFormat.parse(temp);
            if (timeNow.equals(fromTimeTemp)) {
                return true;
            }
        }
        return false;
    }

    /**
     * true if current time is between from time and to time other return fail
     *
     * @param date
     * @param fromTime
     * @param toTime
     * @return
     * @throws Exception
     */
    public static boolean checkTimeBetween(Date date, String fromTime, String toTime) throws Exception {

        //validate from time and to time
        if (!validateTime(fromTime)) {
            return false;
        }
        if (!validateTime(toTime)) {
            return false;
        }
        String today = DateTimeUtils.convertDateToString(date, "yyyyMMdd");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHH:mm");

        Date fromTimeDate = simpleDateFormat.parse(today + fromTime);
        Date toTimeDate = simpleDateFormat.parse(today + toTime);

        //check time fromDate and time toDate
        if (date.after(fromTimeDate) && date.before(toTimeDate)) {
            return true;
        }
        if (date.compareTo(toTimeDate) == 0 || date.compareTo(fromTimeDate) == 0) {
            return true;
        }

        return false;
    }

    /**
     * Get String of today is formated YYYYMMDD
     *
     * @param fromTimeSet
     * @param toTimeSet
     * @return today
     * @throws Exception
     */
    public static String getTodayDependParam(String fromTimeSet, String toTimeSet) throws Exception {
        try {
            fromTimeSet = fromTimeSet.trim();
            toTimeSet = toTimeSet.trim();
            Calendar calendar = Calendar.getInstance();
            //validate from time and to time
            if (!DateTimeUtils.validateTime(fromTimeSet)) {
                return "";
            }
            if (!DateTimeUtils.validateTime(toTimeSet)) {
                return "";
            }
            String today = convertDateToString(new Date(), "yyyyMMdd");
            /*String today = calendar.get(Calendar.YEAR) + ""
             + (calendar.get(Calendar.MONTH) + 1) + ""
             + calendar.get(Calendar.DAY_OF_MONTH);*/
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHH:mm");

            Date fromTime = simpleDateFormat.parse(today + fromTimeSet);
            Date toTime = simpleDateFormat.parse(today + toTimeSet);
            if (fromTime.after(toTime)) {
                if (calendar.getTime().before(toTime)) { // da sang ngay tiep theo, giam 1 ngay trong ten file
                    calendar.add(Calendar.DAY_OF_MONTH, -1);
                    today = convertDateToString(calendar.getTime(), "yyyyMMdd");
                }
            }
            return today;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Get String of file day to get file,format: YYYYMMDD
     *
     * @param exportTime
     * @return today
     * @throws Exception
     */
    public static String getFileTimeLabel(String exportTime) throws Exception {
        try {
            if (!DateTimeUtils.validateTime(exportTime)) {
                return "";
            }
            Calendar calendar = Calendar.getInstance();
            String today = convertDateToString(new Date(), "yyyyMMdd");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHH:mm");
            Date todayExportTime = simpleDateFormat.parse(today + exportTime);
            if (calendar.getTime().before(todayExportTime)) {
                calendar.add(Calendar.DAY_OF_MONTH, -1);
                today = convertDateToString(calendar.getTime(), "yyyyMMdd");
            }
            return today;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * get Num Of Day from startDate to endDate
     *
     * @param startDateTime
     * @param endDate
     * @return
     * @throws Exception
     */
    public static Integer getNumOfDay(Date startDateTime, Date endDate) throws Exception {
        try {
            if (startDateTime == null || endDate == null) {
                return null;
            }
            Date startDate = convertStringToDate(convertDateToString(startDateTime, "yyyy-MM-dd"));
            final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
            int diffInDays = (int) ((endDate.getTime() - startDate.getTime()) / DAY_IN_MILLIS);
            return diffInDays;
        } catch (Exception e) {
            logger.error("Exception ", e);
            return null;
            //throw e;
        }
    }

    /*
     * checkCurrentDateTimebetween
     * 
     */
    public static boolean checkCurrentDateTimebetween(Date curentDate, Date dateStart, Date dateEnd) {
        if (curentDate != null && dateStart != null && dateEnd != null) {
            return curentDate.after(dateStart) && curentDate.before(dateEnd);
        }
        return false;
    }

    /*
     * addMuniteToDateTime
     * 
     */
    public static Date addMuniteToDateTime(Date pdteDatetime, int plMunite) {
        Date dteDateTimeAfterAdd = pdteDatetime;
        if (pdteDatetime != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(pdteDatetime);
            cal.add(Calendar.MINUTE, plMunite);
            dteDateTimeAfterAdd = cal.getTime();
        }
        return dteDateTimeAfterAdd;
    }

    public static int compareDateTime(Date d1, Date d2) {
        int result = 0;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(d1);
        cal2.setTime(d2);
        if (cal1.after(cal2)) {
            result = 1;
        } else if (cal1.before(cal2)) {
            result = -1;
        }
        return result;
    }

    public static boolean changeDate(Date d1, Date d2) {
        if (d1 == null && d2 == null) {
            return false;
        }
        if (d1 == null || d2 == null) {
            return true;
        }

        return d1.compareTo(d2) == 0;
    }

    public static boolean changeDateddMMyyyy(Date d1, Date d2) {
        if (d1 == null && d2 == null) {
            return false;
        }
        if (d1 == null || d2 == null) {
            return true;
        }
        return DateTimeUtils.convertDateToString(d1, "dd/MM/yyyy").compareTo(DateTimeUtils.convertDateToString(d2, "dd/MM/yyyy")) != 0;
    }

    public static XMLGregorianCalendar getCurrentXMLGregorianCalendar() throws DatatypeConfigurationException {
        return convertDateToXMLDate(Calendar.getInstance().getTime());
    }

    public static XMLGregorianCalendar convertDateToXMLDate(Date date) throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        xmlDate.setDay(calendar.get(Calendar.DATE));
        xmlDate.setMonth(calendar.get(Calendar.MONTH) + 1);
        xmlDate.setYear(calendar.get(Calendar.YEAR));
        return xmlDate;
    }

    public static XMLGregorianCalendar createXMLDate(int day, int month, int year) throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        xmlDate.setDay(day);
        xmlDate.setMonth(month);
        xmlDate.setYear(year);
        return xmlDate;
    }

    /**
     * convertDateToYYYYMMDD
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static String convertDateToYYYYMMDD(Date date) throws Exception {
        return convertDateToString(date, "yyyyMMdd");
    }

    /**
     * convertDateToYYYYMMDDHHmmss
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static String convertDateToYYYYMMDDHHmmss(Date date) throws Exception {
        return convertDateToString(date, PATTERN_YYYYMMDDHHMMSS);
    }

    /**
     * convertDateTo convertDateToDDMMYYYYHHmmss
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static String convertDateToDDMMYYYYHHmmss(Date date) throws Exception {
        return convertDateToString(date, PATTERN_DDMMYYYYHHMMSS);
    }

    /**
     * convertDateTo convertDateToDDMMYYYY
     *
     * @param date
     * @return
     * @throws Exception
     */
    public static String convertDateToDDMMYYYY(Date date) throws Exception {
        return convertDateToString(date, PATTERN_DDMMYYYY);
    }

    public static Date convertStringYYYYMMDDHHmmssToDate(String str) throws Exception {
        return convertStringToTime(str, PATTERN_YYYYMMDDHHMMSS);
    }

    /**
     * convertToStartDate
     *
     * @param date
     * @return
     */
    public static Date convertToStartDate1(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date convertToStartDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
        return calendar.getTime();
    }

    /**
     * convertToEndDate
     *
     * @param date
     * @return
     */
    public static Date convertToEndDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static HashMap getFromDateToDateFromIssueDate(Date issueDate) {
        HashMap hashMap = new HashMap();
        if (issueDate != null) {
            Calendar calendarIssueDate = Calendar.getInstance();
            calendarIssueDate.setTime(issueDate);
            calendarIssueDate.set(calendarIssueDate.get(Calendar.YEAR), calendarIssueDate.get(Calendar.MONTH), calendarIssueDate.get(Calendar.DATE), 0, 0, 0);
            calendarIssueDate.set(Calendar.MILLISECOND, 0);
            Date fromIssueDate = calendarIssueDate.getTime();
            Date toIssueDate = new Date(fromIssueDate.getTime() + Constants.ONE_DAY_MILLISECONDS);
            hashMap.put("startDate", fromIssueDate);
            hashMap.put("endDate", toIssueDate);
        }
        return hashMap;
    }

    public static void setCalendarTime(GregorianCalendar calendar, int year, int month, int day) {
        if (calendar == null) {
            return;
        }
        calendar.set(year, month, day);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public static void main(String[] args) {

    }
}
