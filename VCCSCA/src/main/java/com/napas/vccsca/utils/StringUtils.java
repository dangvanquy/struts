/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import com.napas.vccsca.BO.BankMembershipBO;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * StringUtils
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public final class StringUtils {

    private static final Logger logger = LogManager.getLogger(StringUtils.class);

    /**
     * alphabeUpCaseNumber.
     */
    private static final String ALPHABET_UPPER_CASE_NUMBER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    //private static final String  invoiceNoFormat = "(^[F|B|f|b][A-Za-z0-9]{3}\\-[\\d]{8})";
//    static final Pattern INVOICE_NO_FORMAT_PATTERN = Pattern.compile("(^[F|B|f|b][A-Za-z0-9]{3}\\-[\\d]{8})");
    static final Pattern IP_ADDRESS_PATTERN = Pattern.compile("^(?:[0-9\\*]{1,3}\\.){3}[0-9\\*]{1,3}$");
    private static final String EMAIL_PATTERN
            = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9\\-]+(\\.[A-Za-z0-9\\-]+)*(\\.[A-Za-z]{2,})$";
    /**
     * INVOICE_MAX_LENGTH
     */
    private static final int INVOICE_MAX_LENGTH = 7;
    /**
     * ZERO.
     */
    private static final String ZERO = "0";

    // SignIn Vietnamese
    private static final String[] signInVietnamese = {"á", "ả", "ã", "ạ", "ă", "ằ", "ắ", "ẳ", "ẵ", "ặ", "â", "ầ", "ấ", "ẩ", "ẫ", "ậ", "À", "Á", "Ả", "Ã", "Ạ", "Ă", "Ằ", "Ắ", "Ẳ", "Ẵ", "Ặ", "Â", "Ầ", "Ấ", "Ẩ", "Ẫ", "Ậ", "đ", "Đ", "è", "é", "ẻ", "ẽ", "ẹ", "ê", "ề", "ế", "ể", "ễ", "ệ", "È", "É", "Ẻ", "Ẽ", "Ẹ", "Ê", "Ề", "Ế", "Ể", "Ễ", "Ệ", "ì", "í", "ỉ", "ĩ", "ị", "Ì", "Í", "Ỉ", "Ĩ", "Ị", "ò", "ó", "ỏ", "õ", "ọ", "ô", "ồ", "ố", "ổ", "ỗ", "ộ", "ơ", "ờ", "ớ", "ở", "ỡ", "ợ", "Ò", "Ó", "Ỏ", "Õ", "Ọ", "Ô", "Ồ", "Ố", "Ổ", "Ỗ", "Ộ", "Ơ", "Ờ", "Ớ", "Ở", "Ỡ", "Ợ", "ù", "ú", "ủ", "ũ", "ụ", "ư", "ừ", "ứ", "ử", "ữ", "ự", "Ù", "Ú", "Ủ", "Ũ", "Ụ", "Ư", "Ừ", "Ứ", "Ử", "Ữ", "Ự", "ỳ", "ý", "ỷ", "ỹ", "ỵ", "Y", "Ỳ", "Ý", "Ỷ", "Ỹ", "Ỵ"};

    /**
     * Regular expression to check the word have special character or not
     *
     */
    private static final String SPECIAL_CHARACTER_PATTERN = "^[^\\`\\~\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\=\\+\\[\\{\\]\\}\\\\\\|\\;\\:\\'\\\"\\,\\<\\.\\>\\/\\?]+$";

    /**
     * Creates a new instance of StringUtils
     */
    private static final String VALID_CHARACTER = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static final String REGEX_PRODUCT_PRICE = "^[0-9\\,\\.E]+$";

    public static String chgNull(String rsaStatus) {
        if (isNullOrEmpty(rsaStatus)) {
            return "";
        } else {
            return rsaStatus;
        }
    }

    public static String getBankIdentity(String bankIdentity) {
        for (int i = 0; bankIdentity.length() < 8; i++) {
            bankIdentity = bankIdentity + "F";
        }
        return bankIdentity;
    }

    public static String getBankIdentityNoFF(String bankIdentity) {
        int i = 8;
        while (i > 0) {
            if (bankIdentity.toUpperCase().endsWith("F")) {
                bankIdentity = bankIdentity.substring(0, bankIdentity.length() - 1);
            } else {
                break;
            }
            i--;
        }
        return bankIdentity;
    }

    public static String addYeah(String year) {
        if (year.length() == 2) {
            return "20" + year;
        }
        return year;
    }

    private StringUtils() {
    }

    public static boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * method compare two string
     *
     * @param str1 String
     * @param str2 String
     * @return boolean
     */
    public static boolean compareString(String str1, String str2) {
        if (str1 == null) {
            str1 = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        return str1.equals(str2);
    }

    public static boolean checkCountryCode(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }

        String pattern = "^[0-9-]+$";
        return str.matches(pattern);
    }

    /**
     * method convert long to string
     *
     * @param lng Long
     * @return String
     * @throws java.lang.Exception
     */
    public static String convertFromLongToString(Long lng) throws Exception {
        try {
            return Long.toString(lng);
        } catch (Exception ex) {
//            logger.error("loi xay ra khi convert so", ex);
            throw ex;
        }
    }

    /*
     *  @todo: convert from Long array to String array
     */
    public static String[] convertFromLongToString(Long[] arrLong) throws Exception {
        String[] arrResult = new String[arrLong.length];
        try {
            int length = arrLong.length;
            for (int i = 0; i < length; i++) {
                arrResult[i] = convertFromLongToString(arrLong[i]);
            }
            return arrResult;
        } catch (Exception ex) {
//            logger.error("loi xay ra khi convert so", ex);
            throw ex;
        }
    }

    /*
     *  @todo: convert from String array to Long array
     */
    public static long[] convertFromStringToLong(String[] arrStr) throws Exception {
        long[] arrResult = new long[arrStr.length];
        try {
            int length = arrStr.length;
            for (int i = 0; i < length; i++) {
                arrResult[i] = Long.parseLong(arrStr[i]);
            }
            return arrResult;
        } catch (Exception ex) {
//            logger.error("loi xay ra khi convert so", ex);
            throw ex;
        }
    }

    /*
     *  @todo: convert from String value to Long value
     */
    public static long convertFromStringToLong(String value) throws Exception {
        try {
            return Long.parseLong(value);
        } catch (Exception ex) {
//            logger.error("loi xay ra khi convert so", ex);
            throw ex;
        }
    }


    /*
     * Check String that containt only AlphabeUpCase and Number
     * Return True if String was valid, false if String was not valid
     */
    public static boolean checkAlphabeUpCaseNumber(String value) {
        boolean result = true;
        int length = value.length();
        for (int i = 0; i < length; i++) {
            String temp = value.substring(i, i + 1);
            if (!ALPHABET_UPPER_CASE_NUMBER.contains(temp)) {
                result = false;
                return result;
            }
        }
        return result;
    }

    public static String renameVietnameseToEnglish(String input) {
        if (isNullOrEmpty(input)) {
            return input;
        }
        String nfdNormalizedString = Normalizer.normalize(input.toLowerCase(), Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("").replace('đ', 'd').replaceAll(" +", " ");
    }

    public static String removeSignInVietnamese(String input) {
        if (isNullOrEmpty(input)) {
            return input;
        }
        String file = input;
        file = file.replace('à', 'a');
        file = file.replace('á', 'a');
        file = file.replace('ả', 'a');
        file = file.replace('ã', 'a');
        file = file.replace('ạ', 'a');
        file = file.replace('ă', 'ă');
        file = file.replace('ằ', 'ă');
        file = file.replace('ắ', 'ă');
        file = file.replace('ẳ', 'ă');
        file = file.replace('ẵ', 'ă');
        file = file.replace('ặ', 'ă');
        file = file.replace('â', 'â');
        file = file.replace('ầ', 'â');
        file = file.replace('ấ', 'â');
        file = file.replace('ẩ', 'â');
        file = file.replace('ẫ', 'â');
        file = file.replace('ậ', 'â');
        file = file.replace('À', 'a');
        file = file.replace('Á', 'a');
        file = file.replace('Ả', 'a');
        file = file.replace('Ã', 'a');
        file = file.replace('Ạ', 'a');
        file = file.replace('Ă', 'ă');
        file = file.replace('Ằ', 'ă');
        file = file.replace('Ắ', 'ă');
        file = file.replace('Ẳ', 'ă');
        file = file.replace('Ẵ', 'ă');
        file = file.replace('Ặ', 'ă');
        file = file.replace('Â', 'â');
        file = file.replace('Ầ', 'â');
        file = file.replace('Ấ', 'â');
        file = file.replace('Ẩ', 'â');
        file = file.replace('Ẫ', 'â');
        file = file.replace('Ậ', 'â');
        file = file.replace('đ', 'đ');
        file = file.replace('Đ', 'Đ');
        file = file.replace('è', 'e');
        file = file.replace('é', 'e');
        file = file.replace('ẻ', 'e');
        file = file.replace('ẽ', 'e');
        file = file.replace('ẹ', 'e');
        file = file.replace('ê', 'ê');
        file = file.replace('ề', 'ê');
        file = file.replace('ế', 'ê');
        file = file.replace('ể', 'ê');
        file = file.replace('ễ', 'ê');
        file = file.replace('ệ', 'ê');
        file = file.replace('È', 'e');
        file = file.replace('É', 'e');
        file = file.replace('Ẻ', 'e');
        file = file.replace('Ẽ', 'e');
        file = file.replace('Ẹ', 'e');
        file = file.replace('Ê', 'ê');
        file = file.replace('Ề', 'ê');
        file = file.replace('Ế', 'ê');
        file = file.replace('Ể', 'ê');
        file = file.replace('Ễ', 'ê');
        file = file.replace('Ệ', 'ê');
        file = file.replace('ì', 'i');
        file = file.replace('í', 'i');
        file = file.replace('ỉ', 'i');
        file = file.replace('ĩ', 'i');
        file = file.replace('ị', 'i');
        file = file.replace('Ì', 'i');
        file = file.replace('Í', 'i');
        file = file.replace('Ỉ', 'i');
        file = file.replace('Ĩ', 'i');
        file = file.replace('Ị', 'i');
        file = file.replace('ò', 'o');
        file = file.replace('ó', 'o');
        file = file.replace('ỏ', 'o');
        file = file.replace('õ', 'o');
        file = file.replace('ọ', 'o');
        file = file.replace('ô', 'ô');
        file = file.replace('ồ', 'ô');
        file = file.replace('ố', 'ô');
        file = file.replace('ổ', 'ô');
        file = file.replace('ỗ', 'ô');
        file = file.replace('ộ', 'ô');
        file = file.replace('ơ', 'ơ');
        file = file.replace('ờ', 'ơ');
        file = file.replace('ớ', 'ơ');
        file = file.replace('ở', 'ơ');
        file = file.replace('ỡ', 'ơ');
        file = file.replace('ợ', 'ơ');
        file = file.replace('Ò', 'o');
        file = file.replace('Ó', 'o');
        file = file.replace('Ỏ', 'o');
        file = file.replace('Õ', 'o');
        file = file.replace('Ọ', 'o');
        file = file.replace('Ô', 'ô');
        file = file.replace('Ồ', 'ô');
        file = file.replace('Ố', 'ô');
        file = file.replace('Ổ', 'ô');
        file = file.replace('Ỗ', 'ô');
        file = file.replace('Ộ', 'ô');
        file = file.replace('Ơ', 'ơ');
        file = file.replace('Ờ', 'ơ');
        file = file.replace('Ớ', 'ơ');
        file = file.replace('Ở', 'ơ');
        file = file.replace('Ỡ', 'ơ');
        file = file.replace('Ợ', 'ơ');
        file = file.replace('ù', 'u');
        file = file.replace('ú', 'u');
        file = file.replace('ủ', 'u');
        file = file.replace('ũ', 'u');
        file = file.replace('ụ', 'u');
        file = file.replace('ư', 'ư');
        file = file.replace('ừ', 'ư');
        file = file.replace('ứ', 'ư');
        file = file.replace('ử', 'ư');
        file = file.replace('ữ', 'ư');
        file = file.replace('ự', 'ư');
        file = file.replace('Ù', 'u');
        file = file.replace('Ú', 'u');
        file = file.replace('Ủ', 'u');
        file = file.replace('Ũ', 'u');
        file = file.replace('Ụ', 'u');
        file = file.replace('Ư', 'ư');
        file = file.replace('Ừ', 'ư');
        file = file.replace('Ứ', 'ư');
        file = file.replace('Ử', 'ư');
        file = file.replace('Ữ', 'ư');
        file = file.replace('Ự', 'ư');
        file = file.replace('ỳ', 'y');
        file = file.replace('ý', 'y');
        file = file.replace('ỷ', 'y');
        file = file.replace('ỹ', 'y');
        file = file.replace('ỵ', 'y');
        file = file.replace('Y', 'y');
        file = file.replace('Ỳ', 'y');
        file = file.replace('Ý', 'y');
        file = file.replace('Ỷ', 'y');
        file = file.replace('Ỹ', 'y');
        file = file.replace('Ỵ', 'y');
        return file;
    }

    /**
     * Generate a random string
     *
     * @param length The length of the random string
     * @return A random string
     */
    public static String createRandomString(int length) {
        if (length <= 0) {
            return "";
        }
        String pattern = "abcdefghijklmnopqrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ0123456789";
        int count = 0;
        int parrterLen = pattern.length();
        StringBuilder builder = new StringBuilder();
        Random rand = new Random();
        while (count < length) {
            builder.append((pattern.charAt(rand.nextInt(parrterLen))));
            count++;
        }
        return builder.toString();

    }

    /**
     * <p>
     * Create a random string that specify the min length and the max length
     * </p>
     * <p>
     *
     * @param minLen
     * @param maxLen
     * @return
     */
    public static String createRandomString(int minLen, int maxLen) {
        if (minLen <= 0) {
            return "";
        }
        Random rand = new Random();
        int newStringLen = rand.nextInt((maxLen - minLen) + 1) + minLen;
        char[] newString = new char[newStringLen];
        for (int i = 0; i < newStringLen; i++) {
            newString[i] = VALID_CHARACTER.charAt(rand.nextInt(VALID_CHARACTER.length()));
        }
        return new String(newString);
    }

    public static String standardInvoiceString(Long input) {
        String temp;
        if (input == null) {
            return "";
        }
        temp = input.toString();
        if (temp.length() <= INVOICE_MAX_LENGTH) {
            int count = INVOICE_MAX_LENGTH - temp.length();
            for (int i = 0; i < count; i++) {
                temp = ZERO + temp;
            }
        }
        return temp;
    }

    public static boolean validString(String temp) {
        return !(temp == null || temp.trim().isEmpty());
    }

    /**
     * validate IP Address
     *
     * @param ip
     * @return
     */
    public static boolean validateIPAddress(String ip) {
        if (isNullOrEmpty(ip)) {
            return false;
        }
        /*String ipAddressPartern
         = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
         + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
         + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
         + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
         Pattern pattern = Pattern.compile(ipAddressPartern);*/
        Matcher matcher = IP_ADDRESS_PATTERN.matcher(ip);
        return matcher.matches();
    }

    /**
     * validate multi IP Address
     *
     * @param ipAddress
     * @return
     */
    public static boolean validateMultiIPAddress(String ipAddress) {
        if (isNullOrEmpty(ipAddress)) {
            return false;
        }
        //System.out.println(ipAddress);
        String[] ipArray = ipAddress.split("\\|");

        logger.info("=====> ip " + ipArray.length);
        for (int i = 0, size = ipArray.length; i < size; i++) {
            Matcher matcher = IP_ADDRESS_PATTERN.matcher(ipArray[i].trim());
            if (!matcher.matches()) {
                logger.info("=====> ip " + ipArray[i]);
                return false;
            }
        }
        return true;
    }

    /**
     * getLimitedString check max length
     *
     * @param str
     * @param maxlength
     * @return
     */
    public static String getLimitedString(String str, Integer maxlength) {
        if (str != null && !str.isEmpty()) {
            if (str.length() <= maxlength) {
                return str;
            } else {
                return str.substring(0, maxlength);
            }
        }
        return "";
    }

    /**
     * <p>
     * Last update user: dungnv93
     * </p>
     * <p>
     * Last update Time : 07/09/2017
     * </p>
     * check null or empty
     *
     * @param object
     * @return
     */
    public static Boolean isNullOrEmpty(Object object) {
        if (object == null || object.toString().trim().isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isHexa(String str) {
        boolean isHex = str.matches("[0-9A-Fa-f]+");
        return isHex;
    }

    public static boolean equals(String str1, String str2) {
        if (str1 == null) {
            return str2 == null;
        } else if (str2 == null) {
            return false;
        }
        return str1.trim().equals(str2.trim());
    }

    //Dungnv93 - 12/10/2017
    public static boolean equalsConsiderNullIsEmpty(String str1, String str2) {
        str1 = str1 == null ? "" : str1;
        str2 = str2 == null ? "" : str2;

        return str1.trim().equals(str2.trim());
    }

    /**
     * checkRegularExpression in list accept
     *
     * @param input
     * @return
     */
    public static boolean checkRegularExpression(String input) {
        if (isNullOrEmpty(input)) {
            return false;
        }
//        return input.matches("^[A-Za-z0-9 _]+$");
        return input.matches("^[A-Za-z0-9_]+$");
    }

    /**
     * checkSpecialCharacter in list special character
     *
     * @param input
     * @return
     */
    public static boolean checkSpecialCharacter(String input) {
        if (isNullOrEmpty(input)) {
            return false;
        }
        return input.matches(SPECIAL_CHARACTER_PATTERN);
    }

    /**
     * checkInvoiceNo by format
     *
     * @param invoiceNo
     * @return
     */
//    public static boolean checkInvoiceNo(String invoiceNo) {
//        if (isNullOrEmpty((invoiceNo))) {
//            return false;
//        }
//        return invoiceNo.matches("[a-zA-Z0-9/]+");
//    }
    //haint844 updated 20170914
    public static boolean checkInvoiceNo(String invoiceNo) {
        if (isNullOrEmpty((invoiceNo))) {
            return false;
        }
        if (invoiceNo.length() != 13) {
            return false;
        }
        if (invoiceNo.split("/").length != 2) {
            return false;
        }
        if (!isAlpha(invoiceNo.split("/")[0])) {
            return false;
        }
        if (!"E".equalsIgnoreCase(invoiceNo.substring(5, 6))) {
            return false;
        }
        if (!invoiceNo.substring(6).matches("[0-9]+")) {
            return false;
        }
        return invoiceNo.matches("[a-zA-Z0-9/]+");
    }

    /**
     * checkSpecialCharacter1
     *
     * @param value
     * @return
     */
    public static boolean checkSpecialCharacter1(String value) {
        char c;
        int cint;
        int count = 0;
        int length = value.length();
        for (int i = 0; i < length; i++) {
            c = value.charAt(i);
            cint = (int) c;
            if (cint < 48 || (cint > 57 && cint < 65) || (cint > 90 && cint < 97) || cint > 122) {
                count++;
            }
        }
        return count > 0;
    }

    /**
     * convertToStartDate
     *
     * @param startDate
     * @return
     */
    public static Date convertToStartDate(Date startDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * convertToEndDate
     *
     * @param startDate
     * @return
     */
    public static Date convertToEndDate(Date startDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    /**
     * checkIssueDateSearch <= 30 day @
     *
     *
     * param issueDateFrom @param issueDateTo @return @param issueDateFrom
     * @param issueDateFrom @param issueDateTo @return
     */
    public static boolean checkIssueDateSearch(Date issueDateFrom, Date issueDateTo) {
        long diff = issueDateTo.getTime() - issueDateFrom.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return diffDays <= 31;
    }

    /**
     * encryptPassword SHA-1
     *
     * @return
     */
//    public static String encryptPassword(String password) {
//        MessageDigest md = null;
//        try {
//            md = MessageDigest.getInstance("SHA-1");
//        } catch (Exception e) {
//            logger.error("Exception", e);
//        }
//        if (md == null) {
//            return "";
//        }
//        try {
//            md.update(password.getBytes("UTF-8"));
//        } catch (Exception e) {
//            logger.error("Exception", e);
//        }
//        byte raw[] = md.digest();
//        //String hash = (new BASE64Encoder()).encode(raw);
//        String hash = Base64.encodeBase64String(raw);
//        return hash;
//    }
    public static String format(BigDecimal dblNumber, String strPattern) {
        DecimalFormat fmt = new DecimalFormat(strPattern);
        return fmt.format(dblNumber);
    }

    public static String getFileNameWithoutExtension(String link) {
        String separator = "/";
        String filename = link.substring(link.lastIndexOf(separator) + 1, link.lastIndexOf('.'));
        return filename;
    }

    public static void replace(File file, String oldString, String newString) throws IOException {
        String content = org.apache.commons.io.FileUtils.readFileToString(file);
        org.apache.commons.io.FileUtils.writeStringToFile(file, content.replaceAll(oldString, newString));
    }

    public static boolean isValidEmailAddress(String email) {
        if (isNullOrEmpty(email)) {
            return false;
        }
        return email.matches(EMAIL_PATTERN);

    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        if (StringUtils.isNullOrEmpty(phoneNumber)) {
            return false;
        }
        if (!isNumber(phoneNumber.trim())) {
            return false;
        }
        if (phoneNumber.trim().length() > 12) {
            return false;
        }
        return true;
    }

    public static boolean isNumber(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String pattern = "^[0-9]+$";
        return str.matches(pattern);
    }

    public static boolean isNumber2(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String pattern = "^[-0-9]+$";
        return str.matches(pattern);
    }

    public static boolean isPrice(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String pattern = "^[0-9\\.]+$";
        return str.matches(pattern);
    }

    public static boolean checkProductPrice(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }

        return str.matches(REGEX_PRODUCT_PRICE);
    }

    public static boolean checkPhoneFaxNumber(String str) {
        return isNumber(str);
    }

    public static boolean checkIdNo(String str) {
        return isNumber(str);
    }

    /**
     * <p>
     * Check id number </p>
     * <p>
     * Include identity card number, passport number, motobike driving
     * liscence</p>
     * <p>
     * @param str
     * @return
     */
    public static boolean checkIdNumber(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String patter = "^[ĐA-Za-z/0-9–-]+$";
        return str.matches(patter);

    }

    public static boolean checkAddress(String str) {
        if (str == null || "".equalsIgnoreCase(str)) {
            return false;
        }
//        String pattern = "^[0-9a-zA-Z_/ –-]+$";
//        return str.matches(pattern);
        return true;
    }

    public static boolean checkAddressAllowVietnamese(String str) {
        try {
            if (isNullOrEmpty(str)) {
                return false;
            }
//        String pattern = "^[^\\!-&*\\+\\.\\:-\\@\\[-\\^\\`\\{-\\~]*$";
//        String pattern = "^[a-zA-Z0-9.,() &'/_-]+$";
//            ParamBO tmpParamBO = MemoryDataLoader.getParamBO("REGEX_VALIDATE_DATA", "ADDRESS");
//            logger.info("ADDRESS regex: " + tmpParamBO.getName());

//            return renameVietnameseToEnglish(str).matches(tmpParamBO.getName());
            return true;
        } catch (Exception ex) {
            logger.error("have error ", ex);
        }
        return false;
    }

    /**
     * <p>
     * Last update user: huypq15 </p>
     * <p>
     * Last up date : 20/08/2017
     *
     * <p>
     * @param str
     * @return
     */
    public static boolean checkTaxCode(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }

        str = str.trim();
        int length = str.length();

        if (length != 10 && length != 14) {
            return false;
        }
        String pattern = "^[0-9]+$";

        if (length == 10) {
            if (!str.matches(pattern)) {
                return false;
            }

        }

        if (length == 14) {
            String[] arr = str.split("-");
            if (arr.length != 2) {
                return false;
            }
            if (!arr[0].matches(pattern) || !arr[1].matches(pattern)) {
                return false;
            }
        }

        int A1 = Integer.parseInt(str.substring(0, 1)) * 31;
        int A2 = Integer.parseInt(str.substring(1, 2)) * 29;
        int A3 = Integer.parseInt(str.substring(2, 3)) * 23;
        int A4 = Integer.parseInt(str.substring(3, 4)) * 19;
        int A5 = Integer.parseInt(str.substring(4, 5)) * 17;
        int A6 = Integer.parseInt(str.substring(5, 6)) * 13;
        int A7 = Integer.parseInt(str.substring(6, 7)) * 7;
        int A8 = Integer.parseInt(str.substring(7, 8)) * 5;
        int A9 = Integer.parseInt(str.substring(8, 9)) * 3;
        int A = A1 + A2 + A3 + A4 + A5 + A6 + A7 + A8 + A9;
        int B = A % 11;
        int C = 10 - B;
        if (C != Integer.parseInt(str.substring(9, 10))) {
            return false;
        }

        return true;
    }

    /**
     * Kiem tra MST cua nguoi mua. Sau nay se cho phep MST cua nhieu nuoc
     *
     * @param str
     * @return
     */
    public static boolean checkBuyerTaxCode(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }

        str = str.trim();
        int length = str.length();

        if (length != 10 && length != 14) {
            return true;
        }
        String pattern = "^[0-9]+$";

        //MST Vietnam
        if (length == 10) {
            if (!str.matches(pattern)) {
                return false;
            }

        }

        if (length == 14) {
            String[] arr = str.split("-");
            if (arr.length != 2) {
                return false;
            }
            if (!arr[0].matches(pattern) || !arr[1].matches(pattern)) {
                return false;
            }
        }

        int A1 = Integer.parseInt(str.substring(0, 1)) * 31;
        int A2 = Integer.parseInt(str.substring(1, 2)) * 29;
        int A3 = Integer.parseInt(str.substring(2, 3)) * 23;
        int A4 = Integer.parseInt(str.substring(3, 4)) * 19;
        int A5 = Integer.parseInt(str.substring(4, 5)) * 17;
        int A6 = Integer.parseInt(str.substring(5, 6)) * 13;
        int A7 = Integer.parseInt(str.substring(6, 7)) * 7;
        int A8 = Integer.parseInt(str.substring(7, 8)) * 5;
        int A9 = Integer.parseInt(str.substring(8, 9)) * 3;
        int A = A1 + A2 + A3 + A4 + A5 + A6 + A7 + A8 + A9;
        int B = A % 11;
        int C = 10 - B;
        if (C != Integer.parseInt(str.substring(9, 10))) {
            return false;
        }

        return true;
    }

    public static boolean checkDescriptionAllowVietnamese(String description) {
        if (isNullOrEmpty(description)) {
            return false;
        }
        return true;
//        String pattern = "^[a-zA-Z0-9()., _–-]+$";
//        return renameVietnameseToEnglish(description).matches(pattern);
    }

    public static boolean checkUserNameAllowVietnamese(String username) {
        return checkNameAllowVietnamese(username);
    }

    public static boolean checkAcountFormat(String name) {
        if (isNullOrEmpty(name)) {
            return false;
        }
//        String pattern = "^[^\\!-\\,\\.\\/\\:-\\@\\[-\\^\\`\\{-\\~]+$";
        return true;
//        String pattern = "^[a-zA-Z0-9 _.–-]+$";
//        return name.matches(pattern);
    }

    public static boolean checkBankAccountOwnerAllowVietnamese(String username) {
        return checkNameAllowVietnamese(username);
    }

    public static boolean checkUnitNameAllowVietnamese(String username) {
        return checkNameAllowVietnamese(username);
    }

    public static boolean checkFileNameAllowVietnamese(String fileName) {
        if (isNullOrEmpty(fileName)) {
            return false;
        }
        if (fileName.length() > 200) {
            return false;
        }
        fileName = renameVietnameseToEnglish(fileName);
        String regEx = "^[a-zA-Z0-9\\ _–-]+\\.[a-zA-Z]+$";
        return fileName.matches(regEx);
    }

    public static boolean checkAlphaNumeric(String str) {
        if (isNullOrEmpty((str))) {
            return false;
        }
        return str.matches("[a-zA-Z0-9]+$");
    }

    public static boolean checkProductCode(String str) {
        String pattern = "^[a-zA-Z0-9_–-]+$";
        if (StringUtils.isNullOrEmpty(str)) {
            return false;
        }
        return str.matches(pattern);
    }

    public static boolean checkTemplateCode(String str) {
        String pattern = "^[a-zA-Z0-9\\/]+$";
        if (StringUtils.isNullOrEmpty(str)) {
            return false;
        }
        return str.matches(pattern);
    }

    public static boolean checkProductName(String str) {
        //String pattern = "^[a-zA-Z0-9 _–-]+$";
        if (StringUtils.isNullOrEmpty(str)) {
            return false;
        }
        return true;
//        return str.matches(pattern);
    }

    public static boolean isNumbericAndDots(String str) {
        if (isNullOrEmpty((str))) {
            return false;
        }
        return str.matches("^[\\d\\.]+$");
    }

    /**
     * <p>
     * Last update dev: dungnv93
     * </p>
     * <p>
     * Last update time: 19/09/2017
     * </p>
     *
     * @param productName
     * @return
     */
    public static boolean checkProductNameAllowVietnamese(String productName) {
        try {
            if (isNullOrEmpty(productName)) {
                return false;
            }

            //        String pattern = "^[^\!-\'\*-\,\.\/\:-\@\[-\^\`\{-\~]+$";
            //        String pattern = "^[a-zA-Z0-9 ()_-]+$";
//            ParamBO tmpParamBO = MemoryDataLoader.getParamBO("REGEX_VALIDATE_DATA", "PRODUCT_NAME");
//            logger.info("product name regex: " + tmpParamBO.getName());
//            String productNameAscii = renameVietnameseToEnglish(productName);
//            return productNameAscii.matches(tmpParamBO.getName());
            return true;
        } catch (Exception ex) {
            logger.error("Have error checkProductNameAllowVietnamese ", ex);
        }
        return false;
    }

    /**
     * Check if name is alpha,number, -, _ or space. Especially name can be
     * Vietnamese name
     *
     * @param name
     * @return
     */
    public static boolean checkNameAllowVietnamese(String name) {
        if (isNullOrEmpty(name)) {
            return false;
        }
//        String pattern = "^[^\\!-\\,\\.\\/\\:-\\@\\[-\\^\\`\\{-\\~]+$";
        return true;
//        String pattern = "^[a-zA-Z0-9 _–-]+$";
//        String vnName = renameVietnameseToEnglish(name);
//        return vnName.matches(pattern);
    }

    public static boolean checkCustomerNameAllowVienamese(String str) {
        return checkNameAllowVietnamese(str);
    }

    public static boolean checkEnterpriseNameAllowVietnamese(String name) {
        try {
            if (isNullOrEmpty(name)) {
                return false;
            }
            return true;
//            ParamBO tmpParamBO = MemoryDataLoader.getParamBO("REGEX_VALIDATE_DATA", "ENTERPRISE_NAME");
//            logger.info("ENTERPRISE name regex: " + tmpParamBO.getName());

//        String pattern = "^[a-zA-Z0-9.,\\( \\) &_-]+$";
//        String pattern = "^[a-zA-Z0-9.,() &_-]+$";
//            return renameVietnameseToEnglish(name).matches(tmpParamBO.getName());
        } catch (Exception ex) {
            logger.error("have error ", ex);
        }
        return false;
    }

    public static boolean checkBankNameAllowVietnamese(String str) {
        return checkNameAllowVietnamese(str);
    }

    public static boolean checkDiscount(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String pattern = "^[0-9\\,\\.E]+$";
        return str.matches(pattern);
    }

    public static boolean checkBankAccount(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String pattern = "^[A-Z\\d]+$";
        return str.matches(pattern);
    }

    public static boolean checkWebsite(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        String pattern = "^(https?:\\/\\/)?([\\w\\d\\-_]{2,}\\.+[A-Za-z\\-_]{2,})+\\/?(.)*";

        return str.matches(pattern);
    }

    public static boolean checkLinuxPath(String path) {
        if (isNullOrEmpty(path)) {
            return false;
        }
        String pattern = "^[A-Za-z0-9\\/\\_\\-]*$";

        return path.matches(pattern);

    }

    public static String[] split(String str, char sep) {
        Vector temp = new Vector();

        int t = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == sep) {
                String wk = str.substring(t, i);
                temp.add(wk);
                t = i + 1;
            }
        }
        if (t < str.length()) {
            String wk = str.substring(t);
            temp.add(wk);
        }
        String[] x = new String[temp.size()];
        x = (String[]) temp.toArray(x);
        return x;
    }

    public static void main(String[] args) {
//        String value = "07KPTQ0_011";
//        System.out.println(checkTemplateCode(value));
//        System.out.println(paddingIndex("2"));
//System.out.println(isHexa("abcdefABCDEF0123456789"));
    }

    public static String padding(String value, int len) {
        try {
            String ret;
            if (value.length() != len) {
                for (int i = value.length(); i < len; i++) {
                    value = "0" + value;
                }
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static String paddingLeft(String value, int len) {
        try {
            String ret;
            if (value.length() != len) {
                for (int i = value.length(); i < len; i++) {
                    value = "0" + value;
                }
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static String paddingIndex(String value) {
        try {
            String ret;
            if (value.length() != 2) {
                for (int i = value.length(); i < 2; i++) {
                    value = "0" + value;
                }
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static String paddingIndex(int in) {
        try {
            String value = in + "";
            String ret;
            if (value.length() != 2) {
                for (int i = value.length(); i < 2; i++) {
                    value = "0" + value;
                }
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static String paddingTime(int in) {
        try {
            String value = in + "";
            String ret;
            if (value.length() != 2) {
                for (int i = value.length(); i < 2; i++) {
                    value = "0" + value;
                }
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static String displayKey(String value) {
        try {
            String ret;
            if (value.length() > 20) {
                return value.substring(0, 20) + "...";
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static String displayCut(String value, int len) {
        try {
            String ret;
            if (value.length() > len) {
                return value.substring(0, len) + "...";
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public boolean checkContains(Integer[] arr, Integer value) {
        try {
            List list = Arrays.asList(arr);
            if (list.contains(value)) {
                return true;
            }
        } catch (Exception ex) {
            logger.error("Have an error: " + ex);
            return false;
        }
        return false;
    }

    /**
     *
     * checkSignInVietnamese
     *
     * @param name
     * @return
     */
    public static boolean checkSignInVietnamese(String name) {
        for (int idx = 0; idx < signInVietnamese.length; idx++) {
            if (name.contains(signInVietnamese[idx])) {
                return false;
            }
        }
        return true;
    }

    public static String getrep(BankMembershipBO bankMembershipBO, int id) {
        if (bankMembershipBO == null) {
            return "";
        }
        if (id == 1) {
            return chgNull(bankMembershipBO.getRepresent1());
        } else if (id == 2) {
            return chgNull(bankMembershipBO.getRepresent2());
        } else if (id == 3) {
            return chgNull(bankMembershipBO.getRepresent3());
        } else if (id == 4) {
            return chgNull(bankMembershipBO.getRepresent4());
        } else if (id == 5) {
            return chgNull(bankMembershipBO.getRepresent5());
        } else {
            return "";
        }
    }

    public static String getApp(BankMembershipBO bankMembershipBO, int id) {
        if (bankMembershipBO == null) {
            return "";
        }
        if (id == 1) {
            return chgNull(bankMembershipBO.getAppellation1());
        } else if (id == 2) {
            return chgNull(bankMembershipBO.getAppellation2());
        } else if (id == 3) {
            return chgNull(bankMembershipBO.getAppellation3());
        } else if (id == 4) {
            return chgNull(bankMembershipBO.getAppellation4());
        } else if (id == 5) {
            return chgNull(bankMembershipBO.getAppellation5());
        } else {
            return "";
        }
    }

    public static String getphone(BankMembershipBO bankMembershipBO, int id) {
        if (bankMembershipBO == null) {
            return "";
        }
        if (id == 1) {
            return chgNull(bankMembershipBO.getPhoneNumber1());
        } else if (id == 2) {
            return chgNull(bankMembershipBO.getPhoneNumber2());
        } else if (id == 3) {
            return chgNull(bankMembershipBO.getPhoneNumber3());
        } else if (id == 4) {
            return chgNull(bankMembershipBO.getPhoneNumber4());
        } else if (id == 5) {
            return chgNull(bankMembershipBO.getPhoneNumber5());
        } else {
            return "";
        }
    }

    public static String getEmail(BankMembershipBO bankMembershipBO, int id) {
        if (bankMembershipBO == null) {
            return "";
        }
        if (id == 1) {
            return chgNull(bankMembershipBO.getEmail1());
        } else if (id == 2) {
            return chgNull(bankMembershipBO.getEmail2());
        } else if (id == 3) {
            return chgNull(bankMembershipBO.getEmail3());
        } else if (id == 4) {
            return chgNull(bankMembershipBO.getEmail4());
        } else if (id == 5) {
            return chgNull(bankMembershipBO.getEmail5());
        } else {
            return "";
        }
    }

    public static Date getnow() {
        return new Date();
    }
}
