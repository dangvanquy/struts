///*
// *   Copyright (C) 2018 NAPAS. All rights reserved.
// *   NAPAS VCCS CERTIFICATE
// */
//package com.napas.vccsca.utils;
//
//import java.text.MessageFormat;
//import java.util.Locale;
//import java.util.ResourceBundle;
//import javax.faces.application.FacesMessage;
//import javax.faces.application.FacesMessage.Severity;
//import javax.faces.context.FacesContext;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
///**
// *
// * BundleUtil
// *
// * @author LuongNK
// * @since Aug 9, 2018
// * @version 1.0-SNAPSHOT
// */
//public class BundleUtil {
//
//    private static final Logger logger = LogManager.getLogger(BundleUtil.class);
//    private static final ResourceBundle rb = ResourceBundle.getBundle("config");
//    private static final ResourceBundle rbImport = ResourceBundle.getBundle("import");
//
//    static {
//        try {
//            logger.info("Start init languageBundel...");
//            logger.info("End init languageBundel");
//        } catch (Exception ex) {
//            logger.error("Have error: ", ex);
//        }
//
//    }
//    
//    /**
//     * getText
//     * @param key
//     * @return 
//     */
//    public static String getText(String key) {
//
//        ResourceBundle languageBundle = ResourceBundle.getBundle(getLanguageFolder(),
//                FacesContext.getCurrentInstance().getViewRoot().getLocale());
//        String result = key;
//        try {
//            result = languageBundle.getString(key);
//        } catch (Exception e) {
//            logger.warn(e);
//        }
//        return result;
//    }
//
//   
//
//    /**
//     * getString
//     *
//     * @param key
//     * @param args
//     * @return
//     */
//    public static String getString(String key, String... args) {
//        String result;
//        try {
//            ResourceBundle resourceBundle = ResourceBundle.getBundle(getLanguageFolder(),
//                    FacesContext.getCurrentInstance().getViewRoot().getLocale());
//            MessageFormat messageFormat = new MessageFormat(resourceBundle.getString(key));
//            String[] arguments = new String[args.length];
//            int i = 0;
//            for (String arg : args) {
//                try {
//                    arg = resourceBundle.getString(arg);
//                } catch (Exception e) {
//                    logger.warn(e);
//                }
//                arguments[i++] = arg;
//            }
//            result = messageFormat.format(arguments);
//        } catch (Exception ex) {
////            logger.error("Exception: ", ex);
//            logger.warn(ex);
//            result = key;
//        }
//        return result;
//    }
//
//    /**
//     * getText
//     *
//     * @param key
//     * @param languageFolder
//     * @return
//     */
//    public static String getText(String key, String languageFolder) {
//
//        ResourceBundle bundle = ResourceBundle.getBundle(languageFolder,
//                FacesContext.getCurrentInstance().getViewRoot().getLocale());
//        String result = key;
//        try {
//            result = bundle.getString(key);
//        } catch (Exception e) {
//            logger.warn("Exception ", e);            
//        }
//        return result;
//    }
//
//    /**
//     * getSortCode
//     *
//     * @return
//     */
//    public static String getSortCode() {
//        Locale locale = new Locale("en", "US");
//        ResourceBundle rb1 = ResourceBundle.getBundle("cas", locale);
//        return rb1.getString("LANGUAGE_SORT");
//    }
//
//    /**
//     * addFacesMsg
//     *
//     * @param severity
//     * @param messageId
//     * @param summaryKey
//     * @param detailKey
//     * @param moreInfo
//     */
//    public static void addFacesMsg(Severity severity, String messageId, String summaryKey, String detailKey, String moreInfo) {
//        if (DataUtil.isNullOrEmpty(moreInfo)) {
//            moreInfo = "";
//        }
//        if (!DataUtil.isNullOrEmpty(messageId)) {
//            if (!DataUtil.isNullOrEmpty(summaryKey)) {
//                if (!DataUtil.isNullOrEmpty(detailKey)) {
//                    FacesContext.getCurrentInstance().addMessage(messageId, new FacesMessage(severity, getText(summaryKey) + ": ", getText(detailKey) + ". " + moreInfo));
//                } else {
//                    FacesContext.getCurrentInstance().addMessage(messageId, new FacesMessage(severity, getText(summaryKey), "" + " " + moreInfo));
//                }
//            } else if (!DataUtil.isNullOrEmpty(detailKey)) {
//                FacesContext.getCurrentInstance().addMessage(messageId, new FacesMessage(severity, "", getText(detailKey) + ". " + moreInfo));
//            } else {
//                FacesContext.getCurrentInstance().addMessage(messageId, new FacesMessage(severity, "", "" + " " + moreInfo));
//            }
//        } else if (!DataUtil.isNullOrEmpty(summaryKey)) {
//            if (!DataUtil.isNullOrEmpty(detailKey)) {
//                FacesContext.getCurrentInstance().addMessage("", new FacesMessage(severity, getText(summaryKey) + ": ", getText(detailKey) + ". " + moreInfo));
//            } else {
//                FacesContext.getCurrentInstance().addMessage("", new FacesMessage(severity, getText(summaryKey), "" + " " + moreInfo));
//            }
//        } else if (!DataUtil.isNullOrEmpty(detailKey)) {
//            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(severity, "", getText(detailKey) + ". " + moreInfo));
//        } else {
//            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(severity, "", "" + " " + moreInfo));
//        }
//    }
//
//    /**
//     * sendSuccessMsg
//     *
//     * @param areaId
//     * @param errorCode
//     * @param description
//     */
//    public static void sendSuccessMsg(String areaId, String errorCode, String description) {
//        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_INFO, errorCode + " : ", description));
//    }
//
//    /**
//     * sendErrorMsg
//     *
//     * @param areaId
//     * @param errorCode
//     * @param description
//     */
//    public static void sendErrorMsg(String areaId, String errorCode, String description) {
//        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorCode + " : ", description));
//    }
//
//    /**
//     * sendFatalMsg
//     *
//     * @param areaId
//     * @param errorCode
//     * @param description
//     */
//    public void sendFatalMsg(String areaId, String errorCode, String description) {
//        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_FATAL, errorCode + " : ", description));
//    }
//
//    /**
//     * sendWarnMsg
//     *
//     * @param areaId
//     * @param errorCode
//     * @param description
//     */
//    public void sendWarnMsg(String areaId, String errorCode, String description) {
//        FacesContext.getCurrentInstance().addMessage(areaId, new FacesMessage(FacesMessage.SEVERITY_WARN, errorCode + " : ", description));
//    }
//
//    public static String getConfig(String config) {
//        try {
////            ResourceBundle bundle = ResourceBundle.getBundle("config", FacesContext.getCurrentInstance().getViewRoot().getLocale());
//            return rb.getString(config);
//        } catch (Exception e) {
//            logger.error("Exception ", e);
//            return config;
//        }
//    }
//
//    public static String getImport(String config) {
//        try {
//            return rbImport.getString(config);
//        } catch (Exception e) {
//            logger.error("Exception ", e);
//            return config;
//        }
//    }
//}
