/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;

/**
 *
 * MessageUtils
 *
 * @author LuongNK
 * @since Aug 10, 2018
 * @version 1.0-SNAPSHOT
 */
public class MessageUtils {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(MessageUtils.class);
    private static ResourceBundle rb;

    static {
        try {
            rb = ResourceBundle.getBundle("message");
        } catch (MissingResourceException e) {
            logger.error("MissingResourceException: ", e);
        }
    }

    public static String getMessage(String idmsg) {
        try {
            String msg = rb.getString(idmsg);
            if (StringUtils.isNullOrEmpty(msg)) {
                return idmsg;
            } else {
                return msg;
            }
        } catch (Exception e) {
            return idmsg;
        }
    }
}
