/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sun.misc.BASE64Encoder;

/**
 *
 * PasswordService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public final class PasswordService {

    protected final Log logger = LogFactory.getLog(getClass());
    private static PasswordService instance;

    /**
     * Constructors
     */
    private PasswordService() {
    }

    /**
     * @param plaintext
     * @return
     * @throws Exception
     */
    public synchronized String encrypt(String plaintext) throws Exception {
        MessageDigest md = null;
        String hash = null;
        try {
            md = MessageDigest.getInstance("SHA-1"); // step 2
            md.update(plaintext.getBytes("UTF-8")); // step 3
            byte raw[] = md.digest(); // step 4
            hash = (new BASE64Encoder()).encode(raw); // step 5
        } catch (NoSuchAlgorithmException e) {
            logger.error(e, e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e, e);
        } catch (Exception e) {
            logger.error(e, e);
        }
        return hash; // step 6
    }

    /**
     * @return
     */
    public static synchronized PasswordService getInstance() {
        if (instance == null) {
            instance = new PasswordService();
        }
        return instance;
    }

    /**
     * @return
     * @throws Exception
     */
    public synchronized String genPwd() throws Exception {
        String uuid = UUID.randomUUID().toString();
        StringTokenizer token = new StringTokenizer(uuid, "-");
        String newPwd = token.nextToken();
        return newPwd;
    }
}
