/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import org.apache.logging.log4j.LogManager;

/**
 *
 * Logger
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class Logger {

    private org.apache.logging.log4j.Logger innerLogger;
    private String header;

    private Logger() {
    }

    public static Logger getLogger(String userName, Class<?> clazz) {
        Logger logger = new Logger();
        logger.innerLogger = LogManager.getLogger(clazz);
        logger.header = "userName: " + userName + " - ";
        return logger;
    }

    public void error(String string) {
        innerLogger.error(header + string);
    }

    public void error(String string, Throwable thrwbl) {
        innerLogger.error(header + string, thrwbl);
    }

    public void error(String string, Object[] os) {
        innerLogger.error(header + string, os);
    }

    public void error(String string, Object o) {
        innerLogger.error(header + string, o);
    }

    public void info(String string) {
        innerLogger.info(header + string);
    }

    public void info(String string, Throwable thrwbl) {
        innerLogger.info(header + string, thrwbl);
    }

    public void info(String string, Object[] os) {
        innerLogger.info(header + string, os);
    }

    public void info(String string, Object o) {
        innerLogger.info(header + string, o);
    }

    public void debug(String string) {
        innerLogger.debug(header + string);
    }

    public void debug(String string, Throwable thrwbl) {
        innerLogger.debug(header + string, thrwbl);
    }

    public void debug(String string, Object[] os) {
        innerLogger.debug(header + string, os);
    }

    public void debug(String string, Object o) {
        innerLogger.debug(header + string, o);
    }

}
