/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import java.security.Key;

import java.security.MessageDigest;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * PasswordUtil
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class PasswordUtil {

    private static final Logger logger = LogManager.getLogger(PasswordUtil.class);
    public final static ResourceBundle rb = ResourceBundle.getBundle("securitykey");
    private static String securitykey = "";

    static {
        try {
            securitykey = pDecryptDefault(rb.getString("securitykey"));
        } catch (MissingResourceException e) {
            logger.error("khong tim thay securitykey", e);
        } catch (Exception ex) {
            logger.error("Error", ex);
        }
    }

    /**
     * easypass
     */
    protected final static String[] easyPass = {"123", "963", "852", "741", "789", "147", "258", "369", "abc", "456"};

    /**
     * blacklist mat khau khong duoc phep
     */
//    protected final static String[] blackList = {"qwerty", "password", "passw0rd", "abc123", "iloveyou", "napas@123", "admin@123", "123qwea@", "abc123", "abc@123",
//        "password@123", "qwerty@123", "vtt@2014", "123@123", "123123", "696969"};
    /*
     # Start of group
     (?=.*\d)	#   must contains one digit from 0-9
     (?=.*[a-z])	#   must contains one lowercase characters
     (?=.*[A-Z])	#   must contains one uppercase characters
     (?=.*[@#$%])#   must contains one special symbols in the list "@#$%"
     .		#   match anything with previous condition checking
     {8,20}	#   length at least 8 characters and maximum of 20	
     )		# End of group
     */
    private static final String PASSWORD_PATTERN
            = "((?=.*\\d)"
            + "(?=.*[a-z])"
            + "(?=.*[A-Z])"
            + "(?=.*[@#$%~!%^&()])"
            + ".{8,20})";
    private static final Pattern PASSWORD_PATTERN_OBJ = Pattern.compile(PASSWORD_PATTERN);
    private static Matcher matcher;
    public static final String DEFAULT_PASSWORD = "123456a@";
    private static final String VALID_CHARACTER = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$%~!%^&()";
    private static final String UPPER_CASE_CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER_CASE_CHAR = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMBER = "0123456789";
    private static final String SPECIAL_STRING = "@#$%~!%^&()";
    private static final String SALT_DEVIDER = "#";

    /**
     * Validate password with regular expression
     *
     * @param password password for validation
     * @return true valid password, false invalid password
     */
    public static boolean validateStrongPassword(final String password) {
//        matcher = pattern.matcher(password).m;
//        return matcher.matches();
        return PASSWORD_PATTERN_OBJ.matcher(password).matches();
    }

    /**
     * <p>
     * Encrypt password using SHA-256 algorithm
     *
     * @param password
     * @return
     */
    private static String encryptPasswordBySHA256(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            logger.error("Exception", e);
        }
        if (md == null) {
            return "";
        }
        try {
            md.update(password.getBytes("UTF-8"));
        } catch (Exception e) {
            logger.error("Exception", e);
        }
        byte raw[] = md.digest();
        return Base64.encodeBase64String(raw);

    }

    /**
     * <p>
     * Append the password with a salt</p>
     *
     * @param password The real password
     * @param saltLength The length of the salt appending with the password
     * @return
     */
    private static String encryptPassword(String password, int saltLength) {
        String salt = createSalt(saltLength);
        String input = password + salt;
        String hash = encryptPasswordBySHA256(input);
        return hash + SALT_DEVIDER + salt;
    }

    public static String encryptPassword(String password) {
        Random rand = new Random();
        int saltLength = rand.nextInt(13) + 8;// 8 <= saltLength <= 20
        return encryptPassword(password, saltLength);
    }

    /**
     * <p>
     * Check the password is true or not </p>
     *
     * @param inPassword The pass a user enters
     * @param encrytedPassword
     * @return
     */
    public static boolean checkPassword(String inPassword, String encrytedPassword) {
        if (StringUtils.isNullOrEmpty(inPassword) || StringUtils.isNullOrEmpty(encrytedPassword)) {
            return false;
        }

        int index = encrytedPassword.lastIndexOf(SALT_DEVIDER);
        if (index == -1) {
            return false;
        }

        String password = encrytedPassword.substring(0, index);
        String salt = encrytedPassword.substring(index + SALT_DEVIDER.length(), encrytedPassword.length());
        return encryptPasswordBySHA256(inPassword + salt).equals(password);

    }

    /**
     * <p>
     * Create a String salt</p>
     *
     * @param length The Length of the salt will be created
     *
     *
     * @return A salt string
     */
    private static String createSalt(int length) {
        char[] salt = new char[length];
        int validateWordSize = VALID_CHARACTER.length();
        Random rand = new Random();
        do {
            for (int i = 0; i < length; i++) {
                int pos = rand.nextInt(validateWordSize);
                salt[i] = VALID_CHARACTER.charAt(pos);
            }
        } while (String.valueOf(salt).contains(SALT_DEVIDER));

        return String.valueOf(salt);
    }

    public static String encryptPassHibernate(String pass) {
        return encrypt(pass);
    }

    public static String decryptPasswordHibernate(String enscryptPass) {
        return decrypt(enscryptPass);
    }

    public static String encrypt(String value) {
        if (value == null) {
            return value;
        }
        // SALT is your secret key
        Key key = new SecretKeySpec(securitykey.getBytes(), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return Base64.encodeBase64String(cipher.doFinal(value.getBytes()));
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public static String decrypt(String value) {

        if (value == null) {
            return value;
        }
        // SALT is your secret key
        Key key = new SecretKeySpec(securitykey.getBytes(), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            return new String(cipher.doFinal(Base64.decodeBase64(value)));
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public static String pDecryptDefault(String value) {
        if (value == null) {
            return value;
        }
        // SALT is your secret key
        Key key = new SecretKeySpec("napas".getBytes(), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            return new String(cipher.doFinal(Base64.decodeBase64(value)));
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public static void main(String[] args) throws Exception {
        //System.out.println(decryptPasswordHibernate("aaf1f59c81072642303cebe350be029506757388d4d8c751ad6885ae79b72f8e"));

        securitykey = "123456!@#$%^";
        System.out.println(encrypt("luong"));
        System.out.println(decrypt("e4f74e469bf0ad0c758f2eb7d257d359"));

        System.out.println(encryptPassword("123456a@A"));
    }
}
