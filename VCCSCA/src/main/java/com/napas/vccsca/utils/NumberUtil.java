/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import org.apache.logging.log4j.LogManager;

/**
 *
 * NumberUtil
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public final class NumberUtil {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(NumberUtil.class);

    /**
     *
     * @param d
     * @param scale
     * @return
     */
    public static Double round(Double d, int scale) {
        if (d == null) {
            logger.error("number is null");
            return 0.0;
        }
        return Math.round(d * (Math.pow(10, scale))) / Math.pow(10, scale);
    }

    /**
     * lam tron den 2 so sau dau phay
     *
     * @param d
     * @return
     */
    public static Double round(Double d) {
        if (d == null) {
            logger.error("number is null");
            return 0.0;
        }
        return Math.round(d * 100) * 1d / 100;
    }

    /**
     * <p>
     * Get max integer value by length
     * </p>
     *
     * @param length
     * @return
     */
    public static Double getMaxValueByLength(int length) {
        return Math.pow(10, length);
    }

    public static Integer toNumber(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return 0;
        }
    }

    public static Long chgLong(Object value) {
        try {
            return Long.parseLong(value + "");
        } catch (Exception e) {
            return 0L;
        }
    }

    public static int getBin(String value) {
        try {
            return Integer.parseInt(value.replaceAll("F", ""));
        } catch (Exception e) {
            throw e;
        }
    }
}
