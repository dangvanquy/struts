/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.utils;

import com.napas.vccsca.controller.AuthenticateController;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * CookieHelper
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class CookieHelper {

    private static final int MAX_EXPIRED = 12 * 3600;//7*24*60*60;        

    public static void setCookie(String name, String value, HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = null;

        Cookie[] userCookies = request.getCookies();
        if (userCookies != null && userCookies.length > 0) {
            int length = userCookies.length;
            for (int i = 0; i < length; i++) {
                if (userCookies[i].getName().equals(name)) {
                    cookie = userCookies[i];
                    break;
                }
            }
        }
        if (cookie != null) {
            cookie.setValue(value);
        } else {
            cookie = new Cookie(name, value);
            cookie.setPath(request.getContextPath());
        }
        cookie.setMaxAge(MAX_EXPIRED);
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }

    public static Cookie getCookie(String name, HttpServletRequest request) {
        Cookie cookie;

        Cookie[] userCookies = request.getCookies();
        if (userCookies != null && userCookies.length > 0) {
            int length = userCookies.length;
            for (int i = 0; i < length; i++) {
                if (userCookies[i].getName().equals(name)) {
                    cookie = userCookies[i];
                    return cookie;
                }
            }
        }
        return null;
    }

    public static void deleteCookie(String name, HttpServletRequest request, HttpServletResponse response) {
        Cookie[] userCookies = request.getCookies();
        if (userCookies != null && userCookies.length > 0) {
            int length = userCookies.length;
            for (int i = 0; i < length; i++) {
                if (userCookies[i].getName().equals(name)) {
                    Cookie cookie = userCookies[i];
                    cookie.setMaxAge(0);
                    cookie.setHttpOnly(true);
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                    break;
                }
            }
        }
    }

    public static void deleteAllCookies(HttpServletRequest request, HttpServletResponse response) {
        if (request != null && request.getCookies() != null) {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                //System.out.println("cookies name to be delete: "+cookie.getName());
                cookie.setMaxAge(0);
                cookie.setValue(null);
                cookie.setPath(request.getContextPath());
                cookie.setHttpOnly(true);
                response.addCookie(cookie);
            }
        }
    }
}
