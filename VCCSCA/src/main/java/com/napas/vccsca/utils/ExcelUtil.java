/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.napas.vccsca.utils;

import com.napas.vccsca.BO.ActionLogBO;
import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.form.ActionHistoryForm;
import com.napas.vccsca.form.CertificateForm;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author tuannh8
 */
public class ExcelUtil {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(ExcelUtil.class);
    static List<CellStyle> style;

    /**
     * writeFile
     *
     * @param file
     * @param list
     * @param indexHeader
     * @param flag
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static byte[] writeFile(byte[] file, Object list, int indexHeader, String flag) throws FileNotFoundException, IOException, Exception {

        logger.info("Begin writeFile");
        ByteArrayOutputStream bos = null;
        ByteArrayInputStream bais = null;
        try {
            bais = new ByteArrayInputStream(file);
            bos = new ByteArrayOutputStream();
            XSSFWorkbook book = new XSSFWorkbook(bais);
            XSSFSheet sheet = book.getSheetAt(0);
            //create font error
            // create font  
            XSSFFont fontError = book.createFont();
            fontError.setFontHeightInPoints((short) 10);
            fontError.setColor(IndexedColors.RED.getIndex());
            fontError.setBold(true);
            fontError.setItalic(false);

            XSSFFont fontSuccess = book.createFont();
            fontSuccess.setFontHeightInPoints((short) 10);
            fontSuccess.setColor(IndexedColors.BLUE.getIndex());
            fontSuccess.setBold(true);
            fontSuccess.setItalic(false);

            Row rowHeader = sheet.getRow(indexHeader - 2);
            for (int idx = 0; idx < 8; idx++) {
                if (!StringUtils.isNullOrEmpty(rowHeader.getCell(idx))) {
                    if ("Ngày xuất báo cáo :".equals(rowHeader.getCell(idx).getStringCellValue())) {
                        Cell cellHeader = rowHeader.createCell(idx + 1);
                        cellHeader.setCellValue(DateTimeUtils.convertDateToString(new Date(), "dd/MM/yyyy"));
                    }
                }
            }
            List<RSAKeysBO> rSAKeysBOs = null;
            if ("rootca".equals(flag)) {
                rSAKeysBOs = (List<RSAKeysBO>) list;

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
                style = new ArrayList<>();
                for (int rowIdx = indexHeader; rowIdx < rSAKeysBOs.size() + indexHeader; rowIdx++) {
                    Row row = sheet.getRow(rowIdx);
                    if (style.size() <= 0) {
                        for (int cellIdx = 0; cellIdx < 8; cellIdx++) {
                            style.add(row.getCell(cellIdx).getCellStyle());
                        }
                    }
                    RSAKeysBO sAKeysBO = rSAKeysBOs.get(rowIdx - indexHeader);

                    try {
                        if (cal == null) {
                            cal = Calendar.getInstance();
                        }
                        Date convertedExpDate = dateFormat.parse(sAKeysBO.getExpirationDate());
                        cal.setTime(convertedExpDate);
                        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                    } catch (Exception e) {
                        cal = null;
                    }

                    setCell(row, 0, ((rowIdx - indexHeader) + 1) + "");
                    setCell(row, 1, StringUtils.padding(sAKeysBO.getRsaIndex() + "", 2));
                    setCell(row, 2, cal == null ? "" : DateTimeUtils.convertDateToDDMMYYYY(cal.getTime()) + "");
                    setCell(row, 3, sAKeysBO.getRsaStatus() == 0 ? "Hoạt động" : "Ngừng hoạt động");
                    setCell(row, 4, sAKeysBO.getCreateUser());
                    setCell(row, 5, DateTimeUtils.convertDateToString(sAKeysBO.getCreateDate(), "dd/MM/yyyy"));
                    setCell(row, 6, sAKeysBO.getUpdateUser());
                    setCell(row, 7, DateTimeUtils.convertDateToString(sAKeysBO.getUpdateDate(), "dd/MM/yyyy"));
                }
            }

            List<BankMembershipBO> bankMembershipBOs = null;
            if ("banking".equals(flag)) {
                bankMembershipBOs = (List<BankMembershipBO>) list;

                style = new ArrayList<>();
                for (int rowIdx = indexHeader; rowIdx < bankMembershipBOs.size() + indexHeader; rowIdx++) {
                    Row row = sheet.getRow(rowIdx);
                    if (style.size() <= 0) {
                        for (int cellIdx = 0; cellIdx < 8; cellIdx++) {
                            style.add(row.getCell(cellIdx).getCellStyle());
                        }
                    }

                    BankMembershipBO membershipBO = bankMembershipBOs.get(rowIdx - indexHeader);
                    setCell(row, 0, ((rowIdx - indexHeader) + 1) + "");
                    setCell(row, 1, StringEscapeUtils.unescapeHtml(membershipBO.getBankFullName()) + "");
                    setCell(row, 2, membershipBO.getBin() + "");
                    setCell(row, 3, membershipBO.getStatus() == 0 ? "Hoạt động" : "Không hoạt động");
                    setCell(row, 4, membershipBO.getCreateUser());
                    setCell(row, 5, DateTimeUtils.convertDateToString(membershipBO.getCreateDate(), "dd/MM/yyyy"));
                    setCell(row, 6, membershipBO.getUpdateUser());
                    setCell(row, 7, DateTimeUtils.convertDateToString(membershipBO.getUpdateDate(), "dd/MM/yyyy"));
                }
            }

            List<RequestBO> requestBOs = null;
            if ("request".equals(flag)) {
                requestBOs = (List<RequestBO>) list;

                style = new ArrayList<>();
                for (int rowIdx = indexHeader; rowIdx < requestBOs.size() + indexHeader; rowIdx++) {
                    Row row = sheet.getRow(rowIdx);
                    if (style.size() <= 0) {
                        for (int cellIdx = 0; cellIdx < 9; cellIdx++) {
                            style.add(row.getCell(cellIdx).getCellStyle());
                        }
                    }
                    RequestBO requestBO = requestBOs.get(rowIdx - indexHeader);
                    setCell(row, 0, ((rowIdx - indexHeader) + 1) + "");
                    setCell(row, 1, requestBO.getRegisterId() + "");
                    setCell(row, 2, requestBO.getBin() + "");
                    setCell(row, 3, StringEscapeUtils.unescapeHtml(requestBO.getBankName()) + "");
                    setCell(row, 4, DateTimeUtils.convertDateToString(requestBO.getAppDate(), "dd/MM/yyyy") + "");
                    setCell(row, 5, requestBO.getCreateUser());
                    setCell(row, 6, DateTimeUtils.convertDateToString(requestBO.getCreateDate(), "dd/MM/yyyy"));
                    setCell(row, 7, requestBO.getUpdateUser());
                    setCell(row, 8, DateTimeUtils.convertDateToString(requestBO.getUpdateDate(), "dd/MM/yyyy"));
                }
            }

            List<CertificateForm> digitalCertificateBOs = null;
            if ("certificate".equals(flag)) {
                digitalCertificateBOs = (List<CertificateForm>) list;

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
                style = new ArrayList<>();
                for (int rowIdx = indexHeader; rowIdx < digitalCertificateBOs.size() + indexHeader; rowIdx++) {
                    Row row = sheet.getRow(rowIdx);
                    if (style.size() <= 0) {
                        for (int cellIdx = 0; cellIdx < 9; cellIdx++) {
                            style.add(row.getCell(cellIdx).getCellStyle());
                        }
                    }
                    CertificateForm certificateBO = digitalCertificateBOs.get(rowIdx - indexHeader);

                    Date convertedExpDate = dateFormat.parse(certificateBO.getExpDate());
                    cal.setTime(convertedExpDate);
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));

                    setCell(row, 0, ((rowIdx - indexHeader) + 1) + "");
                    setCell(row, 1, StringUtils.paddingIndex(certificateBO.getRsaIndex() + "") + "");
                    setCell(row, 2, certificateBO.getSerial());
                    setCell(row, 3, DateTimeUtils.convertDateToDDMMYYYY(cal.getTime()) + "");
                    setCell(row, 4, StringEscapeUtils.unescapeHtml(certificateBO.getBankName()));
                    setCell(row, 5, certificateBO.getRegisterId() + "");
                    if (!StringUtils.isNullOrEmpty(certificateBO.getStatus())) {
                        switch (certificateBO.getStatus()) {
                            case 0:
                                setCell(row, 6, "Đã ký/duyệt");
                                break;
                            case 1:
                                setCell(row, 6, "Thu hồi");
                                break;
                            case 2:
                                setCell(row, 6, "Hết hạn");
                                break;
                            default:
                                break;
                        }

//                        if (certificateBO.getStatus()==0) {
//                            setCell(row, 6, "Đã ký/duyệt");
//                        } else if (certificateBO.getStatus()==1) {
//                            setCell(row, 6, "Thu hồi");
//                        } else if (certificateBO.getStatus()==2) {
//                            setCell(row, 6, "Hết hạn");
//                        }
                    }
                    setCell(row, 7, certificateBO.getCreateUser());
                    setCell(row, 8, DateTimeUtils.convertDateToString(certificateBO.getCreateDate(), "dd/MM/yyyy"));
                }
            }

            List<ActionLogBO> actionLogBOs = null;
            if ("actionHistory".equals(flag)) {
                actionLogBOs = (List<ActionLogBO>) list;

                style = new ArrayList<>();
                for (int rowIdx = indexHeader; rowIdx < actionLogBOs.size() + indexHeader; rowIdx++) {
                    try {

                        Row row = sheet.getRow(rowIdx);
                        if (row == null) {
                            row = sheet.createRow(rowIdx);
                        }
                        if (style.size() <= 0) {
                            for (int cellIdx = 0; cellIdx < 7; cellIdx++) {
                                style.add(row.getCell(cellIdx).getCellStyle());
                            }
                        }
                        ActionLogBO actionLogBO = actionLogBOs.get(rowIdx - indexHeader);
                        if (StringUtils.isNullOrEmpty(actionLogBO)) {
                            continue;
                        }
                        setCell(row, 0, ((rowIdx - indexHeader) + 1) + "");
                        setCell(row, 1, actionLogBO.getUserName() + "");
                        setCell(row, 2, actionLogBO.getIp() + "");
                        setCell(row, 3, actionLogBO.getActionName() + "");
                        setCell(row, 4, actionLogBO.getFunction() + "");
                        setCell(row, 5, DateTimeUtils.convertDateToDDMMYYYYHHmmss(actionLogBO.getDateTime()));
                        setCell(row, 6, actionLogBO.getStatus() == 0 ? "Thành công" : "Thất Bại");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            DigitalCertificateBO certificateBO = null;
            if ("certificateDetail".equals(flag)) {
                certificateBO = (DigitalCertificateBO) list;

                style = new ArrayList<>();
                Row row = sheet.getRow(indexHeader);
                if (style.size() <= 0) {
                    for (int cellIdx = 0; cellIdx < 7; cellIdx++) {
                        style.add(row.getCell(cellIdx).getCellStyle());
                    }
                }
                setCell(row, 0, certificateBO.getRegisterId() + "");
                setCell(row, 1, certificateBO.getBankName() + "");
                setCell(row, 2, certificateBO.getIpk() + "");
                setCell(row, 3, certificateBO.getSerial() + "");
                setCell(row, 4, certificateBO.getBankIdentity() + "");
                setCell(row, 5, StringUtils.padding(certificateBO.getExpDate(), 7));
                if (certificateBO.getStatus() == 0) {
                    setCell(row, 6, "Đã ký/duyệt");
                } else if (certificateBO.getStatus() == 1) {
                    setCell(row, 6, "Thu hồi");
                } else if (certificateBO.getStatus() == 2) {
                    setCell(row, 6, "Hết hạn");
                }
            }

            RequestBO requestBO = null;
            if ("exportRequest".equals(flag)) {
                requestBO = (RequestBO) list;

                style = new ArrayList<>();
                Row row = sheet.getRow(indexHeader);
                if (style.size() <= 0) {
                    for (int cellIdx = 0; cellIdx < 7; cellIdx++) {
                        style.add(row.getCell(cellIdx).getCellStyle());
                    }
                }
                setCell(row, 0, requestBO.getBankName() + "");
                setCell(row, 2, requestBO.getRegisterId() + "");
                setCell(row, 4, requestBO.getCreateUser() + "");
                setCell(row, 6, requestBO.getCreateDate() + "");
            }
            book.write(bos);
            logger.info("Writing on XLSX file Finished ...");
            return bos.toByteArray();
        } catch (Exception e) {
            logger.error("Error", e);
            throw e;
        } finally {
            IOUtils.closeQuietly(bais);
            IOUtils.closeQuietly(bos);
        }
    }

    public static Cell setCell(Row row, int idx, String value) {
        Cell cell = row.createCell(idx);
        cell.setCellValue(value);
        cell.setCellStyle(style.get(idx));
        return cell;
    }

    public static void main(String[] args) throws Exception {
//        String fileTemplate = "C:\\Users\\trung\\Desktop\\tem_baocaoCA.xlsx";
//        Path path = Paths.get(fileTemplate);
//        byte[] fileImport = Files.readAllBytes(path);
//        List<Object> list = null;
//        byte[] fileContent = ExcelUtil.writeFile(fileImport, list, 9, 0);
//
////        InputStream inputStream = new ByteArrayInputStream(fileContent);
//        File targetFile = new File("C:\\Users\\trung\\Desktop\\out_test.xlsx");
//        OutputStream outStream = new FileOutputStream(targetFile);
//        outStream.write(fileContent);
//        System.out.println("OK");
    }

}

class CellRangeAddressWrapper implements Comparable<CellRangeAddressWrapper> {

    public CellRangeAddress range;

    /**
     * @param theRange the CellRangeAddress object to wrap.
     */
    public CellRangeAddressWrapper(CellRangeAddress theRange) {
        this.range = theRange;
    }

    /**
     * @param o the object to compare.
     * @return -1 the current instance is prior to the object in parameter, 0:
     * equal, 1: after...
     */
    @Override
    public int compareTo(CellRangeAddressWrapper o) {

        if (range.getFirstColumn() < o.range.getFirstColumn()
                && range.getFirstRow() < o.range.getFirstRow()) {
            return -1;
        } else if (range.getFirstColumn() == o.range.getFirstColumn()
                && range.getFirstRow() == o.range.getFirstRow()) {
            return 0;
        } else {
            return 1;
        }

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.range);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CellRangeAddressWrapper other = (CellRangeAddressWrapper) obj;
        if (!Objects.equals(this.range, other.range)) {
            return false;
        }
        return true;
    }

}
