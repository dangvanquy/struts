/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.common;

/**
 *
 * OutputCommon
 *
 * @author LuongNK
 * @since Aug 11, 2018
 * @version 1.0-SNAPSHOT
 */
public class OutputCommon {

    private String errorCode;
    private String description;
    public Object object;

    public OutputCommon() {
    }

    public OutputCommon(String errorCode, String description, Object object) {
        this.errorCode = errorCode;
        this.description = description;
        this.object = object;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

}
