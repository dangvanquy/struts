/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import java.sql.Date;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * ActionHistoryForm
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class ActionHistoryForm {

    @NotEmpty
    private Integer actionId;
    private String fullName;
    private String fromDate;
    private String toDate;
    private Integer logType;
    private String actionName;
    private String function;
    private Date dateTime;
    private String ip;
    private Integer status;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public Integer getLogType() {
        return logType;
    }

    public void setLogType(Integer logType) {
        this.logType = logType;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public ActionHistoryForm() {

    }

    public ActionHistoryForm(ActionHistoryForm form) {
        form.setFullName(this.fullName);
        form.setFromDate(this.fromDate);
        form.setToDate(this.toDate);
        form.setStatus(this.status);
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
