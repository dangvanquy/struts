/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.utils.DateTimeUtils;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * EditRequestForm
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class EditRequestForm implements Serializable {

    @NotEmpty
    private String requestId;
    private String registerId;
    private String bankIdentity;
    private String ipk;
    private String serial;
    private String month;
    private String year;
    private String appDate;
    private Integer ipkLengh;

    public EditRequestForm() {
    }

    public Integer getIpkLengh() {
        return ipkLengh;
    }

    public void setIpkLengh(Integer ipkLengh) {
        this.ipkLengh = ipkLengh;
    }

    public EditRequestForm(EditRequestForm form) {
        this.requestId = form.getRequestId();
        this.registerId = form.getRegisterId();
        this.bankIdentity = form.getBankIdentity();
        this.ipk = form.getIpk();
        this.serial = form.getSerial();
        this.month = form.getMonth();
        this.year = form.getYear();
        this.appDate = form.getAppDate();
        this.ipkLengh = form.getIpkLengh();
    }

    public RequestBO toBO(RequestBO requestBO) {
        requestBO.setBankIdentity(this.bankIdentity);
        requestBO.setIpk(this.ipk);
        if (this.ipkLengh == null || this.ipkLengh == 0) {
            requestBO.setIpkLengh(this.ipk.length() / 2);
        } else {
            requestBO.setIpkLengh(this.ipkLengh);
        }
        requestBO.setSerial(this.serial);
        requestBO.setExpDate(this.month + "/" + this.year);
        requestBO.setAppDate(DateTimeUtils.convertStringToDate(this.appDate, "dd/MM/yyyy"));
        return requestBO;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getBankIdentity() {
        return bankIdentity;
    }

    public void setBankIdentity(String bankIdentity) {
        this.bankIdentity = bankIdentity;
    }

    public String getIpk() {
        if (this.ipk.length() % 2 != 0) {
            this.ipk = "0" + this.ipk;
        }
        return ipk;
    }

    public void setIpk(String ipk) {
        if (ipk.length() % 2 != 0) {
            ipk = "0" + ipk;
        }
        this.ipk = ipk;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

}
