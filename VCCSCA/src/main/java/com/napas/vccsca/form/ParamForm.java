/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * ParamForm
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class ParamForm implements Serializable {

    @NotEmpty
    private String paramId;
    private String paramName;
    private String tableName;
    private String columnValue;

    public String getParamId() {
        return paramId;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamName() {
        if (paramName == null) {
            paramName = "";
        }
        return paramName.trim();
    }

    public void setParamName(String paramName) {
        if (paramName == null) {
            paramName = "";
        }
        this.paramName = paramName.trim();
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnValue() {
        return columnValue;
    }

    public void setColumnValue(String columnValue) {
        this.columnValue = columnValue;
    }

}
