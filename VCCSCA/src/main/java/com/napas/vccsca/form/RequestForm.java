/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * RequestForm
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class RequestForm implements Serializable {

    @NotEmpty
    private Integer registerId;
    private Integer bin;
    private String bankName;
    private String bankIdentity;
    private String ipk;
    private String serial;
    private String month;
    private String year;
    private String fromDate;
    private String toDate;
    private String createDate;
    private Date appDate;
    private String appDateSearch;
    private Integer status;
    private Integer[] arrBin;

    public RequestForm() {
    }

    public RequestForm(RequestForm form) {
        this.registerId = form.getRegisterId();
        this.bin = form.getBin();
        this.bankName = form.getBankName();
        this.bankIdentity = form.getBankIdentity();
        this.ipk = form.getIpk();
        this.serial = form.getSerial();
        this.month = form.getMonth();
        this.year = form.getYear();
        this.fromDate = form.getFromDate();
        this.toDate = form.getToDate();
        this.createDate = form.getCreateDate();
        this.appDate = form.getAppDate();
        this.status = form.getStatus();
        this.arrBin = form.getArrBin();
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getBankName() {
        if (bankName == null) {
            bankName = "";
        }
        return bankName.trim();
    }

    public void setBankName(String bankName) {
        if (bankName == null) {
            bankName = "";
        }
        this.bankName = bankName.trim();
    }

    public String getBankIdentity() {
        return bankIdentity;
    }

    public void setBankIdentity(String bankIdentity) {
        this.bankIdentity = bankIdentity;
    }

    public String getIpk() {
        return ipk;
    }

    public void setIpk(String ipk) {
        this.ipk = ipk;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAppDateSearch() {
        return appDateSearch;
    }

    public void setAppDateSearch(String appDateSearch) {
        this.appDateSearch = appDateSearch;
    }

    public Integer getBin() {
        return bin;
    }

    public void setBin(Integer bin) {
        this.bin = bin;
    }

    public Integer[] getArrBin() {
        return arrBin;
    }

    public void setArrBin(Integer[] arrBin) {
        this.arrBin = arrBin;
    }

}
