/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.ParamBO;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

/**
 *
 * ConfigWarningSystemForm
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class ConfigWarningSystemForm implements Serializable {

    private String warningId;
    private String warningName;
    private String mailTo;
    private String mailCc;
    private String mailBcc;
    private Integer type;
    private Integer sendNumber;
    private String sendTime;
    private String sendTime1;
    private String sendTime2;
    private Integer dayConfig;
    private String content;
    private String subject;
    private String param;
    private Integer status;
    private String createUser;
    private List listCreateUser;
    private String updateUser;
    private String createDate;
    private String updateDate;
    private List<ParamBO> params;

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public List<ParamBO> getParams() {
        return params;
    }

    public void setParams(List<ParamBO> params) {
        this.params = params;
    }

    public String getWarningId() {
        return warningId;
    }

    public void setWarningId(String warningId) {
        this.warningId = warningId;
    }

    public String getWarningName() {
        return warningName;
    }

    public void setWarningName(String warningName) {
        this.warningName = warningName;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getMailCc() {
        return mailCc;
    }

    public void setMailCc(String mailCc) {
        this.mailCc = mailCc;
    }

    public String getMailBcc() {
        return mailBcc;
    }

    public void setMailBcc(String mailBcc) {
        this.mailBcc = mailBcc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }

    public Integer getDayConfig() {
        return dayConfig;
    }

    public void setDayConfig(Integer dayConfig) {
        this.dayConfig = dayConfig;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public List getListCreateUser() {
        return listCreateUser;
    }

    public void setListCreateUser(List listCreateUser) {
        this.listCreateUser = listCreateUser;
    }

    public String getSendTime1() {
        return sendTime1;
    }

    public void setSendTime1(String sendTime1) {
        this.sendTime1 = sendTime1;
    }

    public String getSendTime2() {
        return sendTime2;
    }

    public void setSendTime2(String sendTime2) {
        this.sendTime2 = sendTime2;
    }

}
