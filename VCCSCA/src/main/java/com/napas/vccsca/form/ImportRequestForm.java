/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import org.springframework.web.multipart.commons.CommonsMultipartFile;
 
public class ImportRequestForm {
 
    // Upload files.
    private CommonsMultipartFile[] fileDatas;
 
    public CommonsMultipartFile[] getFileDatas() {
        return fileDatas;
    }
 
    public void setFileDatas(CommonsMultipartFile[] fileDatas) {
        this.fileDatas = fileDatas;
    }

}