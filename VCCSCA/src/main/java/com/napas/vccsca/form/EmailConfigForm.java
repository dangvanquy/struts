/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * LoginForm
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class EmailConfigForm implements Serializable {

    @NotEmpty
    private String email;
    private String mailServer;
    private Integer port;
    private String securityType;
    private String accountType;
    private String username;
    private String password;
    private String confirmPassword;
    private String description;
    private Integer status;

    public EmailConfigForm() {
    }

    public EmailConfigForm(EmailConfigForm form) {
        this.email = form.getEmail();
        this.mailServer = form.getMailServer();
        this.port = form.getPort();
        this.securityType = form.getSecurityType();
        this.accountType = form.getAccountType();
        this.username = form.getUsername();
        this.password = form.getPassword();
        this.confirmPassword = form.getConfirmPassword();
        this.description = form.getDescription();
        this.status = form.getStatus();
    }

    public ConfigEmailServerBO toBO(ConfigEmailServerBO configEmailServerBO) {
        if (StringUtils.isNullOrEmpty(configEmailServerBO)) {
            configEmailServerBO = new ConfigEmailServerBO();
        }
        configEmailServerBO.setEmail(this.email);
        configEmailServerBO.setMailServer(this.mailServer);
        configEmailServerBO.setPort(this.port);
        configEmailServerBO.setSecurityType(this.securityType);
        configEmailServerBO.setPassword(AESUtil.encryption(this.getPassword()));
        configEmailServerBO.setUsername(this.username);
        configEmailServerBO.setDescription(this.description);
        configEmailServerBO.setStatus(this.status);
        return configEmailServerBO;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMailServer() {
        return mailServer;
    }

    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}
