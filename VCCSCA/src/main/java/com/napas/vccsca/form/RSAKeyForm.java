/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.RSAKeysBO;
import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * RSAKeyForm
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class RSAKeyForm implements Serializable {

    @NotEmpty
    private Integer rsaId;
    private Integer rsaIndex;
    private String typeKey;
    private String publicKeyModulus;
    private String publicKeyExponent;
    private String privateKeyExponent1;
    private String privateKeyLength1;
    private String privateKeyExponent2;
    private String privateKeyLength2;
    private Integer rsaLength;
    private String rsaContent;
    private String checksumName;
    private String checksumContent;
    private String expirationDate;
    private String createUser;
    private String updateUser;
    private Date createDate;
    private Date updateDate;
    private String createCsUser;
    private String updateCsUser;
    private Date createCsDate;
    private Date updateCsDate;
    private Integer checksumStatus;
    private Integer rsaStatus;
    private String year;
    private String month;
    private int slotHsm;
    private String privateKeyName;

    public RSAKeyForm() {

    }

    public RSAKeyForm(RSAKeyForm form) {
        form.setRsaId(this.rsaId);
        form.setRsaIndex(this.rsaIndex);
        form.setTypeKey(this.typeKey);
        form.setPublicKeyModulus(this.publicKeyModulus);
        form.setPublicKeyExponent(this.publicKeyExponent);
        form.setRsaLength(this.rsaLength);
        form.setRsaContent(this.rsaContent);
        form.setChecksumName(this.checksumName);
        form.setChecksumContent(this.checksumContent);
        form.setExpirationDate(this.expirationDate);
        form.setCreateUser(this.createUser);
        form.setUpdateUser(this.updateUser);
        form.setCreateDate(this.createDate);
        form.setUpdateDate(this.updateDate);
        form.setCreateCsUser(this.createCsUser);
        form.setUpdateCsUser(this.updateCsUser);
        form.setCreateCsDate(this.createCsDate);
        form.setUpdateCsDate(this.updateCsDate);
        form.setChecksumStatus(this.checksumStatus);
        form.setRsaStatus(this.rsaStatus);
        form.setSlotHsm(this.slotHsm);
        form.setPrivateKeyName(this.privateKeyName);
    }

    public RSAKeysBO convertToBO() {
        RSAKeysBO keysBO = new RSAKeysBO();
        keysBO.setRsaId(this.rsaId);
        keysBO.setRsaIndex(this.rsaIndex);
        keysBO.setTypeKey(this.typeKey);
        keysBO.setPublicKeyModulus(this.publicKeyModulus);
        keysBO.setPublicKeyExponent(this.publicKeyExponent);
        keysBO.setRsaLength(this.rsaLength);
        keysBO.setRsaContent(this.rsaContent);
        keysBO.setChecksumName(this.checksumName);
        keysBO.setChecksumContent(this.checksumContent);
        keysBO.setExpirationDate(this.expirationDate);
        keysBO.setCreateUser(this.createUser);
        keysBO.setUpdateUser(this.updateUser);
        keysBO.setCreateDate(this.createDate);
        keysBO.setUpdateDate(this.updateDate);
        keysBO.setCreateCsUser(this.createCsUser);
        keysBO.setUpdateCsUser(this.updateCsUser);
        keysBO.setCreateCsDate(this.createCsDate);
        keysBO.setUpdateCsDate(this.updateCsDate);
        keysBO.setChecksumStatus(this.checksumStatus);
        keysBO.setRsaStatus(this.rsaStatus);
        keysBO.setSlotHsm(this.slotHsm);
        keysBO.setPrivateKeyName(this.privateKeyName);
        return keysBO;
    }
    public RSAKeysBO updateCheckSumToBO(RSAKeysBO keysBO) {
//        keysBO.setRsaId(this.rsaId);
//        keysBO.setRsaIndex(this.rsaIndex);
//        keysBO.setTypeKey(this.typeKey);
        keysBO.setPublicKeyModulus(this.publicKeyModulus);
        keysBO.setPublicKeyExponent(this.publicKeyExponent);
        keysBO.setRsaLength(this.rsaLength);
        keysBO.setRsaContent(this.rsaContent);
        keysBO.setChecksumName(this.checksumName);
        keysBO.setChecksumContent(this.checksumContent);
        keysBO.setExpirationDate(this.expirationDate);
//        keysBO.setCreateUser(this.createUser);
        keysBO.setUpdateUser(this.updateUser);
//        keysBO.setCreateDate(this.createDate);
        keysBO.setUpdateDate(this.updateDate);
//        keysBO.setCreateCsUser(this.createCsUser);
        keysBO.setUpdateCsUser(this.updateCsUser);
//        keysBO.setCreateCsDate(this.createCsDate);
        keysBO.setUpdateCsDate(this.updateCsDate);
        keysBO.setChecksumStatus(this.checksumStatus);
//        keysBO.setRsaStatus(this.rsaStatus);
//        keysBO.setSlotHsm(this.slotHsm);
//        keysBO.setPrivateKeyName(this.privateKeyName);
        return keysBO;
    }

    public String getPrivateKeyName() {
        return privateKeyName;
    }

    public void setPrivateKeyName(String privateKeyName) {
        this.privateKeyName = privateKeyName;
    }

    public int getSlotHsm() {
        return slotHsm;
    }

    public void setSlotHsm(int slotHsm) {
        this.slotHsm = slotHsm;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getChecksumStatus() {
        return checksumStatus;
    }

    public void setChecksumStatus(Integer checksumStatus) {
        this.checksumStatus = checksumStatus;
    }

    public Integer getRsaStatus() {
        return rsaStatus;
    }

    public void setRsaStatus(Integer rsaStatus) {
        this.rsaStatus = rsaStatus;
    }

    public Integer getRsaId() {
        return rsaId;
    }

    public void setRsaId(Integer rsaId) {
        this.rsaId = rsaId;
    }

    public Integer getRsaIndex() {
        return rsaIndex;
    }

    public void setRsaIndex(Integer rsaIndex) {
        this.rsaIndex = rsaIndex;
    }

    public String getTypeKey() {
        return typeKey;
    }

    public void setTypeKey(String typeKey) {
        this.typeKey = typeKey;
    }

    public String getPublicKeyModulus() {
        return publicKeyModulus;
    }

    public void setPublicKeyModulus(String publicKeyModulus) {
        this.publicKeyModulus = publicKeyModulus;
    }

    public String getPublicKeyExponent() {
        return publicKeyExponent;
    }

    public void setPublicKeyExponent(String publicKeyExponent) {
        this.publicKeyExponent = publicKeyExponent;
    }

    public Integer getRsaLength() {
        return rsaLength;
    }

    public void setRsaLength(Integer rsaLength) {
        this.rsaLength = rsaLength;
    }

    public String getRsaContent() {
        return rsaContent;
    }

    public void setRsaContent(String rsaContent) {
        this.rsaContent = rsaContent;
    }

    public String getChecksumName() {
        return checksumName;
    }

    public void setChecksumName(String checksumName) {
        this.checksumName = checksumName;
    }

    public String getChecksumContent() {
        return checksumContent;
    }

    public void setChecksumContent(String checksumContent) {
        this.checksumContent = checksumContent;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCreateCsUser() {
        return createCsUser;
    }

    public void setCreateCsUser(String createCsUser) {
        this.createCsUser = createCsUser;
    }

    public String getUpdateCsUser() {
        return updateCsUser;
    }

    public void setUpdateCsUser(String updateCsUser) {
        this.updateCsUser = updateCsUser;
    }

    public Date getCreateCsDate() {
        return createCsDate;
    }

    public void setCreateCsDate(Date createCsDate) {
        this.createCsDate = createCsDate;
    }

    public Date getUpdateCsDate() {
        return updateCsDate;
    }

    public void setUpdateCsDate(Date updateCsDate) {
        this.updateCsDate = updateCsDate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getPrivateKeyExponent1() {
        return privateKeyExponent1;
    }

    public void setPrivateKeyExponent1(String privateKeyExponent1) {
        this.privateKeyExponent1 = privateKeyExponent1;
    }

    public String getPrivateKeyLength1() {
        return privateKeyLength1;
    }

    public void setPrivateKeyLength1(String privateKeyLength1) {
        this.privateKeyLength1 = privateKeyLength1;
    }

    public String getPrivateKeyExponent2() {
        return privateKeyExponent2;
    }

    public void setPrivateKeyExponent2(String privateKeyExponent2) {
        this.privateKeyExponent2 = privateKeyExponent2;
    }

    public String getPrivateKeyLength2() {
        return privateKeyLength2;
    }

    public void setPrivateKeyLength2(String privateKeyLength2) {
        this.privateKeyLength2 = privateKeyLength2;
    }

}
