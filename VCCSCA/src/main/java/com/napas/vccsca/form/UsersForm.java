/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * UsersForm
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class UsersForm implements Serializable {

    @NotEmpty
    private Integer userId;
    private Integer groupId;
    private String groupName;
    private String username;
    private String password;
    private String fullName;
    private String email;
    private String address;
    private String phoneNumber;
    private String createUser;
    private String updateUser;
    private Date createDate;
    private Date updateDate;
    private Integer status;
    private String note;

    public UsersForm() {

    }

    public UsersForm(UsersForm form) {
        form.setUserId(this.userId);
        form.setGroupId(this.groupId);
        form.setUsername(this.username);
        form.setPassword(this.password);
        form.setFullName(this.fullName);
        form.setEmail(this.email);
        form.setAddress(this.address);
        form.setPhoneNumber(this.phoneNumber);
        form.setCreateUser(this.createUser);
        form.setUpdateUser(this.updateUser);
        form.setCreateDate(this.createDate);
        form.setUpdateDate(this.updateDate);
        form.setStatus(this.status);
        form.setNote(this.note);
    }

    public UsersBO convertToBO(UsersBO usersBO) {
        usersBO.setUserId(this.userId);
        usersBO.setGroupId(this.groupId);
        usersBO.setUsername(this.username);
        if (!StringUtils.isNullOrEmpty(this.password)) {
            usersBO.setPassword(this.password);
        }
        usersBO.setFullName(this.fullName);
        usersBO.setEmail(this.email);
        usersBO.setAddress(this.address);
        usersBO.setPhoneNumber(this.phoneNumber);
        usersBO.setCreateUser(this.createUser);
        usersBO.setUpdateUser(this.updateUser);
        usersBO.setCreateDate(this.createDate);
        usersBO.setUpdateDate(this.updateDate);
        usersBO.setStatus(this.status);
        usersBO.setNote(this.note);
        return usersBO;
    }
    public UsersBO convertUsersFormToBO(UsersBO usersBO) {
        usersBO.setUserId(this.userId);
        usersBO.setGroupId(this.groupId);
        usersBO.setUsername(this.username);
        if (!StringUtils.isNullOrEmpty(this.password)) {
            usersBO.setPassword(this.password);
        }
        usersBO.setFullName(this.fullName);
        usersBO.setEmail(this.email);
        usersBO.setAddress(this.address);
        usersBO.setPhoneNumber(this.phoneNumber);
        usersBO.setUpdateUser(this.updateUser);
        usersBO.setUpdateDate(this.updateDate);
        usersBO.setStatus(this.status);
        usersBO.setNote(this.note);
        return usersBO;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
