/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * LoginForm
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class GroupPermissionForm implements Serializable {

    @NotEmpty
    private String groupId;
    private String groupName;
    private int status;
    private String note;
    private String[] grPermissions;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String[] getGrPermissions() {
        return grPermissions;
    }

    public void setGrPermissions(String[] grPermissions) {
        this.grPermissions = grPermissions;
    }

    public String getPermission(String val) {
        if (StringUtils.isNullOrEmpty(val)) {
            return "";
        }
        if (StringUtils.isNullOrEmpty(grPermissions)) {
            return "";
        }
        for (String grPermission : grPermissions) {
            if (grPermission.equalsIgnoreCase(val)) {
                return "checked";
            }
        }
        return "";
    }

}
