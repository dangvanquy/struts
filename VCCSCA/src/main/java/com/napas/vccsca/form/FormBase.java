/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

/**
 *
 * FormBase
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class FormBase implements Serializable {

    //alert
    private String msTitle;
    private String msValue;
    private String msType;

    public FormBase() {
    }

    public FormBase(String msTitle, String msValue, String msType) {
        this.msTitle = msTitle;
        this.msValue = msValue;
        this.msType = msType;
    }

    public String getMsTitle() {
        return msTitle;
    }

    public void setMsTitle(String msTitle) {
        this.msTitle = msTitle;
    }

    public String getMsValue() {
        return msValue;
    }

    public void setMsValue(String msValue) {
        this.msValue = msValue;
    }

    public String getMsType() {
        return msType;
    }

    public void setMsType(String msType) {
        this.msType = msType;
    }

}
