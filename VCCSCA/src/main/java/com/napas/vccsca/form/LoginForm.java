/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * LoginForm
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class LoginForm implements Serializable {

    @NotEmpty
    private String userName;
    private String passWord;
    private String captcha;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

}
