/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import java.util.Date;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * CertificateForm
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class CertificateForm {

    @NotEmpty
    private Integer certificateId;
    private Integer rsaId;
    private Integer rsaIndex;
    private Integer registerId;
    private String certificate;
    private Integer bin;
    private String bankName;
    private String ipk;
    private String expDate;
    private String month;
    private String year;
    private String serial;
    private String bankIdentity;
    private Integer rsaLengh;
    private String evictionUser;
    private Date evictionDate;
    private String fromDate;
    private String toDate;
    private Date createDate;
    private String createUser;
    private String content;
    private Integer status;
    private Integer binSearch;
    private Integer[] arrBin;

    public CertificateForm() {

    }

    public CertificateForm(CertificateForm form) {
        form.setCertificateId(this.certificateId);
        form.setRsaId(this.rsaId);
        form.setRsaIndex(this.rsaIndex);
        form.setRegisterId(this.registerId);
        form.setCertificate(this.certificate);
        form.setBin(this.bin);
        form.setBankName(this.bankName);
        form.setIpk(this.ipk);
        form.setExpDate(this.expDate);
        form.setMonth(this.month);
        form.setYear(this.year);
        form.setSerial(this.serial);
        form.setBankIdentity(this.bankIdentity);
        form.setRsaLengh(this.rsaLengh);
        form.setEvictionUser(this.evictionUser);
        form.setEvictionDate(this.evictionDate);
        form.setEvictionUser(this.createUser);
        form.setEvictionDate(this.createDate);
        form.setFromDate(this.fromDate);
        form.setToDate(this.toDate);
        form.setStatus(this.status);
        form.setContent(this.content);
    }

    public Integer[] getArrBin() {
        return arrBin;
    }

    public void setArrBin(Integer[] arrBin) {
        this.arrBin = arrBin;
    }

    public Integer getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Integer certificateId) {
        this.certificateId = certificateId;
    }

    public Integer getRsaId() {
        return rsaId;
    }

    public void setRsaId(Integer rsaId) {
        this.rsaId = rsaId;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public Integer getBin() {
        return bin;
    }

    public void setBin(Integer bin) {
        this.bin = bin;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIpk() {
        return ipk;
    }

    public void setIpk(String ipk) {
        this.ipk = ipk;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getBankIdentity() {
        return bankIdentity;
    }

    public void setBankIdentity(String bankIdentity) {
        this.bankIdentity = bankIdentity;
    }

    public Integer getRsaLengh() {
        return rsaLengh;
    }

    public void setRsaLengh(Integer rsaLengh) {
        this.rsaLengh = rsaLengh;
    }

    public String getEvictionUser() {
        return evictionUser;
    }

    public void setEvictionUser(String evictionUser) {
        this.evictionUser = evictionUser;
    }

    public Date getEvictionDate() {
        return evictionDate;
    }

    public void setEvictionDate(Date evictionDate) {
        this.evictionDate = evictionDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getRsaIndex() {
        return rsaIndex;
    }

    public void setRsaIndex(Integer rsaIndex) {
        this.rsaIndex = rsaIndex;
    }

    public Integer getBinSearch() {
        return binSearch;
    }

    public void setBinSearch(Integer binSearch) {
        this.binSearch = binSearch;
    }

    @Override
    public String toString() {
        return "DigitalCertificateBO{" + "certificateId=" + certificateId + ", rsaId=" + rsaId + ", registerId=" + registerId + ", certificate=" + certificate + ", bin=" + bin + ", bankName=" + bankName + ", ipk=" + ipk + ", expDate=" + expDate + ", serial=" + serial + ", bankIdentity=" + bankIdentity + ", rsaLengh=" + rsaLengh + ", evictionUser=" + evictionUser + ", evictionDate=" + evictionDate + ", createUser=" + createUser + ", createDate=" + createDate + ", fromDate=" + fromDate + ", toDate=" + toDate + ", content=" + content + ", status=" + status + '}';
    }

}
