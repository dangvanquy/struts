/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.utils.DateTimeUtils;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * CreateRequestForm
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class CreateRequestForm implements Serializable {

    @NotEmpty
    private Integer registerId;
    private String bankName;
    private String bankIdentity;
    private String ipk;
    private String serial;
    private String month;
    private String year;
    private Integer ipkLengh;
    private String appDate;
    private Integer status;

    public CreateRequestForm() {
    }

    public CreateRequestForm(CreateRequestForm form) {
        this.bankIdentity = form.getBankIdentity();
        this.ipk = form.getIpk();
        this.serial = form.getSerial();
        this.month = form.getMonth();
        this.year = form.getYear();
        this.ipkLengh = form.getIpkLengh();
        this.appDate = form.getAppDate();
        this.status = form.getStatus();
    }

    public RequestBO createBo(RequestBO requestBO) {
        requestBO.setRegisterId(this.registerId);
        requestBO.setBankIdentity(this.bankIdentity);
        requestBO.setIpk(this.ipk);
        requestBO.setSerial(this.serial);
        if (this.ipkLengh == null || this.ipkLengh == 0) {
            requestBO.setIpkLengh(this.ipk.length() / 2);
        } else {
            requestBO.setIpkLengh(this.ipkLengh);
        }
        requestBO.setExpDate(this.month + "/" + this.year);
        requestBO.setAppDate(DateTimeUtils.convertStringToDate(this.appDate, "dd/MM/yyyy"));
        requestBO.setStatus(0); //Soạn thảo
        return requestBO;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIdentity() {
        return bankIdentity;
    }

    public void setBankIdentity(String bankIdentity) {
        this.bankIdentity = bankIdentity;
    }

    public String getIpk() {
        if (this.ipk.length() % 2 != 0) {
            this.ipk = "0" + this.ipk;
        }
        return ipk;
    }

    public void setIpk(String ipk) {
        if (ipk.length() % 2 != 0) {
            ipk = "0" + ipk;
        }
        this.ipk = ipk;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getIpkLengh() {
        return ipkLengh;
    }

    public void setIpkLengh(Integer ipkLengh) {
        this.ipkLengh = ipkLengh;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
