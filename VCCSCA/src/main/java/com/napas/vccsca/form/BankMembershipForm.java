/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.form;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;

/**
 *
 * BankMembershipForm
 *
 * @author CuongTV
 * @since Aug 23, 2018
 * @version 1.0-SNAPSHOT
 */
@Named
@RequestScoped
public class BankMembershipForm implements Serializable {

    @NotEmpty
    private String bankId;
    private Integer bin;
    private String bankFullName;
    private String bankShortName;
    private String appellation1;
    private String represent1;
    private String email1;
    private String phoneNumber1;
    private String appellation2;
    private String represent2;
    private String email2;
    private String phoneNumber2;
    private String appellation3;
    private String represent3;
    private String email3;
    private String phoneNumber3;
    private String appellation4;
    private String represent4;
    private String email4;
    private String phoneNumber4;
    private String appellation5;
    private String represent5;
    private String email5;
    private String phoneNumber5;
    private Integer status;
    private Integer[] arrBin;

    public BankMembershipForm() {

    }

    public BankMembershipForm(BankMembershipForm form) {
        form.setBankId(this.bankId);
        form.setBin(this.bin);
        form.setBankFullName(this.bankFullName);
        form.setBankShortName(this.bankShortName);
        form.setAppellation1(this.appellation1);
        form.setRepresent1(this.represent1);
        form.setEmail1(this.email1);
        form.setPhoneNumber1(this.phoneNumber1);
        form.setAppellation2(this.appellation2);
        form.setRepresent2(this.represent2);
        form.setEmail2(this.email2);
        form.setPhoneNumber2(this.phoneNumber2);
        form.setAppellation3(this.appellation3);
        form.setRepresent3(this.represent3);
        form.setEmail3(this.email3);
        form.setPhoneNumber3(this.phoneNumber3);
        form.setAppellation4(this.appellation4);
        form.setRepresent4(this.represent4);
        form.setEmail4(this.email4);
        form.setPhoneNumber4(this.phoneNumber4);
        form.setAppellation5(this.appellation5);
        form.setRepresent5(this.represent5);
        form.setEmail5(this.email5);
        form.setPhoneNumber5(this.phoneNumber5);
        form.setStatus(this.status);
    }

    public BankMembershipBO createBO() {
        BankMembershipBO bankMembershipBO = new BankMembershipBO();
        if (StringUtils.isNumber(this.bankId)) {
            bankMembershipBO.setBankId(NumberUtil.toNumber(this.bankId));
        } else {
            bankMembershipBO.setBankId(NumberUtil.toNumber(AESUtil.decryption(this.bankId)));
        }
        bankMembershipBO.setBin(this.bin);
        bankMembershipBO.setBankFullName(this.bankFullName);
        bankMembershipBO.setBankShortName(this.bankShortName);
        bankMembershipBO.setAppellation1(this.appellation1);
        bankMembershipBO.setRepresent1(this.represent1);
        bankMembershipBO.setEmail1(this.email1);
        bankMembershipBO.setPhoneNumber1(this.phoneNumber1);
        bankMembershipBO.setAppellation2(this.appellation2);
        bankMembershipBO.setRepresent2(this.represent2);
        bankMembershipBO.setEmail2(this.email2);
        bankMembershipBO.setPhoneNumber2(this.phoneNumber2);
        bankMembershipBO.setAppellation3(this.appellation3);
        bankMembershipBO.setRepresent3(this.represent3);
        bankMembershipBO.setEmail3(this.email3);
        bankMembershipBO.setPhoneNumber3(this.phoneNumber3);
        bankMembershipBO.setAppellation4(this.appellation4);
        bankMembershipBO.setRepresent4(this.represent4);
        bankMembershipBO.setEmail4(this.email4);
        bankMembershipBO.setPhoneNumber4(this.phoneNumber4);
        bankMembershipBO.setAppellation5(this.appellation5);
        bankMembershipBO.setRepresent5(this.represent5);
        bankMembershipBO.setEmail5(this.email5);
        bankMembershipBO.setPhoneNumber5(this.phoneNumber5);
        bankMembershipBO.setStatus(this.status);
        return bankMembershipBO;
    }

    public BankMembershipBO convertToBO(BankMembershipBO bankMembershipBO) {
        if (StringUtils.isNumber(this.bankId)) {
            bankMembershipBO.setBankId(NumberUtil.toNumber(this.bankId));
        } else {
            bankMembershipBO.setBankId(NumberUtil.toNumber(AESUtil.decryption(this.bankId)));
        }
        bankMembershipBO.setBin(this.bin);
        bankMembershipBO.setBankFullName(this.bankFullName);
        bankMembershipBO.setBankShortName(this.bankShortName);
        bankMembershipBO.setAppellation1(this.appellation1);
        bankMembershipBO.setRepresent1(this.represent1);
        bankMembershipBO.setEmail1(this.email1);

        bankMembershipBO.setPhoneNumber1(this.phoneNumber1);
        bankMembershipBO.setAppellation2(this.appellation2);
        bankMembershipBO.setRepresent2(this.represent2);
        bankMembershipBO.setEmail2(this.email2);
        bankMembershipBO.setPhoneNumber2(this.phoneNumber2);
        bankMembershipBO.setAppellation3(this.appellation3);
        bankMembershipBO.setRepresent3(this.represent3);
        bankMembershipBO.setEmail3(this.email3);
        bankMembershipBO.setPhoneNumber3(this.phoneNumber3);
        bankMembershipBO.setAppellation4(this.appellation4);
        bankMembershipBO.setRepresent4(this.represent4);
        bankMembershipBO.setEmail4(this.email4);
        bankMembershipBO.setPhoneNumber4(this.phoneNumber4);
        bankMembershipBO.setAppellation5(this.appellation5);
        bankMembershipBO.setRepresent5(this.represent5);
        bankMembershipBO.setEmail5(this.email5);
        bankMembershipBO.setPhoneNumber5(this.phoneNumber5);

        bankMembershipBO.setStatus(this.status);
        return bankMembershipBO;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public Integer getBin() {
        return bin;
    }

    public void setBin(Integer bin) {
        this.bin = bin;
    }

    public String getBankFullName() {
        return bankFullName;
    }

    public void setBankFullName(String bankFullName) {
        this.bankFullName = bankFullName;
    }

    public String getBankShortName() {
        return bankShortName;
    }

    public void setBankShortName(String bankShortName) {
        this.bankShortName = bankShortName;
    }

    public String getAppellation1() {
        return appellation1;
    }

    public void setAppellation1(String appellation1) {
        this.appellation1 = appellation1;
    }

    public String getRepresent1() {
        return represent1;
    }

    public void setRepresent1(String represent1) {
        this.represent1 = represent1;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getAppellation2() {
        return appellation2;
    }

    public void setAppellation2(String appellation2) {
        this.appellation2 = appellation2;
    }

    public String getRepresent2() {
        return represent2;
    }

    public void setRepresent2(String represent2) {
        this.represent2 = represent2;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getAppellation3() {
        return appellation3;
    }

    public void setAppellation3(String appellation3) {
        this.appellation3 = appellation3;
    }

    public String getRepresent3() {
        return represent3;
    }

    public void setRepresent3(String represent3) {
        this.represent3 = represent3;
    }

    public String getEmail3() {
        return email3;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    public String getPhoneNumber3() {
        return phoneNumber3;
    }

    public void setPhoneNumber3(String phoneNumber3) {
        this.phoneNumber3 = phoneNumber3;
    }

    public String getAppellation4() {
        return appellation4;
    }

    public void setAppellation4(String appellation4) {
        this.appellation4 = appellation4;
    }

    public String getRepresent4() {
        return represent4;
    }

    public void setRepresent4(String represent4) {
        this.represent4 = represent4;
    }

    public String getEmail4() {
        return email4;
    }

    public void setEmail4(String email4) {
        this.email4 = email4;
    }

    public String getPhoneNumber4() {
        return phoneNumber4;
    }

    public void setPhoneNumber4(String phoneNumber4) {
        this.phoneNumber4 = phoneNumber4;
    }

    public String getAppellation5() {
        return appellation5;
    }

    public void setAppellation5(String appellation5) {
        this.appellation5 = appellation5;
    }

    public String getRepresent5() {
        return represent5;
    }

    public void setRepresent5(String represent5) {
        this.represent5 = represent5;
    }

    public String getEmail5() {
        return email5;
    }

    public void setEmail5(String email5) {
        this.email5 = email5;
    }

    public String getPhoneNumber5() {
        return phoneNumber5;
    }

    public void setPhoneNumber5(String phoneNumber5) {
        this.phoneNumber5 = phoneNumber5;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer[] getArrBin() {
        return arrBin;
    }

    public void setArrBin(Integer[] arrBin) {
        this.arrBin = arrBin;
    }

}
