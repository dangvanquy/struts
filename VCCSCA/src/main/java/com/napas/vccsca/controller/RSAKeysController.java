/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import ISO9796_2.EMVcert;
import ISO9796_2.VCCS_CA_FileFormat;
import SafenetPL.PCIPL_Adap;
import com.napas.vccsca.BO.ConfigHsmBO;
import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.HSM.GenKeyPair;
import com.napas.vccsca.form.RSAKeyForm;
import com.napas.vccsca.service.ChecksumService;
import com.napas.vccsca.service.ConfigHsmService;
import com.napas.vccsca.service.RSAKeyService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * RSAKeysController
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class RSAKeysController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(RSAKeysController.class);
    private static final String RSA_MANAGEMENT_PAGE_REDIRECT = "quan-ly-rsa.html";
    private static final String CREATE_RSA_PAGE_REDIRECT = "tao-moi-rsa.html";
    private static final String EDIT_RSA_PAGE_REDIRECT = "chinh-sua-rsa.html";
    private static final String RSA_DETAIL_PAGE_REDIRECT = "xem-chi-tiet-rsa.html";
    private static final String IMPORT_RSA_PAGE_REDIRECT = "import-rsa.html";

    private static final String CREATE_RSA_PAGE_ACTION = "createKey.do";
    private static final String EDIT_RSA_PAGE_ACTION = "editKey.do";
    private static final String LOCK_RSA_PAGE_ACTION = "lockRsaKey.do";
    private static final String UNLOCK_RSA_PAGE_ACTION = "unlockRsaKey.do";
    private static final String IMPORT_RSA_PAGE_ACTION = "importKey.do";
    private static final String EXPORT_RSA_PAGE_ACTION = "exportKey.do";

    @RequestMapping(value = CREATE_RSA_PAGE_ACTION, method = RequestMethod.POST)
    public void createRSAKey(@ModelAttribute("createKeyForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            if (!checkExpDate(keyForm, request)) {
                response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

            ConfigHsmService hsmService = new ConfigHsmService();
            ConfigHsmBO configHsmBO = hsmService.getConfigHsmBySlot(keyForm.getSlotHsm());

            if (StringUtils.isNullOrEmpty(configHsmBO) || StringUtils.isNullOrEmpty(configHsmBO.getSlot())
                    || StringUtils.isNullOrEmpty(configHsmBO.getSlot())) {
                sendMessageError(request, "approve.request.fail.slot");
                putActionErrorLog(request, "Tạo mới khóa RSA");
                response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                return;
            }
            if (!StringUtils.isNumber(keyForm.getRsaLength() + "") || keyForm.getRsaLength() < 512) {
                sendMessageError(request, "rsa.len.error");
                putActionErrorLog(request, "Tạo mới khóa RSA");
                response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                return;
            }
            if (keyForm.getRsaLength().intValue() > 16384) {
                sendMessageError(request, "rsa.max.len");
                putActionErrorLog(request, "Tạo mới khóa RSA");
                response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

            GenKeyPair genKeyPair = new GenKeyPair();

            String rsaPriName = "Pri" + keyForm.getRsaLength() + "" + DateTimeUtils.convertDateToYYYYMMDDHHmmss(new Date());
            KeyPair keyPair = genKeyPair.createRSASUN(keyForm.getRsaLength());

            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

            //import PrivateKey to HSM from Modulus&PrivateExponent
            String cAPK_modulus = publicKey.getModulus().toString(16);//"B5A2D7130183614F9461E7D120D5FE7EEE627524EB972FDA552484B9FC11AA96F36FB0080F28AB375209E33C4215B897766BD3EF16CDF03AA9496AEBF5E1FEF61446BE47E30B671880B04E66487EBC74E84B87F0FC026257A97E069DB54666368D9D8355BF37545ED8620C44BA6C7CADB7262A8C6A34394ABAAF60C43684669B7862221967CFF8B5C384C9E71B6D02FB5BC315CC36BEE673A3596F20CC7E49CE3899AC42577515B847E425BCF691CF8164141B2ADC06F0CDF9DFB1483728A8BD2F033FF442957E5D97BE731CC2B8C64B5AD3804BE3D5BE47BC3C355908DB8F4D0FA2ACFFE9E703777CB5D45F92AB87D9D9ED1A27FD2E00DB";
            String cAPriK_exponent = privateKey.getPrivateExponent().toString(16);//"79173A0CABACEB8A62EBEFE0C08EA9A9F441A36DF264CA918E18587BFD611C64A24A755AB4C5C77A36B142282C0E7B0FA447E29F64894AD1C630F1F2A3EBFF4EB82F298542079A1055CADEEEDAFF284DF0325AA0A80196E51BA959BE78D99979B3BE578E7F7A383F3AEC082DD19DA873CF6EC70846CD7B87271F95D703288588770AE234665D1BA7CBC657D74DC05CAE6A1C37B96029B188C7F0A09DF80BE7A59D9E9C9408929BBFCC930BBA6BF6F147F0A9D4714B68FC91117407074E55FA8985E0C10D7560B7406925EFF3FF03559AC34357BCA62F07784578B751C7292E1E3657F5E9B4EA89C008F2A71B759393F27093AB63306AF23B";
            PCIPL_Adap.importPrivateKey(configHsmBO.getSlot(), AESUtil.decryption(configHsmBO.getPassword()), rsaPriName, cAPK_modulus, cAPriK_exponent, null);

            RSAKeysBO rsaKeyBO = keyForm.convertToBO();

            rsaKeyBO.setPrivateKeyName(rsaPriName);
            rsaKeyBO.setPublicKeyExponent(publicKey.getPublicExponent().toString(16));// hex
            rsaKeyBO.setPublicKeyModulus(publicKey.getModulus().toString(16));
            rsaKeyBO.setPublicKeyModulusByte(publicKey.getEncoded());
            rsaKeyBO.setChecksumName("null");
            rsaKeyBO.setTypeKey("HSM");
            rsaKeyBO.setExpirationDate(StringUtils.padding(keyForm.getMonth(), 2) + "/" + keyForm.getYear());
            rsaKeyBO.setCreateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            rsaKeyBO.setCreateDate(new Date());

            keyService.addKeys(rsaKeyBO);
            sendMessageSuccess(request, "rsa.key.create.success");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tạo mới khóa RSA");
        }
        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = EDIT_RSA_PAGE_ACTION, method = RequestMethod.POST)
    public void editRSAKey(@ModelAttribute("editKeyForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            boolean chkUpdate = false;
            RSAKeysBO rsaKeyBO = keyService.getKeyById(keyForm.getRsaId());
            HashMap hashMap = new HashMap();
            hashMap.put("rsaIndex", rsaKeyBO.getRsaIndex());
            hashMap.put("rsaStatus", 0);

            List<RSAKeysBO> keysBOs = keyService.getKeys(hashMap);

            for (RSAKeysBO keysBO : keysBOs) {
                if (keysBO.getRsaId().equals(keyForm.getRsaId())) {
                    chkUpdate = true;
                }
            }
            if (StringUtils.isNullOrEmpty(keysBOs) || keysBOs.size() <= 0) {
                rsaKeyBO.setRsaContent(keyForm.getRsaContent());
                rsaKeyBO.setChecksumContent(keyForm.getChecksumContent());
                rsaKeyBO.setRsaStatus(keyForm.getRsaStatus());
                rsaKeyBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                rsaKeyBO.setUpdateDate(new Date());
                rsaKeyBO.setSlotHsm(keyForm.getSlotHsm());

                keyService.updateKeys(rsaKeyBO);
                putActionLog(request, "Chỉnh sửa khóa RSA, id:" + keyForm.getRsaId());
                sendMessageSuccess(request, "rsa.key.edit.success");
            } else if (keyForm.getRsaStatus() == 0 && !chkUpdate) {
                sendMessageError(request, "rsa.exist.key.is.active");
                putActionErrorLog(request, "Chỉnh sửa khóa RSA, id:" + keyForm.getRsaId());
            } else {
                rsaKeyBO.setRsaContent(keyForm.getRsaContent());
                rsaKeyBO.setChecksumContent(keyForm.getChecksumContent());
                rsaKeyBO.setRsaStatus(keyForm.getRsaStatus());
                rsaKeyBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                rsaKeyBO.setUpdateDate(new Date());
                rsaKeyBO.setSlotHsm(keyForm.getSlotHsm());

                keyService.updateKeys(rsaKeyBO);
                putActionLog(request, "Chỉnh sửa khóa RSA, id:" + keyForm.getRsaId());
                sendMessageSuccess(request, "rsa.key.edit.success");
            }
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Chỉnh sửa khóa RSA, id:" + keyForm.getRsaId());
        }
        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = LOCK_RSA_PAGE_ACTION, method = RequestMethod.POST)
    public void lockRsaKey(@RequestParam("rsaId") String rsaId,
            @RequestParam("rsaIndex") String rsaIndex, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            RSAKeysBO keysBO = keyService.getKeyById(NumberUtil.toNumber(AESUtil.decryption(rsaId)));
            keysBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            keysBO.setUpdateDate(new Date());
            keysBO.setRsaStatus(1);
            if (!StringUtils.isNullOrEmpty(keysBO.getChecksumStatus()) && keysBO.getChecksumStatus().intValue() == 0) {
                keysBO.setChecksumStatus(1);
            }

            keyService.updateKeys(keysBO);
            sendMessageSuccess(request, "rsa.key.deactive.success");
            putActionLog(request, "Lock khóa RSA, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Lock khóa RSA, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        }
        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = UNLOCK_RSA_PAGE_ACTION, method = RequestMethod.POST)
    public void unlockRsaKey(@RequestParam("rsaId") String rsaId,
            @RequestParam("rsaIndex") String rsaIndex, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            HashMap hashMap = new HashMap();
            hashMap.put("rsaIndex", NumberUtil.toNumber(AESUtil.decryption(rsaIndex)));
            hashMap.put("rsaStatus", 0);

            List<RSAKeysBO> keysBOs = keyService.getKeys(hashMap);

            if (StringUtils.isNullOrEmpty(keysBOs) || keysBOs.size() <= 0) {
                RSAKeysBO keysBO = keyService.getKeyById(NumberUtil.toNumber(AESUtil.decryption(rsaId)));
                keysBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                keysBO.setUpdateDate(new Date());
                keysBO.setRsaStatus(0);
                if (!StringUtils.isNullOrEmpty(keysBO.getChecksumStatus()) && keysBO.getChecksumStatus().intValue() == 1) {
                    keysBO.setChecksumStatus(0);
                }

                keyService.updateKeys(keysBO);
                sendMessageSuccess(request, "rsa.key.active.success");
                putActionLog(request, "UnLock khóa RSA, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
            } else {
                sendMessageError(request, "rsa.exist.key.is.active");
                putActionErrorLog(request, "UnLock khóa RSA, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
            }

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "UnLock khóa RSA, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        }
        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = RSA_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.GET)
    public String loadRSAKeyManagement(@ModelAttribute("keysForm") RSAKeyForm keyForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(searchMap.get("rsaIndex"))) {
                keyForm.setRsaIndex(NumberUtil.toNumber(searchMap.get("rsaIndex").toString()));
            }
            if (!StringUtils.isNullOrEmpty(searchMap.get("rsaStatus"))) {
                keyForm.setRsaStatus(NumberUtil.toNumber(searchMap.get("rsaStatus").toString()));
            } else {
                keyForm.setRsaStatus(0);
                searchMap.put("rsaStatus", 0);
            }

            searchMap.put("oderdesc", "createDate");
            List<RSAKeysBO> keysList = keyService.getKeys(searchMap);
            List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
            List<String> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                    allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }

            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }
            model.addAttribute("keysForm", keyForm);
            model.addAttribute("keysList", keysList);
            model.addAttribute("allKeysList", allKeysList);
            putActionLog(request, "Truy vấn dữ liệu khóa RSA");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu khóa RSA");
        }
        return RSA_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = RSA_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchRSAKey(@ModelAttribute("keysForm") RSAKeyForm keyForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();

            searchMap.put("rsaIndex", keyForm.getRsaIndex());
            searchMap.put("rsaStatus", keyForm.getRsaStatus());
            List<RSAKeysBO> keysList = keyService.getKeys(searchMap);

            List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
            List<String> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                    allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }

            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }

            model.addAttribute("keysForm", searchMap);
            model.addAttribute("keysList", keysList);
            model.addAttribute("allKeysList", allKeysList);
            putActionLog(request, "Tim kiếm dữ liệu khóa RSA");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tim kiếm dữ liệu khóa RSA");
        }
        return RSA_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = CREATE_RSA_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preCreateRsaKey(@ModelAttribute("createChecksumForm") RSAKeyForm keyForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            ConfigHsmService hsmService = new ConfigHsmService();

            List<RSAKeysBO> keysList = keyService.getAllKeys();
            List<String> indexLst = new ArrayList();
            int idx = 0;
            while (idx <= 99) {
                indexLst.add(idx, StringUtils.paddingIndex(idx + ""));
                idx = idx + 1;
            }
            for (RSAKeysBO keysBO : keysList) {
                indexLst.remove(("0".equals(StringUtils.chgNull(keysBO.getRsaStatus() + ""))
                        || (!StringUtils.isNullOrEmpty(keysBO.getChecksumStatus()) && keysBO.getChecksumStatus().intValue() == 0)) ? StringUtils.paddingIndex(keysBO.getRsaIndex() + "") : "");
            }

            List<ConfigHsmBO> hsmBOs = hsmService.findConfigHsm();

            model.addAttribute("indexLst", indexLst);
            model.addAttribute("slotLst", hsmBOs);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return CREATE_RSA_PAGE_REDIRECT;
    }

    @RequestMapping(value = EDIT_RSA_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preEditRSAKey(@RequestParam("rsaId") String rsaId, Model model,
            HttpServletRequest request) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            ConfigHsmService hsmService = new ConfigHsmService();

            model.addAttribute("keysBO", keyService.getKeyById(NumberUtil.toNumber(AESUtil.decryption(rsaId))));
            model.addAttribute("slotLst", hsmService.findConfigHsm());
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return EDIT_RSA_PAGE_REDIRECT;
    }

    @RequestMapping(value = RSA_DETAIL_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preViewRSAKeyDetail(@RequestParam("rsaId") String rsaId, Model model,
            HttpServletRequest request) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            ConfigHsmService hsmService = new ConfigHsmService();

            model.addAttribute("keysBO", keyService.getKeyById(NumberUtil.toNumber(AESUtil.decryption(rsaId))));
            model.addAttribute("slotLst", hsmService.findConfigHsm());
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return RSA_DETAIL_PAGE_REDIRECT;
    }

    @RequestMapping(value = IMPORT_RSA_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preImportRSAKey(@ModelAttribute("importRsaForm") RSAKeyForm keyForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            ConfigHsmService hsmService = new ConfigHsmService();

            HashMap hashMap = new HashMap();
            hashMap.put("checksumStatus", "0");
            List<RSAKeysBO> tmpAllKeysLst = checksumService.getChecksum(hashMap);
            List<RSAKeysBO> listKeyBo = new ArrayList();

            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (StringUtils.isNullOrEmpty(keysBO.getRsaStatus()) && keysBO.getChecksumStatus().intValue() == 0) {
                    listKeyBo.add(keysBO);
                }
            }
            model.addAttribute("slotLst", hsmService.findConfigHsm());
            model.addAttribute("listKeyBo", listKeyBo);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return IMPORT_RSA_PAGE_REDIRECT;
    }

    @RequestMapping(value = IMPORT_RSA_PAGE_REDIRECT, method = RequestMethod.POST)
    public String loadDataRsaKey(@RequestParam("index") String rsaIndex, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();

            HashMap hashMap = new HashMap();
            hashMap.put("checksumStatus", "0");
            List<RSAKeysBO> tmpAllKeysLst = checksumService.getChecksum(hashMap);
            List<String> indexLst = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                    indexLst.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }

            model.addAttribute("indexLst", indexLst);

            HashMap searchMap = new HashMap();
            searchMap.put("rsaIndex", rsaIndex);
            searchMap.put("checksumStatus", "0");
            RSAKeysBO keysBO = checksumService.getChecksum(hashMap).get(0);
            model.addAttribute("rsaKeyForm", keysBO);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return IMPORT_RSA_PAGE_REDIRECT;
    }

    @RequestMapping(value = IMPORT_RSA_PAGE_ACTION, method = RequestMethod.POST)
    public void importRsaKey(@ModelAttribute("importRsaForm") RSAKeyForm keyForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("Start importRsaKey ");
            RSAKeyService keyService = new RSAKeyService();
            ChecksumService checksumService = new ChecksumService();
            RSAKeysBO rsaKeyBO = checksumService.getChecksumByIdIndex(keyForm.getRsaId(), keyForm.getRsaIndex());
            ConfigHsmService hsmService = new ConfigHsmService();

            String privateKeyExponent = keyForm.getPrivateKeyExponent1() + keyForm.getPrivateKeyExponent2();
            if ((privateKeyExponent.length() % 2) != 0) {
                if (!StringUtils.isNullOrEmpty(keyForm.getPrivateKeyExponent1())) {
                    if (keyForm.getPrivateKeyExponent1().length() % 2 != 0) {
                        sendMessageError(request, "approve.request.fail.slot1");
                        putActionErrorLog(request, "Import khóa RSA");
                        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                        return;
                    }
                }
                if (!StringUtils.isNullOrEmpty(keyForm.getPrivateKeyExponent2())) {
                    if (keyForm.getPrivateKeyExponent2().length() % 2 != 0) {
                        sendMessageError(request, "approve.request.fail.slot2");
                        putActionErrorLog(request, "Import khóa RSA");
                        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                        return;
                    }
                }
            }

            if (sha256(hexStringToByteArray(keyForm.getPrivateKeyExponent1() + keyForm.getPrivateKeyExponent2())).equals(rsaKeyBO.getChecksumName())) {

                ConfigHsmBO configHsmBO = hsmService.getConfigHsmBySlot(keyForm.getSlotHsm());
                if (StringUtils.isNullOrEmpty(configHsmBO) || StringUtils.isNullOrEmpty(configHsmBO.getSlot())
                        || StringUtils.isNullOrEmpty(configHsmBO.getSlot())) {
                    sendMessageError(request, "approve.request.fail.slot");
                    putActionErrorLog(request, "Import khóa RSA");
                    response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                    return;
                }

                if (!StringUtils.isHexa(keyForm.getPrivateKeyExponent1())) {
                    sendMessageError(request, "approve.request.fail.slot1.hex");
                    putActionErrorLog(request, "Import khóa RSA");
                    response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                    return;
                }
                if (!StringUtils.isHexa(keyForm.getPrivateKeyExponent2())) {
                    sendMessageError(request, "approve.request.fail.slot2");
                    putActionErrorLog(request, "Import khóa RSA");
                    response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                    return;
                }

                privateKeyExponent = keyForm.getPrivateKeyExponent1() + keyForm.getPrivateKeyExponent2();
                String rsaPriName = "Pri" + rsaKeyBO.getRsaLength() + "" + DateTimeUtils.convertDateToYYYYMMDDHHmmss(new Date());

                //import PrivateKey to HSM from Modulus&PrivateExponent
                String CAPK_modulus = rsaKeyBO.getPublicKeyModulus();//"B5A2D7130183614F9461E7D120D5FE7EEE627524EB972FDA552484B9FC11AA96F36FB0080F28AB375209E33C4215B897766BD3EF16CDF03AA9496AEBF5E1FEF61446BE47E30B671880B04E66487EBC74E84B87F0FC026257A97E069DB54666368D9D8355BF37545ED8620C44BA6C7CADB7262A8C6A34394ABAAF60C43684669B7862221967CFF8B5C384C9E71B6D02FB5BC315CC36BEE673A3596F20CC7E49CE3899AC42577515B847E425BCF691CF8164141B2ADC06F0CDF9DFB1483728A8BD2F033FF442957E5D97BE731CC2B8C64B5AD3804BE3D5BE47BC3C355908DB8F4D0FA2ACFFE9E703777CB5D45F92AB87D9D9ED1A27FD2E00DB";
                String CAPriK_exponent = privateKeyExponent;//"79173A0CABACEB8A62EBEFE0C08EA9A9F441A36DF264CA918E18587BFD611C64A24A755AB4C5C77A36B142282C0E7B0FA447E29F64894AD1C630F1F2A3EBFF4EB82F298542079A1055CADEEEDAFF284DF0325AA0A80196E51BA959BE78D99979B3BE578E7F7A383F3AEC082DD19DA873CF6EC70846CD7B87271F95D703288588770AE234665D1BA7CBC657D74DC05CAE6A1C37B96029B188C7F0A09DF80BE7A59D9E9C9408929BBFCC930BBA6BF6F147F0A9D4714B68FC91117407074E55FA8985E0C10D7560B7406925EFF3FF03559AC34357BCA62F07784578B751C7292E1E3657F5E9B4EA89C008F2A71B759393F27093AB63306AF23B";
                PCIPL_Adap.importPrivateKey(configHsmBO.getSlot(), AESUtil.decryption(configHsmBO.getPassword()), rsaPriName, CAPK_modulus, CAPriK_exponent, null);

                rsaKeyBO.setPrivateKeyName(rsaPriName);
                rsaKeyBO.setRsaStatus(0);
                rsaKeyBO.setTypeKey("Import");
                rsaKeyBO.setCreateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                rsaKeyBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                rsaKeyBO.setCreateDate(new Date());
                rsaKeyBO.setUpdateDate(new Date());
                keyService.updateKeys(rsaKeyBO);

                sendMessageSuccess(request, "rsa.key.import.success");
            } else {
                sendMessageError(request, "rsa.key.import.checksum.error");
            }

            putActionLog(request, "Import khóa RSA");
        } catch (Exception ex) {
            logger.error("Have an error: ", "error");
            sendMessageError(request, "rsa.key.import.checksum.error");
            putActionErrorLog(request, "Import khóa RSA");
        }
        response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = EXPORT_RSA_PAGE_ACTION, method = RequestMethod.POST)
    public void exportRSAKey(@RequestParam("idKeyRsaEx") String idKeyRsaEx, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            //import rsa key
            RSAKeyService keyService = new RSAKeyService();
            ConfigHsmService hsmService = new ConfigHsmService();

            RSAKeysBO rsaKeyBO = keyService.getKeyById(NumberUtil.toNumber(AESUtil.decryption(idKeyRsaEx)));
            ConfigHsmBO configHsmBO = hsmService.getConfigHsmBySlot(rsaKeyBO.getSlotHsm());

            if (StringUtils.isNullOrEmpty(configHsmBO) || StringUtils.isNullOrEmpty(configHsmBO.getSlot())
                    || StringUtils.isNullOrEmpty(configHsmBO.getSlot())) {
                sendMessageError(request, "approve.request.fail.slot");
                putActionErrorLog(request, "Export khóa RSA");
                response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

//            logger.info("RsaIndex : " + rsaKeyBO.getRsaIndex());
//            logger.info("getExpirationDate : " + rsaKeyBO.getExpirationDate());
//            logger.info("Slot : " + configHsmBO.getSlot());
//            logger.info("Password : " + configHsmBO.getPassword());
//            logger.info("PrivateKeyName : " + rsaKeyBO.getPrivateKeyName());
            String[] ex = rsaKeyBO.getExpirationDate().split("/");
            byte[] expirationDate = hexStringToByteArray(ex[0] + ex[1].substring(2, 4));
            byte[] rsaIndexArr = hexStringToByteArray(StringUtils.paddingLeft(rsaKeyBO.getRsaIndex() + "", 2));

            VCCS_CA_FileFormat format = new VCCS_CA_FileFormat();
            format.setCAPKindex(rsaIndexArr[0]);//byte) 0x99

            if ("010001".equals(StringUtils.paddingLeft(rsaKeyBO.getPublicKeyExponent(), 6))) {
                format.setCAPKexponent(new byte[]{(byte) 0x01, 00, (byte) 0x01});
            } else {
                format.setCAPKexponent(new byte[]{(byte) 0x03});
            }
            format.setCAPKExpirationDate(expirationDate);//new byte[]{(byte) 0x12, (byte) 0x26}
            byte[] CAFile = new EMVcert().genVCCS_CAPKFile(configHsmBO.getSlot(), AESUtil.decryption(configHsmBO.getPassword()), rsaKeyBO.getPrivateKeyName(), "", format);

            String fileName = "10100000.N" + StringUtils.paddingIndex(rsaKeyBO.getRsaIndex() + "");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);
            response.setContentLength(CAFile.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(CAFile));
            FileCopyUtils.copy(inputStream, response.getOutputStream());

            putActionLog(request, "Export khóa RSA");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Export khóa RSA");
            response.sendRedirect(RSA_MANAGEMENT_PAGE_REDIRECT);
        }
    }

    /**
     * checkExpDate
     *
     * @param keyForm
     * @param request
     * @return
     */
    private boolean checkExpDate(RSAKeyForm keyForm, HttpServletRequest request) throws Exception {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
        String expDate = keyForm.getMonth() + "/" + keyForm.getYear();
        Date convertedDate = dateFormat.parse(expDate);
        cal.setTime(convertedDate);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(DateTimeUtils.convertToEndDate(new Date())) < 0) {
            sendMessageError(request, "exp.date.validate.after.sysdate");
            return false;
        }
        return true;
    }
}
