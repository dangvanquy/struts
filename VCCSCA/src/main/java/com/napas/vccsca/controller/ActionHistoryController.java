/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.ActionLogBO;
import com.napas.vccsca.form.ActionHistoryForm;
import com.napas.vccsca.service.ActionHistoryService;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.ExcelUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.util.FileCopyUtils;

/**
 *
 * ReportCertificateController
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ActionHistoryController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ActionHistoryController.class);
    private static final String ACTION_HISTORY_PAGE_REDIRECT = "lich-su-truy-cap.html";
    private static final String EXPORT_ACTION_HISTORY = "exportHistory.do";
    private HashMap searchMap = null;

    @RequestMapping(value = ACTION_HISTORY_PAGE_REDIRECT, method = RequestMethod.GET)
    public String loadActionHistoryManagement(@ModelAttribute("actionHistoryForm") ActionHistoryForm actionHistoryForm, Model model,
            Integer offset, Integer maxResults, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ActionHistoryService actionHistoryService = new ActionHistoryService();

            if (StringUtils.isNullOrEmpty(offset)) {
                searchMap = new HashMap();
            } else {
                if (!StringUtils.isNullOrEmpty(searchMap.get("fullName"))) {
                    actionHistoryForm.setFullName(String.valueOf(searchMap.get("fullName")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("fromDate"))) {
                    actionHistoryForm.setFromDate(String.valueOf(searchMap.get("fromDate")).substring(0, 10));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("toDate"))) {
                    actionHistoryForm.setToDate(String.valueOf(searchMap.get("toDate")).substring(0, 10));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("status"))) {
                    actionHistoryForm.setStatus(NumberUtil.toNumber(String.valueOf(searchMap.get("status"))));
                }
            }

            List<ActionLogBO> actionsList = actionHistoryService.getActionHistory(searchMap, offset, maxResults);

            model.addAttribute("actionHistoryForm", actionHistoryForm);
            model.addAttribute("actionsList", actionsList);
            model.addAttribute("offset", offset != null ? offset : 0);
            model.addAttribute("count", actionHistoryService.getCount(searchMap));
            putActionLog(request, "Truy vấn dữ liệu lịch sử truy cập");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu lịch sử truy cập");
        }

        return ACTION_HISTORY_PAGE_REDIRECT;
    }

    @RequestMapping(value = ACTION_HISTORY_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchActionHistory(@ModelAttribute("actionHistoryForm") ActionHistoryForm actionHistoryForm, Model model,
            Integer offset, Integer maxResults, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {

            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getFromDate())
                    && DateTimeUtils.convertStringToTime(actionHistoryForm.getFromDate(), "dd/MM/yyyy") == null) {
                sendMessageError(request, "date.validate.unformatted");
                return ACTION_HISTORY_PAGE_REDIRECT;
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getToDate())
                    && DateTimeUtils.convertStringToTime(actionHistoryForm.getToDate(), "dd/MM/yyyy") == null) {
                sendMessageError(request, "date.validate.unformatted");
                return ACTION_HISTORY_PAGE_REDIRECT;
            }

            ActionHistoryService actionHistoryService = new ActionHistoryService();
            searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getFullName().trim())) {
                searchMap.put("fullName", actionHistoryForm.getFullName().trim());
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getFromDate())) {
                searchMap.put("fromDate", actionHistoryForm.getFromDate());
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getToDate())) {
                searchMap.put("toDate", actionHistoryForm.getToDate());
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getStatus())) {
                searchMap.put("status", actionHistoryForm.getStatus());
            }
            if (!validateSearchForm(actionHistoryForm, request)) {
                return ACTION_HISTORY_PAGE_REDIRECT;
            }

            List<ActionLogBO> actionsList = actionHistoryService.getActionHistory(searchMap, offset, maxResults);

//            model.addAttribute("actionHistoryForm", searchMap);
            model.addAttribute("actionsList", actionsList);
            model.addAttribute("offset", offset != null ? offset : 0);
            model.addAttribute("count", actionHistoryService.getCount(searchMap));
            putActionLog(request, "Tìm kiếm dữ liệu lịch sử truy cập");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu lịch sử truy cập");
        }
        return ACTION_HISTORY_PAGE_REDIRECT;
    }

    @RequestMapping(value = EXPORT_ACTION_HISTORY, method = RequestMethod.POST)
    public String exportActionHistory(@ModelAttribute("actionHistoryForm") ActionHistoryForm actionHistoryForm, Model model,
            Integer offset, Integer maxResults, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ActionHistoryService actionHistoryService = new ActionHistoryService();
            HashMap searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getFullName().trim())) {
                searchMap.put("fullName", actionHistoryForm.getFullName().trim());
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getFromDate())) {
                searchMap.put("fromDate", actionHistoryForm.getFromDate());
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getToDate())) {
                searchMap.put("toDate", actionHistoryForm.getToDate());
            }
            if (!StringUtils.isNullOrEmpty(actionHistoryForm.getStatus())) {
                searchMap.put("status", actionHistoryForm.getStatus());
            }
            if (!validateSearchForm(actionHistoryForm, request)) {
                return ACTION_HISTORY_PAGE_REDIRECT;
            }
            int count = actionHistoryService.getCount(searchMap).intValue();
            List<ActionLogBO> actionsList = actionHistoryService.getActionHistory(searchMap, 0, count);
            List<ActionLogBO> actionsList2 = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                if (i >= count) {
                    break;
                }
                actionsList2.add(actionsList.get(i));
            }

//            model.addAttribute("actionHistoryForm", searchMap);
            model.addAttribute("actionsList", actionsList2);
            model.addAttribute("offset", offset != null ? offset : 0);
            model.addAttribute("count", count);

            Path path = Paths.get(tem_act_hit_export);
            byte[] fileContent = ExcelUtil.writeFile(Files.readAllBytes(path), actionsList, 8, "actionHistory");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "lichsutruycap.xlsx");
            response.setContentLength(fileContent.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());

            putActionLog(request, "Export lịch sử truy cập");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Export lịch sử truy cập");
        }
        return ACTION_HISTORY_PAGE_REDIRECT;
    }

    /**
     * Validate all input on search form
     *
     * @param actionHistoryForm
     * @param request
     * @return false/true
     */
    private boolean validateSearchForm(ActionHistoryForm actionHistoryForm, HttpServletRequest request) {
        Date fromDate = DateTimeUtils.convertStringToDate(actionHistoryForm.getFromDate(), "dd/MM/yyyy");
        Date toDate = DateTimeUtils.convertStringToDate(actionHistoryForm.getToDate(), "dd/MM/yyyy");

        Date currentDate = Calendar.getInstance().getTime();
        if (!StringUtils.isNullOrEmpty(fromDate)) {
            if (fromDate.after(currentDate)) {
                sendMessageError(request, "report.from.date.over.current.date");
                return false;
            }
        }
        if (!StringUtils.isNullOrEmpty(toDate)) {
            if (toDate.after(currentDate)) {
                sendMessageError(request, "report.to.date.over.current.date");
                return false;
            }
        }
        if (!StringUtils.isNullOrEmpty(fromDate) && !StringUtils.isNullOrEmpty(toDate)) {
            if (fromDate.after(toDate)) {
                sendMessageError(request, "report.from.date.over.to.date");
                return false;
            }
        }
        return true;
    }
}
