/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.PermissionRightBO;
import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.common.OutputCommon;
import com.napas.vccsca.form.LoginForm;
import com.napas.vccsca.service.PermissionRightService;
import com.napas.vccsca.constants.Constants;
import com.napas.vccsca.utils.CookieHelper;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.napas.vccsca.service.UsersService;
import com.napas.vccsca.utils.AESUtil;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * AuthenticateController
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class AuthenticateController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(AuthenticateController.class);
    private static final String CHANGE_PASS_PAGE_REDIRECT = "doi-mat-khau.html";
    private static final String LOGIN_PAGE_REDIRECT = "dang-nhap.html";
    private static final String HOME_PAGE_REDIRECT = "trang-chu.html";

    private LoginForm loginForm;
    private static ResourceBundle rb;
    private static String[] allowedUrls;
    private static String[] allowTemplate;
    private UsersService userService;

    private static String defaultURL;
    private static String loginURL;
    private static String recoverPasswordUrl;
    private static String homeURL;
    private static String page404URL;

    static {
        try {
            rb = ResourceBundle.getBundle("cas");
            allowedUrls = rb.getString("AllowUrl").split(",");
            allowTemplate = rb.getString("AllowTemplate").split(",");

            defaultURL = rb.getString("defaultUrl");
            loginURL = rb.getString("loginUrl");
            recoverPasswordUrl = rb.getString("recoverPasswordUrl");
            page404URL = rb.getString("page404URL");
            homeURL = rb.getString("homeUrl");
        } catch (MissingResourceException e) {
            logger.error("MissingResourceException: ", e);
        }
    }

    @PostConstruct
    public void init() {
        loginForm = new LoginForm();
        logger.info("authenticate init method");
    }

    @GetMapping("/*")
    public String init2(Model model, HttpServletRequest request) {
        String linkFull = request.getRequestURI();
        linkFull = linkFull.replace(defaultURL, "");
        if ("".equals(linkFull)) {
            linkFull = defaultURL;
        }
        if (alowURL(linkFull, allowedUrls) || alowTempalteURL(linkFull, allowTemplate)) {
            return request.getServletPath().replaceAll("/", "");
        } else {
            putActionErrorLog(request, "Truy cập trái phép");
            return defaultURL + page404URL;
        }
    }

    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    public void loginForm(@Valid @ModelAttribute("LoginForm") LoginForm loginForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (result.hasErrors()) {
            sendMessageError(request, "error");
            putActionErrorLog(request, "Đăng nhập hệ thống");
            response.sendRedirect(LOGIN_PAGE_REDIRECT);
            return;
        }
        logger.info("start login");
        if (loginForm.getUserName().trim().isEmpty()) {
            sendMessageError(request, "login.user.notinput");
            putActionErrorLog(request, "Đăng nhập hệ thống");
            response.sendRedirect(LOGIN_PAGE_REDIRECT);
            return;
        } else if (loginForm.getPassWord().trim().isEmpty()) {
            sendMessageError(request, "login.pass.notinput");
            putActionErrorLog(request, "Đăng nhập hệ thống");
            response.sendRedirect(LOGIN_PAGE_REDIRECT);
            return;
        }

        try {
            javax.servlet.http.HttpSession httpSession;
            httpSession = request.getSession();
            String ip = getClientIP(request);

            userService = new UsersService();
            OutputCommon outputCommon = userService.getUser(loginForm.getUserName().trim(), AESUtil.encryptionPassword(loginForm.getPassWord().trim()));
            if (StringUtils.isNullOrEmpty(outputCommon.getErrorCode())) {
                UsersBO usersBO = (UsersBO) outputCommon.getObject();

                List<PermissionRightBO> permissionRightBOs = new PermissionRightService().getMenuPermission(usersBO.getGroupId());
                if (StringUtils.isNullOrEmpty(permissionRightBOs)) {
//                    sendMessageError(request, "logout.notpermission");
//                    putActionErrorLog(request, "Đăng nhập hệ thống");
//                    response.sendRedirect(LOGIN_PAGE_REDIRECT);
//                    return;
                }else {
                    for (PermissionRightBO permissionRightBO : permissionRightBOs) {
                        httpSession.setAttribute(Constants.PERMISSION.search(permissionRightBO.getPermissionCode().toString()), "true");
                    }
                }

                String tokenLogin = AESUtil.encryption("napasAES_" + usersBO.getUsername() + "_" + usersBO.getUserId() + "_KEY");
                httpSession.setAttribute("tokenLogin", tokenLogin);
                httpSession.setAttribute("userName", (usersBO.getUsername()));
                httpSession.setAttribute("fullName", (usersBO.getFullName()));

                Cookie jSessionId = CookieHelper.getCookie("JSESSIONID", request);
                if (jSessionId != null) {
                    jSessionId.setValue(httpSession.getId());
                    CookieHelper.setCookie("JSESSIONID", httpSession.getId(), request, response);
                    httpSession.setAttribute("jSessionId-ip", jSessionId.getValue() + ip);
                }
                CookieHelper.setCookie("tokenLogin", tokenLogin, request, response);
                CookieHelper.setCookie("userName", (usersBO.getUsername()), request, response);

                logger.info("end login");
                putActionLog(request, "Đăng nhập hệ thống");
                response.sendRedirect(HOME_PAGE_REDIRECT);
            } else {
                sendMessageError(request, outputCommon.getErrorCode());
                putActionErrorLog(request, "Đăng nhập hệ thống");
                response.sendRedirect(LOGIN_PAGE_REDIRECT);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            sendMessageError(request, e.getMessage());
            putActionErrorLog(request, "Đăng nhập hệ thống");
            response.sendRedirect(LOGIN_PAGE_REDIRECT);
        }
    }

    @RequestMapping(value = "logout.do", method = RequestMethod.POST)
    public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("AuthenticateController logout!");
        try {
            putActionLog(request, "Đăng xuất hệ thống");

            request.getSession().removeAttribute("tokenLogin");
            request.getSession().removeAttribute("userName");
            CookieHelper.deleteAllCookies(request, response);
            sendMessageSuccess(request, "logout.Sussess");

            response.sendRedirect(LOGIN_PAGE_REDIRECT);
        } catch (Exception e) {
            logger.error(e.getMessage());
            sendMessageError(request, e.getMessage());
            response.sendRedirect(LOGIN_PAGE_REDIRECT);
        }

    }

    private Boolean alowURL(String url, String[] listAlowUrl) {
        if (defaultURL.equals(url) || loginURL.equals(url) || recoverPasswordUrl.equals(url) || page404URL.equals(url) || homeURL.equals(url)) {
            return true;
        }
        for (String str : listAlowUrl) {
            if (!StringUtils.isNullOrEmpty(str) && str.equalsIgnoreCase(url)) {
                return true;
            }
        }
        return false;
    }

    private Boolean alowTempalteURL(String url, String[] listPrefixAlowUrl) {
        if (listPrefixAlowUrl != null && listPrefixAlowUrl.length > 0) {
            for (String str : listPrefixAlowUrl) {
                if (str.startsWith("*.") && url.endsWith(str.substring(1))) {
                    return true;
                }
            }
        }
        return false;
    }
}
