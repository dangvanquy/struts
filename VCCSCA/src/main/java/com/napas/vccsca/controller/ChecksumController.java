/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.form.RSAKeyForm;
import com.napas.vccsca.service.ChecksumService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * ChecksumController
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ChecksumController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ChecksumController.class);
    private static final String CHECKSUM_MANAGEMENT_PAGE_REDIRECT = "quan-ly-checksum.html";
    private static final String CREATE_CHECKSUM_PAGE_REDIRECT = "tao-moi-checksum.html";
    private static final String EDIT_CHECKSUM_PAGE_REDIRECT = "chinh-sua-checksum.html";
    private static final String CHECKSUM_DETAIL_PAGE_REDIRECT = "xem-chi-tiet-checksum.html";

    /**
     * createChecksum
     *
     * @param keyForm
     * @param model
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "createChecksum.do", method = RequestMethod.POST)
    public void createChecksum(@ModelAttribute("createCheckSumForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            if (!validateChecksum(request, keyForm)) {
                putActionErrorLog(request, "Tạo mới Checksum");
                response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

            RSAKeysBO keysBO = keyForm.convertToBO();

            keysBO.setRsaLength(keyForm.getPublicKeyModulus().length() * 4);
            keysBO.setExpirationDate(keyForm.getMonth() + "/" + keyForm.getYear());
            keysBO.setCreateCsUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            keysBO.setCreateCsDate(new Date());
//            keysBO.setUpdateCsUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
//            keysBO.setUpdateCsDate(new Date());
//            keysBO.setPublicKeyModulusByte(pub.getEncoded());

            checksumService.addChecksum(keysBO);
            sendMessageSuccess(request, "checksum.create.success");
            putActionLog(request, "Tạo mới Checksum");
            response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tạo mới Checksum");
        }
    }

    /**
     * editChecksum
     *
     * @param keyForm
     * @param model
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "editChecksum.do", method = RequestMethod.POST)
    public void editChecksum(@ModelAttribute("editChecksumForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            if (!validateChecksum(request, keyForm)) {
                putActionErrorLog(request, "Chỉnh sửa checksum ");
                response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
                return;
            }
            HashMap hashMap = new HashMap();
            boolean chkUpdate = false;
            hashMap.put("checksumStatus", 1);
            hashMap.put("rsaIndex", keyForm.getRsaIndex());
            List<RSAKeysBO> keysBOs = checksumService.getChecksum(hashMap);
            for (RSAKeysBO keysBO : keysBOs) {
                if (keysBO.getRsaIndex() == keyForm.getRsaIndex()) {
                    chkUpdate = true;
                }
            }
            if (StringUtils.isNullOrEmpty(keysBOs) || keysBOs.size() <= 0) {
                RSAKeysBO keysBO = checksumService.getChecksumById(keyForm.getRsaId());
                if (StringUtils.isNullOrEmpty(keysBO)) {
                    sendMessageError(request, "error");
                    putActionErrorLog(request, "Chỉnh sửa checksum ");
                    response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
                    return;
                }

                keysBO = keyForm.updateCheckSumToBO(keysBO);
                keysBO.setRsaLength(keyForm.getPublicKeyModulus().length() * 4);
                keysBO.setExpirationDate(keyForm.getMonth() + "/" + keyForm.getYear());
                keysBO.setUpdateCsUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                keysBO.setUpdateCsDate(new Date());

                checksumService.updateChecksum(keysBO);
                sendMessageSuccess(request, "checksum.edit.success");
                putActionLog(request, "Chỉnh sửa checksum ");
            } else {
                if (keyForm.getChecksumStatus() == 0 && !chkUpdate) {
                    sendMessageError(request, "rsa.exist.checksum.is.active");
                    putActionErrorLog(request, "Chỉnh sửa checksum ");
                } else {
                    RSAKeysBO keysBO = checksumService.getChecksumById(keyForm.getRsaId());
                    if (StringUtils.isNullOrEmpty(keysBO)) {
                        sendMessageError(request, "error");
                        putActionErrorLog(request, "Chỉnh sửa checksum ");
                        response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
                        return;
                    }
                    keysBO = keyForm.updateCheckSumToBO(keysBO);
                    keysBO.setExpirationDate(keyForm.getMonth() + "/" + keyForm.getYear());
                    keysBO.setUpdateCsUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                    keysBO.setUpdateCsDate(new Date());

                    checksumService.updateChecksum(keysBO);
                    sendMessageSuccess(request, "checksum.edit.success");
                    putActionLog(request, "Chỉnh sửa checksum ");
                }
            }

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Chỉnh sửa checksum ");
        }
        response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
    }

    /**
     * lockChecksum
     *
     * @param rsaId
     * @param model
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "lockChecksum.do", method = RequestMethod.POST)
    public void lockChecksum(@RequestParam("rsaId") String rsaId, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            RSAKeysBO keysBO = checksumService.getChecksumById(NumberUtil.toNumber(AESUtil.decryption(rsaId)));

            keysBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            keysBO.setUpdateDate(new Date());
            keysBO.setChecksumStatus(1);
            if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus()) && keysBO.getRsaStatus().intValue() == 0) {
                keysBO.setRsaStatus(1);
            }

            checksumService.updateChecksum(keysBO);
            sendMessageSuccess(request, "rsa.key.deactive.success");
            putActionLog(request, "Dừng hoạt động checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Dừng hoạt động checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        }
        response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
    }

    /**
     * unlockChecksum
     *
     * @param rsaId
     * @param rsaIndex
     * @param model
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "unlockChecksum.do", method = RequestMethod.POST)
    public void unlockChecksum(@RequestParam("rsaId") String rsaId, @RequestParam("rsaIndex") String rsaIndex, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            HashMap hashMap = new HashMap();
            hashMap.put("rsaIndex", NumberUtil.toNumber(AESUtil.decryption(rsaIndex)));
            hashMap.put("checksumStatus", 0);
            hashMap.put("rsaStatusor", 0);

//            List<RSAKeysBO> keysBOs = checksumService.getChecksum(hashMap);
            List<RSAKeysBO> keysBOs = checksumService.getCheckExChecksum(hashMap);

            if (StringUtils.isNullOrEmpty(keysBOs) || keysBOs.size() <= 0) {
                RSAKeysBO keysBO = checksumService.getChecksumById(NumberUtil.toNumber(AESUtil.decryption(rsaId)));
                keysBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                keysBO.setUpdateDate(new Date());
                keysBO.setChecksumStatus(0);

                if (StringUtils.isNumber(keysBO.getRsaStatus() + "") && keysBO.getRsaStatus() == 1) {
                    keysBO.setRsaStatus(0);
                }

                checksumService.updateChecksum(keysBO);
                sendMessageSuccess(request, "rsa.key.active.success");
                putActionLog(request, "Mở khóa checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
            } else {
                putActionErrorLog(request, "Mở khóa checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
                sendMessageError(request, "rsa.exist.key.is.active");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Mở khóa checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        }
        response.sendRedirect(CHECKSUM_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = CHECKSUM_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.GET)
    public String loadChecksumManagement(@ModelAttribute("checksumForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            HashMap searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(searchMap)) {
                if (!StringUtils.isNullOrEmpty(searchMap.get("rsaIndex"))) {
                    keyForm.setRsaIndex(NumberUtil.toNumber(searchMap.get("rsaIndex").toString()));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("checksumStatus"))) {
                    keyForm.setChecksumStatus(NumberUtil.toNumber(searchMap.get("checksumStatus").toString()));
                } else {
                    searchMap.put("checksumStatus", 0);
                    keyForm.setChecksumStatus(0);
                }
            }

            List<RSAKeysBO> tmpAllKeysLst = checksumService.getAllChecksum();
            List<String> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getChecksumStatus())) {
                    allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }

            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }
            model.addAttribute("checksumForm", keyForm);
            model.addAttribute("keysList", checksumService.getChecksum(searchMap));
            model.addAttribute("allKeysList", allKeysList);
            putActionLog(request, "Truy vấn dữ liệu Checksum");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu Checksum");
        }
        return CHECKSUM_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = CHECKSUM_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchChecksum(@ModelAttribute("checksumForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            HashMap searchMap = new HashMap();
            searchMap.put("rsaIndex", keyForm.getRsaIndex());
            searchMap.put("checksumStatus", keyForm.getChecksumStatus());

            List<RSAKeysBO> tmpAllKeysLst = checksumService.getAllChecksum();
            List<String> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getChecksumStatus())) {
                    allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }
            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }
            model.addAttribute("checksumForm", keyForm);
            model.addAttribute("keysList", checksumService.getChecksum(searchMap));
            model.addAttribute("allKeysList", allKeysList);
//            if (StringUtils.isNullOrEmpty(searchMap.get("rsaIndex"))
//                    && StringUtils.isNullOrEmpty(searchMap.get("checksumStatus"))) {
//                sendMessageError(request, "common.require.select.info.search");
//            } else {
//                sendMessageSuccess(request, "common.search.success");
//            }
            putActionLog(request, "Tìm kiếm dữ liệu checksum");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu checksum");
        }
        return CHECKSUM_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = EDIT_CHECKSUM_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preEditChecksum(@RequestParam("rsaId") String rsaId, Model model, HttpServletRequest request) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            RSAKeysBO keysBO = checksumService.getChecksumById(NumberUtil.toNumber(AESUtil.decryption(rsaId)));
            RSAKeyForm editChecksumForm = new RSAKeyForm();
            String[] arr = keysBO.getExpirationDate().split("/");
            editChecksumForm.setMonth(arr[0]);
            editChecksumForm.setYear(arr[1]);
            model.addAttribute("keysBO", keysBO);
            model.addAttribute("editChecksumForm", editChecksumForm);

            putActionLog(request, "Chỉnh sửa dữ liệu checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Chỉnh sửa dữ liệu checksum, id:" + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        }
        return EDIT_CHECKSUM_PAGE_REDIRECT;
    }

    @RequestMapping(value = CHECKSUM_DETAIL_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preViewChecksumDetail(@RequestParam("rsaId") String rsaId, Model model, HttpServletRequest request) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            RSAKeysBO keysBO = checksumService.getChecksumById(NumberUtil.toNumber(AESUtil.decryption(rsaId)));
            model.addAttribute("keysBO", keysBO);
            putActionLog(request, "Xem thông tin chi tiết checksum, id: " + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Xem thông tin chi tiết checksum, id: " + NumberUtil.toNumber(AESUtil.decryption(rsaId)));
        }
        return CHECKSUM_DETAIL_PAGE_REDIRECT;
    }

    @RequestMapping(value = CREATE_CHECKSUM_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preCreateChecksum(@ModelAttribute("createChecksumForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ChecksumService checksumService = new ChecksumService();
            List<RSAKeysBO> keysList = checksumService.getAllChecksum();
            List<String> indexLst = new ArrayList();
            int idx = 0;
            while (idx <= 99) {
                indexLst.add(idx, StringUtils.paddingIndex(idx + ""));
                idx = idx + 1;
            }
            for (RSAKeysBO keysBO : keysList) {
                indexLst.remove(("0".equals(StringUtils.chgNull(keysBO.getChecksumStatus() + ""))
                        || (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) && keysBO.getRsaStatus().intValue() == 0) ? StringUtils.paddingIndex(keysBO.getRsaIndex() + "") : "");
            }
            model.addAttribute("indexLst", indexLst);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return CREATE_CHECKSUM_PAGE_REDIRECT;
    }

    private boolean validateChecksum(HttpServletRequest request, RSAKeyForm form) throws Exception {
        if (StringUtils.isNullOrEmpty(form.getRsaIndex())) {
            sendMessageError(request, "checksum.require.select.index");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getPublicKeyModulus())) {
            sendMessageError(request, "checksum.public.key.modulus.not.empty");
            return false;
        }
        if (!StringUtils.isHexa(form.getPublicKeyModulus())) {
            sendMessageError(request, "checksum.public.key.modulus.hex");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getPublicKeyExponent())) {
            sendMessageError(request, "checksum.public.key.exponent.not.empty");
            return false;
        }
        if (!StringUtils.isHexa(form.getPublicKeyExponent())) {
            sendMessageError(request, "checksum.public.key.exponent.hex");
            return false;
        }

        if (!"010001".equals(StringUtils.padding(form.getPublicKeyExponent(), 2)) && !"03".equals(StringUtils.padding(form.getPublicKeyExponent(), 2))) {
            sendMessageError(request, "checksum.public.key.exponent.err");
            return false;
        }

        if (StringUtils.isNullOrEmpty(form.getChecksumName())) {
            sendMessageError(request, "checksum.name.not.empty");
            return false;
        }
        if (!StringUtils.isHexa(form.getChecksumName())) {
            sendMessageError(request, "checksum.name.hex");
            return false;
        }
        if (form.getChecksumName().length() != 64) {
            sendMessageError(request, "Privatekey Checksum (SHA256) không đủ độ dài");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getMonth()) && !StringUtils.isNullOrEmpty(form.getYear())) {
            sendMessageError(request, "common.require.select.month");
            return false;
        }
        if (!StringUtils.isNullOrEmpty(form.getMonth()) && StringUtils.isNullOrEmpty(form.getYear())) {
            sendMessageError(request, "common.require.select.year");
            return false;
        }
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
        String expDate = form.getMonth() + "/" + form.getYear();
        Date convertedDate = dateFormat.parse(expDate);
        cal.setTime(convertedDate);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(DateTimeUtils.convertToEndDate(new Date())) < 0) {
            sendMessageError(request, "exp.date.validate.after.sysdate");
            return false;
        }
        return true;
    }
}
