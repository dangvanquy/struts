/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.form.CertificateForm;
import com.napas.vccsca.service.BankMembershipService;
import com.napas.vccsca.service.CertificateService;
import com.napas.vccsca.service.RSAKeyService;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.ExcelUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.util.FileCopyUtils;

/**
 *
 * ReportCertificateController
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ReportCertificateController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ReportCertificateController.class);
    private static final String REPORT_CERTIFICATE_PAGE_REDIRECT = "bao-cao-chung-thu-so.html";
    private static final String EXPORT_CERTIFICATE_REPORT = "exportCertReport.do";

    @RequestMapping(value = REPORT_CERTIFICATE_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preReportCertificate(@ModelAttribute("certificateForm") CertificateForm certificateForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            CertificateService certificateService = new CertificateService();
            BankMembershipService bankingService = new BankMembershipService();
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(searchMap)) {
                if (!StringUtils.isNullOrEmpty(searchMap.get("rsaIndex"))) {
                    certificateForm.setCertificateId(NumberUtil.toNumber(searchMap.get("rsaIndex").toString()));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("month"))) {
                    certificateForm.setMonth((String) searchMap.get("month"));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("year"))) {
                    certificateForm.setYear((String) searchMap.get("year"));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("serial"))) {
                    certificateForm.setSerial((String) searchMap.get("serial").toString());
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("bin"))) {
                    certificateForm.setBin(NumberUtil.toNumber(searchMap.get("bin").toString()));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("registerId"))) {
                    certificateForm.setRegisterId(NumberUtil.toNumber(searchMap.get("registerId").toString()));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("status"))) {
                    certificateForm.setStatus(NumberUtil.toNumber(searchMap.get("status").toString()));
                }
            }

            List<CertificateForm> certificatesList = certificateService.getCertificateReport(searchMap);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
            Calendar cal = Calendar.getInstance();

            for (CertificateForm certificateForm1 : certificatesList) {
                Date convertedExpDate = dateFormat.parse(certificateForm1.getExpDate());
                cal.setTime(convertedExpDate);
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                certificateForm1.setExpDate(DateTimeUtils.convertDateTimeToString(cal.getTime()));
            }

            List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
            List<String> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                    allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }

            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }

            Collections.sort(allKeysList);

            model.addAttribute("indexLst", allKeysList);
            model.addAttribute("certificateForm", certificateForm);
            model.addAttribute("banksList", bankingService.findAll());
            model.addAttribute("certificatesList", certificatesList);
            putActionLog(request, "Truy vấn dữ liệu báo cáo");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu báo cáo");
        }
        return REPORT_CERTIFICATE_PAGE_REDIRECT;
    }

    @RequestMapping(value = REPORT_CERTIFICATE_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchReportCertificate(@ModelAttribute("certificateForm") CertificateForm certificateForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            CertificateService certificateService = new CertificateService();
            BankMembershipService bankingService = new BankMembershipService();
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();
            if (checkFormSearch(request, certificateForm)) {
                searchMap.put("rsaIndex", certificateForm.getRsaIndex());
                searchMap.put("month", certificateForm.getMonth());
                searchMap.put("year", certificateForm.getYear());
                if (!StringUtils.isNullOrEmpty(certificateForm.getMonth()) && !StringUtils.isNullOrEmpty(certificateForm.getYear())) {
                    searchMap.put("expDate", StringUtils.padding(certificateForm.getMonth(), 2) + "/" + StringUtils.addYeah(certificateForm.getYear()));
                }
                searchMap.put("serial", certificateForm.getSerial());
                searchMap.put("bin", certificateForm.getBin());
                searchMap.put("registerId", certificateForm.getRegisterId());
                searchMap.put("status", certificateForm.getStatus());
                searchMap.put("arrBin", certificateForm.getArrBin());

                String arrBin = ",";
                Integer[] arr = certificateForm.getArrBin();
                if (!StringUtils.isNullOrEmpty(arr)) {
                    for (Integer bin : certificateForm.getArrBin()) {
                        arrBin += bin + ",";
                    }
                }

                List<CertificateForm> certificatesList = certificateService.getCertificateReport(searchMap);

                List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
                List<String> allKeysList = new ArrayList();
                for (RSAKeysBO keysBO : tmpAllKeysLst) {
                    if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                        allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                    }
                }

                for (int idx = 0; idx < allKeysList.size(); idx++) {
                    for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                        if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                            allKeysList.remove(idx);
                        }
                    }
                }
                Collections.sort(allKeysList);

                model.addAttribute("indexLst", allKeysList);
                model.addAttribute("certificateForm", certificateForm);
                model.addAttribute("banksList", bankingService.findAll());
                model.addAttribute("certificatesList", certificatesList);
                model.addAttribute("lstSelectedBin", arrBin);

                putActionLog(request, "Tìm kiếm dữ liệu báo cáo");
            }
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu báo cáo");
        }
        return REPORT_CERTIFICATE_PAGE_REDIRECT;
    }

    @RequestMapping(value = EXPORT_CERTIFICATE_REPORT, method = RequestMethod.POST)
    public String exportReportCertificate(@ModelAttribute("certificateForm") CertificateForm certificateForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            CertificateService certificateService = new CertificateService();
            BankMembershipService bankingService = new BankMembershipService();
            RSAKeyService keyService = new RSAKeyService();

            HashMap searchMap = new HashMap();
            searchMap.put("rsaIndex", certificateForm.getRsaIndex());
            if (!StringUtils.isNullOrEmpty(certificateForm.getMonth()) && !StringUtils.isNullOrEmpty(certificateForm.getYear())) {
                searchMap.put("expDate", StringUtils.padding(certificateForm.getMonth(), 2) + "/" + StringUtils.addYeah(certificateForm.getYear()));
            }
            searchMap.put("serial", certificateForm.getSerial());
            searchMap.put("bin", certificateForm.getBin());
            searchMap.put("arrBin", certificateForm.getArrBin());
            String arrBin = ",";
            Integer[] arr = certificateForm.getArrBin();
            if (!StringUtils.isNullOrEmpty(arr)) {
                for (Integer bin : certificateForm.getArrBin()) {
                    arrBin += bin + ",";
                }
            }

            searchMap.put("registerId", certificateForm.getRegisterId());
            searchMap.put("status", certificateForm.getStatus());
            List<CertificateForm> certificatesList = certificateService.getCertificateReport(searchMap);

            List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
            List<String> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getRsaIndex())) {
                    allKeysList.add(String.valueOf(keysBO.getRsaIndex()));
                }
            }

            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }
            Collections.sort(allKeysList);
            model.addAttribute("indexLst", allKeysList);
            model.addAttribute("certificateForm", certificateForm);
            model.addAttribute("banksList", bankingService.findAll());
            model.addAttribute("certificatesList", certificatesList);
            model.addAttribute("lstSelectedBin", arrBin);

            Path path = Paths.get(tem_cert_report);
            byte[] fileContent = ExcelUtil.writeFile(Files.readAllBytes(path), certificatesList, 8, "certificate");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "exportCertificate_result.xlsx");
            response.setContentLength(fileContent.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            putActionLog(request, "Export dữ liệu báo cáo");
            sendMessageSuccess(request, "common.export.success");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Export dữ liệu báo cáo");
        }
        return REPORT_CERTIFICATE_PAGE_REDIRECT;
    }

    private boolean checkFormSearch(HttpServletRequest request, CertificateForm certificateForm) {
        if (StringUtils.isNullOrEmpty(certificateForm.getMonth()) && !StringUtils.isNullOrEmpty(certificateForm.getYear())) {
            sendMessageError(request, "common.require.select.month");
            return false;
        } else if (!StringUtils.isNullOrEmpty(certificateForm.getMonth()) && StringUtils.isNullOrEmpty(certificateForm.getYear())) {
            sendMessageError(request, "common.require.select.year");
            return false;
        }
        return true;
    }
}
