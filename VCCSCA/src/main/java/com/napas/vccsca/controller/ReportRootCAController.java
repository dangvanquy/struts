/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.form.RSAKeyForm;
import com.napas.vccsca.service.RSAKeyService;
import com.napas.vccsca.utils.ExcelUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * ReportRootCAController
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ReportRootCAController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ReportRootCAController.class);
    private static final String REPORT_ROOT_CA_PAGE_REDIRECT = "bao-cao-root-ca.html";
    private static final String REPORT_EXPORT_ROOTCA_ACTION = "exportRootCAReport.do";

    @RequestMapping(value = REPORT_ROOT_CA_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preRootCAReport(@ModelAttribute("rootCAForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(searchMap)) {
                if (!StringUtils.isNullOrEmpty(searchMap.get("rsaIndex"))) {
                    keyForm.setRsaIndex(NumberUtil.toNumber(searchMap.get("rsaIndex").toString()));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("month"))) {
                    keyForm.setMonth((String) searchMap.get("month"));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("year"))) {
                    keyForm.setYear((String) searchMap.get("year"));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("rsaStatus"))) {
                    keyForm.setRsaStatus(NumberUtil.toNumber(searchMap.get("rsaStatus").toString()));
                }
            }
            List<RSAKeysBO> keysList = keyService.getKeys(searchMap);
            List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
            List<Integer> allKeysList = new ArrayList();
            for (RSAKeysBO keysBO : tmpAllKeysLst) {
                if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                    allKeysList.add(keysBO.getRsaIndex());
                }
            }

            for (int idx = 0; idx < allKeysList.size(); idx++) {
                for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                    if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                        allKeysList.remove(idx);
                    }
                }
            }
            model.addAttribute("rootCAForm", keyForm);
            model.addAttribute("keysList", keysList);
            model.addAttribute("allKeysList", allKeysList);
            putActionLog(request, "Truy vấn dữ liệu báo cáo");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu báo cáo");
        }
        return REPORT_ROOT_CA_PAGE_REDIRECT;

    }

    @RequestMapping(value = REPORT_ROOT_CA_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchRootCA(@ModelAttribute("rootCAForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();

            if (checkFormSearch(request, keyForm)) {
                if (!StringUtils.isNullOrEmpty(keyForm.getMonth()) && !StringUtils.isNullOrEmpty(keyForm.getYear())) {
                    searchMap.put("expirationDate", StringUtils.padding(keyForm.getMonth(), 2) + "/" + keyForm.getYear());
                }
                searchMap.put("rsaIndex", keyForm.getRsaIndex());
                searchMap.put("month", keyForm.getMonth());
                searchMap.put("year", keyForm.getYear());
                searchMap.put("rsaStatus", keyForm.getRsaStatus());
                List<RSAKeysBO> keysList = keyService.getKeys(searchMap);
                List<RSAKeysBO> tmpAllKeysLst = keyService.getAllKeys();
                List<Integer> allKeysList = new ArrayList();
                for (RSAKeysBO keysBO : tmpAllKeysLst) {
                    if (!StringUtils.isNullOrEmpty(keysBO.getRsaStatus())) {
                        allKeysList.add(keysBO.getRsaIndex());
                    }
                }

                for (int idx = 0; idx < allKeysList.size(); idx++) {
                    for (int jdx = idx + 1; jdx < allKeysList.size(); jdx++) {
                        if (allKeysList.get(idx).equals(allKeysList.get(jdx))) {
                            allKeysList.remove(idx);
                        }
                    }
                }
                model.addAttribute("rootCAForm", keyForm);
                model.addAttribute("keysList", keysList);
                model.addAttribute("allKeysList", allKeysList);

                putActionLog(request, "Tìm kiếm dữ liệu báo cáo");
            } else {
                putActionErrorLog(request, "Tìm kiếm dữ liệu báo cáo");
            }

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu báo cáo");
        }
        return REPORT_ROOT_CA_PAGE_REDIRECT;
    }

    @RequestMapping(value = REPORT_EXPORT_ROOTCA_ACTION, method = RequestMethod.POST)
    public String exportRootCA(@ModelAttribute("rootCAForm") RSAKeyForm keyForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();

            searchMap.put("rsaIndex", keyForm.getRsaIndex());
            if (!StringUtils.isNullOrEmpty(keyForm.getMonth()) && !StringUtils.isNullOrEmpty(keyForm.getYear())) {
                searchMap.put("expirationDate", StringUtils.padding(keyForm.getMonth(), 2) + "/" + keyForm.getYear());
            }
            searchMap.put("status", keyForm.getRsaStatus());
            List<RSAKeysBO> keysList = keyService.getKeys(searchMap);
            model.addAttribute("rootCAForm", keyForm);
            model.addAttribute("keysList", keysList);

            Path path = Paths.get(tem_rootCa_report);
            byte[] fileContent = ExcelUtil.writeFile(Files.readAllBytes(path), keysList, 8, "rootca");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "exportRootCA_result.xlsx");
            response.setContentLength(fileContent.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());

            putActionLog(request, "Export dữ liệu báo cáo");

            if (!StringUtils.isNullOrEmpty(keysList) && keysList.size() <= 0) {
                sendMessageError(request, "report.no.date.export");
            } else {
                sendMessageSuccess(request, "common.export.success");
            }
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Export dữ liệu báo cáo");
        }
        return REPORT_ROOT_CA_PAGE_REDIRECT;

    }

    private boolean checkFormSearch(HttpServletRequest request, RSAKeyForm keyForm) {
        if (StringUtils.isNullOrEmpty(keyForm.getMonth()) && !StringUtils.isNullOrEmpty(keyForm.getYear())) {
            sendMessageError(request, "common.require.select.month");
            return false;
        } else if (!StringUtils.isNullOrEmpty(keyForm.getMonth()) && StringUtils.isNullOrEmpty(keyForm.getYear())) {
            sendMessageError(request, "common.require.select.year");
            return false;
        }
        return true;
    }
}
