/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.form.BankMembershipForm;
import com.napas.vccsca.service.BankMembershipService;
import com.napas.vccsca.utils.ExcelUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * ReportMembershipBankingController
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ReportMembershipBankingController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ReportMembershipBankingController.class);
    private static final String REPORT_MEMBERSHIP_BANKING_PAGE_REDIRECT = "bao-cao-nhtv.html";
    private static final String EXPORT_MEMBERSHIP_BANKING_REPORT = "exportNhtvReport.do";

    @RequestMapping(value = REPORT_MEMBERSHIP_BANKING_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preMembershipBankingManagement(@ModelAttribute("reportMembershipBankingForm") BankMembershipForm bankMembershipForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            BankMembershipService bankingService = new BankMembershipService();
            HashMap searchMap = new HashMap();

            if (!StringUtils.isNullOrEmpty(searchMap)) {
                if (!StringUtils.isNullOrEmpty(searchMap.get("bankId"))) {
                    bankMembershipForm.setBankId(searchMap.get("bankId").toString());
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("bin"))) {
                    bankMembershipForm.setBin(NumberUtil.toNumber(searchMap.get("bin").toString()));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("status"))) {
                    bankMembershipForm.setStatus(NumberUtil.toNumber(searchMap.get("status").toString()));
                }
            }
            List<BankMembershipBO> banksList = bankingService.getMembershipBanking(searchMap, null);

            model.addAttribute("bankMembershipForm", bankMembershipForm);
            model.addAttribute("banksList", banksList);
            model.addAttribute("allBanks", bankingService.findAll());
            putActionLog(request, "Truy vấn dữ liệu báo cáo");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu báo cáo");
        }
        return REPORT_MEMBERSHIP_BANKING_PAGE_REDIRECT;

    }

    @RequestMapping(value = REPORT_MEMBERSHIP_BANKING_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchMembershipBanking(@ModelAttribute("reportMembershipBankingForm") BankMembershipForm bankMembershipForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            BankMembershipService bankingService = new BankMembershipService();
            HashMap searchMap = new HashMap();

            searchMap.put("bankId", bankMembershipForm.getBankId());
            searchMap.put("bin", bankMembershipForm.getBin());
            searchMap.put("status", bankMembershipForm.getStatus());

            // cho phep chon nhieu ngan hang
            String arrBin = ",";
            Integer[] arr = bankMembershipForm.getArrBin();
            if (!StringUtils.isNullOrEmpty(arr)) {
                for (Integer bin : bankMembershipForm.getArrBin()) {
                    arrBin += bin + ",";
                }
            }
            List<BankMembershipBO> banksList = bankingService.getMembershipBanking(searchMap, arr);
            model.addAttribute("lstSelectedBin", arrBin);

            model.addAttribute("bankMembershipForm", bankMembershipForm);
            model.addAttribute("banksList", banksList);
            model.addAttribute("allBanks", bankingService.findAll());
            putActionLog(request, "Tìm kiếm dữ liệu báo cáo");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu báo cáo");
        }
        return REPORT_MEMBERSHIP_BANKING_PAGE_REDIRECT;

    }

    @RequestMapping(value = EXPORT_MEMBERSHIP_BANKING_REPORT, method = RequestMethod.POST)
    public String exportReportMembershipBanking(@ModelAttribute("reportMembershipBankingForm") BankMembershipForm bankMembershipForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            BankMembershipService bankingService = new BankMembershipService();
            HashMap searchMap = new HashMap();

            searchMap.put("bankId", bankMembershipForm.getBankId());
            searchMap.put("bin", bankMembershipForm.getBin());
            searchMap.put("status", bankMembershipForm.getStatus());

            // cho phep chon nhieu ngan hang
            String arrBin = ",";
            Integer[] arr = bankMembershipForm.getArrBin();
            if (!StringUtils.isNullOrEmpty(arr)) {
                for (Integer bin : bankMembershipForm.getArrBin()) {
                    arrBin += bin + ",";
                }
            }
            List<BankMembershipBO> banksList = bankingService.getMembershipBanking(searchMap, arr);
            model.addAttribute("lstSelectedBin", arrBin);

            model.addAttribute("bankMembershipForm", bankMembershipForm);
            model.addAttribute("banksList", banksList);
            model.addAttribute("allBanks", bankingService.findAll());

            Path path = Paths.get(tem_nhtv_report);
            byte[] fileContent = ExcelUtil.writeFile(Files.readAllBytes(path), banksList, 8, "banking");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "exportBanking_result.xlsx");
            response.setContentLength(fileContent.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            putActionLog(request, "Export dữ liệu báo cáo");
            sendMessageSuccess(request, "common.export.success");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Export dữ liệu báo cáo");
        }
        return REPORT_MEMBERSHIP_BANKING_PAGE_REDIRECT;
    }

}
