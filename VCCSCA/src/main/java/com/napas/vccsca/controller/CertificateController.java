/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.BO.RSAKeysBO;
import static com.napas.vccsca.HSM.CARequestFile.hexStringToByteArray;
//import static com.napas.vccsca.HSM.CARequestFile.byteToHex;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.form.CertificateForm;
import com.napas.vccsca.form.ImportCertificateForm;
import com.napas.vccsca.service.BankMembershipService;
import com.napas.vccsca.service.CertificateService;
import com.napas.vccsca.service.RSAKeyService;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.crypto.Cipher;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

/**
 *
 * CertificateController
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@MultipartConfig
@RequestMapping("/")
public class CertificateController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(CertificateController.class);
    private static final String CERTIFICATE_PAGE_REDIRECT = "quan-ly-chung-thu-so.html";
    private static final String CERTIFICATE_DETAIL_PAGE_REDIRECT = "chi-tiet-chung-thu-so.html";
    private static final String CERTIFICATE_VERIFY_PAGE_REDIRECT = "xac-minh-chung-thu-so.html";

    private static final String CERTIFICATE_RECALL_PAGE_ACTION = "recallCertificate.do";
    private static final String CERTIFICATE_EXPORT_PAGE_ACTION = "exportCertificate.do";
    private static final String CERTIFICATE_RESTORE_PAGE_ACTION = "restoreCertificate.do";
    private static final String CERTIFICATE_IMPORT_PAGE_ACTION = "importCertificate.do";
    private static final String CERTIFICATE_VERIFY_PAGE_ACTION = "verifyCertificate.do";
    private CertificateForm certificateForm;
    private DigitalCertificateBO certificateBO;

    /**
     * for getHexbyte function
     */
    static int countIndex = 0;
    private HashMap<String, String> hexDataCert;
    private byte[] certByte;
    private boolean checkOK = false;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        System.out.println("Target=" + target);

        if (target.getClass() == ImportCertificateForm.class) {

            // Đăng ký để chuyển đổi giữa các đối tượng multipart thành byte[]
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
    }

    @RequestMapping(value = CERTIFICATE_PAGE_REDIRECT, method = RequestMethod.GET)
    public String loadCert(@Valid @ModelAttribute("CertificateForm") CertificateForm certificateForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START get certificate list info");

            List<DigitalCertificateBO> certificateList;
            List<BankMembershipBO> bankList = new ArrayList();
            BankMembershipService bankingService = new BankMembershipService();
            CertificateService certificateService = new CertificateService();
            certificateList = certificateService.findAllDesc();

            //check het han
            certificateService.getCertExpired();

            bankList = bankingService.findAll();

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
            Calendar cal = Calendar.getInstance();

            for (DigitalCertificateBO certificateBO : certificateList) {
                Date convertedExpDate = dateFormat.parse(certificateBO.getExpDate());
                cal.setTime(convertedExpDate);
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                certificateBO.setExpDate(DateTimeUtils.convertDateTimeToString(cal.getTime()));
            }

            Integer[] listRsaId = new Integer[certificateList.size()];
            for (int idx = 0; idx < certificateList.size(); idx++) {
                listRsaId[idx] = certificateList.get(idx).getRsaId().intValue();
            }
            HashMap searchKey = new HashMap();
            searchKey.put("listId", listRsaId);
            List<RSAKeysBO> aKeysBOs = new RSAKeyService().getKeys(searchKey);
            for (DigitalCertificateBO digitalCertificateBO : certificateList) {
                for (RSAKeysBO aKeysBO : aKeysBOs) {
                    if (digitalCertificateBO.getRsaId().intValue() == aKeysBO.getRsaId().intValue()) {
                        digitalCertificateBO.setRsaId(aKeysBO.getRsaIndex());
                        break;
                    }
                }
            }

            model.addAttribute("certificateList", certificateList);
            model.addAttribute("bankList", bankList);
            model.addAttribute("numberOfRepresent", 1);

            logger.info("END get certificate list info");
            putActionLog(request, "Truy vấn dữ liệu Chứng thư số");
        } catch (Exception e) {
            model.addAttribute("certificateForm", null);
            putActionErrorLog(request, "Truy vấn dữ liệu Chứng thư số");
//            throw e;
        }
        return CERTIFICATE_PAGE_REDIRECT;
    }

    @RequestMapping(value = CERTIFICATE_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchCertificate(@Valid @ModelAttribute("CertificateForm") CertificateForm certificateForm,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START searchCertificate");

            List<DigitalCertificateBO> certificateList;
            List<BankMembershipBO> bankList = new ArrayList();
            BankMembershipService bankingService = new BankMembershipService();
            CertificateService certificateService = new CertificateService();
            if (!StringUtils.isNullOrEmpty(certificateForm)) {
                this.certificateForm = certificateForm;
                HashMap hmCertificate = new HashMap();
                hmCertificate.put("bin", certificateForm.getBin());
                hmCertificate.put("serial", certificateForm.getSerial());
                hmCertificate.put("bankIdentity", certificateForm.getBankIdentity());
                if (!StringUtils.isNullOrEmpty(certificateForm.getMonth()) && !StringUtils.isNullOrEmpty(certificateForm.getYear())) {
                    hmCertificate.put("expDate", certificateForm.getMonth() + "/" + StringUtils.addYeah(certificateForm.getYear()));
                }
                hmCertificate.put("status", certificateForm.getStatus());
                hmCertificate.put("fromDate", certificateForm.getFromDate());
                hmCertificate.put("toDate", certificateForm.getToDate());
                hmCertificate.put("binSearch", certificateForm.getBinSearch());
                model.addAttribute("certificateForm", this.certificateForm);
                certificateList = certificateService.getCertificates(hmCertificate);
            } else {
                certificateList = certificateService.findAll();
            }

            Integer[] listRsaId = new Integer[certificateList.size()];
            for (int idx = 0; idx < certificateList.size(); idx++) {
                listRsaId[idx] = certificateList.get(idx).getRsaId().intValue();
            }
            HashMap searchKey = new HashMap();
            searchKey.put("listId", listRsaId);
            List<RSAKeysBO> aKeysBOs = new RSAKeyService().getKeys(searchKey);
            for (DigitalCertificateBO digitalCertificateBO : certificateList) {
                for (RSAKeysBO aKeysBO : aKeysBOs) {
                    if (digitalCertificateBO.getRsaId().intValue() == aKeysBO.getRsaId().intValue()) {
                        digitalCertificateBO.setRsaId(aKeysBO.getRsaIndex());
                        break;
                    }
                }
            }
            
            bankList = bankingService.findAll();
            model.addAttribute("bankList", bankList);
            model.addAttribute("certificateList", certificateList);

            logger.info("END searchCertificate");
            putActionLog(request, "Tìm kiếm dữ liệu Chứng thư số");
        } catch (Exception e) {
            model.addAttribute("certificateForm", null);
            logger.error(e);
            sendMessageError(request, "search.certificate.fail");
            putActionErrorLog(request, "Truy vấn dữ liệu Chứng thư số");
        }
        return CERTIFICATE_PAGE_REDIRECT;
    }

    @RequestMapping(value = CERTIFICATE_DETAIL_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getCertificateDetail(@RequestParam("certificateId") String certificateId,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START get certificate by id to view");

            CertificateService certificateService = new CertificateService();
            certificateBO = new DigitalCertificateBO();
            certificateBO = certificateService.getCertificateById(NumberUtil.toNumber(AESUtil.decryption(certificateId)));

            model.addAttribute("hexDataCert", hexDataCert);
            model.addAttribute("certificateBO", certificateBO);
            model.addAttribute("rSAKeysBO", new RSAKeyService().getKeyById(certificateBO.getRsaId()));

            logger.info("END get certificate by id to view");
            putActionLog(request, "Truy vấn chi tiết dữ liệu Chứng thư số id:" + NumberUtil.toNumber(AESUtil.decryption(certificateId)));
        } catch (Exception e) {
            putActionErrorLog(request, "Truy vấn chi tiết dữ liệu Chứng thư số id:" + NumberUtil.toNumber(AESUtil.decryption(certificateId)));
            throw e;
        }
        return CERTIFICATE_DETAIL_PAGE_REDIRECT;
    }

    @RequestMapping(value = CERTIFICATE_RECALL_PAGE_ACTION, method = RequestMethod.POST)
    public void recallCertificate(@RequestParam("cerIdRecall") String cerIdRecall,
            @RequestParam("recallContent") String recallContent,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START recall certificate");

            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            CertificateService certificateService = new CertificateService();
            certificateBO = new DigitalCertificateBO();
            certificateBO = certificateService.getCertificateById(NumberUtil.toNumber(AESUtil.decryption(cerIdRecall)));
            certificateBO.setContent(recallContent);
            certificateBO.setStatus(1);
            certificateBO.setEvictionUser(username);
            certificateBO.setEvictionDate(new Date());
            certificateService.updateCertificate(certificateBO);

            sendMessageSuccess(request, "certificate.recall.success");

            logger.info("END recall certificate");
            putActionLog(request, "Thu hồi chứng thư số, id:" + NumberUtil.toNumber(AESUtil.decryption(cerIdRecall)));
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "certificate.recall.fail");
            putActionErrorLog(request, "Thu hồi chứng thư số, id:" + NumberUtil.toNumber(AESUtil.decryption(cerIdRecall)));
        }
        response.sendRedirect(CERTIFICATE_PAGE_REDIRECT);
    }

    @RequestMapping(value = CERTIFICATE_RESTORE_PAGE_ACTION, method = RequestMethod.POST)
    public void restoreCertificate(@RequestParam("cerIdRes") String cerIdRes,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START restore certificate");

            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            CertificateService certificateService = new CertificateService();
            BankMembershipService membershipService = new BankMembershipService();

            certificateBO = certificateService.getCertificateById(NumberUtil.toNumber(AESUtil.decryption(cerIdRes)));
            certificateBO.setStatus(0);
            certificateBO.setRestoreUser(username);
            certificateBO.setRestoreDate(new Date());

            BankMembershipBO bankMembershipBO = membershipService.getMembershipBankByBin(certificateBO.getBin());
            if (bankMembershipBO != null && bankMembershipBO.getStatus() == 0) {
                certificateService.updateCertificate(certificateBO);
                certificateService.updateRQCertificate();
                sendMessageSuccess(request, "certificate.restore.success");
            } else {
                sendMessageError(request, "certificate.restore.nhtv.fail");
            }

            logger.info("END restore certificate");
            putActionLog(request, "Khôi phục chứng thư số, id:" + NumberUtil.toNumber(AESUtil.decryption(cerIdRes)));
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "certificate.restore.fail");
            putActionErrorLog(request, "Khôi phục chứng thư số, id:" + NumberUtil.toNumber(AESUtil.decryption(cerIdRes)));
        }
        response.sendRedirect(CERTIFICATE_PAGE_REDIRECT);
    }

    @RequestMapping(value = CERTIFICATE_EXPORT_PAGE_ACTION, method = RequestMethod.POST)
    public String exportCertificate(@RequestParam("certId") String certId,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            CertificateService certificateService = new CertificateService();
            certificateBO = certificateService.getCertificateById(NumberUtil.toNumber(AESUtil.decryption(certId)));

            RSAKeyService keyService = new RSAKeyService();
            RSAKeysBO rsaKeyBO = keyService.getKeyById(certificateBO.getRsaId());
            if (StringUtils.isNullOrEmpty(rsaKeyBO)) {
                rsaKeyBO = keyService.getKeyByIndex(certificateBO.getRsaId());
            }

            byte[] fileContent = certificateBO.getCertificate();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "" + StringUtils.padding(certificateBO.getRegisterId() + "", 6) + ".i" + StringUtils.paddingIndex(rsaKeyBO.getRsaIndex() + ""));
            response.setContentLength(fileContent.length);

            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            putActionLog(request, "Export chứng thư số, id:" + NumberUtil.toNumber(AESUtil.decryption(certId)));

        } catch (Exception ex) {
            putActionErrorLog(request, "Export chứng thư số, id:" + NumberUtil.toNumber(AESUtil.decryption(certId)));
        }
        return CERTIFICATE_PAGE_REDIRECT;
    }

    @RequestMapping(value = CERTIFICATE_VERIFY_PAGE_REDIRECT, method = RequestMethod.GET)
    public String uploadMultiFileHandler(Model model, HttpServletRequest request, HttpServletResponse response) {

        try {
            HashMap searchMap = new HashMap();
            searchMap.put("rsaStatus", 0);
            searchMap.put("privateKeyNameCheck", "true");
            searchMap.put("slotHsmCheck", "true");

            List<RSAKeysBO> allKeysList = new RSAKeyService().getKeys(searchMap);
            model.addAttribute("allKeysList", allKeysList);
            model.addAttribute("importCertificateForm", new ImportCertificateForm());

            if (checkOK) {
                checkOK = false;
                if (!StringUtils.isNullOrEmpty(hexDataCert) && hexDataCert.size() > 0) {
                    if (verifyCer()) {
                        model.addAttribute("hexDataCert", hexDataCert);
                    } else {
                        checkOK = false;
                    }
                } else {
                    checkOK = false;
                }
            } else {
                hexDataCert = null;
            }

        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "error");
        }
        return CERTIFICATE_VERIFY_PAGE_REDIRECT;
    }

    @RequestMapping(value = CERTIFICATE_IMPORT_PAGE_ACTION, method = RequestMethod.POST)
    public void importCertificate(@RequestParam("file") final MultipartFile file, Model model,
            @RequestParam("index") String index, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            if (!file.isEmpty()) {
                if (!StringUtils.isNullOrEmpty(index)) {
                    RSAKeyService keyService = new RSAKeyService();
                    RSAKeysBO rsaKeyBO = keyService.getKeyByIndex(NumberUtil.toNumber(index));

                    byte[] fileContentBase = file.getBytes();
                    certByte = fileContentBase;
                    String[] list = byteToHexArr(fileContentBase);
                    hexDataCert = new HashMap<String, String>();
                    //5.1.1)Unsigned IPK Output Extension
                    //Header 
                    String usHeader = getHexbyteStart(list, 1);
                    hexDataCert.put("usHeader", usHeader);

//                    if (!comparedata("usHeader", "22")) {
//                        sendMessageError(request, "verify.certificate.read.file.fail2");
//                        return CERTIFICATE_VERIFY_PAGE_REDIRECT;
//                    }
                    //Service Identifier 
                    String usServiceIdentifier = getHexbyte(list, 4);
                    hexDataCert.put("usServiceIdentifier", usServiceIdentifier);
                    System.out.println("usServiceIdentifier : " + usServiceIdentifier);
                    //Issuer Identification Number (IIN) 
                    String usIssuerIdentificationNumber = getHexbyte(list, 4);
                    hexDataCert.put("usIssuerIdentificationNumber", usIssuerIdentificationNumber);
                    System.out.println("usIssuerIdentificationNumber : " + usIssuerIdentificationNumber);

                    //Certificate Serial Number
                    String usCertificateSerialNumber = getHexbyte(list, 3);
                    hexDataCert.put("usCertificateSerialNumber", usCertificateSerialNumber);
                    System.out.println("usCertificateSerialNumber : " + usCertificateSerialNumber);

                    //Certificate Expiration Date 
                    String usCertificateExpirationDate = getHexbyte(list, 2);
                    hexDataCert.put("usCertificateExpirationDate", usCertificateExpirationDate);
                    System.out.println("usCertificateExpirationDate : " + usCertificateExpirationDate);

                    //IPK Modulus Remainder Length 
                    String usIPKModulusRemainderLength = getHexbyte(list, 1);
                    hexDataCert.put("usIPKModulusRemainderLength", usIPKModulusRemainderLength);
                    System.out.println("usIPKModulusRemainderLength" + usIPKModulusRemainderLength);
                    System.out.println("Integer.parseInt(usIPKModulusRemainderLength, 16)" + Integer.parseInt(usIPKModulusRemainderLength, 16));

                    if (Integer.parseInt(usIPKModulusRemainderLength, 16) > 0) {
                        //IPK Modulus Remainder 
                        String usIPKModulusRemainder = getHexbyte(list, Integer.parseInt(usIPKModulusRemainderLength, 16));//Phần còn lại của IPK Modulus. Xuất hiện trong trường hợp NI > NCA – 36. Gồm (NI - NCA + 36) byte đầu tiên (bên trái) của IPK Modulus 
                        hexDataCert.put("usIPKModulusRemainder", usIPKModulusRemainder);
                    }

                    //IPK Exponent Length 
                    String usIPKExponentLength = getHexbyte(list, 1);
                    hexDataCert.put("usIPKExponentLength", usIPKExponentLength);
                    System.out.println("usIPKExponentLength : " + usIPKExponentLength);
                    //IPK Exponent 
                    String usIPKExponent = getHexbyte(list, Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3);
                    hexDataCert.put("usIPKExponent", usIPKExponent);
                    System.out.println("usIPKExponent : " + usIPKExponent);
                    //CA Public Key Index  
                    String usCAPublicKeyIndex = getHexbyte(list, 1);
                    hexDataCert.put("usCAPublicKeyIndex", usCAPublicKeyIndex);
                    System.out.println("usCAPublicKeyIndex : " + usCAPublicKeyIndex);

                    //----------------------------------------------phan ky
                    String allSign = getHexbyte(list, list.length - countIndex);
                    byte[] allSignbyte = hexStringToByteArray(allSign);

                    //Trong đó e, NI , and NCA lần lượt là độ dài tính bằng byte của IPK Exponent, IPK Modulus, và CA Public Key dùng để tạo chứng thư số. 
                    int e = Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3;
                    int Nca = allSignbyte.length;//NCA Độ dài khóa của CA (tính bằng byte) 
                    hexDataCert.put("Nca", Nca + "");
                    RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(rsaKeyBO.getPublicKeyModulus(), 16), new BigInteger(rsaKeyBO.getPublicKeyExponent(), 16));
                    PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(spec);

                    try {
                        Cipher cipher = Cipher.getInstance("RSA/ECB/NOPADDING");
                        cipher.init(Cipher.DECRYPT_MODE, publicKey);
                        byte[] allSignbyteDecode = cipher.doFinal(allSignbyte);

                        System.out.println(">>>>" + byteToHex(allSignbyteDecode));
                        list = byteToHexArr(allSignbyteDecode);
                    } catch (Exception exx) {
                        putActionErrorLog(request, "Import file chứng thư");
                        sendMessageError(request, "verify.certificate.read.file.fail2");
                        System.out.println("Loi doc file: " + exx);
                        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
                        return;
                    }

                    //5.1.2)IPK Certificate 
                    //Recovered Data Header
                    String recoveredDataHeader = getHexbyteStart(list, 1);
                    hexDataCert.put("recoveredDataHeader", recoveredDataHeader);
                    //Certificate Format
                    String certificateFormat = getHexbyte(list, 1);
                    hexDataCert.put("certificateFormat", certificateFormat);
                    //Issuer Identification Number (IIN) 
                    String issuerIdentificationNumber = getHexbyte(list, 4);
                    hexDataCert.put("issuerIdentificationNumber", issuerIdentificationNumber);
                    //Certificate Expiration Date
                    String certificateExpirationDate = getHexbyte(list, 2);
                    hexDataCert.put("certificateExpirationDate", certificateExpirationDate);
                    //Certificate Serial Number 
                    String certificateSerialNumber = getHexbyte(list, 3);
                    hexDataCert.put("certificateSerialNumber", certificateSerialNumber);

                    //Hash Algorithm Indicator 1
                    String hashAlgorithmIndicator = getHexbyte(list, 1);
                    hexDataCert.put("hashAlgorithmIndicator", hashAlgorithmIndicator);
                    //IPK Algorithm Indicator 1
                    String iPKAlgorithmIndicator = getHexbyte(list, 1);
                    hexDataCert.put("iPKAlgorithmIndicator", iPKAlgorithmIndicator);
                    //IPK Length 1
                    String iPKLength = getHexbyte(list, 1);
                    hexDataCert.put("iPKLength", iPKLength);
                    //IPK Exponent Length 1
                    String iPKExponentLength = getHexbyte(list, 1);
                    hexDataCert.put("iPKExponentLength", iPKExponentLength);

                    //IPK Modulus or Leftmost part of the IPK Modulus NCA – 36 
                    String iPKModulus = getHexbyte(list, Nca - 36);
                    hexDataCert.put("iPKModulus", iPKModulus);
                    System.out.println("iPKModulus: " + iPKModulus);

                    //Hash Result 20
                    String hashResult = getHexbyte(list, 20);
                    hexDataCert.put("hashResult", hashResult);
                    //Recovered Data Trailer 1
                    String recoveredDataTrailer = getHexbyte(list, 1);
                    hexDataCert.put("recoveredDataTrailer", recoveredDataTrailer);

                    logger.debug("BIN======>" + NumberUtil.getBin(usIssuerIdentificationNumber.replaceAll("F", "")));
                    BankMembershipService service = new BankMembershipService();
                    BankMembershipBO membershipBO = service.getMembershipBankByBin(NumberUtil.getBin(usIssuerIdentificationNumber.replaceAll("F", "")));
                    if (StringUtils.isNullOrEmpty(membershipBO)) {
                        sendMessageError(request, "request.import.not.bin", StringUtils.getBankIdentityNoFF(usIssuerIdentificationNumber));
                        putActionErrorLog(request, "Import file chứng thư");
                        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
                        return;
                    }
                    if (membershipBO.getStatus() != 0) {
                        sendMessageError(request, "import.app.nhtv.fail");
                        putActionErrorLog(request, "Import file chứng thư");
                        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
                        return;
                    }

                    String[] fn = file.getOriginalFilename().split("\\.");
                    hexDataCert.put("regNum", fn[0]);
                    hexDataCert.put("bankFullName", membershipBO.getBankFullName());
                    hexDataCert.put("rsaIndex", index);
                    hexDataCert.put("rsaLengh", rsaKeyBO.getRsaLength() + "");
                    hexDataCert.put("rsaId", rsaKeyBO.getRsaId() + "");
                    hexDataCert.put("lenPublicKeyModulus", (rsaKeyBO.getPublicKeyModulus().length() / 2) + "");
                    hexDataCert.put("exDate1", certificateExpirationDate.substring(0, 2) + "/20" + certificateExpirationDate.substring(2, 4));

                    if (verifyCer()) {
                        model.addAttribute("hexDataCert", hexDataCert);
                        checkOK = true;
                    } else {
                        sendMessageError(request, "verify.certificate.read.file.fail2");
                    }
                    putActionErrorLog(request, "Import file chứng thư");
                } else {
                    putActionErrorLog(request, "Import file chứng thư");
                    sendMessageError(request, "certificate.import.index.validate.null");
                }
            } else {
                putActionErrorLog(request, "Import file chứng thư");
                sendMessageError(request, "certificate.import.upload.validate.null");
            }
        } catch (Exception ex) {
            putActionErrorLog(request, "Import file chứng thư");
            System.out.println("Loi doc file: " + ex);
            sendMessageError(request, "verify.certificate.read.file.fail2");
            response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
            return;
        }
        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
        return;
    }

    @RequestMapping(value = CERTIFICATE_VERIFY_PAGE_ACTION, method = RequestMethod.POST)
    public void verifyCertificate(@Valid
            @ModelAttribute("CertificateForm") CertificateForm certificateForm,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            CertificateService certificateService = new CertificateService();

            if (!StringUtils.isNullOrEmpty(hexDataCert) && verifyCer()) {

                String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
                if (certificateService.getCountBySeria(hexDataCert.get("certificateSerialNumber")) == 0l) {

                    certificateBO = new DigitalCertificateBO();
                    certificateBO.setRsaId(NumberUtil.toNumber(hexDataCert.get("rsaId")));
                    certificateBO.setRegisterId(NumberUtil.toNumber(hexDataCert.get("regNum")));//NumberUtil.toNumber(hexDataCert.get("registerId")))
                    certificateBO.setBin(NumberUtil.toNumber(hexDataCert.get("issuerIdentificationNumber").replaceAll("F", "")));
                    certificateBO.setBankName(hexDataCert.get("bankFullName"));
                    certificateBO.setIpk(hexDataCert.get("iPKModulus"));
                    certificateBO.setExpDate(hexDataCert.get("certificateExpirationDate").substring(0, 2) + "/20" + hexDataCert.get("certificateExpirationDate").substring(2, 4));
                    certificateBO.setSerial(hexDataCert.get("certificateSerialNumber"));
                    certificateBO.setBankIdentity(hexDataCert.get("usIssuerIdentificationNumber"));
                    certificateBO.setCertificate(certByte);
                    certificateBO.setRsaLengh(NumberUtil.toNumber(hexDataCert.get("rsaLengh")));
                    certificateBO.setSignUser(username);
                    certificateBO.setSignDate(new Date());
                    certificateBO.setCreateUser(username);
                    certificateBO.setCreateDate(new Date());
                    certificateBO.setIpkLengh(Integer.parseInt(hexDataCert.get("iPKLength"), 16));
                    certificateBO.setStatus(0);

                    hexDataCert.clear();
                    hexDataCert = null;
                    certByte = null;

                    BankMembershipBO bankMembershipBO = new BankMembershipService().getMembershipBankByBin(certificateBO.getBin());
                    if (bankMembershipBO == null) {
                        sendMessageError(request, "ver.app.nhtv.fail");
                        putActionErrorLog(request, "Xác thực file chứng thư");
                        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
                        return;
                    }
                    if (bankMembershipBO.getStatus() != 0) {
                        sendMessageError(request, "ver.app.nhtv.fail");
                        putActionErrorLog(request, "Xác thực file chứng thư");
                        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
                        return;
                    }

                    certificateService.addCertificate(certificateBO);
                    putActionLog(request, "Xác thực file chứng thư");
                    sendMessageSuccess(request, "verify.certificate.success");
                } else {
                    putActionErrorLog(request, "Xác thực file chứng thư");
                    sendMessageError(request, "verify.certificate.exist");
                }
            } else {
                putActionErrorLog(request, "Xác thực file chứng thư");
                sendMessageError(request, "verify.certificate.fail2");
            }
        } catch (Exception ex) {
            logger.error("Xác thực file chứng thư >>" + ex);
            putActionErrorLog(request, "Xác thực file chứng thư");
            sendMessageError(request, "verify.certificate.fail");
//            throw ex;
        }
        response.sendRedirect(CERTIFICATE_VERIFY_PAGE_REDIRECT);
    }

    /**
     * getHexbyte
     *
     * @param byteData
     * @param pos
     * @param bytelen
     * @return
     */
    public static String getHexbyte(String[] byteData, int bytelen) {

        String ret = "";
        for (int jdx = 0; jdx < bytelen; jdx++) {
            ret += byteData[countIndex + jdx];
        }
        countIndex = countIndex + bytelen;
        return ret;
    }

    /**
     * verifyCer
     *
     * @return
     */
    private boolean verifyCer() throws Exception {
        //a) len cert !=  CA Public Key Modulus
        if (!hexDataCert.get("Nca").equals(hexDataCert.get("lenPublicKeyModulus"))) {
            return false;
        }
        //b) recoveredDataTrailer != BC
        if (!comparedata("recoveredDataTrailer", "BC")) {
            return false;
        }
        //c) recoveredDataHeader != 6A
        if (!comparedata("recoveredDataHeader", "6A")) {
            return false;
        }
        //d) certificateFormat != 02
        if (!comparedata("certificateFormat", "02")) {
            return false;
        }

        //e) Nối tiếp nhau từ trái sang phải, 
        //các trường dữ liệu từ vị trí thứ 2 đến thứ 10 trong Bảng 5-3 
        //(từ trường Certificate Format đến IPK Length hoặc Leftmost Digits của IPK), 
        //tiếp theo là IPK Remainder (nếu có) và cuối cùng là IPK Exponent
        //f) Áp dụng thuật toán hàm băm SHA-1 đưa ra kết quả tiếp nối của bước trước để tạo ra kết quả băm. 
        String hexsha = hexDataCert.get("certificateFormat")
                + hexDataCert.get("issuerIdentificationNumber")
                + hexDataCert.get("certificateExpirationDate")
                + hexDataCert.get("certificateSerialNumber")
                + hexDataCert.get("hashAlgorithmIndicator")
                + hexDataCert.get("iPKAlgorithmIndicator")
                + hexDataCert.get("iPKLength")
                + hexDataCert.get("iPKExponentLength")
                + hexDataCert.get("iPKModulus");
//        System.out.println(">>" + hexsha);

        //g) So sánh trường Hash Result đã được tính từ bước trước với Hash Result đã được khôi phục, nếu chúng không giống nhau, việc xác thực không thành công. 
        String checksha = byteToHex(hashByteArraySHA1(hexStringToByteArray(hexsha)));
        if (!checksha.equalsIgnoreCase(hexDataCert.get("hashResult"))) {
//            return false;
        }
        //h) Xác minh trường Issuer Identification Number (IIN) phải là chính xác. Nếu không thì xác thực không thành công. 
        if (!comparedata("issuerIdentificationNumber", hexDataCert.get("usIssuerIdentificationNumber"))) {
            return false;
        }
        //i) Xác minh trường Certificate Expiration Date phải chính xác. Nếu ngày của trường Certificate Expiration Date là trước ngày thực hiện xác thực, chứng thư số đã hết hạn, trong trường hợp này việc xác thực không thành công. j
        if (!comparedata("certificateExpirationDate", hexDataCert.get("usCertificateExpirationDate"))) {
            return false;
        }
        //j) Nếu trường IPK Algorithm Indicator không bằng hex 01, việc xác thực không thành công. 
        if (!comparedata("iPKAlgorithmIndicator", "01")) {
            return false;
        }

        //k) Nếu trường Issuer PK Modulus yêu cầu chèn thêm và việc chèn thêm đó không bằng NCA – 36 - NI byte của hex BB, việc xác thực không thành công. 
        if (!((hexDataCert.get("iPKModulus").length() / 2) + "").equals((NumberUtil.toNumber(hexDataCert.get("Nca")) - 36) + "")) {
            return false;
        }
        return true;
    }

    /**
     * comparedata
     *
     * @param recoveredDataTrailer
     * @param value
     * @return
     */
    private boolean comparedata(String recoveredDataTrailer, String value) {
        return value.equals(StringUtils.chgNull(hexDataCert.get(recoveredDataTrailer)));
    }

    /**
     * getHexbyte
     *
     * @param byteData
     * @param pos
     * @param bytelen
     * @return
     */
    public static String getHexbyteStart(String[] byteData, int bytelen) {
        countIndex = 0;
        String ret = "";
        for (int jdx = 0; jdx < bytelen; jdx++) {
            ret += byteData[countIndex + jdx];
        }
        countIndex = countIndex + bytelen;
        return ret;
    }
}
