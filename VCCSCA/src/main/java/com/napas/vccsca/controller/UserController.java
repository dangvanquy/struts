/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.GroupsPermissionBO;
import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.form.UsersForm;
import com.napas.vccsca.service.GroupPemissionService;
import com.napas.vccsca.service.UsersService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * UserController
 *
 * @author CuongTV
 * @since Aug 22, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class UserController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(UserController.class);
    private static final String CREATE_USER_PAGE_REDIRECT = "tao-moi-nguoi-dung.html";
    private static final String USER_MANAGEMENT_PAGE_REDIRECT = "quan-ly-nguoi-dung.html";
    private static final String USER_DETAIL_PAGE_REDIRECT = "xem-chi-tiet-nguoi-dung.html";
    private static final String EDIT_USER_PAGE_REDIRECT = "chinh-sua-nguoi-dung.html";

    @RequestMapping(value = "createAccount.do", method = RequestMethod.POST)
    public void createUser(@ModelAttribute("createUserForm") UsersForm usersForm, BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            UsersService userService = new UsersService();
            if (!validateUser(request, usersForm, "create")) {
                putActionErrorLog(request, "Tạo mới tài khoản");
                response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("username", usersForm.getUsername());
            hashMap.put("oremail", usersForm.getEmail());
            List<UsersBO> list = userService.getUser(hashMap);
            if (!StringUtils.isNullOrEmpty(list) && list.size() > 0) {
                UsersBO bO = list.get(0);
                if (bO.getUsername().equals(usersForm.getUsername())) {
                    sendMessageError(request, "user.account.exist");
                } else if (bO.getEmail().equals(usersForm.getEmail())) {
                    sendMessageError(request, "user.email.exist");
                }

                response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

            UsersBO userBO = new UsersBO();
            userBO = usersForm.convertToBO(userBO);
            userBO.setPassword(AESUtil.encryptionPassword(usersForm.getPassword()));
            userBO.setCreateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            userBO.setCreateDate(new Date());

            userService.addUsers(userBO);
            sendMessageSuccess(request, "user.create.success");
            putActionLog(request, "Tạo mới tài khoản");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tạo mới tài khoản");
        }
        response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = "editAccount.do", method = RequestMethod.POST)
    public void editUser(@ModelAttribute("editUserForm") UsersForm usersForm, BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            UsersService userService = new UsersService();

            if (!validateUser(request, usersForm, "edit")) {
                response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
                return;
            }
            UsersBO userBO = userService.getUserById(usersForm.getUserId());
            if (!userBO.getUsername().equals(usersForm.getUsername())) {
                HashMap hashMap = new HashMap();
                hashMap.put("username", usersForm.getUsername());
                List<UsersBO> list = userService.getUser(hashMap);
                if (!StringUtils.isNullOrEmpty(list) && list.size() > 0) {
                    sendMessageError(request, "user.account.exist");
                    response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
                    return;
                }
            }
            if (!usersForm.getEmail().equals(userBO.getEmail())) {
                HashMap hashMap = new HashMap();
                hashMap.put("email", usersForm.getEmail());
                List<UsersBO> list = userService.getUser(hashMap);
                if (!StringUtils.isNullOrEmpty(list) && list.size() > 0) {
                    sendMessageError(request, "user.email.exist");
                    response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
                    return;
                }
            }
            userBO = usersForm.convertUsersFormToBO(userBO);
            if (!StringUtils.isNullOrEmpty(usersForm.getPassword())) {
                userBO.setPassword(AESUtil.encryptionPassword(usersForm.getPassword()));
            }

            userBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            userBO.setUpdateDate(new Date());
            userService.updateUsers(userBO);

            sendMessageSuccess(request, "user.edit.success");
            putActionLog(request, "Chỉnh sửa tài khoản, id:" + usersForm.getUserId());
            response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Chỉnh sửa tài khoản, id:" + usersForm.getUserId());
        }
    }

    @RequestMapping(value = "lockUser.do", method = RequestMethod.POST)
    public void lockChecksum(@RequestParam("userId") String userId, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            UsersService userService = new UsersService();
            UsersBO usersBO = userService.getUserById(NumberUtil.toNumber(AESUtil.decryption(userId)));

            usersBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            usersBO.setUpdateDate(new Date());
            usersBO.setStatus(1);

            userService.updateUsers(usersBO);
            sendMessageSuccess(request, "user.lock.success");
            putActionLog(request, "Khóa tài khoản, id:" + NumberUtil.toNumber(AESUtil.decryption(userId)));
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Khóa tài khoản, id:" + NumberUtil.toNumber(AESUtil.decryption(userId)));
        }
        response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = "unlockUser.do", method = RequestMethod.POST)
    public void unlockChecksum(@RequestParam("userId") String userId, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            UsersService userService = new UsersService();
            UsersBO usersBO = userService.getUserById(NumberUtil.toNumber(AESUtil.decryption(userId)));

            usersBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            usersBO.setUpdateDate(new Date());
            usersBO.setStatus(0);

            userService.updateUsers(usersBO);
            sendMessageSuccess(request, "user.unlock.success");
            putActionLog(request, "Mở khóa tài khoản, id:" + NumberUtil.toNumber(AESUtil.decryption(userId)));

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Mở khóa tài khoản, id:" + NumberUtil.toNumber(AESUtil.decryption(userId)));
        }
        response.sendRedirect(USER_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = USER_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preUserManagement(@ModelAttribute("userForm") UsersForm userForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            UsersService userService = new UsersService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();
            HashMap searchMap = new HashMap();
            if (!StringUtils.isNullOrEmpty(searchMap)) {
                userForm.setFullName((String) searchMap.get("fullName"));
                userForm.setPhoneNumber((String) searchMap.get("phoneNumber"));
                userForm.setEmail((String) searchMap.get("email"));
                if (!StringUtils.isNullOrEmpty(searchMap.get("status"))) {
                    userForm.setStatus(NumberUtil.toNumber(searchMap.get("status") + ""));
                }
            }

            List<UsersBO> usersList = userService.getUser(searchMap);
            List<GroupsPermissionBO> groupsList = groupPemissionService.getAllGroupsPermission();
            HashMap mapgr = new HashMap();
            for (GroupsPermissionBO gpbo : groupsList) {
                mapgr.put(gpbo.getGroupId(), StringEscapeUtils.unescapeHtml(gpbo.getGroupName()));
            }
            model.addAttribute("groupsListMap", mapgr);
            model.addAttribute("userForm", userForm);
            model.addAttribute("usersList", usersList);
            model.addAttribute("groupsList", groupPemissionService.getAllGroupsPermission());
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return USER_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = USER_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchUser(@ModelAttribute("userForm") UsersForm userForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            UsersService userService = new UsersService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();

            HashMap searchMap = new HashMap();
            searchMap.put("fullName", userForm.getFullName());
            searchMap.put("phoneNumber", userForm.getPhoneNumber());
            searchMap.put("email", userForm.getEmail());
            searchMap.put("status", userForm.getStatus());

            List<UsersBO> usersList = userService.getUser(searchMap);
            List<GroupsPermissionBO> groupsList = groupPemissionService.getAllGroupsPermission();
            HashMap mapgr = new HashMap();
            for (GroupsPermissionBO gpbo : groupsList) {
                mapgr.put(gpbo.getGroupId(), StringEscapeUtils.unescapeHtml(gpbo.getGroupName()));
            }
            model.addAttribute("groupsListMap", mapgr);
            model.addAttribute("userForm", userForm);
            model.addAttribute("usersList", usersList);
            sendMessageError(request, "");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return USER_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = CREATE_USER_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preCreateUser(Model model, HttpServletRequest request) throws Exception {
        try {
            GroupPemissionService groupPemissionService = new GroupPemissionService();
            List groupsList = groupPemissionService.getAllGroupsPermission();
            model.addAttribute("groupsList", groupsList);

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return CREATE_USER_PAGE_REDIRECT;
    }

    @RequestMapping(value = EDIT_USER_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preEditUser(@RequestParam("userId") String userId, Model model, HttpServletRequest request) throws Exception {
        try {
            UsersService userService = new UsersService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();
            UsersBO userBO = userService.getUsersById(NumberUtil.toNumber(AESUtil.decryption(userId)));
            model.addAttribute("userBO", userBO);

            List groupsList = groupPemissionService.getAllGroupsPermission();
            model.addAttribute("groupsList", groupsList);
            model.addAttribute("groupsListEdit", groupPemissionService.getAllGroupsPermission());

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return EDIT_USER_PAGE_REDIRECT;
    }

    @RequestMapping(value = USER_DETAIL_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preViewUserDetail(@RequestParam("userId") String userId, Model model, HttpServletRequest request) throws Exception {
        try {
            UsersService userService = new UsersService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();
            UsersBO userBO = userService.getUserById(NumberUtil.toNumber(AESUtil.decryption(userId)));
            List groupsList = groupPemissionService.getAllGroupsPermission();
            model.addAttribute("groupsList", groupsList);
            model.addAttribute("userBO", userBO);

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return USER_DETAIL_PAGE_REDIRECT;
    }

    private boolean validateUser(HttpServletRequest request, UsersForm form, String flg) throws Exception {
        if (StringUtils.isNullOrEmpty(form.getFullName())) {
            sendMessageError(request, "user.fullname.not.empty");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getUsername())) {
            sendMessageError(request, "user.username.not.empty");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getPassword()) && !"edit".equals(flg)) {
            sendMessageError(request, "user.password.not.empty");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getEmail())) {
            sendMessageError(request, "user.email.not.empty");
            return false;
        }
        if (!StringUtils.isValidEmailAddress(form.getEmail())) {
            sendMessageError(request, "email.config.validate.unformatted");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getGroupId())) {
            sendMessageError(request, "user.require.select.group.permission");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getGroupId())) {
            sendMessageError(request, "user.require.select.group.permission");
            return false;
        }
        if (StringUtils.isNullOrEmpty(form.getGroupId())) {
            sendMessageError(request, "user.require.select.group.permission");
            return false;
        }
//        HashMap hashMap = new HashMap();
//        UsersBO usersBO = null;
//        boolean chkAccount = false;
//        hashMap.put("username", form.getUsername());
//        List<UsersBO> lstUsersBO = userService.chkUser(hashMap);
//        if (!StringUtils.isNullOrEmpty(form.getUserId())) {
//            usersBO = userService.getUserById(form.getUserId());
//        }
//        if (!StringUtils.isNullOrEmpty(lstUsersBO)) {
//            if (lstUsersBO.size() > 1) {
//                sendMessageError(request, "user.account.exist");
//                return false;
//            }
//        }
//        if (!StringUtils.isNullOrEmpty(usersBO)) {
//            for (UsersBO bO : lstUsersBO) {
//                if (bO.getUserId().equals(form.getUserId())) {
//                    chkAccount = true;
//                }
//            }
//        }
//        if (!chkAccount && !StringUtils.isNullOrEmpty(lstUsersBO) && lstUsersBO.size() > 0) {
//            sendMessageError(request, "user.account.exist");
//            return false;
//        }

        return true;
    }
}
