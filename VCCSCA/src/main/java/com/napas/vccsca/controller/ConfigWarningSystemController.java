/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.form.ConfigWarningSystemForm;
import com.napas.vccsca.service.ConfigWarningSystemService;
import com.napas.vccsca.service.ParamService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * WarningController
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ConfigWarningSystemController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ConfigWarningSystemController.class);
    private static final String ALERT_PAGE_REDIRECT = "quan-ly-canh-bao.html";
    private static final String PRE_EDIT_ALERT_REDIRECT = "chinh-sua-canh-bao.html";
    private static final String VIEW_ALERT_REDIRECT = "chi-tiet-canh-bao.html";

    private static final String ADD_ALERT_ACTION = "createAlert.do";
    private static final String EDIT_ALERT_ACTION = "editAlert.do";
    private static final String DELETE_ALERT_ACTION = "deleteAlert.do";

//    private HashMap searchMap = null;
    private List listCreateUser = null;
    private List<ParamBO> paramBOs = null;

    /**
     * viewListConfigWarningSystem
     *
     * @configWarningSystem configWarningSystemForm
     * @configWarningSystem model
     * @configWarningSystem request
     * @configWarningSystem response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = ALERT_PAGE_REDIRECT, method = RequestMethod.GET)
    public String viewListConfigWarningSystem(@ModelAttribute("configWarningSystemForm") ConfigWarningSystemForm configWarningSystemForm,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START viewListConfigWarningSystem");
        try {
            ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
            ParamService paramService = new ParamService();

            listCreateUser = configWarningSystemService.getAllCreateUserConfigWarningSystem();
            paramBOs = paramService.findParam();

            configWarningSystemForm.setParams(paramBOs);
            configWarningSystemForm.setListCreateUser(listCreateUser);
            model.addAttribute("configWarningSystemForm", configWarningSystemForm);

            List<ConfigWarningSystemBO> list = configWarningSystemService.findConfigWarningSystem(null);
            model.addAttribute("listConfigWarningSystem", list);
            putActionLog(request, "Truy vấn thông tin cảnh báo");
            logger.info("END viewListConfigWarningSystem");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn thông tin cảnh báo");
        }
        return ALERT_PAGE_REDIRECT;
    }

    /**
     * searchListConfigWarningSystem
     *
     * @param configWarningSystemForm
     * @param model
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = ALERT_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchListConfigWarningSystem(@ModelAttribute("configWarningSystemForm") ConfigWarningSystemForm configWarningSystemForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START searchListConfigWarningSystem");
        ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
        try {
            HashMap searchMap = null;
            if (StringUtils.isNullOrEmpty(listCreateUser) || listCreateUser.size() == 0) {
                listCreateUser = configWarningSystemService.getAllCreateUserConfigWarningSystem();
            }
            configWarningSystemForm.setListCreateUser(listCreateUser);
            if (!StringUtils.isNullOrEmpty(configWarningSystemForm)) {
                if (StringUtils.isNullOrEmpty(searchMap)) {
                    searchMap = new HashMap();
                }
                searchMap.put("warningName", configWarningSystemForm.getWarningName());
                searchMap.put("createUser", configWarningSystemForm.getCreateUser());
                if (!StringUtils.isNullOrEmpty(configWarningSystemForm.getStatus())) {
                    searchMap.put("status", configWarningSystemForm.getStatus());
                } else {
                    searchMap.remove("status");
                }

                model.addAttribute("configWarningSystemForm", configWarningSystemForm);
            }
            List<ConfigWarningSystemBO> list = configWarningSystemService.findConfigWarningSystem(searchMap);
            model.addAttribute("listConfigWarningSystem", list);
            putActionLog(request, "Tìm kiếm thông tin cảnh báo");
            logger.info("END searchListConfigWarningSystem");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm thông tin cảnh báo");
        }
        return ALERT_PAGE_REDIRECT;
    }

    /**
     * createConfigWarningSystem
     *
     * @configWarningSystem configWarningSystemForm
     * @configWarningSystem request
     * @configWarningSystem response
     * @throws Exception
     */
    @RequestMapping(value = ADD_ALERT_ACTION, method = RequestMethod.POST)
    public void createConfigWarningSystem(@ModelAttribute("configWarningSystemForm") ConfigWarningSystemForm configWarningSystemForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START createConfigWarningSystem");
        try {
            ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
            if (!validate(request, configWarningSystemForm)) {
                sendMessageError(request, "configWarningSystem.create.error");
                response.sendRedirect(ALERT_PAGE_REDIRECT);
                return;
            }

            HashMap searchMap = new HashMap();
            searchMap.put("warningNameCheck", configWarningSystemForm.getWarningName());
            List<ConfigWarningSystemBO> bOs = configWarningSystemService.findConfigWarningSystem(searchMap);

            if (!StringUtils.isNullOrEmpty(bOs) && bOs.size() > 0) {
                sendMessageError(request, "configWarningSystem.name.exit");
                putActionErrorLog(request, "Tạo mới cảnh báo");
                response.sendRedirect(ALERT_PAGE_REDIRECT);
                return;
            }

            ConfigWarningSystemBO configWarningSystemBO = new ConfigWarningSystemBO();

            configWarningSystemBO.setType(configWarningSystemForm.getType());
            configWarningSystemBO.setWarningName(configWarningSystemForm.getWarningName());
            configWarningSystemBO.setDayConfig(configWarningSystemForm.getDayConfig());
            configWarningSystemBO.setMailTo(configWarningSystemForm.getMailTo());
            configWarningSystemBO.setMailCc(configWarningSystemForm.getMailCc());
            configWarningSystemBO.setMailBcc(configWarningSystemForm.getMailBcc());
//            configWarningSystemBO.setSendTime(configWarningSystemForm.getSendTime());
            configWarningSystemBO.setSendTime(configWarningSystemForm.getSendTime1() + ":" + configWarningSystemForm.getSendTime2());
            configWarningSystemBO.setSendNumber(configWarningSystemForm.getSendNumber());
            configWarningSystemBO.setStatus(configWarningSystemForm.getStatus());
            configWarningSystemBO.setSubject(configWarningSystemForm.getSubject());
            configWarningSystemBO.setContent(configWarningSystemForm.getContent());
            configWarningSystemBO.setParam(configWarningSystemForm.getParam());

            configWarningSystemBO.setCreateDate(new Date());
            configWarningSystemBO.setCreateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            configWarningSystemService.addConfigWarningSystem(configWarningSystemBO);

            sendMessageSuccess(request, "configWarningSystem.create.success");
            putActionLog(request, "Tạo mới cảnh báo");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "configWarningSystem.create.error");
            putActionErrorLog(request, "Tạo mới cảnh báo");
        }
        response.sendRedirect(ALERT_PAGE_REDIRECT);
        logger.info("END createConfigWarningSystem");
    }

    /**
     * preEditAlert
     *
     * @param idRequest
     * @param model
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = PRE_EDIT_ALERT_REDIRECT, method = RequestMethod.GET)
    public String preEditAlert(@RequestParam("idRequest") String idRequest, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START preEditAlert");
        ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
        ParamService paramService = new ParamService();
        ConfigWarningSystemForm configWarningSystemForm = new ConfigWarningSystemForm();
        try {
            if (StringUtils.isNullOrEmpty(paramBOs) || paramBOs.size() == 0) {
                paramBOs = paramService.findParam();
            }
            configWarningSystemForm.setParams(paramBOs);

            HashMap search = new HashMap();
            search.put("warningId", NumberUtil.toNumber(AESUtil.decryption(idRequest)));
            ConfigWarningSystemBO warningSystemBO = configWarningSystemService.getConfigWarningSystemById(search);

            if (!StringUtils.isNullOrEmpty(warningSystemBO)) {
                configWarningSystemForm.setWarningId(AESUtil.encryption(warningSystemBO.getWarningId() + ""));
                configWarningSystemForm.setType(warningSystemBO.getType());
                configWarningSystemForm.setWarningName(warningSystemBO.getWarningName());
                configWarningSystemForm.setDayConfig(warningSystemBO.getDayConfig());
                configWarningSystemForm.setMailTo(warningSystemBO.getMailTo());
                configWarningSystemForm.setMailCc(warningSystemBO.getMailCc());
                configWarningSystemForm.setMailBcc(warningSystemBO.getMailBcc());
                configWarningSystemForm.setSendNumber(warningSystemBO.getSendNumber());
                configWarningSystemForm.setStatus(warningSystemBO.getStatus());
//                configWarningSystemForm.setSendTime(warningSystemBO.getSendTime());
                String[] time = StringUtils.chgNull(warningSystemBO.getSendTime()).split(":");
                if (time.length == 2) {
                    configWarningSystemForm.setSendTime1(time[0]);
                    configWarningSystemForm.setSendTime2(time[1]);
                }

                configWarningSystemForm.setSubject(warningSystemBO.getSubject());
                configWarningSystemForm.setContent(warningSystemBO.getContent());
                configWarningSystemForm.setParam(warningSystemBO.getParam());
            }
            model.addAttribute("configWarningSystemForm", configWarningSystemForm);
            logger.info("END preEditAlert");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return PRE_EDIT_ALERT_REDIRECT;
    }

    /**
     * preEditAlert
     *
     * @param idRequest
     * @param model
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = VIEW_ALERT_REDIRECT, method = RequestMethod.GET)
    public String viewDetailAlert(@RequestParam("idRequest") String idRequest, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START preEditAlert");
        ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
        ParamService paramService = new ParamService();
        ConfigWarningSystemForm configWarningSystemForm = new ConfigWarningSystemForm();
        try {
            if (StringUtils.isNullOrEmpty(paramBOs) || paramBOs.size() == 0) {
                paramBOs = paramService.findParam();
            }
            configWarningSystemForm.setParams(paramBOs);

            HashMap search = new HashMap();
            search.put("warningId", NumberUtil.toNumber(AESUtil.decryption(idRequest)));
            ConfigWarningSystemBO warningSystemBO = configWarningSystemService.getConfigWarningSystemById(search);

            if (!StringUtils.isNullOrEmpty(warningSystemBO)) {
                configWarningSystemForm.setWarningId(AESUtil.encryption(warningSystemBO.getWarningId() + ""));
                configWarningSystemForm.setType(warningSystemBO.getType());
                configWarningSystemForm.setWarningName(warningSystemBO.getWarningName());
                configWarningSystemForm.setDayConfig(warningSystemBO.getDayConfig());
                configWarningSystemForm.setMailTo(warningSystemBO.getMailTo());
                configWarningSystemForm.setMailCc(warningSystemBO.getMailCc());
                configWarningSystemForm.setMailBcc(warningSystemBO.getMailBcc());
                configWarningSystemForm.setSendNumber(warningSystemBO.getSendNumber());
                configWarningSystemForm.setStatus(warningSystemBO.getStatus());

                String[] time = StringUtils.chgNull(warningSystemBO.getSendTime()).split(":");
                if (time.length == 2) {
                    configWarningSystemForm.setSendTime1(time[0]);
                    configWarningSystemForm.setSendTime2(time[1]);
                }

//                configWarningSystemForm.setSendTime(warningSystemBO.getSendTime());
                configWarningSystemForm.setSubject(warningSystemBO.getSubject());
                configWarningSystemForm.setContent(warningSystemBO.getContent());
                configWarningSystemForm.setParam(warningSystemBO.getParam());
            }
            model.addAttribute("configWarningSystemForm", configWarningSystemForm);
            putActionLog(request, "Xem chi tiết cảnh báo, id :" + NumberUtil.toNumber(AESUtil.decryption(idRequest)));
            logger.info("END preEditAlert");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Xem chi tiết cảnh báo, id :" + NumberUtil.toNumber(AESUtil.decryption(idRequest)));
        }
        return VIEW_ALERT_REDIRECT;
    }

    /**
     * editConfigWarningSystem
     *
     * @configWarningSystem configWarningSystemForm
     * @configWarningSystem request
     * @configWarningSystem response
     * @throws Exception
     */
    @RequestMapping(value = EDIT_ALERT_ACTION, method = RequestMethod.POST)
    public void editConfigWarningSystem(@ModelAttribute("configWarningSystemForm") ConfigWarningSystemForm configWarningSystemForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START createConfigWarningSystem");
        try {
            ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
            if (!validate(request, configWarningSystemForm)) {
                response.sendRedirect(ALERT_PAGE_REDIRECT);
                return;
            }

            HashMap search = new HashMap();
            search.put("warningId", NumberUtil.toNumber(AESUtil.decryption(configWarningSystemForm.getWarningId())));
            ConfigWarningSystemBO warningSystemBO = configWarningSystemService.getConfigWarningSystemById(search);

            if (!StringUtils.isNullOrEmpty(warningSystemBO)) {

                if (!configWarningSystemForm.getWarningName().equals(warningSystemBO.getWarningName())) {
                    HashMap searchMap = new HashMap();
                    searchMap.put("warningNameCheck", configWarningSystemForm.getWarningName());
                    List<ConfigWarningSystemBO> bOs = configWarningSystemService.findConfigWarningSystem(searchMap);

                    if (!StringUtils.isNullOrEmpty(bOs) && bOs.size() > 0) {
                        sendMessageError(request, "configWarningSystem.name.exit");
                        putActionErrorLog(request, "Tạo mới cảnh báo");
                        response.sendRedirect(ALERT_PAGE_REDIRECT);
                        return;
                    }
                }

                warningSystemBO.setType(configWarningSystemForm.getType());
                warningSystemBO.setWarningName(configWarningSystemForm.getWarningName());
                warningSystemBO.setDayConfig(configWarningSystemForm.getDayConfig());
                warningSystemBO.setMailTo(configWarningSystemForm.getMailTo());
                warningSystemBO.setMailCc(configWarningSystemForm.getMailCc());
                warningSystemBO.setMailBcc(configWarningSystemForm.getMailBcc());
                warningSystemBO.setSendNumber(configWarningSystemForm.getSendNumber());
                warningSystemBO.setSendTime(configWarningSystemForm.getSendTime1() + ":" + configWarningSystemForm.getSendTime2());
                warningSystemBO.setSubject(configWarningSystemForm.getSubject());
                warningSystemBO.setContent(configWarningSystemForm.getContent());
                warningSystemBO.setParam(configWarningSystemForm.getParam());
                warningSystemBO.setStatus(configWarningSystemForm.getStatus());
                warningSystemBO.setLastScan(null);

                warningSystemBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
                warningSystemBO.setUpdateDate(new Date());
                configWarningSystemService.updateConfigWarningSystem(warningSystemBO);

                sendMessageSuccess(request, "configWarningSystem.edit.success");
            } else {
                sendMessageError(request, "configWarningSystem.edit.error");
            }
            putActionLog(request, "Chỉnh sửa thông tin cảnh báo, id: " + NumberUtil.toNumber(AESUtil.decryption(configWarningSystemForm.getWarningId())));
        } catch (Exception e) {
            logger.error("configWarningSystem.edit.error", e);
            sendMessageError(request, "configWarningSystem.edit.error");
            putActionErrorLog(request, "Chỉnh sửa thông tin cảnh báo, id: " + NumberUtil.toNumber(AESUtil.decryption(configWarningSystemForm.getWarningId())));
        }
        response.sendRedirect(ALERT_PAGE_REDIRECT);
        logger.info("END createConfigWarningSystem");
    }

    /**
     * deleteConfigWarningSystem
     *
     * @configWarningSystem configWarningSystemId
     * @configWarningSystem request
     * @configWarningSystem response
     * @throws Exception
     */
    @RequestMapping(value = DELETE_ALERT_ACTION, method = RequestMethod.POST)
    public void deleteConfigWarningSystem(@RequestParam(value = "txtconfigWarningSystemId", required = true) String configWarningSystemId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START deleteConfigWarningSystem");
        try {
            ConfigWarningSystemService configWarningSystemService = new ConfigWarningSystemService();
            if (StringUtils.isNullOrEmpty(configWarningSystemId)) {
                sendMessageError(request, "configWarningSystem.delete.error");
                response.sendRedirect(ALERT_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("warningId", NumberUtil.toNumber(AESUtil.decryption(configWarningSystemId)));
            ConfigWarningSystemBO configWarningSystemBO = configWarningSystemService.getConfigWarningSystemById(hashMap);
            configWarningSystemService.deleteConfigWarningSystem(configWarningSystemBO);
            sendMessageSuccess(request, "configWarningSystem.delete.success");
            putActionLog(request, "Xóa thông tin cảnh báo, id: " + NumberUtil.toNumber(AESUtil.decryption(configWarningSystemId)));
        } catch (Exception e) {
            logger.error("configWarningSystem.delete.error", e);
            sendMessageError(request, "configWarningSystem.delete.error");
            putActionErrorLog(request, "Xóa thông tin cảnh báo, id: " + NumberUtil.toNumber(AESUtil.decryption(configWarningSystemId)));
        }
        response.sendRedirect(ALERT_PAGE_REDIRECT);
        logger.info("END deleteConfigWarningSystem");
    }

    /**
     * validate
     *
     * @configWarningSystem request
     * @configWarningSystem configWarningSystemForm
     * @return
     */
    private boolean validate(HttpServletRequest request, ConfigWarningSystemForm configWarningSystemForm) {
        if (StringUtils.isNullOrEmpty(configWarningSystemForm)) {
            sendMessageError(request, "configWarningSystem.not.configWarningSystem");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getWarningName())) {
            sendMessageError(request, "configWarningSystem.not.warningName");
            return false;
        }
        if (!StringUtils.checkSpecialCharacter(configWarningSystemForm.getWarningName())) {
            sendMessageError(request, "check.Special.Character.err");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getDayConfig())) {
            sendMessageError(request, "configWarningSystem.not.dayConfig");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getMailTo())) {
            sendMessageError(request, "configWarningSystem.not.mailTo");
            return false;
        }

        configWarningSystemForm.setSendTime(configWarningSystemForm.getSendTime1() + ":" + configWarningSystemForm.getSendTime2());
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getSendTime())) {
            sendMessageError(request, "configWarningSystem.not.sendTime");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getSendNumber()) || configWarningSystemForm.getSendNumber() < 1) {
            sendMessageError(request, "configWarningSystem.not.sendNumber");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getSubject())) {
            sendMessageError(request, "configWarningSystem.not.subject");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getContent())) {
            sendMessageError(request, "configWarningSystem.not.content");
            return false;
        }
//        if (StringUtils.isNullOrEmpty(configWarningSystemForm.getParam())) {
//            sendMessageError(request, "configWarningSystem.not.param");
//            return false;
//        }
        return true;
    }

}
