/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.service.RequestService;
import com.napas.vccsca.form.RequestForm;
import com.napas.vccsca.service.BankMembershipService;
import com.napas.vccsca.utils.ExcelUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * ReportRequestController
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ReportRequestController extends BaseBean implements Serializable {
//

    private static final Logger logger = LogManager.getLogger(EmailConfigController.class);
    private static final String REPORT_REQUEST_PAGE_REDIRECT = "bao-cao-yeu-cau-cap-cts.html";
    private static final String EXPORT_REQUEST_REPORT = "exportRequestReport.do";

    @RequestMapping(value = REPORT_REQUEST_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preReportRequest(@ModelAttribute("requestForm") RequestForm requestForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RequestService requestService = new RequestService();
            BankMembershipService bankingService = new BankMembershipService();
            HashMap searchMap = new HashMap();

            model.addAttribute("requestForm", requestForm);
            model.addAttribute("requestsList", requestService.getReportRequests(searchMap, requestForm.getArrBin()));
            model.addAttribute("banksList", bankingService.findAll());
            putActionLog(request, "Truy vấn dữ liệu báo cáo");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu báo cáo");
        }
        return REPORT_REQUEST_PAGE_REDIRECT;

    }

    @RequestMapping(value = REPORT_REQUEST_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchReportRequest(@ModelAttribute("requestForm") RequestForm requestForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RequestService requestService = new RequestService();
            BankMembershipService bankingService = new BankMembershipService();
            HashMap searchMap = new HashMap();
            searchMap.put("registerId", requestForm.getRegisterId());
            searchMap.put("appDate", requestForm.getAppDateSearch());

            String arrBin = ",";
            Integer[] arr = requestForm.getArrBin();
            if (!StringUtils.isNullOrEmpty(arr)) {
                for (Integer bin : requestForm.getArrBin()) {
                    arrBin += bin + ",";
                }
            }
            model.addAttribute("requestsList", requestService.getReportRequests(searchMap, arr));
            model.addAttribute("requestForm", requestForm);
            model.addAttribute("banksList", bankingService.findAll());
            model.addAttribute("lstSelectedBin", arrBin);
            putActionLog(request, "Tìm kiếm dữ liệu báo cáo");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu báo cáo");
        }
        return REPORT_REQUEST_PAGE_REDIRECT;

    }

    @RequestMapping(value = EXPORT_REQUEST_REPORT, method = RequestMethod.POST)
    public String exportReportRequest(@ModelAttribute("requestForm") RequestForm requestForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            RequestService requestService = new RequestService();
            BankMembershipService bankingService = new BankMembershipService();
            HashMap searchMap = new HashMap();

            searchMap.put("registerId", requestForm.getRegisterId());
            searchMap.put("appDate", requestForm.getAppDateSearch());

            String arrBin = ",";
            Integer[] arr = requestForm.getArrBin();
            if (!StringUtils.isNullOrEmpty(arr)) {
                for (Integer bin : requestForm.getArrBin()) {
                    arrBin += bin + ",";
                }
            }
            List<RequestBO> requestList = requestService.getReportRequests(searchMap, arr);
            model.addAttribute("requestsList", requestService.getReportRequests(searchMap, arr));

            model.addAttribute("requestForm", requestForm);
            model.addAttribute("requestsList", requestList);
            model.addAttribute("banksList", bankingService.findAll());
            model.addAttribute("lstSelectedBin", arrBin);

            Path path = Paths.get(tem_request_report);
            byte[] fileContent = ExcelUtil.writeFile(Files.readAllBytes(path), requestList, 8, "request");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "exportRequest_result.xlsx");
            response.setContentLength(fileContent.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            putActionLog(request, "Export dữ liệu báo cáo");
            sendMessageSuccess(request, "common.export.success");
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Export dữ liệu báo cáo");
        }
        return REPORT_REQUEST_PAGE_REDIRECT;
    }

}
