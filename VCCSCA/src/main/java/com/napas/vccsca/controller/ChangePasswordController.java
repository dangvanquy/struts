/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.form.ChangePasswordForm;
import com.napas.vccsca.service.UsersService;
import com.napas.vccsca.utils.AESUtil;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * ChangePasswordController
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ChangePasswordController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ChangePasswordController.class);
    private static final String CHANGE_PASSWORD_PAGE_REDIRECT = "doi-mat-khau.html";
    private static final String CHANGE_PASSWORD_ACTION = "changePass.do";
    private static final String PASSWORD_REGEX = "^[_A-z0-9]*$";
    private ChangePasswordForm changePasswordForm;
    private UsersBO usersBO;
    private Calendar calendar;
    private Date sqlDate;

    @PostConstruct
    public void init() {
        calendar = Calendar.getInstance();
        sqlDate = new java.sql.Date(calendar.getTime().getTime());
        changePasswordForm = new ChangePasswordForm();
        logger.info("ChangePasswordController init method");
    }

    @RequestMapping(value = CHANGE_PASSWORD_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getdata(@Valid @ModelAttribute("ChangePasswordForm") ChangePasswordForm changePasswordForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START get user info");
        UsersService usersService = new UsersService();
        usersBO = usersService.getUsersById(Integer.parseInt(AESUtil.gettokenLogin((String) request.getSession().getAttribute("tokenLogin"), 2)));

        logger.info("END get user info");
        return CHANGE_PASSWORD_PAGE_REDIRECT;
    }

    @RequestMapping(value = CHANGE_PASSWORD_ACTION, method = RequestMethod.POST)
    public void changePasswordForm(@Valid @ModelAttribute("ChangePasswordForm") ChangePasswordForm changePasswordForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (result.hasErrors()) {
            sendMessageError(request, "error");
            response.sendRedirect(CHANGE_PASSWORD_PAGE_REDIRECT);
            return;
        }

        try {
            logger.info("START change password");

            String username = request.getSession().getAttribute("userName").toString();

            if (validateForm(request, usersBO, changePasswordForm)) {

                UsersService usersService = new UsersService();
                usersBO.setPassword(AESUtil.encryptionPassword(changePasswordForm.getNewPassword()));
                usersBO.setUpdateUser(username);
                usersBO.setUpdateDate(sqlDate);

                usersService.updateUsers(usersBO);
                sendMessageSuccess(request, "change.password.success");
                putActionLog(request, "Thay đổi password cho tài khoản " + username);
            }
            logger.info("END change password");
        } catch (Exception e) {
            sendMessageError(request, "error");
            response.sendRedirect(CHANGE_PASSWORD_PAGE_REDIRECT);
            putActionErrorLog(request, "Thay đổi password cho tài khoản " + usersBO.getUsername());
            return;
        }
        response.sendRedirect(CHANGE_PASSWORD_PAGE_REDIRECT);
    }

    private Boolean validateForm(HttpServletRequest request, UsersBO usersBO, ChangePasswordForm changePasswordForm) throws Exception {
        Pattern p = Pattern.compile(PASSWORD_REGEX);
        if (!usersBO.getPassword().equals(AESUtil.encryptionPassword(changePasswordForm.getOldPassword()))) {
            sendMessageError(request, "change.password.old.validate.mismatched");
            return false;
        } else if (!p.matcher(changePasswordForm.getNewPassword()).matches()) {
            sendMessageError(request, "change.password.new.validate.unformatted");
            return false;
        } else if (changePasswordForm.getNewPassword().equals(changePasswordForm.getOldPassword())) {
            sendMessageError(request, "change.password.new.validate.duplicate");
            return false;
        } else if (!changePasswordForm.getNewPassword().equals(changePasswordForm.getConfirmPassword())) {
            sendMessageError(request, "change.password.confirm.validate.not.equal");
            return false;
        }
        return true;
    }
}
