/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.ActionLogBO;
import com.napas.vccsca.service.*;

import java.io.Serializable;
import com.napas.vccsca.form.FormBase;
//import com.napas.vccsca.utils.BundleUtil;
//import com.napas.vccsca.utils.RequestUtil;

import java.util.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.napas.vccsca.constants.Constants;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.MessageUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.inject.Named;
import org.springframework.ui.Model;

/**
 *
 * BaseBean
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Named(value = "baseBean")
public class BaseBean implements Serializable {

    private ActionHistoryService actionHistoryService = new ActionHistoryService();
    private static final Logger logger = LogManager.getLogger(BaseBean.class);
    private FormBase formBase;
//    private static List<ActionLogBO> actionLogBOs;

    private static ResourceBundle rb;
    static String tem_rootCa_report;
    static String tem_cert_report;
    static String tem_request_report;
    static String tem_nhtv_report;
    static String tem_cert_export;
    static String tem_req_export;
    static String tem_act_hit_export;

    static {
        try {
            rb = ResourceBundle.getBundle("config");

            tem_rootCa_report = rb.getString("temRootCareport");
            tem_cert_report = rb.getString("temCertreport");
            tem_request_report = rb.getString("temRequestreport");
            tem_nhtv_report = rb.getString("temNhtvreport");
            tem_cert_export = rb.getString("temCertExport");
            tem_req_export = rb.getString("temReqExport");
            tem_act_hit_export = rb.getString("temActionHistoryExport");
        } catch (MissingResourceException e) {
            logger.error("MissingResourceException: ", e);
        }
    }

    public String getMessage(String idMessage) {
        return MessageUtils.getMessage(idMessage);
    }

    public void sendMessageError(HttpServletRequest request, String message) {
        try {
            request.getSession().removeAttribute("successMessage");
            request.getSession().setAttribute("errMessage", MessageUtils.getMessage(message));
        } catch (Exception e) {
            request.getSession().setAttribute("errMessage", MessageUtils.getMessage("error"));
        }
    }

    public void sendMessageError(HttpServletRequest request, String message, String msg) {
        try {
            request.getSession().removeAttribute("successMessage");
            request.getSession().setAttribute("errMessage", MessageUtils.getMessage(message) + " : " + msg);
        } catch (Exception e) {
            request.getSession().setAttribute("errMessage", MessageUtils.getMessage("error"));
        }
    }

    public void sendMessageSuccess(HttpServletRequest request, String message) {
        try {
            request.getSession().removeAttribute("errMessage");
            request.getSession().setAttribute("successMessage", MessageUtils.getMessage(message));
        } catch (Exception e) {
            request.getSession().setAttribute("successMessage", MessageUtils.getMessage("error"));
        }
    }

    public void sendMessageError(Model model, String message) {
        try {
            model.addAttribute("errMessage", MessageUtils.getMessage(message));
            model.addAttribute("successMessage", "");
        } catch (Exception e) {
            model.addAttribute("errMessage", MessageUtils.getMessage("error"));
        }
    }

    public void sendMessageSuccess(Model model, String message) {
        model.addAttribute("errMessage", "");
        model.addAttribute("successMessage", MessageUtils.getMessage(message));
    }

    public void setModelAttribute(Model model, Object object) {
        try {
            model.addAttribute("dataObject", object);
        } catch (Exception e) {
            model.addAttribute("errMessage", MessageUtils.getMessage("error"));
        }
    }

    public void setModelAttribute(Model model, String key, Object object) {
        try {
            model.addAttribute(key, object);
        } catch (Exception e) {
            model.addAttribute("errMessage", MessageUtils.getMessage("error"));
        }
    }

    public boolean checkLength(Object obj, int length) {
        try {
            return obj.toString().length() <= length;
        } catch (Exception e) {
            throw e;
            //return false;
        }
    }

    public boolean isPositive(Object obj) {
        try {
            if (!isNullOrEmpty(obj)) {
                return Long.parseLong(obj.toString()) > 0;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * <p>
     * <p>
     * Created date: 09/08/2017 </p>
     *
     * @param obj
     * @return
     */
    public static boolean isNullOrEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if ((obj instanceof Collection && ((Collection) obj).isEmpty())
                || (obj.toString().trim().isEmpty() || Constants.FIRST_VALUE.equals(obj.toString().trim()))) {
            return true;
        }
        return false;
    }

    public boolean isNumber(Object obj) {
        try {
            Long.parseLong(obj.toString().trim());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public FormBase getFormBase() {
        return formBase;
    }

    public void setFormBase(FormBase formBase) {
        this.formBase = formBase;
    }

    public HttpServletRequest getRequest() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext extenalContext = facesContext.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) extenalContext.getRequest();
        return request;
    }

    public String getClientIP(HttpServletRequest request) {
        String ipAddress = request.getRemoteAddr();
        return ipAddress;
    }

    /**
     * putActionLog
     *
     * @param request
     */
    protected void putActionLog(HttpServletRequest request, String actionname) throws Exception {
        try {
//            if (StringUtils.isNullOrEmpty(actionLogBOs) || actionLogBOs.size() <= 0) {
//                actionLogBOs = new ArrayList<ActionLogBO>();
//            }
            int usrId = NumberUtil.toNumber(AESUtil.gettokenLogin((String) request.getSession().getAttribute("tokenLogin"), 2));
            ActionLogBO actionLogBO = new ActionLogBO();
            actionLogBO.setUserId(usrId == 0 ? 0 : usrId);
            actionLogBO.setIp(getClientIP(request));
            actionLogBO.setFunction(request.getRequestURI());
            actionLogBO.setStatus(0);
            actionLogBO.setLogType(usrId == 0 ? 1 : 0);//0:nguoi dung; 1:he thong
            actionLogBO.setDateTime(new Date());
            actionLogBO.setActionName(actionname);
            actionLogBO.setUserName((String) request.getSession().getAttribute("fullName"));
            if (StringUtils.isNullOrEmpty(actionHistoryService)) {
                actionHistoryService = new ActionHistoryService();
            }
            actionHistoryService.addActionHistory(actionLogBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    /**
     * putActionLog
     *
     * @param request
     */
    protected void putActionErrorLog(HttpServletRequest request, String actionname) {
        try {
//            if (StringUtils.isNullOrEmpty(actionLogBOs) || actionLogBOs.size() <= 0) {
//                actionLogBOs = new ArrayList<ActionLogBO>();
//            }
            int usrId = NumberUtil.toNumber(AESUtil.gettokenLogin((String) request.getSession().getAttribute("tokenLogin"), 2));
            ActionLogBO actionLogBO = new ActionLogBO();
            actionLogBO.setUserId(usrId == 0 ? 0 : usrId);
            actionLogBO.setIp(getClientIP(request));
            actionLogBO.setFunction(request.getRequestURI());
            actionLogBO.setStatus(1);
            actionLogBO.setLogType(usrId == 0 ? 1 : 0);//0:nguoi dung; 1:he thong
            actionLogBO.setDateTime(new Date());
            actionLogBO.setActionName(actionname);
            actionLogBO.setUserName((String) request.getSession().getAttribute("fullName"));

            if (StringUtils.isNullOrEmpty(actionHistoryService)) {
                actionHistoryService = new ActionHistoryService();
            }
            actionHistoryService.addActionHistory(actionLogBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * ccList
     *
     * @param ccAddress
     * @return
     */
    public List<String> ccList(String ccAddress) {
        if (StringUtils.isNullOrEmpty(ccAddress)) {
            return new ArrayList();
        }
        String[] strings = ccAddress.split(";");
        List listCc = new ArrayList();
        for (String cc : strings) {
            listCc.add(cc);
        }
        return listCc;
    }

    /**
     * bccList
     *
     * @param bccAddress
     * @return
     */
    public List<String> bccList(String bccAddress) {
        if (StringUtils.isNullOrEmpty(bccAddress)) {
            return new ArrayList();
        }
        String[] strings = bccAddress.split(";");
        List listBcc = new ArrayList();
        for (String bcc : strings) {
            listBcc.add(bcc);
        }
        return listBcc;
    }

    /**
     * toList
     *
     * @param toAddress
     * @return
     */
    public List<String> toList(String toAddress) {
        if (StringUtils.isNullOrEmpty(toAddress)) {
            return new ArrayList();
        }
        String[] strings = toAddress.split(";");
        List listTo = new ArrayList();
        for (String to : strings) {
            listTo.add(to);
        }
        return listTo;
    }

    /**
     * sha256
     *
     * @param input
     * @return
     * @throws Exception
     */
    static String sha256(byte[] input) throws Exception {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] result = mDigest.digest(input);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    /**
     * hexStringToByteArray
     *
     * @param s
     * @return
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * byteToHex
     *
     * @param bytes
     * @return
     */
    public static String byteToHex(byte[] bytes) {
        String reString = "";
        for (byte b : bytes) {
            reString += String.format("%02X", b);
        }
        return reString;
    }
    
    /**
     * SHA1
     * @param data
     * @return
     * @throws Exception 
     */
    public static byte[] hashByteArraySHA1(byte[] data) throws Exception
    {
        MessageDigest md = null;
        md = MessageDigest.getInstance("SHA-1");
        return md.digest(data);
    }

    /**
     * byteToHexArr
     *
     * @param bytes
     * @return
     */
    public static String[] byteToHexArr(byte[] bytes) {
        String[] hexArr = new String[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            hexArr[i] = String.format("%02X", bytes[i]);
        }
        return hexArr;
    }

}
