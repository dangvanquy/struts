/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.GroupsPermissionBO;
import com.napas.vccsca.BO.PermissionRightBO;
import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.constants.Constants;
import com.napas.vccsca.service.GroupPemissionService;
import com.napas.vccsca.service.PermissionRightService;
import com.napas.vccsca.constants.Constants.PERMISSION;
import com.napas.vccsca.form.GroupPermissionForm;
import com.napas.vccsca.service.UsersService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * PermissionController
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class PermissionController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(PermissionController.class);
    private static final String PERMISSION_PAGE_REDIRECT = "danh-sach-nhom-quyen.html";
    private static final String VIEW_PERMISSION_REDIRECT = "chi-tiet-nhom-quyen.html";
    private static final String PRE_EDIT_PERMISSION_REDIRECT = "chinh-sua-nhom-quyen.html";

    private static final String ADD_PERMISSION_ACTION = "createGroupPermission.do";
    private static final String EDIT_PERMISSION_ACTION = "editGroupPermission.do";
    private static final String DELETE_PERMISSION_ACTION = "deleteGroupPermission.do";

    private static Field[] fields;

    @PostConstruct
    public void init() {

        fields = PERMISSION.class.getDeclaredFields();
        logger.info("PermissionController init method");
    }

    @RequestMapping(value = PERMISSION_PAGE_REDIRECT, method = RequestMethod.GET)
    public String viewListGroupPemission(@ModelAttribute("groupPermissionForm") GroupPermissionForm groupPermissionForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START viewListGroupPemission");
        GroupPemissionService groupPemissionService = new GroupPemissionService();

        List<GroupsPermissionBO> list = groupPemissionService.getAllGroupsPermission();
        model.addAttribute("listPersons", list);
        logger.info("END viewListGroupPemission");
        return PERMISSION_PAGE_REDIRECT;
    }

    @RequestMapping(value = PERMISSION_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchListGroupPemission(@ModelAttribute("groupPermissionForm") GroupPermissionForm groupPermissionForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START viewListGroupPemission");
        HashMap searchMap = new HashMap();
        GroupPemissionService groupPemissionService = new GroupPemissionService();
        try {
            if (!StringUtils.isNullOrEmpty(groupPermissionForm)) {
                searchMap.put("groupName", groupPermissionForm.getGroupName());
                searchMap.put("status", groupPermissionForm.getStatus());
                model.addAttribute("groupPermissionForm", groupPermissionForm);
            }

            List<GroupsPermissionBO> list = groupPemissionService.findGroupsPermission(searchMap);
            model.addAttribute("listPersons", list);

            putActionLog(request, "Tìm kiếm danh sách quyền");
            logger.info("END viewListGroupPemission");
        } catch (Exception e) {
            putActionErrorLog(request, "Tìm kiếm danh sách quyền");
        }
        return PERMISSION_PAGE_REDIRECT;
    }

    @RequestMapping(value = PRE_EDIT_PERMISSION_REDIRECT, method = RequestMethod.GET)
    public String preEditPermission(@RequestParam("idRequest") String idRequest, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START preEditPermission");
        GroupPemissionService groupPemissionService = new GroupPemissionService();
        GroupPermissionForm groupPermissionForm = new GroupPermissionForm();
        GroupsPermissionBO groupsPermissionBO = groupPemissionService.getgroupPemissionById(NumberUtil.toNumber(AESUtil.decryption(idRequest)));
        try {
            if (!StringUtils.isNullOrEmpty(groupsPermissionBO)) {
                groupPermissionForm.setGroupId(AESUtil.encryption(groupsPermissionBO.getGroupId() + ""));
                groupPermissionForm.setGroupName(groupsPermissionBO.getGroupName());
                groupPermissionForm.setNote(groupsPermissionBO.getDescription());
                groupPermissionForm.setStatus(groupsPermissionBO.getStatus());

                List<PermissionRightBO> permissionRightBOs = new PermissionRightService().getPermissionById(groupsPermissionBO.getGroupId());
                String[] arGrPermissions = new String[permissionRightBOs.size()];
                for (int idx = 0; idx < permissionRightBOs.size(); idx++) {
                    arGrPermissions[idx] = Constants.PERMISSION.search(permissionRightBOs.get(idx).getPermissionCode());
                }
                groupPermissionForm.setGrPermissions(arGrPermissions);
            }
            model.addAttribute("groupPermissionForm", groupPermissionForm);

            logger.info("END preEditPermission");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return PRE_EDIT_PERMISSION_REDIRECT;
    }

    @RequestMapping(value = VIEW_PERMISSION_REDIRECT, method = RequestMethod.GET)
    public String viewPermission(@RequestParam("idRequest") String idRequest, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START viewPermission");
        GroupPemissionService groupPemissionService = new GroupPemissionService();
        GroupPermissionForm groupPermissionForm = new GroupPermissionForm();
        GroupsPermissionBO groupsPermissionBO = groupPemissionService.getgroupPemissionById(NumberUtil.toNumber(AESUtil.decryption(idRequest)));
        try {
            if (!StringUtils.isNullOrEmpty(groupsPermissionBO)) {
                request.getSession().setAttribute("groupId", AESUtil.encryption(groupsPermissionBO.getGroupId() + ""));
                groupPermissionForm.setGroupName(groupsPermissionBO.getGroupName());
                groupPermissionForm.setNote(groupsPermissionBO.getDescription());
                groupPermissionForm.setStatus(groupsPermissionBO.getStatus());

                List<PermissionRightBO> permissionRightBOs = new PermissionRightService().getPermissionById(groupsPermissionBO.getGroupId());
                String[] arGrPermissions = new String[permissionRightBOs.size()];
                for (int idx = 0; idx < permissionRightBOs.size(); idx++) {
                    arGrPermissions[idx] = Constants.PERMISSION.search(permissionRightBOs.get(idx).getPermissionCode().toString());
                }
                groupPermissionForm.setGrPermissions(arGrPermissions);
            }
            model.addAttribute("groupPermissionForm", groupPermissionForm);
            putActionLog(request, "Xem chi tiết nhóm quyền, id:" + NumberUtil.toNumber(AESUtil.decryption(idRequest)));
            logger.info("END viewPermission");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Xem chi tiết nhóm quyền, id:" + NumberUtil.toNumber(AESUtil.decryption(idRequest)));
        }
        return VIEW_PERMISSION_REDIRECT;
    }

    @RequestMapping(value = ADD_PERMISSION_ACTION, method = RequestMethod.POST)
    public void createNewGroupPermission(@Valid
            @ModelAttribute("groupPermissionForm") GroupPermissionForm groupPermissionForm,
            Model model,
            BindingResult result, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        logger.info("createNewGroupPermission START ");
        try {
            PermissionRightService permissionRightService = new PermissionRightService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();

            if (result.hasErrors()) {
                sendMessageError(request, "error");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }

            if (StringUtils.isNullOrEmpty(groupPermissionForm.getGroupName())) {
                sendMessageError(request, "permission.name.not.empty");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            if (!StringUtils.checkSpecialCharacter(groupPermissionForm.getGroupName())) {
                sendMessageError(request, "check.Special.Character.err");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            if (StringUtils.isNullOrEmpty(groupPermissionForm.getGrPermissions()) || groupPermissionForm.getGrPermissions().length <= 0) {
                sendMessageError(request, "permission.notpermission");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            if (!chkPermission(groupPermissionForm, request, groupPemissionService)) {
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            if (groupPemissionService.checkPermisionName(groupPermissionForm.getGroupName()) > 0l) {
                sendMessageError(request, "permission.permission.name.exist");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }

            GroupsPermissionBO groupsPermissionBO = new GroupsPermissionBO();
            groupsPermissionBO.setGroupName(groupPermissionForm.getGroupName());
            groupsPermissionBO.setDescription(groupPermissionForm.getNote());
            groupsPermissionBO.setStatus(groupPermissionForm.getStatus());

            groupsPermissionBO.setCreateUser((String) request.getSession().getAttribute("userName"));
            groupsPermissionBO.setCreateDate(new Date());
            groupPemissionService.addGroupsPermission(groupsPermissionBO);
            List<PermissionRightBO> lstPermissionRightBO = new ArrayList<PermissionRightBO>();
            PermissionRightBO rightBO;
            for (String role : groupPermissionForm.getGrPermissions()) {
                for (Field field : fields) {
                    if (role.equalsIgnoreCase(field.getName())) {
                        String[] f = (String[]) field.get(null);
                        rightBO = new PermissionRightBO();
                        rightBO.setGroupId(groupsPermissionBO.getGroupId());
                        rightBO.setPermissionCode(f[0]);
                        lstPermissionRightBO.add(rightBO);
                        break;
                    }
                }
            }
            permissionRightService.addListPermissionRight(lstPermissionRightBO);
            sendMessageSuccess(request, "permission.create.success");

            putActionLog(request, "Tạo mới nhóm quyền");
            response.sendRedirect(PERMISSION_PAGE_REDIRECT);
        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tạo mới nhóm quyền");
        }

        logger.info("createNewGroupPermission END");
    }

    @RequestMapping(value = EDIT_PERMISSION_ACTION, method = RequestMethod.POST)
    public void editGroupPermission(@Valid
            @ModelAttribute("groupPermissionForm") GroupPermissionForm groupPermissionForm,
            Model model,
            BindingResult result, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        logger.info("createNewGroupPermission START ");
        try {

            if (NumberUtil.toNumber(AESUtil.decryption(groupPermissionForm.getGroupId())) == 1) {
                sendMessageError(request, "permission.edit.admin");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            
            if (StringUtils.isNullOrEmpty(groupPermissionForm.getGroupName())) {
                sendMessageError(request, "permission.name.not.empty");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            if (!StringUtils.checkSpecialCharacter(groupPermissionForm.getGroupName())) {
                sendMessageError(request, "check.Special.Character.err");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }

            PermissionRightService permissionRightService = new PermissionRightService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();
            if (result.hasErrors()) {
                sendMessageError(request, "error");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }

            if (StringUtils.isNullOrEmpty(groupPermissionForm.getGrPermissions()) || groupPermissionForm.getGrPermissions().length <= 0) {
                sendMessageError(request, "permission.notpermission");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            if (!chkPermission(groupPermissionForm, request, groupPemissionService)) {
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }
            GroupsPermissionBO groupsPermissionBO = groupPemissionService.getgroupPemissionById(NumberUtil.toNumber(AESUtil.decryption(groupPermissionForm.getGroupId())));
            groupsPermissionBO.setGroupName(groupPermissionForm.getGroupName());
            groupsPermissionBO.setDescription(groupPermissionForm.getNote());
            groupsPermissionBO.setUpdateUser((String) request.getSession().getAttribute("userName"));
            groupsPermissionBO.setUpdateDate(new Date());
            groupsPermissionBO.setStatus(groupPermissionForm.getStatus());

//            groupsPermissionBO.setCreateUser((String) request.session.getAttribute("userName"));
//            groupsPermissionBO.setCreateDate(new Date());
            groupPemissionService.updateGroupsPermission(groupsPermissionBO);
            List<PermissionRightBO> lstPermissionRightBO = new ArrayList<PermissionRightBO>();
            PermissionRightBO rightBO;
            for (String role : groupPermissionForm.getGrPermissions()) {
                for (Field field : fields) {
                    if (role.equalsIgnoreCase(field.getName())) {
                        String[] f = (String[]) field.get(null);
                        rightBO = new PermissionRightBO();
                        rightBO.setGroupId(groupsPermissionBO.getGroupId());
                        rightBO.setPermissionCode(f[0]);
                        lstPermissionRightBO.add(rightBO);
                        break;
                    }
                }
            }
            permissionRightService.updateListPermissionRight(groupsPermissionBO.getGroupId(), lstPermissionRightBO);

            String userId = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 2);
            UsersBO usersBO = new UsersService().getUserById(NumberUtil.toNumber(userId));
            if (groupsPermissionBO.getGroupId() == usersBO.getGroupId()) {
                List<PermissionRightBO> permissionRightBOs = new PermissionRightService().getMenuPermission(usersBO.getGroupId());
                javax.servlet.http.HttpSession httpSession;
                httpSession = request.getSession();

                //xoa quyen cu
                Field[] fields = PERMISSION.class.getFields();
                for (int i = 0; i < fields.length; i++) {
                    try {
                        httpSession.setAttribute(fields[i].getName(), "false");
                    } catch (Exception e) {
                        continue;
                    }
                }
                //update quyen moi
                for (PermissionRightBO permissionRightBO : permissionRightBOs) {
                    httpSession.setAttribute(Constants.PERMISSION.search(permissionRightBO.getPermissionCode().toString()), "true");
                }
            }

            sendMessageSuccess(request, "permission.edit.success");
            putActionLog(request, "Chỉnh sửa nhóm quyền, id:" + NumberUtil.toNumber(AESUtil.decryption(groupPermissionForm.getGroupId())));

            response.sendRedirect(PERMISSION_PAGE_REDIRECT);
        } catch (Exception e) {
            sendMessageError(request, "permission.edit.error");
            response.sendRedirect(PERMISSION_PAGE_REDIRECT);
//            throw e;
            putActionErrorLog(request, "Chỉnh sửa nhóm quyền, id:" + NumberUtil.toNumber(AESUtil.decryption(groupPermissionForm.getGroupId())));
        }
        logger.info("createNewGroupPermission END");
    }

    @RequestMapping(value = DELETE_PERMISSION_ACTION, method = RequestMethod.POST)
    public void deleteGroupPermission(@RequestParam(value = "txtparamId", required = true) String paramId,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        logger.info("deleteGroupPermission START ");
        try {

            if (NumberUtil.toNumber(AESUtil.decryption(paramId)) == 1) {
                sendMessageError(request, "permission.delete.admin");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
                return;
            }

            PermissionRightService permissionRightService = new PermissionRightService();
            GroupPemissionService groupPemissionService = new GroupPemissionService();
            if (StringUtils.isNullOrEmpty(paramId)) {
                sendMessageError(request, "permission.delete.error");
                response.sendRedirect(PERMISSION_PAGE_REDIRECT);
            }

            HashMap hashMap = new HashMap();
            hashMap.put("groupId", NumberUtil.toNumber(AESUtil.decryption(paramId)));
//            List<UsersBO> ubos = new UsersService().getUser(hashMap);
//            if (StringUtils.isNullOrEmpty(ubos) || ubos.size() == 0) {
            GroupsPermissionBO groupsPermissionBO = groupPemissionService.getgroupPemissionById(NumberUtil.toNumber(AESUtil.decryption(paramId)));
            groupPemissionService.deleteGroupsPermission(groupsPermissionBO);
            permissionRightService.deletePermissionRightByGroupId(groupsPermissionBO.getGroupId());
            sendMessageSuccess(request, "permission.delete.success");
//            } else {
//                sendMessageSuccess(request, "permission.delete.hasUser");
//            }
            putActionLog(request, "Xóa nhóm quyền, id:" + NumberUtil.toNumber(AESUtil.decryption(paramId)));

            response.sendRedirect(PERMISSION_PAGE_REDIRECT);
        } catch (Exception e) {
            sendMessageError(request, "permission.delete.error");
            response.sendRedirect(PERMISSION_PAGE_REDIRECT);
            putActionErrorLog(request, "Xóa nhóm quyền, id:" + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        }

        logger.info("deleteGroupPermission END");
    }

    private boolean chkPermission(GroupPermissionForm groupPermissionForm, HttpServletRequest request, GroupPemissionService groupPemissionService) throws Exception {
        boolean isCheck = false;
        HashMap hashMap = new HashMap();
        GroupsPermissionBO groupsPermissionBO = null;
        hashMap.put("groupName", groupPermissionForm.getGroupName());
        List<GroupsPermissionBO> lstGroupsPermissionBOs = groupPemissionService.chkGroupsPermission(hashMap);
        if (!StringUtils.isNullOrEmpty(groupPermissionForm.getGroupId())) {
            groupsPermissionBO = groupPemissionService.getgroupPemissionById(NumberUtil.toNumber(AESUtil.decryption(groupPermissionForm.getGroupId())));
        }
        if (!StringUtils.isNullOrEmpty(lstGroupsPermissionBOs)) {
            if (lstGroupsPermissionBOs.size() > 1) {
                sendMessageError(request, "permission.groupname.does.exist");
                return false;
            }
        }
        if (!StringUtils.isNullOrEmpty(groupsPermissionBO)) {
            for (GroupsPermissionBO gpbo : lstGroupsPermissionBOs) {
                if (gpbo.getGroupId().equals(groupsPermissionBO.getGroupId())) {
                    isCheck = true;
                }
            }
        }
        if (!isCheck && !StringUtils.isNullOrEmpty(lstGroupsPermissionBOs) && lstGroupsPermissionBOs.size() > 0) {
            sendMessageError(request, "permission.groupname.does.exist");
            return false;
        }
        return true;
    }
}
