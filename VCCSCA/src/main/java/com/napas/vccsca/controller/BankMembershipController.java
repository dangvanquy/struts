/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.form.BankMembershipForm;
import com.napas.vccsca.service.BankMembershipService;
import com.napas.vccsca.service.RequestService;
import com.napas.vccsca.service.CertificateService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * BankMembershipController
 *
 * @author CuongTV
 * @since Aug 22, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class BankMembershipController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(BankMembershipController.class);
    private static final String CREATE_BANK_MEMBERSHIP_PAGE_REDIRECT = "tao-moi-nhtv.html";
    private static final String MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT = "quan-ly-nhtv.html";
    private static final String BANK_MEMBERSHIP_DETAIL_PAGE_REDIRECT = "xem-chi-tiet-nhtv.html";
    private static final String EDIT_MEMBERSHIP_BANKING_PAGE_REDIRECT = "chinh-sua-nhtv.html";

    @RequestMapping(value = "createBankMembership.do", method = RequestMethod.POST)
    public void createMembershipBanking(@ModelAttribute("bankMembershipForm") BankMembershipForm bankMembershipForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("BankMembershipController create membership bank method");
        try {
            BankMembershipService bankingService = new BankMembershipService();
            if (!validateCreateBankingForm(request, bankMembershipForm, bankingService)) {
                putActionErrorLog(request, "Thêm mới Ngân hàng thành viên");
                response.sendRedirect(MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT);
                return;
            }
            BankMembershipBO bankMembershipBO = bankMembershipForm.createBO();
            bankMembershipBO.setCreateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            bankMembershipBO.setCreateDate(new Date());
            bankingService.addBanking(bankMembershipBO);
            sendMessageSuccess(request, "banking.create.success");
            putActionLog(request, "Thêm mới Ngân hàng thành viên");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Thêm mới Ngân hàng thành viên");
        }
        response.sendRedirect(MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = "editBankMembership.do", method = RequestMethod.POST)
    public void editMembershipBanking(@ModelAttribute("bankMembershipForm") BankMembershipForm bankMembershipForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {

            BankMembershipService bankingService = new BankMembershipService();
            BankMembershipBO bankMembershipBO = bankingService.getMembershipBankingById(NumberUtil.toNumber(AESUtil.decryption(bankMembershipForm.getBankId())));

            if (!validateUpdateBankingForm(request, bankMembershipForm, bankingService, bankMembershipBO)) {
                putActionErrorLog(request, "Chỉnh sửa Ngân hàng, bin:" + bankMembershipForm.getBin());
                response.sendRedirect(MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT);
                return;
            }

            bankMembershipBO = bankMembershipForm.convertToBO(bankMembershipBO);
            bankMembershipBO.setUpdateUser(AESUtil.getUserLoginFromToken((String) request.getSession().getAttribute("tokenLogin")));
            bankMembershipBO.setUpdateDate(new Date());
            bankingService.updateBanking(bankMembershipBO);

            if (bankMembershipBO.getStatus() == 1) {
                new CertificateService().autoThuhoichungthu();
            }

            logger.info("END create new request");
            sendMessageSuccess(request, "banking.edit.success");
            putActionLog(request, "Chỉnh sửa Ngân hàng thành viên, bin:" + bankMembershipForm.getBin());

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Chỉnh sửa Ngân hàng thành viên, bin:" + bankMembershipForm.getBin());
        }
        response.sendRedirect(MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = "deleteBankMembership.do", method = RequestMethod.POST)
    public void deleteMembershipBanking(@RequestParam(value = "deleteBankId", required = true) String deleteBankId, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {

            BankMembershipService bankingService = new BankMembershipService();
            RequestService requestService = new RequestService();
            CertificateService certificateService = new CertificateService();

            BankMembershipBO bankMembershipBO = bankingService.getMembershipBankingById(NumberUtil.toNumber(AESUtil.decryption(deleteBankId)));

            HashMap hashMap = new HashMap();
            hashMap.put("bin", bankMembershipBO.getBin());
//            hashMap.put("statusne", 6);
            List<RequestBO> lstRequest = requestService.findRequest(hashMap);
            List<DigitalCertificateBO> lstCertificate = certificateService.getCertificates(hashMap);

            if (StringUtils.isNullOrEmpty(lstCertificate) && StringUtils.isNullOrEmpty(lstRequest)) {

                requestService.deleteRequestByBin(bankMembershipBO.getBin());
                bankingService.deleteBanking(bankMembershipBO);
                sendMessageSuccess(request, "banking.delete.success");
            } else if (!StringUtils.isNullOrEmpty(lstRequest)
                    && !StringUtils.isNullOrEmpty(lstCertificate)
                    && lstRequest.size() <= 0
                    && lstCertificate.size() <= 0) {

                requestService.deleteRequestByBin(bankMembershipBO.getBin());
                bankingService.deleteBanking(bankMembershipBO);
                sendMessageSuccess(request, "banking.delete.success");
            } else {
                sendMessageError(request, "banking.cannot.delete");
            }

            logger.info("END create new request");
            putActionLog(request, "Xóa Ngân hàng thành viên, bin: " + bankMembershipBO.getBin());

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Xóa Ngân hàng thành viên, id: " + NumberUtil.toNumber(AESUtil.decryption(deleteBankId)));
        }
        response.sendRedirect(MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT);
    }

    @RequestMapping(value = MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.GET)
    public String loadMembershipBankingManagement(@ModelAttribute("bankMembershipForm") BankMembershipForm bankMembershipForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {

            BankMembershipService bankingService = new BankMembershipService();

            List<BankMembershipBO> banksList = null;
            HashMap hashMap = new HashMap();
            hashMap.put("oderdesc", "createDate");
            banksList = bankingService.getMembershipBanking(hashMap, null);

            model.addAttribute("bankMembershipForm", bankMembershipForm);
            model.addAttribute("banksList", banksList);
            putActionLog(request, "Truy vấn dữ liệu Ngân hàng thành viên");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu Ngân hàng thành viên");
        }
        return MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchMembershipBanking(@ModelAttribute("bankMembershipForm") BankMembershipForm bankMembershipForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            BankMembershipService bankingService = new BankMembershipService();
            List<BankMembershipBO> banksList = null;
            HashMap hashMap = new HashMap();

            hashMap.put("bin", bankMembershipForm.getBin());
            hashMap.put("bankFullName", bankMembershipForm.getBankFullName().trim());
            hashMap.put("bankShortName", bankMembershipForm.getBankShortName().trim());

            banksList = bankingService.getMembershipBanking(hashMap, null);

            model.addAttribute("bankMembershipForm", hashMap);
            model.addAttribute("banksList", banksList);

            putActionLog(request, "Tìm kiếm dữ liệu Ngân hàng thành viên");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Tìm kiếm dữ liệu Ngân hàng thành viên");
        }
        return MEMBERSHIP_BANKING_MANAGEMENT_PAGE_REDIRECT;
    }

    @RequestMapping(value = EDIT_MEMBERSHIP_BANKING_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preEditMembershipBanking(@RequestParam("bankId") String bankId, Model model, HttpServletRequest request) throws Exception {
        try {
            BankMembershipService bankingService = new BankMembershipService();
            if (!StringUtils.isNullOrEmpty(bankId)) {
                BankMembershipBO membershipBO = bankingService.getMembershipBankingById(NumberUtil.toNumber(AESUtil.decryption(bankId)));
                model.addAttribute("membershipBO", membershipBO);
                putActionLog(request, "truy vấn dữ liệu ngân hàng thành viên, id:" + NumberUtil.toNumber(AESUtil.decryption(bankId)));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "truy vấn dữ liệu ngân hàng thành viên, id:" + NumberUtil.toNumber(AESUtil.decryption(bankId)));
        }
        return EDIT_MEMBERSHIP_BANKING_PAGE_REDIRECT;
    }

    @RequestMapping(value = BANK_MEMBERSHIP_DETAIL_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preViewMembershipBankDetail(@RequestParam("bankId") String bankId, Model model, HttpServletRequest request) throws Exception {
        try {
            BankMembershipService bankingService = new BankMembershipService();
            CertificateService certificateService = new CertificateService();
            if (!StringUtils.isNullOrEmpty(bankId)) {
                BankMembershipBO membershipBO = bankingService.getMembershipBankingById(NumberUtil.toNumber(AESUtil.decryption(bankId)));
                HashMap hashMap = new HashMap();
                hashMap.put("bin", membershipBO.getBin());
                List<DigitalCertificateBO> certificateLst = certificateService.getCertificates(hashMap);
                model.addAttribute("membershipBO", membershipBO);
                model.addAttribute("certificateList", certificateLst);
                putActionLog(request, "Truy vấn dữ liệu chi tiết NHTV, bin:" + membershipBO.getBin());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn dữ liệu chi tiết NHTV, id:" + NumberUtil.toNumber(AESUtil.decryption(bankId)));
        }
        return BANK_MEMBERSHIP_DETAIL_PAGE_REDIRECT;
    }

    private Boolean validateCreateBankingForm(HttpServletRequest request, BankMembershipForm bankingForm, BankMembershipService bankingService) throws Exception {
        try {
            if (StringUtils.isNullOrEmpty(bankingForm.getBin())) {
                sendMessageError(request, "bin.not.empty");
                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getBankFullName())) {
                sendMessageError(request, "bank.full.name.not.empty");
                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getBankFullName())) {
                sendMessageError(request, "bank.short.name.not.empty");
                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getRepresent1())) {
                bankingForm.setRepresent1("");
//                sendMessageError(request, "represent.name.not.empty");
//                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getEmail1())) {
                bankingForm.setEmail1("");
//                sendMessageError(request, "represent.email.not.empty");
//                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getPhoneNumber1())) {
                bankingForm.setPhoneNumber1("");
//                sendMessageError(request, "represent.phone.number.not.empty");
//                return false;
            }
            if (!StringUtils.isNullOrEmpty(bankingForm.getBin())) {
                HashMap hashMap = new HashMap();
                hashMap.put("bin", bankingForm.getBin());
                List<BankMembershipBO> lstBankMembershipBOs = bankingService.chkBankMembership(hashMap);
                if (!StringUtils.isNullOrEmpty(lstBankMembershipBOs) && lstBankMembershipBOs.size() > 0) {
                    sendMessageError(request, "banking.bin.exist");
                    return false;
                }
            }
            if (!StringUtils.isNullOrEmpty(bankingForm.getBankFullName())) {
                HashMap hashMap = new HashMap();
                hashMap.put("bankFullName", bankingForm.getBankFullName().trim());
                List<BankMembershipBO> lstBankMembershipBOs = bankingService.chkBankMembership(hashMap);
                if (!StringUtils.isNullOrEmpty(lstBankMembershipBOs) && lstBankMembershipBOs.size() > 0) {
                    sendMessageError(request, "banking.bankName.exist");
                    return false;
                }
            }
//            if (!StringUtils.isNullOrEmpty(bankingForm.getBankShortName())) {
//                HashMap hashMap = new HashMap();
//                hashMap.put("bankShortName", bankingForm.getBankShortName().trim());
//                List<BankMembershipBO> lstBankMembershipBOs = bankingService.chkBankMembership(hashMap);
//                if (!StringUtils.isNullOrEmpty(lstBankMembershipBOs) && lstBankMembershipBOs.size() > 0) {
//                    sendMessageError(request, "banking.shortName.exist");
//                    return false;
//                }
//            }
            if (!StringUtils.checkSignInVietnamese(bankingForm.getBankShortName())) {
                sendMessageError(request, "banking.shortName.not.use.unicode");
                return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return true;
    }

    private Boolean validateUpdateBankingForm(HttpServletRequest request, BankMembershipForm bankingForm, BankMembershipService bankingService, BankMembershipBO bankMembershipBOB) throws Exception {
        HashMap hashMap = null;
        List<BankMembershipBO> lstBankMembershipBOs = null;
        try {
            if (StringUtils.isNullOrEmpty(bankingForm.getBin())) {
                sendMessageError(request, "bin.not.empty");
                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getBankFullName())) {
                sendMessageError(request, "bank.full.name.not.empty");
                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getBankFullName())) {
                sendMessageError(request, "bank.short.name.not.empty");
                return false;
            }
            
             if (StringUtils.isNullOrEmpty(bankingForm.getRepresent1())) {
                bankingForm.setRepresent1("");
//                sendMessageError(request, "represent.name.not.empty");
//                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getEmail1())) {
                bankingForm.setEmail1("");
//                sendMessageError(request, "represent.email.not.empty");
//                return false;
            }
            if (StringUtils.isNullOrEmpty(bankingForm.getPhoneNumber1())) {
                bankingForm.setPhoneNumber1("");
//                sendMessageError(request, "represent.phone.number.not.empty");
//                return false;
            }
//            if (StringUtils.isNullOrEmpty(bankingForm.getRepresent1())) {
//                sendMessageError(request, "represent.name.not.empty");
//                return false;
//            }
//            if (StringUtils.isNullOrEmpty(bankingForm.getEmail1())) {
//                sendMessageError(request, "represent.email.not.empty");
//                return false;
//            }
//            if (StringUtils.isNullOrEmpty(bankingForm.getPhoneNumber1())) {
//                sendMessageError(request, "represent.phone.number.not.empty");
//                return false;
//            }

            if (!StringUtils.isNullOrEmpty(bankingForm.getBankFullName())
                    && !bankingForm.getBankFullName().equals(bankMembershipBOB.getBankFullName())) {
                hashMap = new HashMap();
                hashMap.put("bankFullName", bankingForm.getBankFullName().trim());
                lstBankMembershipBOs = bankingService.chkBankMembership(hashMap);
                if (!StringUtils.isNullOrEmpty(lstBankMembershipBOs)) {
                    for (BankMembershipBO bankMembershipBO : lstBankMembershipBOs) {
                        if (!bankingForm.getBankId().equals(bankMembershipBO.getBankId())) {
                            sendMessageError(request, "banking.fullName.exist");
                            return false;
                        }
                    }
                }
            }
//            if (!StringUtils.isNullOrEmpty(bankingForm.getBankShortName())
//                    && !bankingForm.getBankShortName().equals(bankMembershipBOB.getBankShortName())) {
//                hashMap = new HashMap();
//                hashMap.put("bankShortName", bankingForm.getBankShortName().trim());
//                lstBankMembershipBOs = bankingService.chkBankMembership(hashMap);
//                if (!StringUtils.isNullOrEmpty(lstBankMembershipBOs)) {
//                    for (BankMembershipBO bankMembershipBO : lstBankMembershipBOs) {
//                        if (!bankingForm.getBankId().equals(bankMembershipBO.getBankId())) {
//                            sendMessageError(request, "banking.shortName.exist");
//                            return false;
//                        }
//                    }
//                }
//            }
            if (bankingForm.getBin().intValue() != bankMembershipBOB.getBin().intValue()) {
                hashMap = new HashMap();
                hashMap.put("bin", bankingForm.getBin());
                lstBankMembershipBOs = bankingService.chkBankMembership(hashMap);
                if (!StringUtils.isNullOrEmpty(lstBankMembershipBOs)) {
                    for (BankMembershipBO bankMembershipBO : lstBankMembershipBOs) {
                        if (!bankingForm.getBankId().equals(bankMembershipBO.getBankId())) {
                            sendMessageError(request, "banking.bin.exist");
                            return false;
                        }
                    }
                }
            }
            if (!StringUtils.checkSignInVietnamese(bankingForm.getBankShortName())) {
                sendMessageError(request, "banking.shortName.not.use.unicode");
                return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            sendMessageError(request, ex.getMessage());
        }
        return true;
    }
}
