/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.ConfigHsmBO;
import com.napas.vccsca.form.ConfigHsmForm;
import com.napas.vccsca.service.ConfigHsmService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * ConfigHsmController
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ConfigHsmController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ConfigHsmController.class);
    private static final String CONFIG_HSM_PAGE_REDIRECT = "cau-hinh-hsm.html";

    private static final String ADD_CONFIG_HSM_ACTION = "createConfigHsm.do";
    private static final String EDIT_CONFIG_HSM_ACTION = "editConfigHsm.do";
    private static final String DELETE_CONFIG_HSM_ACTION = "deleteConfigHsm.do";

    /**
     * viewListConfigHsm
     *
     * @configHsm model
     * @configHsm request
     * @configHsm response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = CONFIG_HSM_PAGE_REDIRECT, method = RequestMethod.GET)
    public String viewListConfigHsm(Model model, HttpServletRequest request) throws Exception {
        logger.info("START viewListConfigHsm");
        ConfigHsmService configHsmService = new ConfigHsmService();
//        List<ConfigHsmBO> list = configHsmService.findConfigHsm();
        List<ConfigHsmBO> list = configHsmService.findConfigHsmDESC();
        model.addAttribute("listConfigHsm", list);
        putActionLog(request, "Truy vấn dữ liệu Config HSM");
        logger.info("END viewListConfigHsm");
        return CONFIG_HSM_PAGE_REDIRECT;
    }

    /**
     * createConfigHsm
     *
     * @configHsm configHsmForm
     * @configHsm request
     * @configHsm response
     * @throws Exception
     */
    @RequestMapping(value = ADD_CONFIG_HSM_ACTION, method = RequestMethod.POST)
    public void createConfigHsm(@ModelAttribute("configHsmForm") ConfigHsmForm configHsmForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START createConfigHsm");
        try {
            ConfigHsmService configHsmService = new ConfigHsmService();
            if (!validate(request, configHsmForm, configHsmService)) {
                putActionErrorLog(request, "Thêm mới dữ liệu Config HSM");
                response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("slot", configHsmForm.getSlot());
            if (configHsmService.getConfigHsm(hashMap) > 0) {
                sendMessageError(request, "configHsm.slot.exist");
                putActionErrorLog(request, "Thêm mới dữ liệu Config HSM");
                response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
                return;
            }
            ConfigHsmBO configHsmBO = new ConfigHsmBO();
            configHsmBO.setSlot(NumberUtil.toNumber(configHsmForm.getSlot()));
            configHsmBO.setPassword(AESUtil.encryption(configHsmForm.getPassword()));
            configHsmBO.setCreateDate(new Date());

            configHsmService.addConfigHsm(configHsmBO);
            sendMessageSuccess(request, "configHsm.create.success");
            putActionLog(request, "Thêm mới dữ liệu Config HSM");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "configHsm.create.error");
            putActionErrorLog(request, "Thêm mới dữ liệu Config HSM");
        }
        response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
        logger.info("END createConfigHsm");
    }

    /**
     * editConfigHsm
     *
     * @configHsm configHsmForm
     * @configHsm request
     * @configHsm response
     * @throws Exception
     */
    @RequestMapping(value = EDIT_CONFIG_HSM_ACTION, method = RequestMethod.POST)
    public void editConfigHsm(@ModelAttribute("configHsmForm") ConfigHsmForm configHsmForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START createConfigHsm");
        try {
            ConfigHsmService configHsmService = new ConfigHsmService();
            if (!validate(request, configHsmForm, configHsmService)) {
                response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("hsmId", NumberUtil.toNumber(AESUtil.decryption(configHsmForm.getHsmId())));
            ConfigHsmBO configHsmBO = configHsmService.getConfigHsmById(hashMap);
            configHsmBO.setSlot(NumberUtil.toNumber(configHsmForm.getSlot()));
            configHsmBO.setPassword(AESUtil.encryption(configHsmForm.getPassword()));
            configHsmBO.setUpdateDate(new Date());
            
            configHsmService.updateConfigHsm(configHsmBO);
            sendMessageSuccess(request, "configHsm.edit.success");
            putActionLog(request, "Chỉnh sửa Slot HSM, id:" + NumberUtil.toNumber(AESUtil.decryption(configHsmForm.getHsmId())));
        } catch (Exception e) {
            logger.error("configHsm.edit.error", e);
            sendMessageError(request, "configHsm.edit.error");
            putActionErrorLog(request, "Chỉnh sửa Slot HSM, id:" + NumberUtil.toNumber(AESUtil.decryption(configHsmForm.getHsmId())));
        }
        response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
        logger.info("END createConfigHsm");
    }

    /**
     * deleteConfigHsm
     *
     * @configHsm hsmId
     * @configHsm request
     * @configHsm response
     * @throws Exception
     */
    @RequestMapping(value = DELETE_CONFIG_HSM_ACTION, method = RequestMethod.POST)
    public void deleteConfigHsm(@RequestParam(value = "txtHsmId", required = true) String hsmId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START deleteConfigHsm");
        try {
            ConfigHsmService configHsmService = new ConfigHsmService();
            if (StringUtils.isNullOrEmpty(hsmId)) {
                sendMessageError(request, "configHsm.delete.error");
                response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("hsmId", NumberUtil.toNumber(AESUtil.decryption(hsmId)));
            ConfigHsmBO configHsmBO = configHsmService.getConfigHsmById(hashMap);
            configHsmService.deleteConfigHsm(configHsmBO);
            sendMessageSuccess(request, "configHsm.delete.success");
            putActionLog(request, "Xóa Slot HSM, id:" + NumberUtil.toNumber(AESUtil.decryption(hsmId)));

        } catch (Exception e) {
            logger.error("configHsm.delete.error", e);
            sendMessageError(request, "configHsm.delete.error");
            putActionErrorLog(request, "Xóa Slot HSM, id:" + NumberUtil.toNumber(AESUtil.decryption(hsmId)));
        }
        response.sendRedirect(CONFIG_HSM_PAGE_REDIRECT);
        logger.info("END deleteConfigHsm");
    }

    /**
     * validate
     *
     * @configHsm request
     * @configHsm configHsmForm
     * @return
     */
    private boolean validate(HttpServletRequest request, ConfigHsmForm configHsmForm, ConfigHsmService configHsmService) {
        if (StringUtils.isNullOrEmpty(configHsmForm)) {
            sendMessageError(request, "configHsm.not.configHsm");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configHsmForm.getSlot())) {
            sendMessageError(request, "configHsm.not.configHsmSlot");
            return false;
        }
        if (StringUtils.isNullOrEmpty(configHsmForm.getPassword())) {
            sendMessageError(request, "configHsm.not.password");
            return false;
        }
        if (!StringUtils.isNumber(configHsmForm.getSlot())) {
            sendMessageError(request, "configHsm.validate.slot");
            return false;
        }
        if (configHsmForm.getSlot().length() > 10) {
            sendMessageError(request, "configHsm.validate.slot.len");
            return false;
        }
        if (configHsmForm.getPassword().length() > 200) {
            sendMessageError(request, "configHsm.validate.pass.len");
            return false;
        }
        if (Integer.parseInt(configHsmForm.getSlot()) < 0) {
            sendMessageError(request, "configHsm.validate.slot2");
            return false;
        }
        return true;
    }

}
