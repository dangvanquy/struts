/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.form.GroupPermissionForm;
import com.napas.vccsca.form.ParamForm;
import com.napas.vccsca.service.ParamService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * ParamController
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class ParamController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(ParamController.class);
    private static final String PARAM_PAGE_REDIRECT = "cau-hinh-tham-so.html";

    private static final String ADD_PARAM_ACTION = "createParam.do";
    private static final String EDIT_PARAM_ACTION = "editParam.do";
    private static final String DELETE_PARAM_ACTION = "deleteParam.do";

    /**
     * viewListParam
     *
     * @param groupPermissionForm
     * @param model
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = PARAM_PAGE_REDIRECT, method = RequestMethod.GET)
    public String viewListParam(@ModelAttribute("groupPermissionForm") GroupPermissionForm groupPermissionForm, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START viewListParam");
        ParamService paramService = new ParamService();
        List<ParamBO> list = paramService.findParam();
        model.addAttribute("listParam", list);
        putActionLog(request, "Truy vấn thông tin Param");
        logger.info("END viewListParam");
        return PARAM_PAGE_REDIRECT;
    }

    /**
     * createParam
     *
     * @param paramForm
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = ADD_PARAM_ACTION, method = RequestMethod.POST)
    public void createParam(@ModelAttribute("paramForm") ParamForm paramForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START createParam");
        try {
            ParamService paramService = new ParamService();
            if (!validate(request, paramForm, paramService, "")) {
                response.sendRedirect(PARAM_PAGE_REDIRECT);
                return;
            }

            ParamBO paramBO = new ParamBO();
            paramBO.setParamName(paramForm.getParamName());
            paramBO.setTableName(paramForm.getTableName());
            paramBO.setColumnValue(paramForm.getColumnValue());

            paramService.addParam(paramBO);
            sendMessageSuccess(request, "param.create.success");
            putActionLog(request, "Tạo mới Param");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "param.create.error");
            putActionErrorLog(request, "Tạo mới Param");
        }
        response.sendRedirect(PARAM_PAGE_REDIRECT);
        logger.info("END createParam");
    }

    /**
     * editParam
     *
     * @param paramForm
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = EDIT_PARAM_ACTION, method = RequestMethod.POST)
    public void editParam(@ModelAttribute("paramForm") ParamForm paramForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START createParam");
        try {
            ParamService paramService = new ParamService();
            if (!validate(request, paramForm, paramService, "edit"
            )) {
                response.sendRedirect(PARAM_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("paramId", NumberUtil.toNumber(AESUtil.decryption(paramForm.getParamId())));
            ParamBO paramBO = paramService.getParamById(hashMap);

            if (!paramForm.getParamName().equals(paramBO.getParamName())) {
                hashMap.clear();
                hashMap = new HashMap();
                hashMap.put("paramName", paramForm.getParamName());
                if (paramService.getParams(hashMap) > 0) {
                    sendMessageError(request, "param.does.exist");
                    response.sendRedirect(PARAM_PAGE_REDIRECT);
                    return;
                }
            }

            if (!paramForm.getTableName().equals(paramBO.getTableName())
                    || !paramForm.getColumnValue().equals(paramBO.getColumnValue())) {
                hashMap = new HashMap();
                hashMap.put("tableName", paramForm.getTableName());
                hashMap.put("columnValue", paramForm.getColumnValue());
                if (paramService.getParams(hashMap) > 0) {
                    sendMessageError(request, "param.does.exist2");
                    response.sendRedirect(PARAM_PAGE_REDIRECT);
                    return;
                }
            }

            paramBO.setParamName(paramForm.getParamName());
            paramBO.setTableName(paramForm.getTableName());
            paramBO.setColumnValue(paramForm.getColumnValue());

            paramService.updateParam(paramBO);
            sendMessageSuccess(request, "param.edit.success");
            putActionLog(request, "Edit Param, id: " + NumberUtil.toNumber(AESUtil.decryption(paramForm.getParamId())));
        } catch (Exception e) {
            logger.error("param.edit.error", e);
            sendMessageError(request, "param.edit.error");
            putActionErrorLog(request, "Edit Param, id: " + NumberUtil.toNumber(AESUtil.decryption(paramForm.getParamId())));
        }
        response.sendRedirect(PARAM_PAGE_REDIRECT);
        logger.info("END createParam");
    }

    /**
     * deleteParam
     *
     * @param paramId
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = DELETE_PARAM_ACTION, method = RequestMethod.POST)
    public void deleteParam(@RequestParam(value = "txtparamId", required = true) String paramId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("START deleteParam");
        try {
            ParamService paramService = new ParamService();
            if (StringUtils.isNullOrEmpty(paramId)) {
                sendMessageError(request, "param.delete.error");
                response.sendRedirect(PARAM_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("paramId", NumberUtil.toNumber(AESUtil.decryption(paramId)));
            ParamBO paramBO = paramService.getParamById(hashMap);
            paramService.deleteParam(paramBO);
            sendMessageSuccess(request, "param.delete.success");
            putActionLog(request, "Xóa Param, id: " + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        } catch (Exception e) {
            logger.error("param.delete.error", e);
            sendMessageError(request, "param.delete.error");
            putActionErrorLog(request, "Xóa Param, id: " + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        }
        response.sendRedirect(PARAM_PAGE_REDIRECT);
        logger.info("END deleteParam");
    }

    /**
     * validate
     *
     * @param request
     * @param paramForm
     * @return
     */
    private boolean validate(HttpServletRequest request, ParamForm paramForm, ParamService paramService, String flg) {
        if (StringUtils.isNullOrEmpty(paramForm)) {
            sendMessageError(request, "param.not.param");
            return false;
        }
        if (StringUtils.isNullOrEmpty(paramForm.getParamName()) || "0".equals(paramForm.getParamName())) {
            sendMessageError(request, "param.not.paramName");
            return false;
        }
        if (!StringUtils.checkSpecialCharacter(paramForm.getParamName())) {
            sendMessageError(request, "check.Special.Character.err");
            return false;
        }
        if (StringUtils.isNullOrEmpty(paramForm.getTableName()) || "0".equals(paramForm.getTableName())) {
            sendMessageError(request, "param.not.tableName");
            return false;
        }
        if (StringUtils.isNullOrEmpty(paramForm.getColumnValue()) || "0".equals(paramForm.getColumnValue())) {
            sendMessageError(request, "param.not.columeName");
            return false;
        }
        if (!StringUtils.checkSpecialCharacter(paramForm.getParamName())) {
            sendMessageError(request, "param.name.not.special.character");
            return false;
        }

        if (!flg.equals("edit")) {
            HashMap hashMap = new HashMap();
            hashMap.put("paramName", paramForm.getParamName());
            if (paramService.getParams(hashMap) > 0) {
                sendMessageError(request, "param.does.exist");
                return false;
            }

            hashMap = new HashMap();
            hashMap.put("tableName", paramForm.getTableName());
            hashMap.put("columnValue", paramForm.getColumnValue());
            if (paramService.getParams(hashMap) > 0) {
                sendMessageError(request, "param.does.exist2");
                return false;
            }
        }
        return true;
    }

}
