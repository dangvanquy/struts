/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

//import ISO9796_2.EMV_Cert_Request;
//import ISO9796_2.EMVcert;
import ISO9796_2.EMV_Cert_Request;
import ISO9796_2.EMVcert;
import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.BO.ConfigHsmBO;
import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.constants.Constants;
import static com.napas.vccsca.controller.BaseBean.tem_req_export;
import com.napas.vccsca.form.CreateRequestForm;
import com.napas.vccsca.form.EditRequestForm;
import com.napas.vccsca.service.RequestService;
import com.napas.vccsca.form.RequestForm;
import com.napas.vccsca.service.BankMembershipService;
import com.napas.vccsca.service.CertificateService;
import com.napas.vccsca.service.ConfigHsmService;
import com.napas.vccsca.service.RSAKeyService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.MessageUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * RequestController
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class RequestController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(EmailConfigController.class);
    private static final String REQUEST_PAGE_REDIRECT = "quan-ly-yeu-cau.html";
    private static final String REQUEST_FILE_PAGE_REDIRECT = "mau-yeu-cau.html";
    private static final String RESTORE_REQUEST_PAGE_REDIRECT = "danh-sach-yeu-cau-da-xoa.html";
    private static final String REQUEST_DETAIL_PAGE_REDIRECT = "xem-chi-tiet-yeu-cau.html";
    private static final String IMPORT_PAGE_REDIRECT = "tai-yeu-cau.html";

    private static final String CREATE_REQUEST_FILE_PAGE_REDIRECT = "tao-mau-yeu-cau.html";
//    private static final String CREATE_REQUEST_PAGE_REDIRECT = "tao-yeu-cau.html";
    private static final String EDIT_REQUEST_PAGE_REDIRECT = "sua-yeu-cau.html";
    private static final String SUBMISSION_REQUEST_PAGE_REDIRECT = "ky-duyet-yeu-cau.html";

    private static final String CREATE_REQUEST_FILE_PAGE_ACTION = "createIndex.do";
    private static final String CREATE_REQUEST_PAGE_ACTION = "createRequest.do";
    private static final String EDIT_REQUEST_PAGE_ACTION = "editRequest.do";
    private static final String SUBMISSION_REQUEST_PAGE_ACTION = "submissionRequest.do";
    private static final String APPROVE_REQUEST_PAGE_ACTION = "approvalRequest.do";
    private static final String REJECT_REQUEST_PAGE_ACTION = "rejectRequest.do";
    private static final String DELETE_REQUEST_ACTION = "deleteRequest.do";
    private static final String RESTORE_REQUEST_ACTION = "rqdelete.do";
    private static final String EXPORT_REQUEST_ACTION = "exportIndex.do";
    private static final String IMPORT_REQUEST_ACTION = "importRequest.do";

    private RequestForm requestForm;
    private RequestBO requestBO;
    private List<RequestBO> requestListSearch;
    private String requestRegId;

    /**
     * for getHexbyte function
     */
    static int countIndex = 0;
    private HashMap<String, String> hexDataRequest;

    @PostConstruct
    public void init() {
        requestForm = new RequestForm();
        logger.info("RequestController init method");
    }

    @RequestMapping(value = REQUEST_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getRequestList(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START get request list");

            RequestService requestService = new RequestService();

            List<RequestBO> requestList;
            List<RequestBO> requestFileList = requestService.getRequestFileList();

            requestList = requestService.getRequestListDesc();
            requestListSearch = requestList;

            model.addAttribute("listBanks", new BankMembershipService().findAll());
            model.addAttribute("registerList", requestListSearch);
            model.addAttribute("requestFileList", requestFileList);
            model.addAttribute("requestList", requestList);
            model.addAttribute("numberOfRepresent", 1);
            model.addAttribute("serialHex", (new CertificateService().getNewSeria() + "").toUpperCase());

            logger.info("END get request list");
            putActionLog(request, "Truy vấn dữ liệu Request");
        } catch (Exception e) {
            putActionErrorLog(request, "Truy vấn dữ liệu Request");
            throw e;
        }
        return REQUEST_PAGE_REDIRECT;
    }

    @RequestMapping(value = REQUEST_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchRequest(@Valid @ModelAttribute("RequestForm") RequestForm requestForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START searchRequest");

            RequestService requestService = new RequestService();
            HashMap hmRequestList = new HashMap();
            if (!StringUtils.isNullOrEmpty(requestForm)) {
                this.requestForm = requestForm;
                if (!StringUtils.isNullOrEmpty(requestForm.getRegisterId())) {
                    hmRequestList.put("registerId", requestForm.getRegisterId());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getBin())) {
                    hmRequestList.put("bin", requestForm.getBin());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getIpk())) {
                    hmRequestList.put("ipk", requestForm.getIpk());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getSerial())) {
                    hmRequestList.put("serial", requestForm.getSerial());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getBankIdentity())) {
                    hmRequestList.put("bankIdentity", requestForm.getBankIdentity());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getMonth()) && !StringUtils.isNullOrEmpty(requestForm.getYear())) {
                    hmRequestList.put("expDate", requestForm.getMonth() + "/" + requestForm.getYear());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getFromDate())) {
                    hmRequestList.put("fromDate", requestForm.getFromDate());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getToDate())) {
                    hmRequestList.put("toDate", requestForm.getToDate());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getStatus())) {
                    hmRequestList.put("status", requestForm.getStatus());
                }
                model.addAttribute("requestForm", this.requestForm);
            }
            model.addAttribute("requestFileList", requestService.getRequestFileList());
            hmRequestList.put("statusne", 6);
            hmRequestList.put("statusne4", "delete");
            List<RequestBO> requestList = requestService.findRequest(hmRequestList);
            model.addAttribute("requestList", requestList);

            logger.info("END searchRequest");
            putActionLog(request, "Tìm kiếm dữ liệu Request");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "search.request.fail");
            putActionErrorLog(request, "Tìm kiếm dữ liệu Request");
        } finally {
            model.addAttribute("listBanks", new BankMembershipService().findAll());
            model.addAttribute("registerList", requestListSearch);
        }
        return REQUEST_PAGE_REDIRECT;
    }

    @RequestMapping(value = REQUEST_FILE_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getRequestFileList(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logger.info("START get request file list");

        List<RequestBO> requestFileList;

        try {

            RequestService requestService = new RequestService();
            requestFileList = requestService.getRequestFileList();
            requestListSearch = requestFileList;
            model.addAttribute("registerList", requestListSearch);
            model.addAttribute("requestFileList", requestFileList);
            model.addAttribute("numberOfRepresent", 1);

            if (!StringUtils.isNullOrEmpty(requestRegId)) {
                model.addAttribute("regIdTemp", requestRegId);
                requestRegId = "";
            }

            BankMembershipService bankingService = new BankMembershipService();
            List<BankMembershipBO> bankList = bankingService.findAll();
            List<BankMembershipBO> bankListActive = bankingService.findAllBankActive();
            model.addAttribute("bankList", bankList);
            model.addAttribute("bankListActive", bankListActive);

            logger.info("END get request file list");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "error");
        }
        return REQUEST_FILE_PAGE_REDIRECT;
    }

    @RequestMapping(value = REQUEST_FILE_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchRequestFile(@Valid @ModelAttribute("RequestForm") RequestForm requestForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START searchRequestFile");

            RequestService requestService = new RequestService();
            BankMembershipService bankingService = new BankMembershipService();
            HashMap hmRequestFileList = new HashMap();
            if (!StringUtils.isNullOrEmpty(requestForm)) {
                this.requestForm = requestForm;
                if (!StringUtils.isNullOrEmpty(requestForm.getRegisterId())) {
                    hmRequestFileList.put("registerId", requestForm.getRegisterId());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getBin())) {
                    hmRequestFileList.put("bin", requestForm.getBin());
                }
                if (!StringUtils.isNullOrEmpty(requestForm.getCreateDate())) {
                    hmRequestFileList.put("createDate", requestForm.getCreateDate());
                }
                model.addAttribute("requestForm", this.requestForm);
            }
            List<BankMembershipBO> bankList = bankingService.findAll();
            List<RequestBO> requestFileList = requestService.findRequestFile(hmRequestFileList);
            model.addAttribute("requestFileList", requestFileList);
            model.addAttribute("registerList", requestListSearch);
            model.addAttribute("bankList", bankList);

            logger.info("END searchRequestFile");
            putActionLog(request, "Tìm kiếm dữ liệu Request");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "search.request.file.fail");
            putActionErrorLog(request, "Tìm kiếm dữ liệu Request");
        } finally {
            model.addAttribute("registerList", requestListSearch);
        }
        return REQUEST_FILE_PAGE_REDIRECT;
    }

    @RequestMapping(value = RESTORE_REQUEST_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getRequestRemovedList(@Valid @ModelAttribute("RequestForm") RequestForm requestForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START get request removed list");

            RequestService requestService = new RequestService();
            List<RequestBO> requestRemovedList;

            requestRemovedList = requestService.findAllRequestRemove();
            model.addAttribute("requestRemovedList", requestRemovedList);
            model.addAttribute("numberOfRepresent", 1);

            logger.info("END get request removed list");
            putActionLog(request, "Truy vấn dánh sách Request bị xóa");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "search.request.file.fail");
            putActionErrorLog(request, "Truy vấn dánh sách Request bị xóa");
        }
        return RESTORE_REQUEST_PAGE_REDIRECT;
    }

    @RequestMapping(value = RESTORE_REQUEST_PAGE_REDIRECT, method = RequestMethod.POST)
    public String searchRequestRemove(@Valid @ModelAttribute("RequestForm") RequestForm requestForm, Model model,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START searchRequestRemove");

            RequestService requestService = new RequestService();
            HashMap hmRequestRemoveList = new HashMap();
            if (!StringUtils.isNullOrEmpty(requestForm)) {
                this.requestForm = requestForm;
                hmRequestRemoveList.put("registerId", requestForm.getRegisterId());
                hmRequestRemoveList.put("bankName", requestForm.getBankName());
                hmRequestRemoveList.put("ipk", requestForm.getIpk());
                hmRequestRemoveList.put("serial", requestForm.getSerial());
                hmRequestRemoveList.put("bankIdentity", requestForm.getBankIdentity());
                model.addAttribute("requestForm", this.requestForm);
            }
            List<RequestBO> requestRemovedList = requestService.findRequestRemove(hmRequestRemoveList);
            model.addAttribute("requestRemovedList", requestRemovedList);
            logger.info("END searchRequestRemove");
            putActionLog(request, "Tìm kiếm Request bị xóa");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "search.request.removed.fail");
            putActionErrorLog(request, "Tìm kiếm Request bị xóa");
        }
        return RESTORE_REQUEST_PAGE_REDIRECT;
    }

    @RequestMapping(value = REQUEST_DETAIL_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getRequestDetail(@RequestParam("requestId") String requestId,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START get request by id to view");

            RequestService requestService = new RequestService();
            requestBO = new RequestBO();
            requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(requestId)));
            model.addAttribute("requestBO", requestBO);

            logger.info("END get request by id to view");
            putActionLog(request, "Xem thông tin chi tiết Request, id: " + NumberUtil.toNumber(AESUtil.decryption(requestId)));
        } catch (Exception e) {
            putActionErrorLog(request, "Xem thông tin chi tiết Request, id: " + NumberUtil.toNumber(AESUtil.decryption(requestId)));
            throw e;
        }
        return REQUEST_DETAIL_PAGE_REDIRECT;
    }

    @RequestMapping(value = EDIT_REQUEST_PAGE_REDIRECT, method = RequestMethod.GET)
    public String preEditRequest(@RequestParam("requestId") String requestId,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START get request by id to edit");

            RequestService requestService = new RequestService();
            requestBO = new RequestBO();
            requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(requestId)));
            model.addAttribute("requestBO", requestBO);

            logger.info("END get request by id to edit");
            putActionLog(request, "Lấy thông tin chi tiết Request, id: " + NumberUtil.toNumber(AESUtil.decryption(requestId)));
        } catch (Exception e) {
            putActionErrorLog(request, "Lấy thông tin chi tiết Request, id: " + NumberUtil.toNumber(AESUtil.decryption(requestId)));
            throw e;
        }
        return EDIT_REQUEST_PAGE_REDIRECT;
    }

    @RequestMapping(value = EDIT_REQUEST_PAGE_ACTION, method = RequestMethod.POST)
    public void editRequest(@Valid @ModelAttribute("EditRequestForm") EditRequestForm editRequestForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (result.hasErrors()) {
            sendMessageError(request, "error");
            putActionErrorLog(request, "Chỉnh sửa dữ liệu Request");
            return;
        }

        String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");

        try {
            logger.info("START edit request");

            RequestService requestService = new RequestService();
            String expDate = editRequestForm.getMonth() + "/" + editRequestForm.getYear();
            Date convertedExpDate = dateFormat.parse(expDate);
            cal.setTime(convertedExpDate);
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
            if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(DateTimeUtils.convertToEndDate(new Date())) >= 0) {
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date convertedAppDate = dateFormat.parse(editRequestForm.getAppDate());
                cal.setTime(convertedAppDate);
                if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(DateTimeUtils.convertToEndDate(new Date())) <= 0) {
                    requestBO = requestService.getRequestByRegId(NumberUtil.toNumber(editRequestForm.getRegisterId()));
                    requestBO = editRequestForm.toBO(requestBO);
                    requestBO.setIpkLengh(editRequestForm.getIpk().length() * 4);
                    requestBO.setBankIdentity(StringUtils.getBankIdentity(requestBO.getBankIdentity()));
                    requestBO.setStatus(0);
                    requestBO.setUpdateUser(username);
                    requestBO.setUpdateDate(new Date());

                    requestService.updateRequest(requestBO);
                    sendMessageSuccess(request, "edit.request.success");
                    putActionLog(request, "Chỉnh sửa dữ liệu Request, id:" + NumberUtil.toNumber(editRequestForm.getRegisterId()));
                } else {
                    sendMessageError(request, "app.date.validate.before.sysdate");
                    putActionErrorLog(request, "Chỉnh sửa dữ liệu Request, id:" + NumberUtil.toNumber(editRequestForm.getRegisterId()));
                }
            } else {
                sendMessageError(request, "exp.date.validate.after.sysdate");
                putActionErrorLog(request, "Chỉnh sửa dữ liệu Request, id:" + NumberUtil.toNumber(editRequestForm.getRegisterId()));
            }

            logger.info("END edit request");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "edit.request.fail");
            putActionErrorLog(request, "Chỉnh sửa dữ liệu Request, id:" + NumberUtil.toNumber(editRequestForm.getRegisterId()));
        }
        response.sendRedirect(REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = CREATE_REQUEST_PAGE_ACTION, method = RequestMethod.POST)
    public void createRequest(@Valid @ModelAttribute("CreateRequestForm") CreateRequestForm createRequestForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (result.hasErrors()) {
            sendMessageError(request, "error");
            putActionErrorLog(request, "Tạo mới Request");
            return;
        }

        RequestService requestService = new RequestService();
        String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");

        try {
            logger.info("START create new request");
            if (StringUtils.isNullOrEmpty(hexDataRequest) || hexDataRequest.size() == 0) {
                String expDate = createRequestForm.getMonth() + "/" + createRequestForm.getYear();
                Date convertedExpDate = dateFormat.parse(expDate);
                cal.setTime(convertedExpDate);
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(DateTimeUtils.convertToEndDate(new Date())) >= 0) {
                    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date convertedAppDate = dateFormat.parse(createRequestForm.getAppDate());
                    cal.setTime(convertedAppDate);
                    if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(DateTimeUtils.convertToEndDate(new Date())) <= 0) {

                        requestBO = requestService.getRequestByRegId(createRequestForm.getRegisterId());

                        BankMembershipBO bankMembershipBO = new BankMembershipService().getMembershipBankByBin(requestBO.getBin());
                        if (bankMembershipBO == null) {
                            sendMessageError(request, "certificate.app.nhtv.fail");
                            putActionErrorLog(request, "Ký duyệt chứng thư");
                            response.sendRedirect(REQUEST_PAGE_REDIRECT);
                            return;
                        }
                        if (bankMembershipBO.getStatus() != 0) {
                            sendMessageError(request, "certificate.app.nhtv.fail");
                            putActionErrorLog(request, "Ký duyệt chứng thư");
                            response.sendRedirect(REQUEST_PAGE_REDIRECT);
                            return;
                        }

                        if (!StringUtils.isNullOrEmpty(requestBO)) {
                            if (!StringUtils.isNullOrEmpty(requestBO.getStatus()) && requestBO.getStatus() == 6) {

                                requestBO = createRequestForm.createBo(requestBO);
                                requestBO.setBankIdentity(StringUtils.getBankIdentity(requestBO.getBankIdentity()));
                                requestBO.setIpkLengh(createRequestForm.getIpk().length() * 4);
                                requestBO.setBankIdentity(StringUtils.getBankIdentity(requestBO.getBankIdentity()));
                                requestBO.setCreateDate(new Date());
                                requestBO.setCreateUser(username);
//                                requestBO.setUpdateUser(username);
//                                requestBO.setUpdateDate(new Date());
                                requestService.updateRequest(requestBO);
                                sendMessageSuccess(request, "create.request.success");
                                putActionLog(request, "Tạo mới Request");
                            } else {
                                sendMessageError(request, "create.request.exist");
                                putActionErrorLog(request, "Tạo mới Request");
                            }
                        } else {
                            sendMessageError(request, "create.request.not.found.record");
                            putActionErrorLog(request, "Tạo mới Request");
                        }
                    } else {
                        sendMessageError(request, "app.date.validate.before.sysdate");
                        putActionErrorLog(request, "Tạo mới Request");
                    }
                } else {
                    sendMessageError(request, "exp.date.validate.after.sysdate");
                    putActionErrorLog(request, "Tạo mới Request");
                }
            } else {

                if (StringUtils.isNullOrEmpty(requestService.getRequestByRegId(NumberUtil.toNumber(hexDataRequest.get("seRegisteredNumber"))))) {
                    requestBO = new RequestBO();
                    requestBO.setRegisterId(NumberUtil.toNumber(hexDataRequest.get("seRegisteredNumber")));
                    requestBO.setSerial(String.valueOf(hexDataRequest.get("newSerial")));
                    requestBO.setBankName(String.valueOf(hexDataRequest.get("bankFullName")));
                    requestBO.setBankIdentity(hexDataRequest.get("seIssuerIdentificationNumber"));
                    requestBO.setIpk(String.valueOf(hexDataRequest.get("usIPKModulus")));
                    requestBO.setExpDate(hexDataRequest.get("seCertificateExpirationDate").substring(0, 2)
                            + "/20" + hexDataRequest.get("seCertificateExpirationDate").substring(2, 4));
                    requestBO.setBin(NumberUtil.getBin(hexDataRequest.get("seIssuerIdentificationNumber")));
                    requestBO.setIpkLengh(Integer.parseInt(hexDataRequest.get("usLengthofIPKModulus"), 16));
                    requestBO.setAppDate(new Date());
                    requestBO.setCreateUser(username);
                    requestBO.setCreateDate(new Date());
                    requestBO.setStatus(0);

                    BankMembershipBO bankMembershipBO = new BankMembershipService().getMembershipBankByBin(requestBO.getBin());
                    if (bankMembershipBO == null) {
                        sendMessageError(request, "import.app.nhtv.fail");
                        putActionErrorLog(request, "Ký duyệt chứng thư");
                        response.sendRedirect(REQUEST_PAGE_REDIRECT);
                        return;
                    }
                    if (bankMembershipBO.getStatus() != 0) {
                        sendMessageError(request, "import.app.nhtv.fail");
                        putActionErrorLog(request, "Ký duyệt chứng thư");
                        response.sendRedirect(REQUEST_PAGE_REDIRECT);
                        return;
                    }

                    requestService.createRequest(requestBO);
                    hexDataRequest.clear();
                    hexDataRequest = null;
                    sendMessageSuccess(request, "create.request.success");
                    putActionLog(request, "Tạo mới Request");
                } else {
                    sendMessageError(request, "create.request.exist");
                    putActionErrorLog(request, "Tạo mới Request");
                }
            }

            logger.info("END create new request");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "create.request.fail");
            putActionErrorLog(request, "Tạo mới Request");
        }
        response.sendRedirect(REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = CREATE_REQUEST_FILE_PAGE_ACTION, method = RequestMethod.POST)
    public void createRequestFile(@Valid @ModelAttribute("CreateRequestForm") CreateRequestForm createRequestForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (result.hasErrors()) {
            sendMessageError(request, "error");
            putActionErrorLog(request, "Tạo file Yêu cầu");
            return;
        }
        try {
            logger.info("START create new request file");
            RequestService requestService = new RequestService();
            if (StringUtils.isNullOrEmpty(requestService.getRequestByRegId(createRequestForm.getRegisterId()))) {
                String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
                BankMembershipService bankingService = new BankMembershipService();
                BankMembershipBO bankBO = bankingService.getMembershipBankingById(Integer.parseInt(createRequestForm.getBankName()));
                requestBO = new RequestBO();
                requestBO.setRegisterId(createRequestForm.getRegisterId());
                requestBO.setBin(bankBO.getBin());
                requestBO.setBankName(bankBO.getBankFullName());
                requestBO.setStatus(6);
                requestBO.setCreateUser(username);
                requestBO.setCreateDate(new Date());
                requestService.createRequest(requestBO);
                requestRegId = AESUtil.encryption(String.valueOf(requestBO.getRegisterId()));

                sendMessageSuccess(request, "create.file.request.success");
                putActionLog(request, "Tạo file Yêu cầu");
            } else {
                sendMessageError(request, "create.file.request.exist");
                putActionErrorLog(request, "Tạo file Yêu cầu");
            }

            logger.info("END create new request file");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "create.file.request.fail");
            putActionErrorLog(request, "Tạo file Yêu cầu");
        }
        response.sendRedirect(REQUEST_FILE_PAGE_REDIRECT);
    }

    @RequestMapping(value = DELETE_REQUEST_ACTION, method = RequestMethod.POST)
    public void deleteRequest(@RequestParam(value = "paramId", required = true) String paramId,
            HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START delete request");
            RequestService requestService = new RequestService();
            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(paramId)));
            if (StringUtils.isNullOrEmpty(requestBO)) {
                sendMessageError(request, "delete.request.fail");
                putActionErrorLog(request, "Xóa request, id: " + NumberUtil.toNumber(AESUtil.decryption(paramId)));
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }
            requestBO.setStatus(NumberUtil.toNumber(4 + "" + requestBO.getStatus()));
            requestBO.setUpdateUser(username);
            requestBO.setUpdateDate(new Date());
            requestService.updateRequest(requestBO);
            sendMessageSuccess(request, "delete.request.success");

            logger.info("END delete request");
            putActionLog(request, "Xóa request, id: " + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "delete.request.fail");
            putActionErrorLog(request, "Xóa request, id: " + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        }
        response.sendRedirect(REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = RESTORE_REQUEST_ACTION, method = RequestMethod.POST)
    public void restoreRequest(@RequestParam(value = "paramId", required = true) String paramId,
            HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START restoreRequest");

            RequestService requestService = new RequestService();
            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            List<String> requestError = new ArrayList<>();
            List<String> bankError = new ArrayList<>();
            requestBO = new RequestBO();
            BankMembershipService bankingService = new BankMembershipService();
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");

            String[] listParamId = null;
            if (paramId.contains(",")) {
                listParamId = paramId.split(",");
            }

            if (!StringUtils.isNullOrEmpty(listParamId)) {
                for (String listParam : listParamId) {
                    requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(listParam)));
                    BankMembershipBO bankBO = bankingService.getMembershipBankByBin(requestBO.getBin());
                    if (!StringUtils.isNullOrEmpty(bankBO)) {
                        if (bankBO.getStatus().equals(Constants.BANK_STATUS.DEACTIVATE)) {
                            if (!bankError.contains(bankBO.getBankFullName())) {
                                bankError.add(bankBO.getBankFullName());
                            }
                        } else {
                            Date convertedDate = dateFormat.parse(requestBO.getExpDate());
                            cal.setTime(convertedDate);
                            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                            if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(new Date()) < 0) {
                                if (!requestError.contains(String.valueOf(requestBO.getRegisterId()))) {
                                    requestError.add(String.valueOf(requestBO.getRegisterId()));
                                }
                            } else {
                                String stt = String.valueOf(requestBO.getStatus());
                                if (stt.length() != 2) {
                                    sendMessageError(request, "restore.request.bank.not.exist");
                                    putActionErrorLog(request, "Khôi phục Yêu cầu");
                                    response.sendRedirect(RESTORE_REQUEST_PAGE_REDIRECT);
                                    return;
                                }
                                requestBO.setStatus(NumberUtil.toNumber(stt.substring(1, 2)));

                                requestBO.setUpdateUser(username);
                                requestBO.setUpdateDate(new Date());
                                requestService.updateRequest(requestBO);
                                sendMessageSuccess(request, "restore.request.success");
                                putActionLog(request, "Khôi phục Yêu cầu, id: " + NumberUtil.toNumber(AESUtil.decryption(listParam)));
                            }
                        }
                    } else {
                        sendMessageError(request, "restore.request.bank.not.exist");
                        putActionErrorLog(request, "Khôi phục Yêu cầu");
                    }
                }
            } else {
                requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(paramId)));
                BankMembershipBO bankBO = bankingService.getMembershipBankByBin(requestBO.getBin());
                if (!StringUtils.isNullOrEmpty(bankBO)) {
                    if (bankBO.getStatus().equals(Constants.BANK_STATUS.DEACTIVATE)) {
                        bankError.add(bankBO.getBankFullName());
                    } else {
                        Date convertedDate = dateFormat.parse(requestBO.getExpDate());
                        cal.setTime(convertedDate);
                        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                        if (DateTimeUtils.convertToEndDate(cal.getTime()).compareTo(new Date()) < 0) {
                            requestError.add(String.valueOf(requestBO.getRegisterId()));
                        } else {
                            String stt = String.valueOf(requestBO.getStatus());
                            if (stt.length() != 2) {
                                sendMessageError(request, "restore.request.bank.not.exist");
                                putActionErrorLog(request, "Khôi phục Yêu cầu");
                                response.sendRedirect(RESTORE_REQUEST_PAGE_REDIRECT);
                                return;
                            }
                            requestBO.setStatus(NumberUtil.toNumber(stt.substring(1, 2)));

                            requestBO.setUpdateUser(username);
                            requestBO.setUpdateDate(new Date());
                            requestService.updateRequest(requestBO);
                            sendMessageSuccess(request, "restore.request.success");
                            putActionLog(request, "Khôi phục Yêu cầu, id:" + NumberUtil.toNumber(AESUtil.decryption(paramId)));
                        }
                    }
                } else {
                    sendMessageError(request, "restore.request.bank.not.exist");
                    putActionErrorLog(request, "Khôi phục Yêu cầu");
                }
            }

            if (!CollectionUtils.isEmpty(requestError)) {
                request.getSession().setAttribute("requestError", requestError);
                request.getSession().setAttribute("requestMessageError", MessageUtils.getMessage("restore.request.expDate.before.now"));
            }
            if (!CollectionUtils.isEmpty(bankError)) {
                request.getSession().setAttribute("bankError", bankError);
                request.getSession().setAttribute("bankMessageError", MessageUtils.getMessage("restore.request.bank.status.deactivate"));
            }
            logger.info("END restoreRequest");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "restore.request.fail");
            putActionErrorLog(request, "Khôi phục yêu cầu");
        }
        response.sendRedirect(RESTORE_REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = SUBMISSION_REQUEST_PAGE_REDIRECT, method = RequestMethod.GET)
    public String prepareSubmissionRequest(@RequestParam("requestId") String requestId,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        try {
            logger.info("START prepareSubmissionRequest");

            RequestService requestService = new RequestService();
            requestBO = new RequestBO();
            requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(requestId)));
            String[] expDate = requestBO.getExpDate().split("/");
            RSAKeyService keyService = new RSAKeyService();
            HashMap searchMap = new HashMap();
            searchMap.put("rsaStatus", 0);
            searchMap.put("privateKeyNameCheck", "true");
            searchMap.put("slotHsmCheck", "true");

            List<RSAKeysBO> allKeysList = keyService.getKeys(searchMap);
            model.addAttribute("allKeysList", allKeysList);

            model.addAttribute("requestBO", requestBO);
            model.addAttribute("month", expDate[0]);
            model.addAttribute("year", expDate[1]);

            logger.info("END prepareSubmissionRequest");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "error");
        }
        return SUBMISSION_REQUEST_PAGE_REDIRECT;
    }

    @RequestMapping(value = SUBMISSION_REQUEST_PAGE_ACTION, method = RequestMethod.POST)
    public void submissionRequest(@RequestParam(value = "paramId", required = true) String paramId,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START submissionRequest");

            RequestService requestService = new RequestService();
            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            if (requestBO == null) {
                requestBO = new RequestBO();
                requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(paramId)));
            }

            BankMembershipBO bankMembershipBO = new BankMembershipService().getMembershipBankByBin(requestBO.getBin());
            if (bankMembershipBO == null) {
                sendMessageError(request, "certificate.app.nhtv.fail1");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }
            if (bankMembershipBO.getStatus() != 0) {
                sendMessageError(request, "certificate.app.nhtv.fail1");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }

            HashMap hashMap = new HashMap();
            hashMap.put("serial", requestBO.getSerial());
            List<DigitalCertificateBO> bOs = new CertificateService().getCertificates(hashMap);
            if (!StringUtils.isNullOrEmpty(bOs) && bOs.size() > 0) {
                sendMessageError(request, "request.import.serial.ex");
                putActionErrorLog(request, "Trình ký yêu cầu");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }

            requestBO.setStatus(1);
            requestBO.setSignUser(username);
            requestBO.setSignDate(new Date());

            requestService.updateRequest(requestBO);
            sendMessageSuccess(request, "submission.request.success");

            logger.info("END submissionRequest");
            putActionLog(request, "Trình ký yêu cầu, id:" + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "submission.request.fail");
            putActionErrorLog(request, "Trình ký yêu cầu, id:" + NumberUtil.toNumber(AESUtil.decryption(paramId)));
        }
        response.sendRedirect(REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = APPROVE_REQUEST_PAGE_ACTION, method = RequestMethod.POST)
    public void approveRequest(@RequestParam(value = "paramId", required = true) String paramId,
            @RequestParam(value = "paramIndex", required = true) String paramIndex,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START approveRequest");

            if (StringUtils.isNullOrEmpty(paramIndex)) {
                sendMessageError(request, "checksum.require.select.index");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }

            CertificateService certificateService = new CertificateService();
            RequestService requestService = new RequestService();
            RSAKeyService keyService = new RSAKeyService();
            ConfigHsmService hsmService = new ConfigHsmService();

            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(paramId)));
            if (requestBO == null) {
                sendMessageError(request, "error");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }
            requestBO.setStatus(2);
            requestBO.setAcceptUser(username);
            requestBO.setAcceptDate(new Date());

            BankMembershipBO bankMembershipBO = new BankMembershipService().getMembershipBankByBin(requestBO.getBin());
            if (bankMembershipBO == null) {
                sendMessageError(request, "certificate.app.nhtv.fail");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }
            if (bankMembershipBO.getStatus() != 0) {
                sendMessageError(request, "certificate.app.nhtv.fail");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }

            RSAKeysBO rsaKeyBO = keyService.getKeyByIndex(NumberUtil.toNumber(paramIndex));
            ConfigHsmBO configHsmBO = hsmService.getConfigHsmBySlot(rsaKeyBO.getSlotHsm());

            if (StringUtils.isNullOrEmpty(configHsmBO) || StringUtils.isNullOrEmpty(configHsmBO.getSlot())
                    || StringUtils.isNullOrEmpty(configHsmBO.getSlot())) {
                sendMessageError(request, "approve.request.fail.slot");
                putActionErrorLog(request, "Ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }

            if (!StringUtils.isNullOrEmpty(rsaKeyBO)) {

//                logger.info("BankIdentity : " + requestBO.getBankIdentity());
//                logger.info("ExpDate : " + requestBO.getExpDate());
//                logger.info("Serial : " + requestBO.getSerial());
//                logger.info("RsaIndex : " + rsaKeyBO.getRsaIndex());
//                logger.info("Slot : " + configHsmBO.getSlot());
//                logger.info("Password : " + configHsmBO.getPassword());
//                logger.info("PrivateKeyName : " + rsaKeyBO.getPrivateKeyName());
                //==========================bat dau ky
                byte[] issuerIdentifier = hexStringToByteArray(requestBO.getBankIdentity());
                String[] ex = requestBO.getExpDate().split("/");
                byte[] expirationDate = hexStringToByteArray(ex[0] + ex[1].substring(2, 4));
                byte[] serialNumber = hexStringToByteArray(requestBO.getSerial());

//                logger.info("Start create EMV_Cert_Request ");
                //Generate IssCert using imported PrivateKey
                EMV_Cert_Request certReq = new EMV_Cert_Request();
                byte arrayIndex = (byte) ((Character.digit(StringUtils.paddingIndex(rsaKeyBO.getRsaIndex() + "").charAt(0), 16) << 4) 
                        + Character.digit(StringUtils.paddingIndex(rsaKeyBO.getRsaIndex() + "").charAt(0 + 1), 16));

                certReq.setCAPublicKeyIndex(arrayIndex);//(byte) 0x99
                certReq.setIssuerIdentifier(issuerIdentifier);//new byte[]{(byte) 0x97, (byte) 0x04, (byte) 0x36, (byte) 0xFF}
                certReq.setCertificateExpirationDate(expirationDate);//new byte[]{(byte) 0x12, (byte) 0x24}
                certReq.setCertificateSerialNumber(serialNumber);//new byte[]{(byte) 0x36, (byte) 0x36, (byte) 0x36}
                certReq.setIssuerPublicKeyModulus(requestBO.getIpk());// "BFA4D4233C60B9B5CE687632FC925A77A7FAFAD467785173BCB1D3BF683AD9FD112071C20777E6F7E51B2174722E172390C8A47608D63ECEF197DD958190F6595A8BE4A48290BF323C86892B48E9FD0C77B8DBADD6718ABD9BBFD84A330500FC7365F0BD66EC61D5646B47E06A1F425F280C5DB0BA052A7F46A3349BFC13B8FEFA477EA22BF7D9EB49F108D4AF8603CD5EF0412752E0F0107B5B76304C397C2E34A13052E04F7C6F2E7284CA7DE77FA695713168E2E24C46E24745B168E8A3167AB97E9F22CF57A70ED2C9034AA5212F82E1009C626200747EEB885407434E31834C49D8CE9C32857D7B9FA242A330632792F4CB029679"

//                logger.info("Start create EMVcert certGen ");
                EMVcert certGen = new EMVcert();
                byte[] cert = certGen.genEMV_IssuerCert(configHsmBO.getSlot(), AESUtil.decryption(configHsmBO.getPassword()), rsaKeyBO.getPrivateKeyName(), "", certReq);
//                logger.info("cert: " + DatatypeConverter.printHexBinary(cert));

                byte[] certFile = certGen.genVCCS_IssuerFile(configHsmBO.getSlot(), AESUtil.decryption(configHsmBO.getPassword()), rsaKeyBO.getPrivateKeyName(), "", certReq);
//                logger.info("cert File: " + DatatypeConverter.printHexBinary(certFile));
//                byte[] certFile = new byte[]{(byte) 0x97, (byte) 0x04, (byte) 0x36, (byte) 0xFF};
                //==========================ket thuc ky 
                DigitalCertificateBO certificateBO = new DigitalCertificateBO();
                certificateBO.setRsaId(rsaKeyBO.getRsaId());
                certificateBO.setRegisterId(requestBO.getRegisterId());
                certificateBO.setCertificate(certFile);
                certificateBO.setBin(requestBO.getBin());
                certificateBO.setBankName(requestBO.getBankName());
                certificateBO.setIpk(requestBO.getIpk());
                certificateBO.setExpDate(requestBO.getExpDate());
                certificateBO.setSerial(requestBO.getSerial());
                certificateBO.setBankIdentity(requestBO.getBankIdentity());
                certificateBO.setRsaLengh(rsaKeyBO.getRsaLength());
                certificateBO.setSignUser(requestBO.getSignUser());
                certificateBO.setSignDate(requestBO.getSignDate());
                certificateBO.setCreateUser(requestBO.getAcceptUser());
                certificateBO.setCreateDate(requestBO.getAcceptDate());
                certificateBO.setIpkLengh(requestBO.getIpkLengh());
                certificateBO.setStatus(0);

                requestService.updateRequest(requestBO);
                certificateService.addCertificate(certificateBO);

                sendMessageSuccess(request, "approve.request.success");
                putActionLog(request, "Ký duyệt chứng thư");

            } else {
                sendMessageError(request, "approve.request.rsa.not.exist");
                putActionErrorLog(request, "Ký duyệt chứng thư");
            }
            logger.info("END approveRequest");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            sendMessageError(request, "approve.request.fail");
            putActionErrorLog(request, "Ký duyệt chứng thư");
        }
        response.sendRedirect(REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = REJECT_REQUEST_PAGE_ACTION, method = RequestMethod.POST)
    public void rejectRequest(@RequestParam(value = "paramId", required = true) String paramId,
            @RequestParam(value = "paramContent", required = true) String paramContent,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.info("START rejectRequest");

            if (StringUtils.isNullOrEmpty(paramContent)) {
                sendMessageError(request, "certificate.recall.content.validate.empty");
                putActionErrorLog(request, "Từ chối ký duyệt chứng thư");
                response.sendRedirect(REQUEST_PAGE_REDIRECT);
                return;
            }

            RequestService requestService = new RequestService();
            String username = AESUtil.getUserLoginFromToken(request.getSession().getAttribute("tokenLogin").toString(), 1);
            if (!StringUtils.isNullOrEmpty(paramContent)) {
                requestBO = requestService.getRequestById(NumberUtil.toNumber(AESUtil.decryption(paramId)));
                if (requestBO == null) {
                    sendMessageError(request, "error");
                    putActionErrorLog(request, "Từ chối ký duyệt chứng thư");
                    response.sendRedirect(REQUEST_PAGE_REDIRECT);
                    return;
                }
                BankMembershipBO bankMembershipBO = new BankMembershipService().getMembershipBankByBin(requestBO.getBin());
                if (bankMembershipBO == null) {
                    sendMessageError(request, "error");
                    putActionErrorLog(request, "Từ chối ký duyệt chứng thư");
                    response.sendRedirect(REQUEST_PAGE_REDIRECT);
                    return;
                }
                if (bankMembershipBO.getStatus() != 0) {
                    sendMessageError(request, "certificate.app.nhtv.fail");
                    putActionErrorLog(request, "Từ chối ký duyệt chứng thư");
                    response.sendRedirect(REQUEST_PAGE_REDIRECT);
                    return;
                }
                requestBO.setStatus(3);
                requestBO.setContent(paramContent);
                requestBO.setUpdateUser(username);
                requestBO.setUpdateDate(new Date());

                requestService.updateRequest(requestBO);
                sendMessageSuccess(request, "reject.request.success");
                putActionLog(request, "Từ chối ký duyệt chứng thư");
            } else {
                sendMessageError(request, "reject.request.content.empty");
                putActionErrorLog(request, "Từ chối ký duyệt chứng thư");
            }
            logger.info("END rejectRequest");
        } catch (Exception e) {
            logger.error(e);
            sendMessageError(request, "reject.request.fail");
            putActionErrorLog(request, "Từ chối ký duyệt chứng thư");
        }
        response.sendRedirect(REQUEST_PAGE_REDIRECT);
    }

    @RequestMapping(value = EXPORT_REQUEST_ACTION, method = RequestMethod.POST)
    public String exportRequest(@RequestParam("regId") String regId,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        InputStream inputStream = null;
        try {
            RequestService requestService = new RequestService();
            requestBO = new RequestBO();
            requestBO = requestService.getRequestByRegId(NumberUtil.toNumber(AESUtil.decryption(regId)));

            Path path = Paths.get(tem_req_export);
            File file = path.toFile();

            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFParagraph> paragraphs = document.getParagraphs();
            for (XWPFParagraph p : paragraphs) {
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.getText(0);
                        if (text != null && text.contains("bankName")) {
                            text = text.replace("bankName", StringEscapeUtils.unescapeHtml(requestBO.getBankName()));
                            r.setText(text, 0);
                        }
                    }
                }
            }
            for (XWPFTable tbl : document.getTables()) {
                for (XWPFTableRow row : tbl.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        for (XWPFParagraph p : cell.getParagraphs()) {
                            for (XWPFRun r : p.getRuns()) {
                                String text = r.getText(0);
                                if (text != null && text.contains("registerId")) {
                                    text = text.replace("registerId", String.valueOf(requestBO.getRegisterId()));
                                    r.setText(text, 0);
                                }
                            }
                        }
                    }
                }
            }
            String outFile = file.getAbsolutePath().replace(path.getFileName().toString(), "outFile.docx");
            FileOutputStream out = new FileOutputStream(outFile);
            document.write(out);

            Path path2 = Paths.get(outFile);
            byte[] fileContent = Files.readAllBytes(path2);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + "GiayDeNghiCapChungThuSo_NAPAS_VCCSCA_BM02.doc");
            response.setContentLength(fileContent.length);
            inputStream = new BufferedInputStream(new ByteArrayInputStream(fileContent));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            Files.delete(path2);
            putActionLog(request, "Tải file yêu cầu");

        } catch (Exception ex) {
            putActionErrorLog(request, "Tải file yêu cầu");
            throw ex;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return REQUEST_FILE_PAGE_REDIRECT;
    }

    @RequestMapping(value = IMPORT_REQUEST_ACTION, method = RequestMethod.POST)
    public String importRequest(@RequestParam("file") final MultipartFile file,
            Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            if (!file.isEmpty()) {
                byte[] fileContent = file.getBytes();
                String[] list = byteToHexArr(fileContent);
                hexDataRequest = new HashMap<String, String>();

                //4.1.1 Unsigned IPK Input Extension
                //Header 
                String usHeader = getHexbyteStart(list, 1);
                hexDataRequest.put("usHeader", usHeader);
                //Length of IPK Modulus  
                String usLengthofIPKModulus = getHexbyte(list, 1);
                hexDataRequest.put("usLengthofIPKModulus", usLengthofIPKModulus);

                int decimal = Integer.parseInt(usLengthofIPKModulus, 16);
                //IPK Modulus 
                String usIPKModulus = getHexbyte(list, decimal);//NI: là độ dài của IPK Modulus (đơn vị: byte). 
                hexDataRequest.put("usIPKModulus", usIPKModulus);
                System.out.println("usIPKModulus:" + usIPKModulus);
                //IPK Exponent Length  
                String usIPKExponentLength = getHexbyte(list, 1);
                hexDataRequest.put("usIPKExponentLength", usIPKExponentLength);
                System.out.println("usIPKExponentLength: " + usIPKExponentLength);
                //IPK Exponent 
                String usIPKExponent = getHexbyte(list, Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3);//1 or 3 IPK Exponent. Định dạng hex, chỉ nhận một trong hai giá trị 03 (3) hoặc 01 00 01 (65537) 
                hexDataRequest.put("usIPKExponent", usIPKExponent);
                System.out.println("usIPKExponent:" + usIPKExponent);
                //Registered Number  
                String usRegisteredNumber = getHexbyte(list, 3);
                hexDataRequest.put("usRegisteredNumber", usRegisteredNumber);
                System.out.println("usRegisteredNumber:" + usRegisteredNumber);

                String allSign = getHexbyte(list, list.length - countIndex);
                byte[] allSignbyte = hexStringToByteArray(allSign);

                int Ni = Integer.parseInt(usLengthofIPKModulus, 16);

                RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(usIPKModulus, 16), new BigInteger(usIPKExponent, 16));
                PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(spec);

                Cipher cipher = Cipher.getInstance("RSA/ECB/NOPADDING");
                cipher.init(Cipher.DECRYPT_MODE, publicKey);
                byte[] allSignbyteDecode = cipher.doFinal(allSignbyte);

                System.out.println(">>>>" + byteToHex(allSignbyteDecode));
                list = byteToHexArr(allSignbyteDecode);

                //4.1.2 Se lf-Signed IPK Data
                //Header 
                String seHeader = getHexbyteStart(list, 1);
                hexDataRequest.put("seHeader", seHeader);
                //Service Identifier  
                String seServiceIdentifier = getHexbyte(list, 4);
                hexDataRequest.put("seServiceIdentifier", seServiceIdentifier);
                //Certificate Format  
                String seCertificateFormat = getHexbyte(list, 1);
                hexDataRequest.put("seCertificateFormat", seCertificateFormat);
                //Issuer Identification Number (IIN) 
                String seIssuerIdentificationNumber = getHexbyte(list, 4);
                hexDataRequest.put("seIssuerIdentificationNumber", seIssuerIdentificationNumber);
                //Certificate Expiration Date 
                String seCertificateExpirationDate = getHexbyte(list, 2);
                hexDataRequest.put("seCertificateExpirationDate", seCertificateExpirationDate);
                //Registered Number  
                String seRegisteredNumber = getHexbyte(list, 3);
                hexDataRequest.put("seRegisteredNumber", seRegisteredNumber);
                //Hash Algorithm Indicator  
                String seHashAlgorithmIndicator = getHexbyte(list, 1);
                hexDataRequest.put("seHashAlgorithmIndicator", seHashAlgorithmIndicator);
                //Issuer’s Public Key Algorithm Indicator  
                String seIssuersPublicKeyAlgorithmIndicator = getHexbyte(list, 1);
                hexDataRequest.put("seIssuersPublicKeyAlgorithmIndicator", seIssuersPublicKeyAlgorithmIndicator);
                //IPK Modulus Length  
                String seIPKModulusLength = getHexbyte(list, 1);
                hexDataRequest.put("seIPKModulusLength", seIPKModulusLength);
                //IPK Exponent Length 
                String seIPKExponentLength = getHexbyte(list, 1);
                hexDataRequest.put("seIPKExponentLength", seIPKExponentLength);

                int e = Integer.parseInt(usIPKExponentLength, 16) == 1 ? 1 : 3;
                int leftmostLen = Ni - (39 + e);
                //Leftmost part of IPK Modulus  
                String seLeftmostpartofIPKModulus = getHexbyte(list, leftmostLen);//NI − (39+ e) Phần tử bên trái của IPK Modulus, tính bằng cách lấy [NI − (39 + e)] byte đầu tiên của IPK Modulus
                hexDataRequest.put("seLeftmostpartofIPKModulus", seLeftmostpartofIPKModulus);

                //IPK Exponent 
                String seIPKExponent = getHexbyte(list, e);//e IPK Exponent, định dạng hex. Giá trị bằng 03 (3) hoặc 01 00 01 (65537)
                hexDataRequest.put("seIPKExponent", seIPKExponent);
                //Hash Result 
                String seHashResult = getHexbyte(list, 20);
                hexDataRequest.put("seHashResult", seHashResult);

                BankMembershipService service = new BankMembershipService();
                BankMembershipBO membershipBO = service.getMembershipBankByBin(NumberUtil.getBin(StringUtils.getBankIdentityNoFF(seIssuerIdentificationNumber)));
                if (StringUtils.isNullOrEmpty(membershipBO)) {
                    sendMessageError(request, "request.import.not.bin", StringUtils.getBankIdentityNoFF(seIssuerIdentificationNumber));
                    putActionErrorLog(request, "Import file yêu cầu");
                    hexDataRequest = null;
                    return IMPORT_PAGE_REDIRECT;
                }

                if (membershipBO.getStatus() != 0) {
                    sendMessageError(request, "certificate.app.nhtv.fail");
                    putActionErrorLog(request, "Import file yêu cầu");
                    response.sendRedirect(REQUEST_PAGE_REDIRECT);
                    hexDataRequest = null;
                    return IMPORT_PAGE_REDIRECT;
                }

                hexDataRequest.put("bankFullName", membershipBO.getBankFullName());
                hexDataRequest.put("exDate1", seCertificateExpirationDate.substring(0, 2));
                hexDataRequest.put("exDate2", seCertificateExpirationDate.substring(2, 4));
                hexDataRequest.put("newSerial", new CertificateService().getNewSeria().toUpperCase());
                model.addAttribute("hexDataRequest", hexDataRequest);

            } else {
                hexDataRequest = null;
                putActionErrorLog(request, "Import file yêu cầu");
                sendMessageError(request, "request.import.upload.validate.null");
            }
        } catch (Exception ex) {
            hexDataRequest = null;
            putActionErrorLog(request, "Import file yêu cầu");
            System.out.println("Loi doc file: " + ex);
        }
        return IMPORT_PAGE_REDIRECT;
    }

    /**
     * getHexbyte
     *
     * @param byteData
     * @param pos
     * @param bytelen
     * @return
     */
    public static String getHexbyte(String[] byteData, int bytelen) {
        String ret = "";
        for (int jdx = 0; jdx < bytelen; jdx++) {
            ret += byteData[countIndex + jdx];
        }
        countIndex = countIndex + bytelen;
        return ret;
    }

    /**
     * getHexbyte
     *
     * @param byteData
     * @param pos
     * @param bytelen
     * @return
     */
    public static String getHexbyteStart(String[] byteData, int bytelen) {
        countIndex = 0;
        String ret = "";
        for (int jdx = 0; jdx < bytelen; jdx++) {
            ret += byteData[countIndex + jdx];
        }
        countIndex = countIndex + bytelen;
        return ret;
    }
}
