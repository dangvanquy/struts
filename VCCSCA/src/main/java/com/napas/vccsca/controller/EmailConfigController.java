/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.email.SendEmail;
import com.napas.vccsca.form.EmailConfigForm;
import com.napas.vccsca.service.EmailConfigService;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.StringUtils;
import java.io.IOException;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * EmailConfigController
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
@RequestMapping("/")
public class EmailConfigController extends BaseBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(EmailConfigController.class);
    private static final String EMAIL_CONFIG_PAGE_REDIRECT = "cau-hinh-email.html";
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", Pattern.CASE_INSENSITIVE);
    private static final Pattern PASSWORD_REGEX = Pattern.compile("^[_A-z0-9!@#$%^&*]*$", Pattern.CASE_INSENSITIVE);
    private EmailConfigForm emailConfigForm;
    private ConfigEmailServerBO emailConfigBO;

    @PostConstruct
    public void init() {
        emailConfigForm = new EmailConfigForm();
        logger.info("EmailConfigController init method");
    }

    @RequestMapping(value = EMAIL_CONFIG_PAGE_REDIRECT, method = RequestMethod.GET)
    public String getdata(@Valid @ModelAttribute("EmailConfigForm") EmailConfigForm emailConfigForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logger.info("START get config email server");
        EmailConfigService emailConfigService = new EmailConfigService();

        try {
            emailConfigBO = emailConfigService.getEmailConfig();
            if (emailConfigBO != null) {
                emailConfigBO.setPassword(AESUtil.decryption(emailConfigBO.getPassword()));
            } else {
                emailConfigBO = new ConfigEmailServerBO();
            }

            setModelAttribute(model, emailConfigBO);
            putActionLog(request, "Truy vấn thông tin cấu hình Email");
            logger.info("END get config email server");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Truy vấn thông tin cấu hình Email");
        }
        return EMAIL_CONFIG_PAGE_REDIRECT;
    }

    @RequestMapping(value = EMAIL_CONFIG_PAGE_REDIRECT, method = RequestMethod.POST)
    public void emailConfigForm(@Valid @ModelAttribute("EmailConfigForm") EmailConfigForm emailConfigForm,
            Model model, BindingResult result, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (result.hasErrors()) {
            sendMessageError(request, "error");
            return;
        }
        EmailConfigService emailConfigService = new EmailConfigService();
        logger.info("START add config email server");
        try {
            if (validateEmailConfig(request, emailConfigForm)) {
                if (StringUtils.isNullOrEmpty(emailConfigBO)) {
                    emailConfigBO = new ConfigEmailServerBO();
                }
                emailConfigBO = emailConfigForm.toBO(emailConfigBO);

                SendEmail sendEmail = new SendEmail(logger).getInstance(emailConfigBO);
                sendEmail.loadEmailConfig(emailConfigBO);

                if (sendEmail.checkEmail()) {
                    emailConfigService.updateEmailConfig(emailConfigBO);
                    sendMessageSuccess(request, "config.validate.success");
                } else {
                    sendMessageError(request, "config.validate.error.cn");
                }
            }
            putActionLog(request, "Chỉnh sửa thông tin cấu hình Email");

        } catch (Exception ex) {
            logger.error("Have an error: ", ex.getMessage());
            sendMessageError(request, ex.getMessage());
            putActionErrorLog(request, "Chỉnh sửa thông tin cấu hình Email");
        }
        response.sendRedirect(EMAIL_CONFIG_PAGE_REDIRECT);

        logger.info("END add config email server");
    }

    private Boolean validateEmailConfig(HttpServletRequest request, EmailConfigForm emailConfigForm) throws IOException {
        if (!validateEmail(emailConfigForm.getEmail())) {
            sendMessageError(request, "email.config.validate.unformatted");
            return false;
        }
        if (emailConfigForm.getEmail().trim().length() > 50) {
            sendMessageError(request, "email.server.config.validate.lengh");
            return false;
        }
        if (StringUtils.isNullOrEmpty(emailConfigForm.getUsername())) {
            sendMessageError(request, "email.config.validate.usr");
            return false;
        }
        if (!emailConfigForm.getEmail().toLowerCase().startsWith(emailConfigForm.getUsername().toLowerCase())) {
            sendMessageError(request, "email.config.validate.usr.err");
            return false;
        }
        if (!emailConfigForm.getPassword().equals(emailConfigForm.getConfirmPassword())) {
            sendMessageError(request, "confirm.password.config.validate.not.equal");
            return false;
        }
        if (emailConfigForm.getMailServer().trim().length() > 50) {
            sendMessageError(request, "mail.server.config.validate.lengh");
            return false;
        }
        return true;
    }

    private static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}
