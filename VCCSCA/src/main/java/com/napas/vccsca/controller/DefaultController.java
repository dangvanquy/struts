/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.controller;

import com.napas.gw.Napas_GW_Mail;
import com.napas.vccsca.form.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalTime;
import javax.annotation.PostConstruct;

/**
 *
 * DefaultController
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
@Controller
//@RequestMapping("/")
public class DefaultController {
    
    @PostConstruct
    public void init() {
        Napas_GW_Mail gW_Mail = new Napas_GW_Mail();
        gW_Mail.main(null);
    }
//    @GetMapping()
//    public String mainTutorial(Model model) {
//        addCommonAttributes(model);
//        return "home";
//    }

//    @GetMapping("/login")
//    public String login(Model model) {
//        addCommonAttributes(model);
//        return "login";
//    }
    @GetMapping("/changePassword")
    public String changePassword(Model model) {
        addCommonAttributes(model);
        return "changePassword";
    }
    
    @GetMapping("/recoverPassword")
    public String recoverPassword(Model model) {
        addCommonAttributes(model);
        return "recoverPassword";
    }
    
    @GetMapping("/requestFile")
    public String requestFile(Model model) {
        addCommonAttributes(model);
        return "requestFile";
    }
    
    @GetMapping("/request")
    public String request(Model model) {
        addCommonAttributes(model);
        return "request";
    }
    
    @GetMapping("/certificate")
    public String certificate(Model model) {
        addCommonAttributes(model);
        return "certificate";
    }
    
    @GetMapping("/verifyCertificate")
    public String verifyCertificate(Model model) {
        addCommonAttributes(model);
        return "verifyCertificate";
    }
    
    @GetMapping("/bankMembership")
    public String bankMembership(Model model) {
        addCommonAttributes(model);
        return "bankMembership";
    }
    
    @GetMapping("/rsa")
    public String rsa(Model model) {
        addCommonAttributes(model);
        return "rsa";
    }
    
    @GetMapping("/checksum")
    public String checksum(Model model) {
        addCommonAttributes(model);
        return "checksum";
    }
    
    @GetMapping("/warning")
    public String warning(Model model) {
        addCommonAttributes(model);
        return "warning";
    }
    
    @GetMapping("/user")
    public String user(Model model) {
        addCommonAttributes(model);
        return "user";
    }
    
    @GetMapping("/loginHistory")
    public String loginHistory(Model model) {
        addCommonAttributes(model);
        return "loginHistory";
    }
    
    @GetMapping("/removeRequestList")
    public String removeRequestList(Model model) {
        addCommonAttributes(model);
        return "removeRequestList";
    }
    
    @GetMapping("/permissionList")
    public String permissionList(Model model) {
        addCommonAttributes(model);
        return "permissionList";
    }
    
    @GetMapping("/emailConfig")
    public String emailConfig(Model model) {
        addCommonAttributes(model);
        return "emailConfig";
    }
    
    @GetMapping("/paramConfig")
    public String paramConfig(Model model) {
        addCommonAttributes(model);
        return "paramConfig";
    }
    
    @GetMapping("/java")
    public String javaTutorial(Model model) {
        addCommonAttributes(model);
        return "defJavaTutorial";
    }
    
    @GetMapping("/spring")
    public String springTutorial(Model model) {
        addCommonAttributes(model);
        return "defSpringTutorial";
    }
    
    private void addCommonAttributes(Model model) {
        model.addAttribute("time", LocalTime.now());
    }
}
