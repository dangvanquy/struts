/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.vccsca.constants;

/**
 *
 * Constants
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class Constants extends Tester {

    public static final long ONE_DAY_MILLISECONDS = 24 * 60 * 60 * 1000 - 1;
    public static final String FIRST_VALUE = "-1";//selectOneMenu no selection value 

    public static final class ORDER {

        public static final String ASCENDING = "ASCENDING";
        public static final String DESCENDING = "DESCENDING";

    }

    public static class LOGIN_MESSAGE {

        public static final String PASSWORD_NOT_CHANGED = "PASSWORD_NOT_CHANGED";
    }

    public static class LOGIN_FAIL {

        public static final String LOGIN_FAIL_OVER_SHOW_CAPTCHA = "LOGIN_FAIL_OVER_SHOW_CAPTCHA";
        public static final String LOGIN_FAIL_OVER_LOCK_ACCOUNT = "LOGIN_FAIL_OVER_LOCK_ACCOUNT";
    }

    public static class SEND_EMAL_CLASS {

        public static final String SEND_MAIL_SSLTLS = "com.napas.vccsca.email.InterfaceSendEmaiSSLTLS";
        public static final String SEND_MAIL_SSL = "com.napas.vccsca.email.InterfaceSendEmailSSL";
        public static final String SEND_MAIL_TLS = "com.napas.vccsca.email.InterfaceSendEmailTLS";

    }

    public static class INDEX_MENU {

        public static final String CREATE_INFO_ENTERPRISE = "/khoi-tao-thong-tin.html";
        public static final String INVOICE_PUBLISH_ANNOUNCEMENT = "/lap-thong-bao-phat-hanh.html";
        public static final String CREATE_INVOICE = "/lap-hoa-don.html";
        public static final String INVOICE_MANAGEMENT = "/quan-ly-hoa-don.html";
        public static final String PRODUCT_MANAGEMENT = "/quan-ly-san-pham.html";
        public static final String REPORT_INVOICE_USED = "/thong-ke-tinh-trang-su-dung-hoa-don.html";
        public static final String REPORT_SMS_USED = "/thong-ke-tinh-trang-su-dung-sms.html";
    }

    public static class KPI_LOG {

        public static final String APPLICATION_CODE = "EINVOICE";
        public static final String SERVICE_CODE = "EINVOICE_WEB";
        public static final String UNPARSABLE_OBJECT = "UNPARSABLE_OBJECT";

        public static class TRANSACTION_STATUS {

            public static final Integer SUCCESS = 1;
            public static final Integer FAIL = 0;
        }

        public static class ERROR_CODE {

            public static final Long COMMON_ERROR = 0L;
        }

    }

    public static class PERMISSION {

        //Quản lý yêu cầu
        public static final String[] REQUEST_VIEW_INDEX = {"0001", "mau-yeu-cau.html"};//xem Register Number
        public static final String[] REQUEST_CREATE_INDEX = {"0002", "createIndex.do"};//tao Register Number
        public static final String[] REQUEST_EXPORT_INDEX = {"0003", "exportIndex.do"};//export Register Number

        public static final String[] REQUEST_VIEW = {"1001", "quan-ly-yeu-cau.html"};//xem request
        public static final String[] REQUEST_ADD = {"1002", "createRequest.do"};//tao request theo Register Number,
        public static final String[] REQUEST_EDIT = {"1003", "editRequest.do"};//edit request
        public static final String[] REQUEST_DELETE = {"1004", "deleteRequest.do"};//xoa request
        public static final String[] REQUEST_IMPORT = {"1005", "importRequest.do"};//import yeu cau
        public static final String[] REQUEST_SUBMISSION = {"1006", "submissionRequest.do"};//trinh ky
        public static final String[] REQUEST_APPROVAL = {"1007", "approvalRequest.do"};//ky duyet
        public static final String[] REQUEST_REJECT = {"1007", "rejectRequest.do"};//tu choi

        //Quản lý chứng thư số     
        public static final String[] CERTIFICATE_VIEW = {"2001", "quan-ly-chung-thu-so.html"};//xem chung thu
        public static final String[] CERTIFICATE_EXPORT = {"2002", "exportCertificate.do"};//xuat file chung thu
        public static final String[] CERTIFICATE_RECALL = {"2003", "recallCertificate.do"};//thu hoi chung thu
        public static final String[] CERTIFICATE_RESTORE = {"2004", "restoreCertificate.do"};//khoi phuc chung thu da thu hoi
        public static final String[] CERTIFICATE_VERIFY = {"2005", "xac-minh-chung-thu-so.html"};//kiem tra chung thu
        public static final String[] CERTIFICATE_VERIFY_CREATE = {"2006", "verifyCertificate.do"};//kiem tra chung thu
        public static final String[] CERTIFICATE_IMPORT = {"2007", "importCertificate.do"};//luu chung thu vao he thong

        //Quản lý ngân hàng thành viên
        public static final String[] BANKMEMBERSHIP_VIEW = {"3001", "quan-ly-nhtv.html"};//xem thong tin ngan hang thanh vien
        public static final String[] BANKMEMBERSHIP_EDIT = {"3002", "editBankMembership.do"};//ching sua thong tin ngan hang thanh vien
        public static final String[] BANKMEMBERSHIP_DELETE = {"3003", "deleteBankMembership.do"};//xoa ngan hang thanh vien
        public static final String[] BANKMEMBERSHIP_ADD = {"3004", "createBankMembership.do"};//them moi ngan hang thanh vien

        //Báo cáo
        public static final String[] REPORT_VIEW_ROOTCA = {"4001", "bao-cao-root-ca.html"};//xem bao cao
        public static final String[] REPORT_EXPORT_ROOTCA = {"4002", "exportRootCAReport.do"};//export bao cao
        public static final String[] REPORT_VIEW_CERT = {"4003", "bao-cao-chung-thu-so.html"};//xem bao cao
        public static final String[] REPORT_EXPORT_CERT = {"4004", "exportCertReport.do"};//export bao cao
        public static final String[] REPORT_VIEW_REQUEST = {"4005", "bao-cao-yeu-cau-cap-cts.html"};//xem bao cao
        public static final String[] REPORT_EXPORT_REQUEST = {"4006", "exportRequestReport.do"};//export bao cao
        public static final String[] REPORT_VIEW_NHTV = {"4007", "bao-cao-nhtv.html"};//xem bao cao
        public static final String[] REPORT_EXPORT_NHTV = {"4008", "exportNhtvReport.do"};//export bao cao

        //Quan tri he thong, admin
        public static final String[] ADMIN_VIEW_ACCOUNT = {"5001", "quan-ly-nguoi-dung.html"};//Quản lý tài khoản
        public static final String[] ADMIN_ADD_ACCOUNT = {"5002", "createAccount.do"};//Quản lý tài khoản
        public static final String[] ADMIN_EDIT_ACCOUNT = {"5003", "editAccount.do"};//Quản lý tài khoản
        public static final String[] ADMIN_LOCK_ACCOUNT = {"5003", "lockUser.do"};//Quản lý tài khoản
        public static final String[] ADMIN_UNLOCK_ACCOUNT = {"5003", "unlockUser.do"};//Quản lý tài khoản

        public static final String[] ADMIN_VIEW_GROUPPERMISION = {"6001", "danh-sach-nhom-quyen.html"};//Quản lý nhóm quyền
        public static final String[] ADMIN_ADD_GROUPPERMISION = {"6002", "createGroupPermission.do"};//Quản lý nhóm quyền
        public static final String[] ADMIN_EDIT_GROUPPERMISION = {"6003", "editGroupPermission.do"};//Quản lý nhóm quyền
        public static final String[] ADMIN_DELETE_GROUPPERMISION = {"6004", "deleteGroupPermission.do"};//Quản lý nhóm quyền

        public static final String[] ADMIN_VIEW_ALERT = {"7001", "quan-ly-canh-bao.html"};//Cấu hình cảnh báo
        public static final String[] ADMIN_ADD_ALERT = {"7002", "createAlert.do"};//Cấu hình cảnh báo
        public static final String[] ADMIN_EDIT_ALERT = {"7003", "editAlert.do"};//Cấu hình cảnh báo
        public static final String[] ADMIN_DELETE_ALERT = {"7004", "deleteAlert.do"};//Cấu hình cảnh báo

        public static final String[] ADMIN_VIEW_KEY = {"8001", "quan-ly-rsa.html"};//Quản lý khóa
        public static final String[] ADMIN_ADD_KEY = {"8002", "createKey.do"};//Quản lý khóa
        public static final String[] ADMIN_EDIT_KEY = {"8003", "editKey.do"};//Quản lý khóa
        public static final String[] ADMIN_LOCK_KEY = {"8003", "lockRsaKey.do"};//Quản lý khóa
        public static final String[] ADMIN_UNLOCK_KEY = {"8003", "unlockRsaKey.do"};//Quản lý khóa
        public static final String[] ADMIN_IMPORT_KEY = {"8004", "importKey.do"};//Quản lý khóa
        public static final String[] ADMIN_EXPORT_KEY = {"8005", "exportKey.do"};//Quản lý khóa

        public static final String[] ADMIN_VIEW_CHECKSUM = {"9001", "quan-ly-checksum.html"};//Quản lý CheckSum
        public static final String[] ADMIN_ADD_CHECKSUM = {"9002", "createChecksum.do"};//Quản lý CheckSum
        public static final String[] ADMIN_EDIT_CHECKSUM = {"9003", "editChecksum.do"};//Quản lý CheckSum
        public static final String[] ADMIN_LOCK_CHECKSUM = {"9003", "lockChecksum.do"};//Quản lý CheckSum
        public static final String[] ADMIN_UNLOCK_CHECKSUM = {"9003", "unlockChecksum.do"};//Quản lý CheckSum

        public static final String[] ADMIN_VIEW_RQDELETE = {"1008", "danh-sach-yeu-cau-da-xoa.html"};//Danh sách yêu cầu đã xóa
        public static final String[] ADMIN_RECOVER_RQDELETE = {"1009", "rqdelete.do"};//Khôi phục yêu cầu đã xóa

        public static final String[] ADMIN_VIEW_HISTORY = {"1101", "lich-su-truy-cap.html"};//Lịch sử truy cập
        public static final String[] ADMIN_EXPORT_HISTORY = {"1102", "exportHistory.do"};//Lịch sử truy cập

        public static final String[] ADMIN_VIEW_PARAM = {"1201", "cau-hinh-tham-so.html"};//Cấu hình tham số cảnh báo
        public static final String[] ADMIN_ADD_PARAM = {"1202", "createParam.do"};//Cấu hình tham số cảnh báo
        public static final String[] ADMIN_EDIT_PARAM = {"1203", "editParam.do"};//Cấu hình tham số cảnh báo
        public static final String[] ADMIN_DELETE_PARAM = {"1204", "deleteParam.do"};//Cấu hình tham số cảnh báo

        public static final String[] ADMIN_VIEW_EMAIL = {"1301", "cau-hinh-email.html"};//Cấu hình Email
        public static final String[] ADMIN_ADD_EMAIL = {"1302", "cau-hinh-email.html"};//Cấu hình Email
        public static final String[] ADMIN_EDIT_EMAIL = {"1303", "editEmail.do"};//Cấu hình Email

        public static final String[] LOGIN_ACTION = {"1401", "login.do"};//đăng nhập
        public static final String[] LOGOUT_ACTION = {"1402", "logout.do"};//đăng xuất
        public static final String[] RECOVER_PASSWORD_ACTION = {"1403", "recoverPassword.do"};//lấy lại mật khẩu
        public static final String[] CHANGE_PASSWORD_ACTION = {"1404", "changePass.do"};//thay đổi mật khẩu

        public static final String[] ADMIN_VIEW_CONFIG_HSM = {"1501", "cau-hinh-hsm.html"};//Cấu hình tham số cảnh báo
        public static final String[] ADMIN_ADD_CONFIG_HSM = {"1502", "createConfigHsm.do"};//Cấu hình tham số cảnh báo
        public static final String[] ADMIN_EDIT_CONFIG_HSM = {"1503", "editConfigHsm.do"};//Cấu hình tham số cảnh báo
        public static final String[] ADMIN_DELETE_CONFIG_HSM = {"1504", "deleteConfigHsm.do"};//Cấu hình tham số cảnh báo

        public static boolean eval(String val) {
            return evaluate(new PERMISSION().getClass(), val);
        }

        public static String search(String val) {
            return _search(new PERMISSION().getClass(), val);
        }

        public static String search(String val, int pos) {
            return __search(new PERMISSION().getClass(), val, pos);
        }
    }

    public static final class STATUS {

        public static final String ACTIVE = "Hoạt động";
        public static final String DEACTIVATE = "Ngưng hoạt động";

    }

    public static final class BANK_STATUS {

        public static final Integer ACTIVE = 0;
        public static final Integer DEACTIVATE = 1;

    }

    public static final class USER_STATUS {

        public static final Integer ACTIVE = 0;
        public static final Integer DEACTIVATE = 1;

    }

    public static final class SECURITY_TYPE {

        public static final String SSL_TLS = "SSL/TLS";
        public static final String SSL = "SSL";
        public static final String TLS = "TLS";
        public static final String NONE = "NONE";

    }

    public static final class ACCOUNT_TYPE {

        public static final String POP3 = "POP3";
        public static final String EXCHANGE = "EXCHANGE";
        public static final String IMAP = "IMAP";

    }

}
