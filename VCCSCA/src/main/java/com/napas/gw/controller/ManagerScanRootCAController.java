/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.controller;

import com.napas.gw.service.CertificateService;
import com.napas.gw.service.ConfigWarningSystemService;
import com.napas.gw.service.ParamService;
import com.napas.gw.service.RSAKeyService;
import com.napas.gw.service.SendMailHistoryService;
import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.BO.SendMailHistoryBO;
import com.napas.vccsca.database.SessionManager;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * ManagerScanEmailController
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class ManagerScanRootCAController extends BaseController {

    private static final Logger logger = LogManager.getLogger(ManagerScanRootCAController.class);

    @Override
    public void process() {
        logger.info("ManagerScanRootCAController start ");
        Session session = null;
        Calendar calendar = null;
        HashMap hashMap = new HashMap();
        List<RSAKeysBO> aKeysBOs = null;
        SendMailHistoryBO mailHistoryBO;
        HashMap<Integer, ParamBO> paramBOs = null;
        try {
            session = SessionManager.getSession();
            session.beginTransaction();

            ConfigWarningSystemService warningSystemService = new ConfigWarningSystemService(session);
            SendMailHistoryService historyService = new SendMailHistoryService(session);
            RSAKeyService rSAKeyService = new RSAKeyService(session);
            ParamService paramService = new ParamService(session);

            hashMap.put("type", 1);//1:root ca
            hashMap.put("lastScan", "true");
            List<ConfigWarningSystemBO> configWarningSystemBOs = warningSystemService.findConfigWarningSystem(hashMap);
            if (logger.isDebugEnabled()) {
                logger.debug("get list configWarningSystemBOs size :" + configWarningSystemBOs.size());
            }

            if (StringUtils.isNullOrEmpty(paramBOs)) {
                List<ParamBO> tmpparamBOs = paramService.findParam();
                paramBOs = new HashMap<>();
                for (ParamBO tmpparamBO : tmpparamBOs) {
                    paramBOs.put(tmpparamBO.getParamId(), tmpparamBO);
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug("get list paramBOs size :" + paramBOs.size());
            }

            for (ConfigWarningSystemBO configWarningSystemBO : configWarningSystemBOs) {
                hashMap.clear();
                calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, configWarningSystemBO.getDayConfig());
                hashMap.put("expDateAlert", calendar);
                aKeysBOs = rSAKeyService.getKeys(hashMap);
                if (logger.isDebugEnabled()) {
                    logger.debug("warning for :" + configWarningSystemBO.getWarningName());
                    logger.debug("get list aKeysBOs has warning :" + aKeysBOs.size());
                }

                if (!StringUtils.isNullOrEmpty(aKeysBOs) && aKeysBOs.size() > 0) {
                    for (RSAKeysBO keysBO : aKeysBOs) {
                        calendar = Calendar.getInstance();
                        mailHistoryBO = new SendMailHistoryBO();

                        //send time
                        String[] timesend = configWarningSystemBO.getSendTime().split(":");
                        calendar.set(Calendar.HOUR, NumberUtil.toNumber(timesend[0]));
                        calendar.set(Calendar.MINUTE, NumberUtil.toNumber(timesend[1]));

                        mailHistoryBO.setProcessTime(calendar.getTime());

                        mailHistoryBO.setSubject(configWarningSystemBO.getSubject());
                        mailHistoryBO.setMailTo(configWarningSystemBO.getMailTo());
                        mailHistoryBO.setMailCc(configWarningSystemBO.getMailCc());
                        mailHistoryBO.setMailBcc(configWarningSystemBO.getMailBcc());
                        mailHistoryBO.setContent(getContentEmail(configWarningSystemBO, paramBOs, keysBO, session));
                        mailHistoryBO.setSendNumber(configWarningSystemBO.getSendNumber());
                        mailHistoryBO.setSuccessCount(0);
                        mailHistoryBO.setCreateDate(new Date());
                        mailHistoryBO.setTem_id(configWarningSystemBO.getWarningId());
                        historyService.addNewHistory(mailHistoryBO);
                        SessionManager.commit(session);

                        if (logger.isDebugEnabled()) {
                            logger.debug("Create Mail warning for RsaIndex :" + keysBO.getRsaIndex());
                        }
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Update LastAccess scaner warning for :" + configWarningSystemBO.getWarningName());
                    logger.debug("Update LastAccess scaner warning date :" + DateTimeUtils.convertDateToString(new Date(), "yyyy/MM/dd"));
                }
                configWarningSystemBO.setLastScan(new Date());
                warningSystemService.updateConfigWarningSystem(configWarningSystemBO);
                SessionManager.commit(session);
            }

        } catch (Exception ex) {
            SessionManager.rollbackSession(session);
            logger.error("ManagerScanRootCAController Error :", ex);
        } finally {
            SessionManager.closeSession(session);
        }
        logger.info("ManagerScanRootCAController end ");
    }

    /**
     * getContentEmail
     *
     * @param emailTemplateBO
     * @param instantInput
     * @param lstContent
     * @return
     */
    private static String getContentEmail(ConfigWarningSystemBO configWarningSystemBO,
            HashMap<Integer, ParamBO> paramBOs, RSAKeysBO keysBO, Session session) throws Exception {
        if (StringUtils.isNullOrEmpty(configWarningSystemBO)) {
            throw new Exception("Xay ra loi load template email");
        }
        String fieldParam = configWarningSystemBO.getParam();

        String[] lstParamId;
        if (!StringUtils.isNullOrEmpty(fieldParam)) {
            lstParamId = fieldParam.split(",");
        } else {
            return configWarningSystemBO.getContent();
        }

        ParamBO paramBO = null;
        HashMap<String, String> map = new HashMap<>();
        for (int idx = 0; idx < lstParamId.length; idx++) {
            paramBO = paramBOs.get(NumberUtil.toNumber(lstParamId[idx]));
            map.put(paramBO.getColumnValue(), getParamData(paramBO, keysBO, session));
        }
        String textBody = configWarningSystemBO.getContent();
        for (String key : map.keySet()) {
            textBody = textBody.replace("{" + key + "}", map.get(key));
        }

        return textBody;
    }

    /**
     * getParamData
     *
     * @param paramBO
     * @param certificateBO
     * @return
     * @throws Exception
     */
    private static String getParamData(ParamBO paramBO, RSAKeysBO keysBO, Session session) throws Exception {
        if ("TBL_DIGITAL_CERTIFICATE".equals(paramBO.getTableName())) {
            return new CertificateService(session).getDataByParam(keysBO.getRsaId(), paramBO.getColumnValue()) + "";
        } else if ("TBL_RSA_KEYS".equals(paramBO.getTableName())) {
            return getMethod(paramBO.getColumnValue(), RSAKeysBO.class).invoke(keysBO, null) + "";
        }
        return "";
    }

    public static void main(String[] args) throws Exception {
        new ManagerScanRootCAController().process();
//        ParamBO paramBO = new ParamBO();
//        paramBO.setTableName("TBL_REQUEST");
//        paramBO.setColumnValue("bankName");
//
//        DigitalCertificateBO certificateBO = new DigitalCertificateBO();
//        certificateBO.setCreateUser("admin");
//        certificateBO.setBankName("okkkkk");
//        certificateBO.setBin(1234);
//        certificateBO.setRegisterId(1234);
//        certificateBO.setRsaId(1);
//        System.out.println(">>>>" + getParamData(paramBO, certificateBO));
    }

}
