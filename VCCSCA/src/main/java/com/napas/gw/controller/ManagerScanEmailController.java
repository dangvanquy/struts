/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.controller;

import static com.napas.gw.controller.BaseController.getMethod;
import com.napas.gw.service.CertificateService;
import com.napas.gw.service.ConfigWarningSystemService;
import com.napas.gw.service.EmailConfigService;
import com.napas.gw.service.SendMailHistoryService;

import com.napas.gw.utils.business.email.SendEmail;
import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.BO.SendMailHistoryBO;
import com.napas.vccsca.database.SessionManager;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * ManagerScanEmailController
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class ManagerScanEmailController extends BaseController {

    private static final Logger logger = LogManager.getLogger(ManagerScanEmailController.class);

    @Override
    public void process() {
        logger.info("ManagerScanEmailController start ");
        Session session = null;
        HashMap hashMap = new HashMap();
        ConfigEmailServerBO configEmailServerBO;
        try {
            session = SessionManager.getSession();
            session.beginTransaction();

            SendMailHistoryService historyService = new SendMailHistoryService(session);
            EmailConfigService configService = new EmailConfigService(session);

            hashMap.put("lastScan", "true");
            List<SendMailHistoryBO> historyBOs = historyService.getSendMailHistoryBO(hashMap);

            if (!StringUtils.isNullOrEmpty(historyBOs) && historyBOs.size() > 0) {

                configEmailServerBO = configService.getEmailConfig();
                if (StringUtils.isNullOrEmpty(configEmailServerBO)) {
                    logger.error("Error : Khong load duoc config");
                    return;
                }
                SendEmail sendEmail = new SendEmail(logger).getInstance(configEmailServerBO);
                sendEmail.loadEmailConfig(configEmailServerBO);

                for (SendMailHistoryBO historyBO : historyBOs) {
                    String ret = sendEmail.sendEmail(
                            toList(historyBO.getMailTo()),
                            ccList(historyBO.getMailCc()),
                            bccList(historyBO.getMailBcc()),
                            historyBO.getSubject(),
                            historyBO.getContent(), null);
                    if ("".equals(ret)) {
                        historyBO.setLastUpdate(new Date());
                        historyBO.setSuccessCount(historyBO.getSuccessCount() + 1);
                        historyService.updateHistory(historyBO);
                        SessionManager.commit(session);
                    } else {
                        logger.error("Error :" + ret);
                    }
                }
            }
        } catch (Exception ex) {
            SessionManager.rollbackSession(session);
            logger.error("ManagerScanEmailController Error :", ex);
        } finally {
            SessionManager.closeSession(session);
        }
        logger.info("ManagerScanEmailController end ");
    }

    /**
     * getContentEmail
     *
     * @param emailTemplateBO
     * @param instantInput
     * @param lstContent
     * @return
     */
    private static String getContentEmail(ConfigWarningSystemBO configWarningSystemBO,
            HashMap<Integer, ParamBO> paramBOs, RSAKeysBO keysBO, Session session) throws Exception {
        if (StringUtils.isNullOrEmpty(configWarningSystemBO)) {
            throw new Exception("Xay ra loi load template email");
        }
        String fieldParam = configWarningSystemBO.getParam();

        String[] lstParamId;
        if (!StringUtils.isNullOrEmpty(fieldParam)) {
            lstParamId = fieldParam.split(",");
        } else {
            return configWarningSystemBO.getContent();
        }

        ParamBO paramBO = null;
        HashMap<String, String> map = new HashMap<>();
        for (int idx = 0; idx < lstParamId.length; idx++) {
            paramBO = paramBOs.get(NumberUtil.toNumber(lstParamId[idx]));
            map.put(paramBO.getColumnValue(), getParamData(paramBO, keysBO, session));
        }
        String textBody = configWarningSystemBO.getContent();
        for (String key : map.keySet()) {
            textBody = textBody.replace("{" + key + "}", map.get(key));
        }

        return textBody;
    }

    /**
     * getParamData
     *
     * @param paramBO
     * @param certificateBO
     * @return
     * @throws Exception
     */
    private static String getParamData(ParamBO paramBO, RSAKeysBO keysBO, Session session) throws Exception {
        if ("TBL_DIGITAL_CERTIFICATE".equals(paramBO.getTableName())) {
            return new CertificateService(session).getDataByParam(keysBO.getRsaId(), paramBO.getColumnValue()) + "";
        } else if ("TBL_RSA_KEYS".equals(paramBO.getTableName())) {
            return getMethod(paramBO.getColumnValue(), RSAKeysBO.class).invoke(keysBO, null) + "";
        }
        return "";
    }

    public static void main(String[] args) throws Exception {
//        ConfigEmailServerBO emailConfigBO = new ConfigEmailServerBO(1, "mail.eprtech.com", 25, "luongnk@eprtech.com", "luongnk", "Luong@123", 0, "SSL/TLS");
//        SendEmail sendEmail = new SendEmail(logger).getInstance(emailConfigBO);
//
//        sendEmail.loadEmailConfig(emailConfigBO);
//      
//        String to = "eprtest01@gmail.com";
//        sendEmail.sendEmail(to, "Subject new SSL SCAN", "body yyyyyyyyyyyy", null);
    }

}
