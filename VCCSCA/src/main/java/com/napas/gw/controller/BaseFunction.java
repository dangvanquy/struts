/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.controller;

/**
 *
 * @author trung
 */
interface BaseFunction {

    /**
     * process
     */
    public void process();
}
