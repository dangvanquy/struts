/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.controller;


import com.napas.vccsca.utils.StringUtils;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trung
 */
public class BaseController implements BaseFunction {

    @Override
    public void process() {
    }

    public static Method getMethod(String key, Class clazz) {
        Method fields[] = clazz.getMethods();
        for (int i = 0; i < fields.length; i++) {
            if (("get" + key).equalsIgnoreCase(fields[i].getName())) {
                return fields[i];
            }
        }
        return null;
    }

    /**
     * ccList
     *
     * @param ccAddress
     * @return
     */
    public List<String> ccList(String ccAddress) {
        if (StringUtils.isNullOrEmpty(ccAddress)) {
            return new ArrayList();
        }
        String[] strings = ccAddress.split(";");
        List listCc = new ArrayList();
        for (String cc : strings) {
            listCc.add(cc);
        }
        return listCc;
    }

    /**
     * bccList
     *
     * @param bccAddress
     * @return
     */
    public List<String> bccList(String bccAddress) {
        if (StringUtils.isNullOrEmpty(bccAddress)) {
            return new ArrayList();
        }
        String[] strings = bccAddress.split(";");
        List listBcc = new ArrayList();
        for (String bcc : strings) {
            listBcc.add(bcc);
        }
        return listBcc;
    }

    /**
     * toList
     *
     * @param toAddress
     * @return
     */
    public List<String> toList(String toAddress) {
        if (StringUtils.isNullOrEmpty(toAddress)) {
            return new ArrayList();
        }
        String[] strings = toAddress.split(";");
        List listTo = new ArrayList();
        for (String to : strings) {
            listTo.add(to);
        }
        return listTo;
    }
}
