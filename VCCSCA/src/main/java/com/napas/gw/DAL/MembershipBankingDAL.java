/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;

import com.napas.vccsca.BO.BankMembershipBO;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * MembershipBankingDAL
 *
 * @author CuongTV
 * @since Aug 23, 2018
 * @version 1.0-SNAPSHOT
 */
public class MembershipBankingDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(MembershipBankingDAL.class);
    Session session = null;

    public MembershipBankingDAL(Session session) {
        this.session = session;
    }

    public void addBanking(BankMembershipBO banking) throws Exception {

        try {

            session.save(banking);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public void deleteBanking(BankMembershipBO banking) throws Exception {

        try {

            session.delete(banking);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public void updateBanking(BankMembershipBO banking) throws Exception {

        try {

            session.saveOrUpdate(banking);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public List<BankMembershipBO> getAllBanking() throws Exception {

        try {

            return session
                    .createCriteria(BankMembershipBO.class)
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public List<BankMembershipBO> getMembershipBanking(HashMap hashMap) throws Exception {

        try {

            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankId"))) {
                    cri.add(Restrictions.eq("bankId", NumberUtil.toNumber(hashMap.get("bankId").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", NumberUtil.toNumber(hashMap.get("bin").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankFullName"))) {
                    cri.add(Restrictions.ilike("bankFullName", (String) hashMap.get("bankFullName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankShortName"))) {
                    cri.add(Restrictions.ilike("bankShortName", (String) hashMap.get("bankShortName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", NumberUtil.toNumber(hashMap.get("status").toString())));
                }
            }
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public BankMembershipBO getMembershipBankById(int bankId) throws Exception {

        try {

            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (!StringUtils.isNullOrEmpty(bankId)) {
                cri.add(Restrictions.eq("bankId", bankId));
            }
            return (BankMembershipBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public String getDataByParam(int key, String property) {

        try {

            Criteria cri = session.createCriteria(BankMembershipBO.class);
            if (!StringUtils.isNullOrEmpty(key)) {
                cri.add(Restrictions.eq("bin", key));
            }
            if (!StringUtils.isNullOrEmpty(property)) {
                cri.setProjection(Projections.projectionList().add(Projections.property(property)));
            }
            return StringUtils.chgNull(cri.uniqueResult() + "");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

}
