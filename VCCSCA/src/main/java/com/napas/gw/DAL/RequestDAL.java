/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;

import com.napas.vccsca.BO.RequestBO;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * RequestDAL
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class RequestDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(RequestDAL.class);
    Session session = null;

    public RequestDAL(Session session) {
        this.session = session;
    }

    public void createRequest(RequestBO requestBO) throws Exception {

        try {

            session.saveOrUpdate(requestBO);
            
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public void updateRequest(RequestBO requestBO) throws Exception {

        try {

            session.saveOrUpdate(requestBO);
            
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public void deleteRequest(RequestBO requestBO) throws Exception {

        try {

            session.delete(requestBO);
            
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> getRequestList() throws Exception {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> getRequestFileList() throws Exception {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("status", 6));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> findRequest(HashMap hashMap) throws Exception {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("ipk"))) {
                    cri.add(Restrictions.eq("ipk", hashMap.get("ipk")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                    cri.add(Restrictions.eq("serial", hashMap.get("serial")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankIdentity"))) {
                    cri.add(Restrictions.eq("bankIdentity", hashMap.get("bankIdentity")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expDate"))) {
                    cri.add(Restrictions.eq("expDate", hashMap.get("expDate")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("createDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) = TO_DATE('" + hashMap.get("createDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("fromDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) >= TO_DATE('" + hashMap.get("fromDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("toDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) <= TO_DATE('" + hashMap.get("toDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
            }
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> findRequestFile(HashMap hashMap) {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("createDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) = TO_DATE('" + hashMap.get("createDate") + "','dd/mm/yyyy') "));
                }
            }
            cri.add(Restrictions.eq("status", 6));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public RequestBO getRequestById(Integer requestId) throws Exception {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("requestId", requestId));
            return (RequestBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public RequestBO getRequestByRegId(Integer registerId) throws Exception {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("registerId", registerId));
            return (RequestBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> findAllRequestRemove() throws Exception {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            cri.add(Restrictions.eq("status", 4));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> findRequestRemove(HashMap hashMap) {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("ipk"))) {
                    cri.add(Restrictions.eq("ipk", hashMap.get("ipk")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                    cri.add(Restrictions.eq("serial", hashMap.get("serial")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankIdentity"))) {
                    cri.add(Restrictions.eq("bankIdentity", hashMap.get("bankIdentity")));
                }
            }
            cri.add(Restrictions.eq("status", 4));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RequestBO> getRequests(HashMap hashMap) {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("registerId"))) {
                    cri.add(Restrictions.eq("registerId", hashMap.get("registerId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("appDate"))) {
                    cri.add(Restrictions.eq("appDate", hashMap.get("appDate")));
                }

            }
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public String getDataByParam(int key, String property) {

        try {

            Criteria cri = session.createCriteria(RequestBO.class);
            if (!StringUtils.isNullOrEmpty(key)) {
                cri.add(Restrictions.eq("requestId", key));
            }
            if (!StringUtils.isNullOrEmpty(property)) {
                cri.setProjection(Projections.projectionList().add(Projections.property(property)));
            }
            return StringUtils.chgNull(cri.uniqueResult() + "");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }
}
