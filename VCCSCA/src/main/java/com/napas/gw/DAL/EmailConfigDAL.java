/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;

import com.napas.vccsca.BO.ConfigEmailServerBO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * EmailConfigDAL
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class EmailConfigDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(EmailConfigDAL.class);
    Session session = null;

    public EmailConfigDAL(Session session) {
        this.session = session;
    }

    public void updateEmailConfig(ConfigEmailServerBO emailConfigBO) throws Exception {

        try {

            session.saveOrUpdate(emailConfigBO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }

    public ConfigEmailServerBO getEmailConfig() throws Exception {
        try {
//            Criteria cri = session.createCriteria(ConfigEmailServerBO.class).add(Restrictions.like("emailConfigId", 1));;
//            return (ConfigEmailServerBO) cri.uniqueResult();
            
            Criteria cri = session.createCriteria(ConfigEmailServerBO.class);
            return (ConfigEmailServerBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {

        }
    }
}
