/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;


import com.napas.vccsca.BO.DigitalCertificateBO;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.StringUtils;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * CertificateDAL
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class CertificateDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(CertificateDAL.class);

    Session session = null;

    public CertificateDAL(Session session) {
        this.session = session;
    }

    /**
     * addCertificate
     *
     * @param certificateBO
     * @throws Exception
     */
    public void addCertificate(DigitalCertificateBO certificateBO) throws Exception {
        try {
            session.saveOrUpdate(certificateBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    /**
     * updateCertificate
     *
     * @param certificateBO
     */
    public void updateCertificate(DigitalCertificateBO certificateBO) {
        try {
            session.saveOrUpdate(certificateBO);
            
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    /**
     * getAllCertificates
     *
     * @return
     */
    public List<DigitalCertificateBO> getAllCertificates() {
        try {
            return session
                    .createCriteria(DigitalCertificateBO.class)
                    .list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    /**
     * getCertificates
     *
     * @param hashMap
     * @return
     */
    public List<DigitalCertificateBO> getCertificates(HashMap hashMap) {
        try {
            Criteria cri = session.createCriteria(DigitalCertificateBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("bin"))) {
                    cri.add(Restrictions.eq("bin", hashMap.get("bin")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaId"))) {
                    cri.add(Restrictions.eq("rsaId", hashMap.get("rsaId")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankName"))) {
                    cri.add(Restrictions.eq("bankName", hashMap.get("bankName")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("serial"))) {
                    cri.add(Restrictions.eq("serial", hashMap.get("serial")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("bankIdentity"))) {
                    cri.add(Restrictions.eq("bankIdentity", hashMap.get("bankIdentity")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expDate"))) {
                    cri.add(Restrictions.like("expDate", hashMap.get("expDate")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", hashMap.get("status")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("fromDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) >= TO_DATE('" + hashMap.get("fromDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("toDate"))) {
                    cri.add(Restrictions.sqlRestriction(" TRUNC(CREATE_DATE) <= TO_DATE('" + hashMap.get("toDate") + "','dd/mm/yyyy') "));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expDateAlert"))) {
                    Calendar calendar = (Calendar) hashMap.get("expDateAlert");
                    cri.add(Restrictions.sqlRestriction(" '" + calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + "/' || EXP_DATE = '" + DateTimeUtils.convertDateToString(calendar.getTime(), "dd/MM/YYYY") + "'"));
                }
            }
            cri.addOrder(Order.asc("certificateId"));
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    /**
     * getCertificateById
     *
     * @param certificateId
     * @return
     */
    public DigitalCertificateBO getCertificateById(Integer certificateId) {
        try {
            Criteria cri = session.createCriteria(DigitalCertificateBO.class);
            if (!StringUtils.isNullOrEmpty(certificateId)) {
                cri.add(Restrictions.eq("certificateId", certificateId));
            }
            return (DigitalCertificateBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public String getDataByParam(String key, String property) {
        try {
            Criteria cri = session.createCriteria(DigitalCertificateBO.class);
            if (!StringUtils.isNullOrEmpty(key)) {
                cri.add(Restrictions.eq("username", key));
            }
            if (!StringUtils.isNullOrEmpty(property)) {
                cri.setProjection(Projections.projectionList().add(Projections.property(property)));
            }
            return StringUtils.chgNull(cri.uniqueResult() + "");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
        }
    }
    public String getDataByParam(int key, String property) {
        try {
            Criteria cri = session.createCriteria(DigitalCertificateBO.class);
            if (!StringUtils.isNullOrEmpty(key)) {
                cri.add(Restrictions.eq("rsaId", key));
            }
            if (!StringUtils.isNullOrEmpty(property)) {
                cri.setProjection(Projections.projectionList().add(Projections.property(property)));
            }
            return StringUtils.chgNull(cri.uniqueResult() + "");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {
        }
    }
}
