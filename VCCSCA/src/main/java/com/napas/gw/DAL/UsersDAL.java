/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;


import com.napas.vccsca.BO.UsersBO;
import com.napas.vccsca.common.OutputCommon;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


/**
 *
 * UsersDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class UsersDAL extends BaseDAL {

    Session session = null;

    public UsersDAL(Session session) {
        this.session = session;
    }

    public void addUsers(UsersBO users) throws Exception {

        try {

            session.save(users);
            
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public void deleteUsers(UsersBO users) throws Exception {

        try {

            session.delete(users);
            
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public void updateUsers(UsersBO users) throws Exception {

        try {

            session.saveOrUpdate(users);
            
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public List<UsersBO> getAllUsers() throws Exception {

        try {

            return session
                    .createCriteria(UsersBO.class)
                    .list();
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public List<UsersBO> getUser(HashMap hashMap) throws Exception {

        try {

            Criteria cri = session.createCriteria(UsersBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("fullName"))) {
                    cri.add(Restrictions.ilike("fullName", (String) hashMap.get("fullName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("username"))) {
                    cri.add(Restrictions.ilike("username", (String) hashMap.get("username"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("phoneNumber"))) {
                    cri.add(Restrictions.ilike("phoneNumber", (String) hashMap.get("phoneNumber"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("email"))) {
                    cri.add(Restrictions.ilike("email", (String) hashMap.get("email"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("status"))) {
                    cri.add(Restrictions.eq("status", NumberUtil.toNumber(hashMap.get("status").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("groupId"))) {
                    cri.add(Restrictions.eq("groupId", NumberUtil.toNumber(hashMap.get("groupId").toString())));
                }
            }
            cri.addOrder(Order.asc("username"));
            List list = cri.list();
            return list;
        } catch (Exception ex) {
            throw ex;
        } finally {

        }

    }

    public boolean checkEmailExist(String email, Long userId) throws Exception {

        try {

            StringBuilder strQueryBuilder = new StringBuilder("select 1 from UsersBO where lower(emailAddress) = :emailAddress and rownum <=1");
            if (userId != null) {
                strQueryBuilder.append(" and userId <> :userId ");
            }
            Object obj;
            if (userId == null) {
                obj = session
                        .createQuery(strQueryBuilder.toString())
                        .setParameter("emailAddress", email.toLowerCase())
                        .uniqueResult();
            } else {
                obj = session
                        .createQuery(strQueryBuilder.toString())
                        .setParameter("emailAddress", email.toLowerCase())
                        .setParameter("userId", userId)
                        .uniqueResult();
            }
            return obj != null && (Long.valueOf(obj.toString())).intValue() > 0;
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public UsersBO getUsersById(Object id) throws Exception {

        try {

            List list = session.createQuery("from UsersBO where userId = :userId").setParameter("userId", id).list();
            if (list.isEmpty()) {
                return null;
            }
            return (UsersBO) list.get(0);
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public UsersBO getUserById(Integer userId) throws Exception {

        try {

            Criteria cri = session.createCriteria(UsersBO.class);
            if (!StringUtils.isNullOrEmpty(userId)) {
                cri.add(Restrictions.eq("userId", userId));
            }
            return (UsersBO) cri.uniqueResult();
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public String getDataByParam(String key, String property) throws Exception {

        try {

            Criteria cri = session.createCriteria(UsersBO.class);
            if (!StringUtils.isNullOrEmpty(key)) {
                cri.add(Restrictions.eq("username", key));
            }
            if (!StringUtils.isNullOrEmpty(property)) {
                cri.setProjection(Projections.projectionList().add(Projections.property(property)));
            }
            return StringUtils.chgNull(cri.uniqueResult() + "");
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public UsersBO getUserByEmail(String email) throws Exception {

        try {

            Criteria cri = session.createCriteria(UsersBO.class);
            if (!StringUtils.isNullOrEmpty(email)) {
                cri.add(Restrictions.eq("email", email));
            }
            return (UsersBO) cri.uniqueResult();
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public OutputCommon getUser(String userName, String password) throws Exception {

        try {

            Criteria cri = session.createCriteria(UsersBO.class);
            cri.add(Restrictions.eq("username", userName));
            List<UsersBO> list = cri.list();
            if (list == null || list.size() == 0) {
                return new OutputCommon("login.user.notexist", "", null);
            } else {
                UsersBO usersBO = list.get(0);
                if (usersBO != null && password.equals(usersBO.getPassword())) {
                    return new OutputCommon("", "", usersBO);
                } else {
                    return new OutputCommon("login.pass.error", "", null);
                }
            }
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }
}
