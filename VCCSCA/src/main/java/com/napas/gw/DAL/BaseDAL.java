/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;

import com.napas.vccsca.constants.Constants;
import com.napas.vccsca.database.SessionManager;
import java.io.Serializable;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * BaseDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class BaseDAL implements Serializable {

    private static SessionManager sessionManager = null;

    /**
     * getSession
     *
     * @return
     */
    public static Session getSession() {
        Session session = null;
        try {
            if (sessionManager == null) {
                sessionManager = new SessionManager();
            }
            session = sessionManager.getSession();
            session.beginTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            session = null;
        }
        return session;
    }

    /**
     * closeSession
     *
     * @param session
     */
    public void closeSession(Session session) {
        try {
            if (session != null) {
                if (sessionManager == null) {
                    sessionManager = new SessionManager();
                }
                sessionManager.closeSession(session);
            }
        } catch (Exception e) {
            e.printStackTrace();
            session = null;
        }
    }

    /**
     * addOrder
     *
     * @param cri
     * @param begin
     * @param rowPerPage
     * @param fieldOrder
     * @param orderValue
     * @throws Exception
     */
    void addOrder(Criteria cri, int begin, int rowPerPage, String fieldOrder, String orderValue) throws Exception {
        if (fieldOrder != null) {
            switch (orderValue) {
                case Constants.ORDER.ASCENDING:
                    cri.addOrder(Order.asc(fieldOrder));
                    break;
                case Constants.ORDER.DESCENDING:
                    cri.addOrder(Order.desc(fieldOrder));
                    break;
            }
        }
        cri.setFirstResult(begin);
        cri.setMaxResults(rowPerPage);
    }
}
