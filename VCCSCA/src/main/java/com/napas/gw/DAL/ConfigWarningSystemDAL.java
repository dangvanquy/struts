/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;

import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.StringUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * ConfigWarningSystemDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ConfigWarningSystemDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ConfigWarningSystemDAL.class);
    Session session = null;

    public ConfigWarningSystemDAL(Session session) {
        this.session = session;
    }

    public void addConfigWarningSystem(ConfigWarningSystemBO configWarningBO) throws Exception {
        try {
            session.save(configWarningBO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public void deleteConfigWarningSystem(ConfigWarningSystemBO configWarningBO) {
        try {
            session.delete(configWarningBO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public void updateConfigWarningSystem(ConfigWarningSystemBO configWarningBO) {
        try {
            session.saveOrUpdate(configWarningBO);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    /**
     * findConfigWarningSystem
     *
     * @param searchMap
     * @param maxResults
     * @return
     */
    public List<ConfigWarningSystemBO> findConfigWarningSystem(HashMap searchMap) {
        try {
            Criteria cri = session.createCriteria(ConfigWarningSystemBO.class);
            if (searchMap != null) {
                if (!StringUtils.isNullOrEmpty(searchMap.get("createUser"))) {
                    cri.add(Restrictions.eq("createUser", searchMap.get("createUser")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("warningId"))) {
                    cri.add(Restrictions.eq("warningId", searchMap.get("warningId")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("warningName"))) {
                    cri.add(Restrictions.ilike("warningName", (String) searchMap.get("warningName"), MatchMode.ANYWHERE));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("status"))) {
                    cri.add(Restrictions.eq("status", searchMap.get("status")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("type"))) {
                    cri.add(Restrictions.eq("type", searchMap.get("type")));
                }
                if (!StringUtils.isNullOrEmpty(searchMap.get("lastScan")) && "true".equals(searchMap.get("lastScan"))) {
                    cri.add(Restrictions.sqlRestriction(" ( LAST_SCAN IS NULL OR TO_CHAR(LAST_SCAN,'yyyy/mm/dd') != '"
                            + DateTimeUtils.convertDateToString(new Date(), "yyyy/MM/dd") + "' ) "));
                }
            }
            return cri.list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public ConfigWarningSystemBO getConfigWarningSystemById(HashMap hashMap) {
        try {
            Criteria cri = session.createCriteria(ConfigWarningSystemBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("warningId"))) {
                    cri.add(Restrictions.eq("warningId", hashMap.get("warningId")));
                }
            }
            return (ConfigWarningSystemBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public ConfigWarningSystemBO getConfigWarningSystemById(Integer warningId) {
        try {
            Criteria cri = session.createCriteria(ConfigWarningSystemBO.class);
            cri.add(Restrictions.eq("warningId", warningId));
            return (ConfigWarningSystemBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public List getAllCreateUserConfigWarningSystem() {
        try {
            return session.createCriteria(ConfigWarningSystemBO.class)
                    .setProjection(Projections.property("createUser"))
                    .setProjection(Projections.distinct(Projections.property("createUser"))).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }
}
