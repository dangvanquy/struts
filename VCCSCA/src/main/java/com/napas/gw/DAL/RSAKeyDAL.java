/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;


import com.napas.vccsca.BO.RSAKeysBO;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.NumberUtil;
import com.napas.vccsca.utils.StringUtils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * RSAKeyDAL
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class RSAKeyDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(RSAKeyDAL.class);
    Session session = null;

    public RSAKeyDAL(Session session) {
        this.session = session;
    }

    public void addKeys(RSAKeysBO keysBO) throws Exception {

        try {

            session.save(keysBO);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public void updateKeys(RSAKeysBO keysBO) throws Exception {

        try {

            session.saveOrUpdate(keysBO);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public List<RSAKeysBO> getAllKeys() throws Exception {

        try {

            return session
                    .createCriteria(RSAKeysBO.class)
                    .addOrder(Order.asc("rsaIndex"))
                    .list();
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public List<RSAKeysBO> getKeysByRsaStatus() throws Exception {

        try {

            Criteria cri = session.createCriteria(RSAKeysBO.class);
            cri.add(Restrictions.isNull("rsaStatus"));
            cri.addOrder(Order.asc("rsaIndex"));
            return cri.list();
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    public List<RSAKeysBO> getKeys(HashMap hashMap) throws Exception {

        try {

            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaIndex"))) {
                    cri.add(Restrictions.eq("rsaIndex", NumberUtil.toNumber(hashMap.get("rsaIndex").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("rsaStatus"))) {
                    cri.add(Restrictions.eq("rsaStatus", NumberUtil.toNumber(hashMap.get("rsaStatus").toString())));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expirationDate"))) {
                    cri.add(Restrictions.eq("expirationDate", hashMap.get("expirationDate")));
                }
                if (!StringUtils.isNullOrEmpty(hashMap.get("expDateAlert"))) {
                    Calendar calendar = (Calendar) hashMap.get("expDateAlert");
                    cri.add(Restrictions.sqlRestriction(" '" + calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + "/' || EXPIRATION_DATE = '" + DateTimeUtils.convertDateToString(calendar.getTime(), "dd/MM/YYYY") + "'"));
                }
                cri.add(Restrictions.isNotNull("rsaStatus"));
            }
            cri.addOrder(Order.asc("rsaIndex"));
            return cri.list();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public RSAKeysBO getKeyById(Integer rsaId) throws Exception {

        try {

            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaId)) {
                cri.add(Restrictions.eq("rsaId", rsaId));
            }
            return (RSAKeysBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public RSAKeysBO getKeyByIndex(Integer rsaIndex) throws Exception {

        try {

            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(rsaIndex)) {
                cri.add(Restrictions.eq("rsaIndex", rsaIndex));
            }
            return (RSAKeysBO) cri.uniqueResult();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

    public String getDataByParam(int key, String property) {

        try {

            Criteria cri = session.createCriteria(RSAKeysBO.class);
            if (!StringUtils.isNullOrEmpty(key)) {
                cri.add(Restrictions.eq("rsaId", key));
            }
            if (!StringUtils.isNullOrEmpty(property)) {
                cri.setProjection(Projections.projectionList().add(Projections.property(property)));
            }
            return StringUtils.chgNull(cri.uniqueResult() + "");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw ex;
        } finally {

        }
    }

}
