/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;



import com.napas.vccsca.BO.SendMailHistoryBO;
import com.napas.vccsca.utils.DateTimeUtils;
import com.napas.vccsca.utils.StringUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * SendMailHistoryDAL
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendMailHistoryDAL extends BaseDAL {

    Session session = null;

    public SendMailHistoryDAL(Session session) {
        this.session = session;
    }

    public void addNewHistory(SendMailHistoryBO sendMailHistoryBO) throws Exception {
        try {
            session.save(sendMailHistoryBO);
        } catch (Exception ex) {
            throw ex;
        } finally {

        }
    }

    /**
     * getSendMailHistoryBO
     *
     * @param hashMap
     * @return
     */
    public List<SendMailHistoryBO> getSendMailHistoryBO(HashMap searchMap) {
        try {
            Criteria cri = session.createCriteria(SendMailHistoryBO.class);
            if (searchMap != null) {

                if (!StringUtils.isNullOrEmpty(searchMap.get("lastScan")) && "true".equals(searchMap.get("lastScan"))) {
                    cri.add(Restrictions.isNotNull("tem_id"));
                    cri.add(Restrictions.sqlRestriction(" SUCCESS_COUNT < SEND_NUMBER "));
                    cri.add(Restrictions.sqlRestriction(" TO_Date(TO_CHAR(SYSDATE,'DD-Mon-RR') || ' ' || TO_CHAR(PROCESS_TIME,'HH24:MI'), 'DD-Mon-RR HH24:MI') < SYSDATE  "));
                    cri.add(Restrictions.sqlRestriction(" ( LAST_UPDATE IS NULL OR TO_CHAR(LAST_UPDATE,'yyyy/mm/dd') != '"
                            + DateTimeUtils.convertDateToString(new Date(), "yyyy/MM/dd") + "' ) "));
                }
            }
            return cri.list();
        } catch (Exception e) {
            throw e;
        } finally {
        }
    }

    public void updateHistory(SendMailHistoryBO sendMailHistoryBO) {
        session.saveOrUpdate(sendMailHistoryBO);
    }
}
