/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.DAL;

import com.napas.vccsca.BO.ParamBO;
import com.napas.vccsca.utils.StringUtils;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * ParamDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ParamDAL extends BaseDAL {

    private static final Logger logger = LogManager.getLogger(ParamDAL.class);
    Session session = null;

    public ParamDAL(Session session) {
        this.session = session;
    }

    public void addParam(ParamBO paramBO) throws Exception {
        try {
            session.save(paramBO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public void deleteParam(ParamBO paramBO) {
        try {
            session.delete(paramBO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public void updateParam(ParamBO paramBO) {
        try {
            session.saveOrUpdate(paramBO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public List<ParamBO> findParam() {
        try {
            return session.createCriteria(ParamBO.class).list();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

    public ParamBO getParamById(HashMap hashMap) {
        try {
            Criteria cri = session.createCriteria(ParamBO.class);
            if (hashMap != null) {
                if (!StringUtils.isNullOrEmpty(hashMap.get("paramId"))) {
                    cri.add(Restrictions.eq("paramId", hashMap.get("paramId")));
                }
            }
            return (ParamBO) cri.uniqueResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
        }
    }

}
