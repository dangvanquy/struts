/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw;

//import com.napas.gw.thread.ManagerScanEmailThread;
import com.napas.gw.controller.ManagerScanEmailController;
import com.napas.gw.controller.ManagerScanCertController;
import com.napas.gw.controller.ManagerScanRootCAController;
import com.napas.gw.thread.ManagerScanThread;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * UsersDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class Napas_GW_Mail {

    private static final Logger logger = LogManager.getLogger(Napas_GW_Mail.class);
//    public final static ResourceBundle rb = ResourceBundle.getBundle("config");//cas

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        logger.info("START Napas_GW_Mail");
        start_service();
        logger.info("END Napas_GW_Mail");
    }

    /**
     * start_service
     */
    public static void start_service() {

        logger.info("START SERVICE ALERT CERT SCAN MAIL");
        ManagerScanThread managerScanCertThread = new ManagerScanThread(ManagerScanCertController.class);
        managerScanCertThread.start();
        logger.info("END SERVICE ALERT CERT SCAN MAIL");

        logger.info("START SERVICE ROOT CA SCAN MAIL");
        ManagerScanThread managerScanRootCAThread = new ManagerScanThread(ManagerScanRootCAController.class);
        managerScanRootCAThread.start();
        logger.info("END SERVICE ROOT CA SCAN MAIL");
        
        logger.info("START SERVICE ROOT CA SCAN MAIL");
        ManagerScanThread managerScanEmailThread = new ManagerScanThread(ManagerScanEmailController.class);
        managerScanEmailThread.start();
        logger.info("END SERVICE ROOT CA SCAN MAIL");

//        logger.info("START WAIT SOCKET SEND EMAIL");
//        ManagerScanThread managerSocketEmailThread = new ManagerScanThread();
//        managerSocketEmailThread.start();
//        logger.info("END WAIT SOCKET SEND EMAIL");
    }

}
