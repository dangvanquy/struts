/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.MembershipBankingDAL;
import com.napas.vccsca.BO.BankMembershipBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * BankingService
 *
 * @author CuongTV
 * @since Aug 23, 2018
 * @version 1.0-SNAPSHOT
 */
public class BankingService extends BaseService implements Serializable {

    private static MembershipBankingDAL bankingDAL;
    Session session;

    public BankingService(Session session) {
        this.session = session;
        bankingDAL = new MembershipBankingDAL(session);
    }

    public void addBanking(BankMembershipBO banking) throws Exception {
        getBankingDAL().addBanking(banking);
    }

    public void updateBanking(BankMembershipBO banking) throws Exception {
        getBankingDAL().updateBanking(banking);
    }

    public void deleteBanking(BankMembershipBO banking) throws Exception {
        getBankingDAL().deleteBanking(banking);
    }

    public BankMembershipBO getMembershipBankingById(int bankId) throws Exception {
        return getBankingDAL().getMembershipBankById(bankId);
    }

    public List<BankMembershipBO> findAll() throws Exception {
        return getBankingDAL().getAllBanking();
    }

    public List<BankMembershipBO> getMembershipBanking(HashMap hashMap) throws Exception {
        return getBankingDAL().getMembershipBanking(hashMap);
    }

    public String getDataByParam(int key, String property) throws Exception {
        return getBankingDAL().getDataByParam(key, property);
    }

    public MembershipBankingDAL getBankingDAL() {
        if (bankingDAL == null) {
            bankingDAL = new MembershipBankingDAL(session);
        }
        return bankingDAL;
    }
}
