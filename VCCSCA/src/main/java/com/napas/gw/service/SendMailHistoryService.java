/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;


import com.napas.gw.DAL.SendMailHistoryDAL;
import com.napas.vccsca.BO.SendMailHistoryBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * SendMailHistoryService
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendMailHistoryService extends BaseService implements Serializable {

    private SendMailHistoryDAL sendMailHisDAL;
    Session session;

    public SendMailHistoryService(Session session) {
        this.session = session;
    }

    public void addNewHistory(SendMailHistoryBO sendMailHistoryBO) throws Exception {
        getSendMailHistoryDAL().addNewHistory(sendMailHistoryBO);
    }
    public void updateHistory(SendMailHistoryBO sendMailHistoryBO) throws Exception {
        getSendMailHistoryDAL().updateHistory(sendMailHistoryBO);
    }

    public List<SendMailHistoryBO> getSendMailHistoryBO(HashMap hashMap) throws Exception {
        return getSendMailHistoryDAL().getSendMailHistoryBO(hashMap);
    }

    public SendMailHistoryDAL getSendMailHistoryDAL() {
        if (sendMailHisDAL == null) {
            sendMailHisDAL = new SendMailHistoryDAL(session);
        }
        return sendMailHisDAL;
    }

}
