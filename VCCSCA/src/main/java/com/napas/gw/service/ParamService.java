/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.ParamDAL;
import com.napas.vccsca.BO.ParamBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * ParamService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ParamService extends BaseService implements Serializable {

    private static ParamDAL paramDAL;
    Session session;

    public ParamService(Session session) {
        this.session = session;
        paramDAL = new ParamDAL(session);
    }

    public void addParam(ParamBO paramBO) throws Exception {
        getParamDAL().addParam(paramBO);
    }

    public void updateParam(ParamBO paramBO) throws Exception {
        getParamDAL().updateParam(paramBO);
    }

    public void deleteParam(ParamBO paramBO) throws Exception {
        getParamDAL().deleteParam(paramBO);
    }

    public ParamBO getParamById(HashMap hashMap) {
        return getParamDAL().getParamById(hashMap);
    }

    public List<ParamBO> findParam() {
        return getParamDAL().findParam();
    }

    public ParamDAL getParamDAL() {
        if (paramDAL == null) {
            paramDAL = new ParamDAL(session);
        }
        return paramDAL;
    }
}
