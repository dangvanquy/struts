/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.ConfigWarningSystemDAL;
import com.napas.vccsca.BO.ConfigWarningSystemBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * ConfigWarningSystemService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class ConfigWarningSystemService extends BaseService implements Serializable {

    private ConfigWarningSystemDAL configWarningSystemDAL;
    Session session;

    public ConfigWarningSystemService(Session session) {
        this.session = session;
        configWarningSystemDAL = new ConfigWarningSystemDAL(session);
    }

    public void addConfigWarningSystem(ConfigWarningSystemBO configWarningSystemBO) throws Exception {
        getConfigWarningSystemDAL().addConfigWarningSystem(configWarningSystemBO);
    }

    public void updateConfigWarningSystem(ConfigWarningSystemBO configWarningSystemBO) throws Exception {
        getConfigWarningSystemDAL().updateConfigWarningSystem(configWarningSystemBO);
    }

    public void deleteConfigWarningSystem(ConfigWarningSystemBO configWarningSystemBO) throws Exception {
        getConfigWarningSystemDAL().deleteConfigWarningSystem(configWarningSystemBO);
    }

    public ConfigWarningSystemBO getConfigWarningSystemById(HashMap hashMap) {
        return getConfigWarningSystemDAL().getConfigWarningSystemById(hashMap);
    }
    public ConfigWarningSystemBO getConfigWarningSystemById(Integer warningId) {
        return getConfigWarningSystemDAL().getConfigWarningSystemById(warningId);
    }

    public List getAllCreateUserConfigWarningSystem() {
        return getConfigWarningSystemDAL().getAllCreateUserConfigWarningSystem();
    }

    public List<ConfigWarningSystemBO> findConfigWarningSystem(HashMap searchMap) {
        return getConfigWarningSystemDAL().findConfigWarningSystem(searchMap);
    }

    public ConfigWarningSystemDAL getConfigWarningSystemDAL() {
        if (configWarningSystemDAL == null) {
            configWarningSystemDAL = new ConfigWarningSystemDAL(session);
        }
        return configWarningSystemDAL;
    }
}
