/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.CertificateDAL;
import com.napas.vccsca.BO.DigitalCertificateBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * CertificateService
 *
 * @author CuongTV
 * @since Aug 31, 2018
 * @version 1.0-SNAPSHOT
 */
public class CertificateService extends BaseService implements Serializable {

    private CertificateDAL certificateDAL;
    Session session;

    public CertificateService(Session session) {
        this.session = session;
        certificateDAL = new CertificateDAL(session);
    }

    public void addCertificate(DigitalCertificateBO certificateBO) throws Exception {
        getCertificateDAL().addCertificate(certificateBO);
    }

    public void updateCertificate(DigitalCertificateBO certificateBO) throws Exception {
        getCertificateDAL().updateCertificate(certificateBO);
    }

    public List<DigitalCertificateBO> findAll() throws Exception {
        return getCertificateDAL().getAllCertificates();
    }

    public List<DigitalCertificateBO> getCertificates(HashMap hashMap) throws Exception {
        return getCertificateDAL().getCertificates(hashMap);
    }

    public DigitalCertificateBO getCertificateById(Integer id) throws Exception {
        return getCertificateDAL().getCertificateById(id);
    }

    public CertificateDAL getCertificateDAL() {
        if (certificateDAL == null) {
            certificateDAL = new CertificateDAL(session);
        }
        return certificateDAL;
    }

    public void setCertificateDAL(CertificateDAL certificateDAL) {
        this.certificateDAL = certificateDAL;
    }

    public String getDataByParam(String key, String property) throws Exception {
        return getCertificateDAL().getDataByParam(key, property);
    }
    public String getDataByParam(int key, String property) throws Exception {
        return getCertificateDAL().getDataByParam(key, property);
    }

}
