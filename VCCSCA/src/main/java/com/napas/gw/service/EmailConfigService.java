/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.EmailConfigDAL;
import com.napas.vccsca.BO.ConfigEmailServerBO;
import java.io.Serializable;
import org.hibernate.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * EmailConfigService
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class EmailConfigService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(EmailConfigService.class);
    private static EmailConfigDAL emailConfigDAL;
    Session session;

    public EmailConfigService(Session session) {
        this.session = session;
        emailConfigDAL = new EmailConfigDAL(session);
    }

    public void updateEmailConfig(ConfigEmailServerBO emailConfigBO) throws Exception {
        getEmailConfigDAL().updateEmailConfig(emailConfigBO);
    }

    public ConfigEmailServerBO getEmailConfig() throws Exception {
        return getEmailConfigDAL().getEmailConfig();
    }

    public EmailConfigDAL getEmailConfigDAL() {
        if (emailConfigDAL == null) {
            emailConfigDAL = new EmailConfigDAL(session);
        }
        return emailConfigDAL;
    }

    public void setEmailConfigDAL(EmailConfigDAL emailConfigDAL) {
        this.emailConfigDAL = emailConfigDAL;
    }

}
