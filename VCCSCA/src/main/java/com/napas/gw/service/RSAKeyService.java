/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.RSAKeyDAL;
import com.napas.vccsca.BO.RSAKeysBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * RSAKeyService
 *
 * @author CuongTV
 * @since Aug 29, 2018
 * @version 1.0-SNAPSHOT
 */
public class RSAKeyService extends BaseService implements Serializable {

    private RSAKeyDAL keyDAL;
    Session session;

    public RSAKeyService(Session session) {
        this.session = session;
        keyDAL = new RSAKeyDAL(session);
    }

    public void addKeys(RSAKeysBO keys) throws Exception {
        getRSAKeyDAL().addKeys(keys);
    }

    public void updateKeys(RSAKeysBO keys) throws Exception {
        getRSAKeyDAL().updateKeys(keys);
    }

    public RSAKeysBO getKeyById(Integer keyId) throws Exception {
        return getRSAKeyDAL().getKeyById(keyId);
    }

    public RSAKeysBO getKeyByIndex(Integer index) throws Exception {
        return getRSAKeyDAL().getKeyByIndex(index);
    }

    public List<RSAKeysBO> getKeys(HashMap hashMap) throws Exception {
        return getRSAKeyDAL().getKeys(hashMap);
    }

    public List<RSAKeysBO> getAllKeys() throws Exception {
        return getRSAKeyDAL().getAllKeys();
    }

    public List<RSAKeysBO> getKeysByRsaStatus() throws Exception {
        return getRSAKeyDAL().getKeysByRsaStatus();
    }

    public RSAKeyDAL getRSAKeyDAL() {
        if (keyDAL == null) {
            keyDAL = new RSAKeyDAL(session);
        }
        return keyDAL;
    }

    public void setRSAKeyDAL(RSAKeyDAL keyDAL) {
        this.keyDAL = keyDAL;
    }

    public String getDataByParam(int key, String property) throws Exception {
        return getRSAKeyDAL().getDataByParam(key, property);
    }
}
