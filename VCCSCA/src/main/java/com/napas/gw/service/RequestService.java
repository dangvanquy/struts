/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.RequestDAL;
import com.napas.vccsca.BO.RequestBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * RequestService
 *
 * @author Tuanna
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class RequestService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(RequestService.class);
    private RequestDAL requestDAL;
    Session session;

    public RequestService(Session session) {
        this.session = session;
        requestDAL = new RequestDAL(session);
    }

    public void createRequest(RequestBO requestBO) throws Exception {
        getRequestDAL().createRequest(requestBO);
    }

    public List<RequestBO> getRequestList() throws Exception {
        return getRequestDAL().getRequestList();
    }

    public List<RequestBO> findRequest(HashMap hashMap) throws Exception {
        return getRequestDAL().findRequest(hashMap);
    }

    public List<RequestBO> getRequestFileList() throws Exception {
        return getRequestDAL().getRequestFileList();
    }

    public List<RequestBO> findRequestFile(HashMap hashMap) {
        return getRequestDAL().findRequestFile(hashMap);
    }

    public List<RequestBO> findAllRequestRemove() throws Exception {
        return getRequestDAL().findAllRequestRemove();
    }

    public List<RequestBO> findRequestRemove(HashMap hashMap) {
        return getRequestDAL().findRequestRemove(hashMap);
    }

    public RequestBO getRequestById(Integer requestId) throws Exception {
        return getRequestDAL().getRequestById(requestId);
    }

    public RequestBO getRequestByRegId(Integer registerId) throws Exception {
        return getRequestDAL().getRequestByRegId(registerId);
    }

    public List<RequestBO> getRequests(HashMap hashMap) throws Exception {
        return getRequestDAL().getRequests(hashMap);
    }

    public void updateRequest(RequestBO requestBO) throws Exception {
        getRequestDAL().updateRequest(requestBO);
    }

    public void deleteRequest(RequestBO requestBO) throws Exception {
        getRequestDAL().deleteRequest(requestBO);
    }

    public RequestDAL getRequestDAL() {
        if (requestDAL == null) {
            requestDAL = new RequestDAL(session);
        }
        return requestDAL;
    }

    public void setRequestDAL(RequestDAL requestDAL) {
        this.requestDAL = requestDAL;
    }

    public String getDataByParam(int key, String property) throws Exception {
        return getRequestDAL().getDataByParam(key, property);
    }

}
