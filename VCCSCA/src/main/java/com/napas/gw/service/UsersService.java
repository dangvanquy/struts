/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.service;

import com.napas.gw.DAL.UsersDAL;
//import com.napas.gw.common.OutputCommon;
import com.napas.vccsca.BO.UsersBO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * UsersService
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class UsersService extends BaseService implements Serializable {

    private static final Logger logger = LogManager.getLogger(UsersService.class);
    private Session session;
    private UsersDAL usersDAL;

    public UsersService(Session session) {
        this.session = session;
        usersDAL = new UsersDAL(session);
    }

    public void addUsers(UsersBO users) throws Exception {
        getUsersDAL().addUsers(users);
    }

    public void deleteUsers(UsersBO users) throws Exception {
        getUsersDAL().deleteUsers(users);
    }

    public void updateUsers(UsersBO users) throws Exception {
        logger.info("update user-id :" + users.getUserId());
        getUsersDAL().updateUsers(users);
    }

    public UsersBO getUsersById(Object id) throws Exception {
        return getUsersDAL().getUsersById(id);
    }

    public UsersBO getUserById(Integer userId) throws Exception {
        return getUsersDAL().getUserById(userId);
    }

    public UsersBO getUserByEmail(String email) throws Exception {
        return getUsersDAL().getUserByEmail(email);
    }

    public List<UsersBO> getUser(HashMap hashMap) throws Exception {
        return getUsersDAL().getUser(hashMap);
    }

    public boolean checkEmailExist(String email, Long userID) throws Exception {
        return getUsersDAL().checkEmailExist(email, userID);
    }

//    public OutputCommon getUser(String userName, String password) throws Exception {
//        return getUsersDAL().getUser(userName, password);
//    }

    public String getDataByParam(String key, String property) throws Exception {
        return getUsersDAL().getDataByParam(key, property);
    }

    public List<UsersBO> getAllUsers() throws Exception {
        return getUsersDAL().getAllUsers();
    }

    public UsersDAL getUsersDAL() {
        if (usersDAL == null) {
            usersDAL = new UsersDAL(session);
        }
        return usersDAL;
    }

    public void setUsersDAL(UsersDAL usersDAL) {
        this.usersDAL = usersDAL;
    }

}
