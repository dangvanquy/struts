/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.thread;

/**
 *
 * NPThread
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class NPThread extends Thread {

//    public static final int TIME_LOOP = 1000;
    public static final int TIME_LOOP = 1000 * 60 * 60;//1h
//    public static final int TIME_LOOP = 1000 * 60 * 5;//5p
}
