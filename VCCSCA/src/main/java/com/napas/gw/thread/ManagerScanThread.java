/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.thread;

import com.napas.gw.controller.BaseController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * ManagerScanThread
 * @author trungLuong
 */
public class ManagerScanThread extends NPThread {

    private static final Logger logger = LogManager.getLogger(ManagerScanThread.class);

    Class clazz;

    public ManagerScanThread(Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public synchronized void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() {
        logger.info(clazz.getName() + "====>> Start Run");
        try {
            BaseController certController = (BaseController) clazz.newInstance();
            while (true) {
                logger.info(clazz.getName() + "====>> Start process");
                certController.process();
                logger.info(clazz.getName() + "====>> End process");

                logger.info(clazz.getName() + "====>> Thread Sleep : " + (TIME_LOOP / 1000 / 60) + " min");
                Thread.sleep(TIME_LOOP);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info(clazz.getName() + "====>> End Run");
    }

}
