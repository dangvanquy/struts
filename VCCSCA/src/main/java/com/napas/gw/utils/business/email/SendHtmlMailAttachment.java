/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.utils.business.email;

import com.sun.mail.util.MailSSLSocketFactory;
import com.napas.vccsca.BO.ConfigWarningSystemBO;
import com.napas.vccsca.BO.SendMailHistoryBO;
import com.napas.vccsca.utils.StringUtils;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;

/**
 *
 * SendHtmlMailAttachment
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendHtmlMailAttachment {

    private static final Logger logger = LogManager.getLogger(SendHtmlMailAttachment.class);
    private static final String logo1 = "<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMUAAABCCAYAAADnjRKQAAARqUlEQVR4AezTBw2AUBQAsecEpIAwrDM08Ndqk5NwAb3Zzuv4ehLaA0xhCqZkCjAFmAJMAaYAU7zM2HOU614UxfGfbVt4tm3btm3btm3btm3btn3e949hVpPcmyadN2t9xunuuc1uQOAXSIASaIcxWIw12IU1WIxp6IjSiOxH3juIgoJoipGYizXYiTVYggnogJKI5NKsXyMNqqAfpmIxNgVZhSnojGL43aXct/E/8qOJYeYdWIOlmICOKIXILuR+ihxojolYik1YjwWYiE4ogl9VHnPCzqTRkEZH9b6F6qSpUENSlq315pWCB38XCVADk3AK4tBR1MaHCiVIgZZYibsQB86hG/7UmPd9ZEYP7MEriKZdqI6PNYufDC2wws+Ze+AvB+/MM/EYomE7quBji1JEwXWIEyXblYz4UvCAH6AUJuMmxGUH8a9FfnKIi56hLd61yPwfg3AN4pJLyKO45gkhLnqODnjXJvdHzIP46TzyWhQjER5AnMjXqHyElyINxGMX8INJfmWIByZYzNwd4pFueNtmzctBPDAFb5tk/ofzEJdstjmNyohnEF3jdySTzNWrKj+Xb1stq/T5sEN5NP1s9QLVgQTAGJP8fhCPlDLJXA7xUGebUvSCeKScyXXhSYiLBipcXxTCS4iu0VuSS6qyNZWey5e9tws7uY4+eMfqBRoDCYDn+M5H/mqIR/aazHwd4rGsFmu+FOKRQz7yentWPvtiVIU4MWJDSklUtK6bpXiFOiqnT7shAZIvAnbQHw15v0IC4BjeM1nzixAP/Rom6zM8hCi4gp3YjXM2Nx5iBUWoFKMVxIlBq9JI7LwN3SjFYxQIWpN/UAlF8L6vuy/PIAoeYRfmYCwmYovmHZumhvzvIUHuYz0GoC5yIjniBImPTKiAaXgBUZDUkJld8+7OYozHDOzFKz/fBL4yzLwBg1AXuUxmLo+pGjOnCJOXU/HCOYXJaVdOjMNTSJDHxsIrFKMfxIk+S9JLtJyN/SnFTaQIU4huKIGC6GAcOjbExm4kxrsm73p/Yy1EQSfDtn+iMqLhbc0bBCkVC53GsF1ziI0ZiGSS+wdGQxRM9bH976iCGHhHc+bkujPzfTOIjUoK2T9jOARbgn6tU4q3MRniRJfZmeW/TM2clOI0IoeZowKSoR7qo7Zx0NIQG90VFuxL3ILYaOP8PpnjC+Ykhm2mQ2xkM2Y5vFi+9pbLH4q3VFNoXk/U1MjPgNZBP+oW4wMsgTjRelIO+SdDc51S7ITx9DkxquM/FEBHJxdgJezHVd7Zaru8gyyG2PjFsM3x17ScUxTtOhCG17Ft27Zt27Zt27Zt27Zt27aQOw+zzurNRv6k7cP3tJNB2+lOZyYB5sQAdIcEi25RHfZ5E6AznmX8EGD8B6Km7pLIMDBCEsdMA6PLrAoifkEoKLYSoXxcw3JES16yxpZ/3AtcsNTgzVqgs8YGZUblCntBorwMkHf/IMkLA3wTvNCwbx3gc0ZNn6MofL6n0PfFuhTlmy80PrTnsZ6QLgZGROKyaWC0HF9NFRQziUBGxgFLnu/o24NbNISC9IoWk8LEcOIQkDFBOGRQPd8BuKtTY8mv8Lkg91IdJD474PMJSUcaQznfuNUnv0uBEZt4YBoYdQfV8RUUve38DccFLswZDXkvgTpFUB/LkB7cJiEcZrKkqxUwZwTgLlzj8ZHVCUF0JR674PMML/r225R5kMjiQmAktdMnVbFbI2tQ/CTqmVuDp+rmgLJiArIuepmXk3hACJdoLOmbCcypAbhstPzkOTnMGy0hWnqxM6lJ06HEb6KTC4GReeGpnF9M20GKt24uwo09+YkCoogTH2x9kKwEKKsEIGuhPEfKfbtBFknnSWBOCo1r+AItHvL4IlzvES6S008T4n0H5Hd3OjBKDOy6vta4Fn9MqDah3a9ai3s4k8Dh1mGhIC8oqwcgq5OUq/9ICIDbXDwbSfS38AJ4s4WQ1u/fgAJlINDnpIDtn6U8/zvQ5zvEYtlnoBL+lwit2Bszivhssws5oYP7SPqb2hK7/GiRY+58Qf8a74nU9g3CGsTCgbJWALIKWcbPBcbfIgr42Y/wWacHiAuEQgFclAIbKY9Zxk8Hxt8jCvl5gD6orhloe0SiBXFArpKDjHHg+QsiF0F1iFl9sgg75YLosbep4OXUYyKuHYPCIDdIQ94NQF5kHhscWEK8J+L40Zcc0LdUmlMTmDNd4w13DXl4eHxg4pPqX4WI60dnQkDfKsOdlRWJ2cR7dGOV/d2c5k2RMerPFmGmXxYJFhwUi87ktH5nXCeimBqVG1C+TmNL4x/VfgrL+KwmGRSDB7yrNGckMKc55DOgnynE4zPaTWpwn45Q0NtLn1cAnZcl+PZ+YSMgYtppQo3Watm/9GvdLQO9fYCfIEKaGNYaackAZWUHZG22jK8AjOc2AlubhIpJc3bqtIQo/qWQlpYnRGCeUxIYP0Chdxggo7SXJeYuIpHDq4j3hgGRkpsshTYF+4rIXTb8C4jwsy6JaSeK+cpMbSeC6ho3GzAE3VrZHJA1RCqxCwUr8VQo3DL+EvlIBdLYrwkB0EczO7dOoXs7ICOOZXwyqRA3kogC3M94SFu8QUDkRZdnMnEL9xeR+u36X9W64Op5qpTtYiKAjoGnkH2voKxpgKyqlvEZwZx4Nl86gSzOM7COIvOA27gbEcV4q25Z7qw9QwiQl0QYqaqM+JyLp5hsjHojja/uo0NhMfsUzsfe9cNOf7uwLT+NAqL4YBFh2CGP3qZ+B+ohtYxxqIGBgdTkBw2HjwHOJZX0I8uPz0Qn6e0XBlyKbJdsBOY4SnUv2bKXoM/diHiSz4WBuXskncMV4/9wtusoo1NQrKvxfHQ1TrmWHSmoMOcREEkW7tEp8nVDjEwFGHRA46iWL0CDWkBp3ghCoMBFPqlVA9pT4DxTfVyrQQb1AKHBWGi5ZZ/HRAjwuKTJpnpiVZkgwk4657UlvNG23rrV7wYqY2sBRk10sIB1zEdKDmnhNqUGVEdxnkVEID9ZuuvuH9KAV9sNKQe21a8zTrnWmSHCTrvkNSAizT4vZp0sqBsUv4lS/gweiW5KB5yvCsia5ufIFYO2A4gUBnUUu31BfYCjbRJxhV64QDqp4dCNZsMOYOv7MeOUa9OFIsyMKz530hVdM/OvYQPhV0JugdFKTWYCg2Koae6f50fi9mThIN+IQNIb+q+LAbGHyKh5NOcCFw6AC+Klaj0VrVgDiYOKgG+J7awAonRYozyAoMWObgNNgoJ5S6QyafH+TQQDb/BW5OAAQE56Yo5pyo75xKna7nJXKjD3teYJG/eJKUQmG0WstMQs4p1Nnw8Svf3oiU0MMTz36QHRmwgP1qvMTmYp1E9E6rUNOZHjFHfW9iaEIY+JON4eQH+k0rixyQB5QQ3Osm3Pb7ntxHHiPHOUi1FLiTFEO64BJCQC+pDZAvmG4rHxOV3ZgYtl45lhRFv+La4b5/eyL1MAn9tzNi2RwcEHCYnaXPxcyelX1vPvUOXZbEsmIoCTz0LsimNzRRhx5Fv4EUeElXDjT6PnNv3Hzn1iKwxFYRQ+r3dNUUh6kXQkU3iWoihzwdEd88CimQASicKGK3GcVHP3t9Y/A3ZCEkj/7ifnBbOyxyVtf837IqhrKPhjPtRjM8fjLmbfgkCiOCiiqEgUiOLoI4qZILCvJlfF32TfJGwE0TRzfCwrgSCKjOZlbxIFotj5CGIvgSGKf0UUW4kCUdTMOh6XEgRA/3OSqQC2UP67qymwCFGcFVH8CSxBEHHNyxEEFiGKrv7PMrADUQzNTg82FZ8AAAAAAFyA3to7B2D9fSCK/m3btm3btm3btm3btm3btm125mTmzE5fvuInfTOZ16ZNNtnuTTbty93xlGLoq2HD9aEi/yf5YzeUPViquwopF/eOzG613/nuMGp30MnwyBoeeX+ynXMU6WKIinWNpTLjxL3LQb/D1aKa5JnVZeSj3DDd0awsZ7gebM+jInfYNpVEztaBMkFI1iuJ7/C39yA4wB7hhC/OyJ4v1d2hjUMRnvcvdpftROCRebl+HWmqbvQwPylkAYQNkUWI2drkcO/FmHuZyKz7NSB0fi7H/o5eNlTec5Tbrjsb53MN+rQa7T2oocyBGCi/YQBqDwrSvCIN/iSCIhBlHQdL3g4QIJjqfrkEtG4AinmhaJ8JQoB1y/rQOvQU7UbW9PxdOlw/jjR1Q1BspGsPtADFvLRj58w911HvcSXGum8vCIr9KHNfQ5njopMFyGoNig/EkcQ2VThQy0GxIFsin2Eb48mJ5AyDOlvEAdsVeTlQcD4IWyPP4+8g4pu9HkXfX3xPwHUbxhT48Aqt1EU/j6LeHZjhHiUg/mRmvWAWuhtZN7MVdVDPnqQZ6oBCbBrXKKj8X7hmH9qAxJx4NuyNj3M8k64vQjsOyFAXvUi996OnQWWsJxf6ov5Li0HOsTMwrseK68QRX8aE0kk2hnwP/1e2QgQFcQCPpZ4rIrOK+yNuqrdo71iKcHUBrIyPsXty6lB+xSJSLHIfgPxtFLmp55G2oL+PoYORcqA4x2G3FHjlzAiKwlUK4WV/1/F0XP9CnKzvFXk5UACIGxSVc2qTP+OmPZLYBDHosTwSU+6yCiP2b94/oanXFJHedH8X+58bu08KJfYDgF4pjYq6Zz8NOH+IPO4XsXQsnXWfTCJBOWS+xyDyXG4fCWVf0kB5ne6ZX7NlGUnb34nNMCPntcyg9Z2ez3uQHyxPvakfvymgzjwlJN23iJThIQ1AboOf/3k5UGyamCQwtvsTkZhBweI43XcKu9mGVaisO5u4TwAijagTCeE/mtyLqEcJhNNUdZ9keFfQ5oOTcrm+jnbezaL2/S3+2zagOFaU8/OJ1Hong8JGyUw1GCP8NY4TYlA0dJ8e5v/BbOCDIGs70gQA6S3HPVeZz6Elmt3PwHIY3ccIzPDDdHafYJxkFsUrGRibuJO8p7hvHtq7FmU2le1FUOzDPTdy/kiXoKDSyzjeEeMoEDVmAMXMOp+shG3854agMI3KUJo2/9Uo8R3pH8XgqwuKXSJxWwgKeX8o95RoKNuA4jhRZR4pd2ryggw5gaLou2QsqXoWUP5o3QAUB5TUO5zCC5xVQtS2XQDFw8ntzIDiCM5ntpyKoBhLZebUfSspf1CMfHPcOAbRLkGxWOAoe64TKNbmOM0EtzESGBRThQ3+Mfzutw1B8aqm+xNKOJr21Qi2G39nawCK7dQ+g+JEjz4q97zWWm1AcTrugPX7Nvd8J1AM1gU7+8KOAtUNQLFdfAYYzzgKG3YvrvCHERRxQZwBxX5eo9YExegqs5DuW1Wu+cC4XmngPIL1RARFbN9+VUCxD8r+29FQeedrUAwsf/vK4vsBinyZvOuC0f2BMQybAwXfBHZKnQUQw8pV2lUdfBBffKLQhz2KtjQExSqSvQIu1qqqe4kICtyC50gzdZB7HiOa1yrHc89vwYCelIszKs/lLvJequs+4YZNXREUa3H8DTqYFZvokaB4jxcNg4hh8HZYDkcXI8uDlEv37Mza8Mo6oOD8ANp8ljuzXwmV/vgRFHqfbPpFL04nsRJsRDlQaLH7DHkvMzXu4TBSChtwT3orFIiKz2sCCgz2VvJiugoisAiK8YKyc3Iv5NwUmYuQF5/BnDGwpRaX89cAxbF2JSqCYqr0bO2qknbunqCIMTTQ76JeGIcXPDNR7iINaA6O+XsNUJyX+uT37snXWpbz/f16jjSLOjAtLsfdoPggfZhypJ9TSJOXco1St+tNeer08oQ0fg6XbpMCQOGbyeH4wat2ofB9qHNeBU6JsgfAhbyCmehyzgco+07ByJXOJ+ogd8O0TuL82ATq+AzIGw8mwjtYWB7DGq7ydwp4c3dDLweSt3PQw0SSPzh5C6KDm1lfrsH1ldPz4HyrEr1MJDm2qzEtJxNQc2Mix54IeBKb/VHo4Q6e9Xjh4+7B2MY5xcAhWcMU8krat5j1Rx/pU8sfr0aPbV1Rv19utL+4N2pyvx9T7HP9NNFdvwx/1+Mk9vv9BwKJuJP451h+AAAAAElFTkSuQmCC\">";
    private static final String logo2 = "<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAA8CAYAAADPLpCHAAAPgklEQVR4AezcBWzjyP8F8BRumY57TMvMzMzMzKzfVcvMdNxlZmZmZtYyMxPdbTnb5v2fpEiyrGQ8kzj6H8yTPsLU43G+dT3gOnR0AhUAEfAtwxw6OrqgdXR0Qevo6ILW0QWtC1pHF7SOji5oxZNMQbmoAXWnYTSORtEg6k7V6Qc/2/mAClMz6kHD6Rd3e72pORWlNDb16xMqTW2pp7u9n93tdaMa9JWN1/ErKkPtqA+NprHu9v5HDSk3JfGznRDKSY2pN42kMdSXOlNZSuuQyKLTRdJQOlnRzudT4xMikeh69/coaACpqBwNoI10H2q5R+OtihtAEOWmcFpJdyGfRDpDgyhMoW/fU1daRc8hn9v0M/2g0FYSKk3DaBu9hHxiaRe1p+QKbRajufQKcrlIQylMUNBdCKq2X+2Id4mx/z8FDSA99aN99A72JJ56CNrsCHsSR4MoxEs771FPOgn/k0CTKbXENV0Me/KEalu0FUbr4HtiaQwl9VLUgwmq9tzsIXmntr+gVyFw6ealzQmwN3O8tFMA9ucKfWlxTc/C3rTx0k4Gegh78oXgTh1BUHXkzggALhJn94OodalnXG6tIJfo4t9E4BJLYR7a3AP7U9dDOx0QmNyg971cz1Bywt7EmQsOQEobv7unFs/SQbSAoOrUg/GwypjTL8AilbWaUngr5tQIfHp5aPcF7M8uD+1MRuCy2Ms1zYLAZIipnWGwL5slBoihtJGg6tLThXYVdASFiO7OxRH4rPIwu+Ap0XSLzrqdp8fkglxiPPTvMAKb3B7abADPeUXXDP27rPiLvdXQRjA9hjhRtID6UQ8aRxvpNZkzQnLWIwUdIKi6+XKjPwWdSOHuvhehztSV8pkvfjeIE02baCDVplKUl6pTBMXAOodNbZale7SEelBZ+sxiOq85PYV10pq+9EiJWZk/qAkVd2tK8yUfG6Z7ON8hdIGmUUcqSOkE/fuWBkm0d8bwMxklBuUZBQPlmnSA4E5Nh2RYnGnpHEHF4tPF8ODPA74UdAzVM8yO9aGx1IP6mjs3E+L8YjH4yUXxEOes+RnTx8FrZVgn1DRgEsVFqQXtZZe4Cz7yNA/sY//Gyt4YAFSEONck22xIr8zP5xJF/SndJKhYeqY0nkWeUSnol1TMND7pRqOoL8yTDhLTWc0kLspyiLPPptmYbyFOjMWffnNuSLRZA9YJs6l/LRUeOarAOrUl2/3c4UNYoN/TI4KK5WfL403MDZmCvkUZPZxvPfqRwqmmeTQeC3Gy2bDcuUTxiw2mdPQ5fWtQT/EvwWiIs0LyXF5BnMKK/UtC6ehrU/9+hjgRhmPkhVyOUk9iYdgfFmgOekNQsfp8dUTGPRIV9DH6WPWOkE1iyi1U4jgLIc4owc8mpYo0gnbSbT+mvBaajr0F4gyQvE7HIU5pwc+mo4Y0ng4JBrkyaW/6pfgLarlKg+lzm4u6CMUQVKy7WB+x7157Kui1lMKXP3HNIM5JyeOchTiNPfzMx/QrvYZ96Wlq4zHEqSrZv3MQp4KX5+9lFA/7kt/UxhT4FidNoo9tLOrK5CSo2Hy5JZwJkcaCnkAhvj6z/QJxZkkc4z2J5fLMHp5t/4T9KWeeGhSHMysSkTjXwqZHlFGUCHuTQMlM5/U+3YDveURFbSzqxotOF3WpFvXO690w9vRjsJB7+DsI2QlxukscI7fEtF+I4fPtELh8qDAL8EzyGn0J63xn+Px8BCYXvZzfF7QfvieKstg0qH1/770Drw/c2+pSsfvOFtfvJ6ZdZ3Gr35kVV+uKSxyjNcQ5bvhsVnJKLPEupe5Um0q7tYE4D0zn1QfibJO8Ri0gjpNCHIzEOYJ5TpOpNVU39G88xFlkMXBtQRfgW7bYMQPlTHTdgGJexyWg2sbrmHWiLFjQMynI1xP4AtZJbcMMx3SFweNl+s5LO40hzkbT55dAnHGS12ktxDlhKKo7EGclpfLSzjSI01vyfPPSKDqlMPh00Sd+FHM+egrF3H3rRN7lN1FlzTTjI8hIX0+iGsS5KXmcfRCni+ELfwtx8vnxvD/K9PkrsgNVQZsZJZ6Ff5V89HpOyf2YSalg3Euu8AJDF7oD61T2o46ioZjTz2Pw/cLr4HMzxh1sZH6u/tGXExkIcVbZNGAqIvks+tKinV0Qp77pDRurQswk8dbHXlinqPvzTSHOaoc5ausBHxs+P8gtVOFNoCiVBTTJ43byZfC7+e5bfDLnKljMyLl4k7fBYhPVk1kJcQbZsHLnolSSy9BxolePJKb30hs+W0hioBps8crZcljngoORfH7eI2gvp8ryOoANBOY8lZf8vm9BnOoKtRNEY0g5My+9RrqZ7vlm6r6jp7eCdlIllYK+4W8H3YM2Ua6b7hJW6Sx4dUqUSAoy3TlEiaUJVN197HQURnmpHz2AXOorXIsEyu6lf61UtnZ6OL8j1NK8AUpxZimzwirnYlLO4OPP3IVMFDbnJOaeLCma0ouhQjInlVpiwPClxHGGqSwtA7gmMWPQh9KZ7gZWX8ghUztTEfjsMP0ShUlc0ydUj5KY5vHnQJwxhs9/ZPGu5VlaTrNoOV2Gde5KFnM62gfFxCe60HrXQ/PSNuqsj5CZp35BmSVephTmlWQH16ksLQPoCbkk0mO6T3GwziRTO0cR2DzwtCgDYL3sd0wP6DElwDoNDW1UgP3pIfFdf0OXoZg3nJartOGux510fxypJbv48oDMN1ilPdC7JQv6rsrSMoAkdAr2p4NpMBcd4GLOIng0egP7k9HQRm/Ym1OURGIq8AkUcz/Sifwrbnks5oLLVqm+IHCJPnSYI7kH+jepPz/W+cLLHo7TsDcFDMfPjMBll9XGHveA9BnsSxQFmxay3sGeXKQwi/5UoSgo5uyLWKRfdN3rpv1eu7spL5HTUUrhYFT3QLeQKOjSEOeFxQ67YfTWpj0Oyc0LMDbnPDVUmP8No4WUAP9zxMPxc9BGcvlxzaZQKot+tKdEKGb7/Uh86p6W8+Lh3JOlwgWFK7KVkphP9CidFcgs8aU1Fh9DamNTSmpHmxQ2K8XSOVpGvamk6ZjjIM5JiqATgjvPX3SARlEhP1bQPqeBdEhh190bOkYzqTNlsfh/KoMlj59IF2ksfSdx7g3orMiDKOeL8y9jYTTt4iv3tJzQMMP/+zjrg16Ov3vcK4nfUTlqTK3dGlFVKkCfU5DFcbYpvjn9EWWiLPQtpQ5Q/5JQVndfmhv6V48qUg76yI/jh7r7Uclw/GZUnQpSSoeNYVGmorcERQkkux9bR2JPQQ2Hjh0F3ZHgg9UORkf+2dUqXzp07CjoswQfSKxs6si+OPrCoWNHMRch+OA6BTmkowu6P8TZ4dCxq6DDfVDKIR8diQ1FPzl0dP7+kd4r0tiho/M3jnlO2yW/o0xH5+9d0EXkl5B1dP7+Bd1V/h9G6uj8/Qu6BIUL+PTOnI6Ojo6Ojo6Ojs7/tXcOsNY7QRT/27Zt27Zt27b52bZt27Zt2/bTJr8mJ5Pb9mvvkzbZ3De7727vTE+ns03nTF5pvDM9ggzng7LoGA/zKuQ3Oaps4bk+gISRI+IuoK1OwEv778jc0ZC3pMBXcaBJ0WlAnY7gYwaXZvjFrc+7vd+SkOkex13C+mUyoVLtTJ5X96Z0RDuXICAMUP/ESA5WCoXuMr7BfjeufZz+9MMMY1WFLALZAIuBsGQAft8zMY71vRwrOUBT425fxksGAPp+qo7ezAm9OkEK/4QkTthRAOwRygR/nOBCW5DkSXoIHW5Ch6tk7hHG7okMaDw8F8b2mID+h35ImP1yKaAbiEOIU7ngE8SkAD2Lz2sYn4Q8U5WB9qAsHnodOYG/kpB6ndQcXI2hjws6IbAodcNorzF/Ey/nr4LmtQ75h5cw7tom1k+Uq9iC9cpl9HFkjFcUIsWjKQ40Q3T4Wea/4PvvRAD0XD57Mv4w8jwLaIot9STZdAkZ4tcrgOhH+IGFVp21KiB3drwd7j1w7HSa0CMU51yugcjxD9G3AserQgbTYphTD1dAQ7Ewggz1ui4UTOidqZfI//4o5Jxt0HcFOZmP7kMjoaMt53s5LKoPMXeE2KQy2HO6fOYPaJJlAedpHu0YgFBA90Ve4Lg2pERDFQPodRj/hIBjXiHKl4Z340IytXfBzNlGCFQuFUBvYf2zAjLQV3u/l3YfxxiIPB8dNqiHixNyAKSFUC0cJmtUU0BTkH83Y06Xwfy9zelnL/gQQDfjTlNBk11l/dp850up8vWtkNM8ZwC7kQstBfldnSf/cKpm2IcAegLVGM4QGw/BgaXQHzbH6MKdE5tQsoMGkMcJG9dRicFFqICB3/NOqDDyvwNgvVy+E4RtMg0jHh0x5FjNZ0uZKyvkLc/QVzF2Y1jIYQD9PPI45LcAlMewdKyAzOPIOCImoAcIeJ9yhscmLxlAt0SuzBI6VsUCOmLI0dlwFfZFPg4PeCfMTBMlPFIwfWXoxf4w838bDpZ/wkIO5H+Re8v//Kev8QL6S7hAv1YdFdA424NFPsfPOHdxdac4QwiAZgigPbBO1TXEI1wWEdBahPJENRYXzQbTX4kI6HtsDCiZ4OMMI5PXzksC0I95YQef/bGHAnoU8luyxreMdUwS0BUNT/cA4cubK154rQ+g37GA9JmvEAHQKhfdhya2mo38mdS5nOMHaAmTNgQCGqCUMFRV+wo43sEw1kPfIB76qIiA/kyYjVowV0VPDmPPs+6+mQDoWyRkOc4QOu6UcEFPyDv04wIAPZ6N4DZlIXK3dR8PXc3J1mtHBPRJdlPoA+j+yP8pwU82AvovucD3tV47o5+MI3Vh5m0ujPQDtGW6JZn5AnR+RI3zKLckr9VVMkBRZhDyIjZfa5TMHE/vWiq3tiNDNoVXiId/irAmhXitnusouorbzZXK+OnGogCajPIRyAvQYW1QDK2ljwMAPQG5s7IcCbg2iH1SJIbuJ/HiZXsJ6M2yEb03AqD7SjiRlYAu5+nMhv5sr0oXDqyL2OAxuKv3gJnahmbshHBAi76ye7yDJxU9ke/ni12QH0E+ijh3AZ56NnwQBwn9Vnk81mjdqfvt4uU5c1MA9wRxbwqetLWpXVKEeedpTgl4ynGNnADV4RjGFgIkF1b95vOUQ3/vBYnKcDDXCPlF5NbIjyB32YcGCPvwpGYdwL7Zzz62QU0wAhvfIb/3C3PMClKFayg8H4MBnOpn7fNL8HzwUyAA3Jn9VzX5De1xHlvY5D9tKIjn4riaQM7jjnGePuXg3xWXpxh94zVY6v8hhils8Wz4ieuFlsgFTdz8PfFWKGwe61DmrVjY0gEiXVFLJpPSDQAAAABJRU5ErkJggg==\">";

    public static void main(String[] args) {
        try {
//            String host = "125.235.240.36";
//            String port = "465";
//            String mailFrom = "tuannh8@viettel.com.vn";
//            String password = "Anhchang178#";

            String host = "smtp.gmail.com";
            String port = "465";
            String mailFrom = "eprtest01@gmail.com";
            String password = "123qwe!@#";

            String mailTo = "trungluong.nk@gmail.com";
            String subject = "Send mail test";
            String htmlMessage = "test 888888888888888888888888";
            String filePath = "";

            ConfigWarningSystemBO configWarningSystemBO = new ConfigWarningSystemBO();
            SendMailHistoryBO mailHistoryBO = new SendMailHistoryBO();

            sendMail(host, port, mailFrom, password, mailTo, subject, htmlMessage, filePath);
            logger.info("Gui mail thanh cong");
        } catch (Exception ex) {
            ex.printStackTrace();
            java.util.logging.Logger.getLogger(SendHtmlMailAttachment.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int sendMessage(String host, String port, String account, String pass, String to, String subject, String content) {
        if ((to == null) || (to.trim().length() == 0)) {
            return -1;
        }

        if (content == null) {
            return -1;
        }
        logger.info("Send email [" + content + "] to email address [" + to + "]");
        try {
            Properties props = System.getProperties();
            props.put("mail.smtps.host", host);
            props.put("mail.smtps.port", port);
            props.put("mail.smtps.auth", "true");
            props.put("mail.transport.protocol", "smtps");
            props.put("mail.smtps.timeout", "90000");
            props.put("mail.smtps.connectiontimeout", "50000");

            MailSSLSocketFactory socketFactory = new MailSSLSocketFactory();
            socketFactory.setTrustAllHosts(true);
            props.put("mail.smtps.ssl.socketFactory", socketFactory);

            Session mailSession = Session.getDefaultInstance(props);

            Multipart multipart = new MimeMultipart();
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setContent(content, "text/html; charset=UTF-8");
            multipart.addBodyPart(messagePart);

            MimeMessage message = new MimeMessage(mailSession);

            message.setFrom(new InternetAddress(account, account, "UTF-8"));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject, "utf-8");
            message.setSentDate(new Date());
            message.setContent(multipart);

            Transport transport = mailSession.getTransport("smtps");

            int portDefault = 465;
            try {
                portDefault = Integer.parseInt(port);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                return -1;
            }
            transport.connect(host, portDefault, account, pass);
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.error("Error send email to [" + to + "]");
            return -1;
        }

        return 0;
    }

    /**
     * sendMail
     *
     * @param host
     * @param port
     * @param mailFrom
     * @param password
     * @param mailTo
     * @param subject
     * @param htmlMessage
     * @param pathFileAttach
     * @return
     * @throws Exception
     */
    public static boolean sendMail(String host, String port, String mailFrom, String password,
            String mailTo, String subject, String htmlMessage, String pathFileAttach) throws Exception {
        sendEmailHtmlContent(host, port, mailFrom, password, mailTo,
                subject, htmlMessage, pathFileAttach);
        return true;
    }

    /**
     * readFile
     *
     * @param path
     * @param encoding
     * @return
     * @throws IOException
     */
    private static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    /**
     * sendEmailHtmlContent
     *
     * @param host
     * @param port
     * @param userName
     * @param password
     * @param toAddress
     * @param subject
     * @param htmlMessage
     * @param pathFileAttach
     * @throws Exception
     * @throws AddressException
     * @throws MessagingException
     */
    public static void sendEmailHtmlContent(String host, String port,
            final String userName, final String password, String toAddress,
            String subject, String htmlMessage, String pathFileAttach) throws Exception, AddressException,
            MessagingException {

        //step1: sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.debug", "true");
        properties.put("mail.smtp.ssl.enable", true);
        //step2: get the session object
        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        });

        //step3: compose message
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(userName));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
        message.setSubject(subject);

        //step4: create MimeBodyPart object and set your message text
        BodyPart messageBodyPart1 = new MimeBodyPart();
        messageBodyPart1.setContent(htmlMessage, "text/html; charset=utf-8");

        //step5: create new MimeBodyPart object and set DataHandler object to this object
        MimeBodyPart messageBodyPart2 = new MimeBodyPart();
        if (!StringUtils.isNullOrEmpty(pathFileAttach)) {
            DataSource source = new FileDataSource(pathFileAttach);
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(pathFileAttach);
        }

        //step6: create Multipart object and add MimeBodyPart objects to this object
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart1);
        if (!StringUtils.isNullOrEmpty(pathFileAttach)) {
            multipart.addBodyPart(messageBodyPart2);
        }

        //step7: set the multiplart object to the message object
        message.setContent(multipart);

        //step8: send message
        Transport.send(message);
    }

}
