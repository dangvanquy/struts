/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.utils.business.email;


import com.napas.gw.bean.FileBean;
import com.napas.gw.constants.Constants;
import com.napas.vccsca.BO.ConfigEmailServerBO;
import com.napas.vccsca.BO.SendMailHistoryBO;
import com.napas.vccsca.utils.AESUtil;
import com.napas.vccsca.utils.StringUtils;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * SendEmail
 *
 * @author LuongNK
 * @since Sep 12, 2018
 * @version 1.0-SNAPSHOT
 */
public class SendEmail {

    protected Logger logger;
    protected String emailAddress;
    protected int port;//= 587;
    protected String server;
    protected boolean auth = true;
    protected String username;
    protected String password;
    protected String securityType;
    protected String smtp;

    protected List<FileBean> attachments;
    protected SendMailHistoryBO sendEmailBO;
//    protected List<String> ccList;
//    protected List<String> bccList;

    public SendEmail getInstance(ConfigEmailServerBO configEmailServerBO) {
        SendEmail sendEmail;
        if ("SSL/TLS".equalsIgnoreCase(configEmailServerBO.getSecurityType())) {
            sendEmail = SendEmailFactory.getInstance(Constants.SEND_EMAL_CLASS.SEND_MAIL_SSLTLS, logger);
        } else if ("SSL".equalsIgnoreCase(configEmailServerBO.getSecurityType())) {
            sendEmail = SendEmailFactory.getInstance(Constants.SEND_EMAL_CLASS.SEND_MAIL_SSL, logger);
        } else if ("TLS".equalsIgnoreCase(configEmailServerBO.getSecurityType())) {
            sendEmail = SendEmailFactory.getInstance(Constants.SEND_EMAL_CLASS.SEND_MAIL_TLS, logger);
        } else {
            sendEmail = SendEmailFactory.getInstance("", logger);
        }
        return sendEmail;
    }

    public SendMailHistoryBO getSendMailHistoryBO() {
        return sendEmailBO;
    }

    public void setSendMailHistoryBO(SendMailHistoryBO sendEmailBO) {
        this.sendEmailBO = sendEmailBO;
        //emailConfigBO = MemoryDataLoader.getConfigEmailServerBO(sendEmailBO.getEmailConfigId());
    }

    public void loadEmailConfig(ConfigEmailServerBO emailConfigBO) {
        server = emailConfigBO.getMailServer();
        port = emailConfigBO.getPort();
        emailAddress = emailConfigBO.getEmail();
        username = emailConfigBO.getUsername();
        password = AESUtil.decryption(emailConfigBO.getPassword());
        securityType = emailConfigBO.getSecurityType();
    }

    public List<FileBean> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<FileBean> attachments) {
        this.attachments = attachments;
    }

    public SendEmail() {
    }

    public SendEmail(Logger logger) {
        this.logger = logger;
    }

    public boolean sendEmail() throws Exception {
        return true;
    }

    protected Properties setProperties() throws Exception {
        return null;
    }

    public String sendEmail(List<String> toList, List<String> ccList, List<String> bccList, String subject, String body, List<FileBean> attachments) throws MessagingException, NoSuchAlgorithmException, KeyManagementException, Exception {
        return null;
    }

    public void doAddToList(MimeMessage message, List<String> toList) throws AddressException, MessagingException {
        if (!StringUtils.isNullOrEmpty(toList)) {
            for (String to : toList) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            }
        }
    }

    public void doAddCcList(MimeMessage message, List<String> ccList) throws AddressException, MessagingException {
        if (!StringUtils.isNullOrEmpty(ccList)) {
            for (String cc : ccList) {
                message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
            }
        }
    }

    public void doAddBccList(MimeMessage message, List<String> bccList) throws AddressException, MessagingException {
        if (!StringUtils.isNullOrEmpty(bccList)) {
            for (String bcc : bccList) {
                message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
            }
        }
    }

    public String getlistTo(List<String> toList) throws AddressException, MessagingException {
        String to = "";
        if (!StringUtils.isNullOrEmpty(toList)) {
            for (String bcc : toList) {
                to += bcc + ";";
            }
        } else {
            return "ERROR";
        }
        return to;
    }
}
