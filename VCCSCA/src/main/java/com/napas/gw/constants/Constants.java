/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.constants;

/**
 *
 * Constants
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class Constants extends Tester {

    public static final long ONE_DAY_MILLISECONDS = 24 * 60 * 60 * 1000 - 1;
    public static final String FIRST_VALUE = "-1";//selectOneMenu no selection value 

    public static final class ORDER {

        public static final String ASCENDING = "ASCENDING";
        public static final String DESCENDING = "DESCENDING";

    }

    public static class SEND_EMAL_CLASS {

        public static final String SEND_MAIL_SSLTLS = "com.napas.gw.business.email.InterfaceSendEmaiSSLTLS";
        public static final String SEND_MAIL_SSL = "com.napas.gw.business.email.InterfaceSendEmailSSL";
        public static final String SEND_MAIL_TLS = "com.napas.gw.business.email.InterfaceSendEmailTLS";

    }

}
