/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.constants;

import com.napas.vccsca.utils.StringUtils;
import java.util.*;
import java.lang.reflect.Field;

/**
 *
 * PermissionRightDAL
 *
 * @author LuongNK
 * @since Aug 9, 2018
 * @version 1.0-SNAPSHOT
 */
public class Tester implements P3cnstInterface {

    public static boolean evaluate(Class clazz, String val) {
        try {
            Field fields[] = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                if (fields[i].getName().endsWith("_EXT")) {
                    continue;
                }
                if (((String) fields[i].get(null)).equals(val)) {
                    return true;
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String _search(Class clazz, String val) {
        try {
            Field fields[] = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                String[] f = (String[]) fields[i].get(null);
                if ((f[0]).equals(val)) {
                    return fields[i].getName();
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return "";

    }

    public static String __search(Class clazz, String val, int cnt) {
        String str = _search(clazz, val);
        if (str == null) {
            return "";
        }

        String work = null;
        try {
            Field fields[] = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                if ((fields[i].getName().equals(str + "__EXT"))) {
                    String rv = (String) fields[i].get(null);
                    work = rv.substring(rv.lastIndexOf(".") + 1);
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if (work == null) {
            return "";
        }
//		String[] vals = work.split("%");
        String[] vals = StringUtils.split(work, '%');

        if (cnt - 1 > vals.length) {
            return "";
        }
        return vals[cnt - 1];
    }

    public boolean executeP3Command_evaluate(Class clazz, String val) {
        boolean blnYN = evaluate(clazz, val);
        return blnYN;
    }

    public String executeP3Command_search(Class clazz, String val) {
        return _search(clazz, val);
    }

    public String executeP3Command__search(Class clazz, String val, int cnt) {
        return __search(clazz, val, cnt);
    }

    public static List getCodeList(Class clazz) {
        List CodeList = new ArrayList();
        try {
            Field fields[] = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                if (fields[i].getName().indexOf("__EXT") == -1) {
                    CodeList.add((String) fields[i].get(null));
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return CodeList;
    }
}
