/*
 *   Copyright (C) 2018 NAPAS. All rights reserved.
 *   NAPAS VCCS CERTIFICATE
 */
package com.napas.gw.constants;

/**
 *
 * P3cnstInterface
 *
 * @author LuongNK
 * @since Aug 24, 2018
 * @version 1.0-SNAPSHOT
 */
public interface P3cnstInterface {

    boolean executeP3Command_evaluate(Class clazz, String val);

    String executeP3Command_search(Class clazz, String val);

    String executeP3Command__search(Class clazz, String val, int cnt);
}
