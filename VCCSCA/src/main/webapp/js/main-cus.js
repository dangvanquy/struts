$("form :input").each(function () {
    if ($(this).attr("require-message") != undefined && $(this).attr("require-message") != "" || $(this).attr("type-message") != "") {
        $(this).attr('oninput', 'setCustomValidity(\'\')');
        $(this).attr('oninvalid', 'showMessageCusstom(this)');
    }
});

$(document).on("keypress", "form :input", function (e) {
    var a = [];
    var k = e.which;
    if ($(this).hasClass("number")) {
        a.push(8);
        for (i = 48; i < 58; i++) {
            a.push(i);
        }
        if (!(a.indexOf(k) >= 0)) {
            e.preventDefault();
        }
    }

    if ($(this).hasClass("phone-number")) {
        a.push(8);
        a.push(40);//(
        a.push(41);//)
        a.push(43);//+
        a.push(45);//-
        a.push(32);//space

        for (i = 48; i < 58; i++) {
            a.push(i);
        }
        if (!(a.indexOf(k) >= 0)) {
            e.preventDefault();
        }
    }
    if ($(this).hasClass("character")) {
        a.push(8);
        for (i = 48; i <= 57; i++) {
            a.push(i);
        }
        for (i = 65; i <= 90; i++) {
            a.push(i);
        }
        for (i = 97; i <= 122; i++) {
            a.push(i);
        }
        if (!(a.indexOf(k) >= 0)) {
            e.preventDefault();
        }
    }
    if ($(this).hasClass("rsa")) {
        a.push(8);
        //0-9
        for (i = 48; i < 58; i++) {
            a.push(i);
        }
        //A-F
        for (i = 65; i <= 70; i++) {
            a.push(i);
        }
        //a-f
        for (i = 97; i < 103; i++) {
            a.push(i);
        }
        if (!(a.indexOf(k) >= 0)) {
            e.preventDefault();
        }
    }
    if ($(this).hasClass("removespecchar")) {
        a.push(33);//!
        a.push(64);//@
        a.push(35);//#
        a.push(36);//$
        a.push(38);//&
        a.push(37);//%
        a.push(94);//^
        a.push(95);//_
        a.push(61);//=
        a.push(42);//*
//        a.push(32);//space
        a.push(47);// /
        a.push(45);// -
        a.push(43);// +
        a.push(40);// (
        a.push(41);// )
        a.push(92);// \
        a.push(91);// [
        a.push(93);// ]
        a.push(123);// {
        a.push(125);// }
        a.push(58);// :
        a.push(59);// ;
        a.push(34);// "
        a.push(39);// '
        a.push(44);// ,
        a.push(46);// .
        a.push(63);// ?
        a.push(96);// `
        a.push(126);// ~
        a.push(124);// |
        a.push(60);// <
        a.push(62);// >

        if ((a.indexOf(k) >= 0)) {
            e.preventDefault();
        }
    }
});


//$('form').on('keyup', 'input[type="text"], input.date', function (event) {
//    if (!validateDate($(this).val())) {
//        // console.log("heo roi");
//        var obj = $(this)[0];
//        var type_msg = obj.getAttribute("type-message");
//        obj.setCustomValidity(type_msg);
//    } else {
//        var obj = $(this)[0];
//        obj.setCustomValidity("");
//        // console.log("ok");
//    }
//});

//$('.date').keyup(function (event) {
//    if ($(this).val() != "" && !validateDate($(this).val())) {
//        // console.log("heo roi");
//        var obj = $(this)[0];
//        var type_msg = obj.getAttribute("type-message");
//        obj.setCustomValidity(type_msg);
//    } else {
//        var obj = $(this)[0];
//        obj.setCustomValidity("");
//        // console.log("ok");
//    }
//});

$('.date').change(function (event) {
    if ($(this).val() != "" && !validateDate($(this).val())) {
        // console.log("heo roi");
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
        obj.reportValidity();
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
        // console.log("ok");
    }
});

$('.number').change(function (event) {
    if ($(this).val() != "") {
        var arr = $(this).val().split("");
        var numberStr = "";
        for (var i = 0; i < arr.length; i++) {
            if ((arr[i].trim() != "") && !isNaN(arr[i])) {
                numberStr += arr[i];
            }
        }
        $(this).val(numberStr);
    }
});
$('.phone-number').change(function (event) {
    if ($(this).val() != "") {
        var arr = $(this).val().split("");
        var numberStr = "";
        for (var i = 0; i < arr.length; i++) {
            if ((arr[i].trim() != "") && !isNaN(arr[i])) {
                numberStr += arr[i];
            }
            if (arr[i] == ")") {
                numberStr += arr[i];
            }
            if (arr[i] == "(") {
                numberStr += arr[i];
            }
            if (arr[i] == "+") {
                numberStr += arr[i];
            }
        }
        $(this).val(numberStr);
    }
});
$('.rsa').change(function (event) {
    if ($(this).val() != "") {
        var arr = $(this).val().split("");
        var rsaStr = "";
        for (var i = 0; i < arr.length; i++) {
            if ((arr[i].trim() != "") && is_hexadecimal(arr[i])) {
                rsaStr += arr[i];
            }
        }
        $(this).val(rsaStr);
        $(this).focus();
    }
});

function is_hexadecimal(str)
{
    regexp = /^[0-9a-fA-F]+$/;

    if (regexp.test(str))
    {
        return true;
    } else
    {
        return false;
    }
}

//console.log(is_hexadecimal("ffffff"));
//
//console.log(is_hexadecimal("fz5500"));


$('#privateKeyExponent1').change(function (event) {
//    if ($(this).val() !== "") {
//        var key = $(this).val();
//        if (key.length % 2 !== 0) {
//            var obj = $(this)[0];
//            obj.setCustomValidity("Mã Private key exponent không chính xác");
//        } else {
//            var obj = $(this)[0];
//            obj.setCustomValidity("");
//        }
//    }
});

$('.validate_date_before').change(function (event) {
    if ($(this).val() != "" && !validateDate($(this).val())) {
    } else {
        if ($('.validate_date_after').val() != null
                && $('.validate_date_after').val() != ""
                && getDate($(this).val()) > getDate($('.validate_date_after').val()))
        {
            $(this).attr("error-validate-before", "");
            var obj = $(this)[0];
            var type_msg = obj.getAttribute("type-message-before");
            obj.setCustomValidity(type_msg);
//            obj.reportValidity();
        } else {
            $('.validate_date_after').removeAttr("error-validate-after");
            $('.validate_date_before').removeAttr("error-validate-before");
            var obj = $(this)[0];
            obj.setCustomValidity("");
//            obj.reportValidity();
        }
    }
});


$('.validate_date_after').change(function (event) {
    if ($(this).val() != "" && !validateDate($(this).val())) {
    } else {
        if ($('.validate_date_before').val() != null
                && $('.validate_date_before').val() != ""
                && getDate($(this).val()) < getDate($('.validate_date_before').val()))
        {
            $(this).attr("error-validate-after", "");
            var obj = $(this)[0];
            var type_msg = obj.getAttribute("type-message-after");
            obj.setCustomValidity(type_msg);
//            obj.reportValidity();
        } else {
            $('.validate_date_after').removeAttr("error-validate-after");
            $('.validate_date_before').removeAttr("error-validate-before");
            var obj = $(this)[0];
            obj.setCustomValidity("");
//            obj.reportValidity();
        }
    }
});


function getDate(dateValue)
{
    var selectedDate = dateValue;
    if (selectedDate == '')
        return false;

    var regExp = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dateArray = selectedDate.match(regExp); // is format OK?

    if (dateArray == null) {
        return false;
    }

    day = dateArray[1];
    month = dateArray[3];
    year = dateArray[5];

    return new Date(year + "-" + month + "-" + day);
}

function validateDate(dateValue)
{
    var selectedDate = dateValue;
    if (selectedDate == '')
        return false;

    var regExp = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dateArray = selectedDate.match(regExp); // is format OK?

    if (dateArray == null) {
        return false;
    }

    day = dateArray[1];
    month = dateArray[3];
    year = dateArray[5];

    if (month < 1 || month > 12) {
        return false;
    } else if (day < 1 || day > 31) {
        return false;
    } else if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
        return false;
    } else if (month == 2) {
        var isLeapYear = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isLeapYear)) {
            return false
        }
    }
    return true;
}

$('.cerSerial').change(function (event) {
    if ($(this).val() != "" && !checkSerial($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkSerial(serialValue)
{
    if (serialValue == '' || serialValue.length > 100) {
        return false;
    }

    var regExp = /^[_A-z0-9]*$/; //Declare Regex
    var serialArray = serialValue.match(regExp); // is format OK?

    if (serialArray == null) {
        return false;
    }
    return true;
}

$('.reqSerial').change(function (event) {
    if ($(this).val() != "" && !checkSerial($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkSerial(serialValue)
{
    if (serialValue == '' || serialValue.length > 38) {
        return false;
    }

    var regExp = /^[_A-z0-9]*$/; //Declare Regex
    var serialArray = serialValue.match(regExp); // is format OK?

    if (serialArray == null) {
        return false;
    }
    return true;
}

$('.ipk').change(function (event) {
    if ($(this).val() != "" && !checkIpk($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkIpk(ipkValue)
{
    if (ipkValue == '' || ipkValue.length > 1000) {
        return false;
    }

    var regExp = /^[_A-z0-9]*$/; //Declare Regex
    var ipkArray = ipkValue.match(regExp); // is format OK?

    if (ipkArray == null) {
        return false;
    }
    return true;
}

$('.bankIdentity').change(function (event) {
    if ($(this).val() != "" && !checkBankIdentity($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkBankIdentity(bankIdentityValue)
{
    if (bankIdentityValue == '' || bankIdentityValue.length > 8) {
        return false;
    }

    var regExp = /^[_A-z0-9]*$/; //Declare Regex
    var bankIdentityArray = bankIdentityValue.match(regExp); // is format OK?

    if (bankIdentityArray == null) {
        return false;
    }
    return true;
}

$('.cfConfirmPass').change(function (event) {
    if ($(this).val() != "" && $('.cfPass').val() != "") {
        if ($(this).val() !== $('.cfPass').val()) {
            var obj = $(this)[0];
            var type_msg = obj.getAttribute("type-message");
            obj.setCustomValidity(type_msg);
        }
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

$('.password').change(function (event) {
    if ($(this).val() != "" && !checkPassword($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
        obj.reportValidity();
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
        obj.reportValidity();
    }
});

function checkPassword(passwordValue)
{
    if (passwordValue == '' || passwordValue.length > 100) {
        return false;
    }
    if (0 < passwordValue.length && passwordValue.length < 8) {
        return false;
    }
//    var regExp = /^[_A-z0-9!@#$%^&*]*$/; //Declare Regex
//    var passwordArray = passwordValue.match(regExp); // is format OK?
//
//    if (passwordArray == null) {
//        return false;
//    }
    return true;
}

$('.smtp').change(function (event) {
    if ($(this).val() != "" && !checkSmtp($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkSmtp(smtpValue)
{
    if (smtpValue == '' || smtpValue.length > 100) {
        return false;
    }

    var regExp = /^\w+\.\w+\.[a-zA-z]{1,100}$/; //Declare Regex
    var smtpArray = smtpValue.match(regExp); // is format OK?

    if (smtpArray == null) {
        return false;
    }
    return true;
}

$('.port').change(function (event) {
    if ($(this).val() != "" && !checkPort($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkPort(portValue)
{
    if (portValue == '' || portValue.length > 10) {
        return false;
    }

    var regExp = /^[0-9]{1,10}$/; //Declare Regex
    var portArray = portValue.match(regExp); // is format OK?

    if (portArray == null) {
        return false;
    }
    return true;
}

$('.registerId').change(function (event) {
    if ($(this).val() != "" && !checkRegisterId($(this).val())) {
        var obj = $(this)[0];
        var type_msg = obj.getAttribute("type-message");
        obj.setCustomValidity(type_msg);
    } else {
        var obj = $(this)[0];
        obj.setCustomValidity("");
    }
});

function checkRegisterId(registerIdValue)
{
    if (registerIdValue == '' || registerIdValue.length > 10) {
        return false;
    }

    var regExp = /^[0-9]{1,10}$/; //Declare Regex
    var registerIdArray = registerIdValue.match(regExp); // is format OK?

    if (registerIdArray == null) {
        return false;
    }
    return true;
}

$('.warningName').change(function (event) {
    if ($(this).val() != "" && !checkSpecChar($(this).val())) {
        var obj = $(this)[0];
        obj.setAttribute("spec-message", "Không được chứa ký tự đặc biệt");
        obj.setCustomValidity("Không được chứa ký tự đặc biệt");
    } else {
        var obj = $(this)[0];
        obj.setAttribute("spec-message", "");
        obj.removeAttribute("spec-message");
        obj.setCustomValidity("");
    }
});

function checkSpecChar(warningNameValue)
{
//    if (warningNameValue == '' || warningNameValue.length > 10) {
//        return false;
//    }

    var arr = warningNameValue.split(" ");
    warningNameValue = "";
    for (var i = 0; i < arr.length; i++) {
        warningNameValue += arr[i];
    }
    //console.log(warningNameValue);

    var regExp = /[!@#$%^&*(),.?":{}|<>]/; //Declare Regex
    var warningNameArray = warningNameValue.match(regExp); // is format OK?

    if (warningNameArray != null) {
        return false;
    }
    return true;
}

function showMessageCusstom(obj) {
    var rq_msg = obj.getAttribute("require-message");
    var type_msg = obj.getAttribute("type-message");
    var type_msg_bf = obj.getAttribute("type-message-before");
    var type_msg_af = obj.getAttribute("type-message-after");
    var checksum = obj.getAttribute("checksum-message");
    var spec_err = obj.getAttribute("spec-message");

    if (obj.hasAttribute("required") === true) {
        if (obj.value === "") {
            if (rq_msg !== null && rq_msg !== "") {
                obj.setCustomValidity(rq_msg);
                return true;
            }
        } else {
            if (obj.hasAttribute("error-validate-before")) {
                if (type_msg_bf !== null && type_msg_bf !== "") {
                    obj.setCustomValidity(type_msg_bf);
                    return true;
                }
            } else if (obj.hasAttribute("error-validate-after")) {
                if (type_msg_af !== null && type_msg_af !== "") {
                    obj.setCustomValidity(type_msg_af);
                    return true;
                }
            } else {
                if (type_msg !== null && type_msg !== "") {
                    obj.setCustomValidity(type_msg);
                    return true;
                }
            }
        }
    } else {
        if (obj.hasAttribute("error-validate-before")) {
            if (type_msg_bf !== null && type_msg_bf !== "") {
                obj.setCustomValidity(type_msg_bf);
                return true;
            }
        } else if (obj.hasAttribute("error-validate-after")) {
            if (type_msg_af !== null && type_msg_af !== "") {
                obj.setCustomValidity(type_msg_af);
                return true;
            }
        } else {
            if (!validateDate(obj.value)) {
                if (type_msg !== null && type_msg !== "") {
                    obj.setCustomValidity(type_msg);
                    return true;
                }
            }
        }
    }

    if (spec_err != null && spec_err != "") {
        if (!checkSpecChar(obj.value)) {
            obj.setCustomValidity(spec_err);
            return true;
        }
    }
    if (checksum != null && checksum != "") {
        if (obj.length !== 64) {
            obj.setCustomValidity(checksum);
            return true;
        } else {
            obj.setCustomValidity("");
            return true;
        }
    }
    obj.setCustomValidity("");
    obj.reportValidity();
    return true;
}

function submitExport(form, action) {
    $('#' + form).attr('action', action);
    $('#' + form).submit();
}

function fillParamToConten(obj, out_id) {
    var values = $('#' + obj).val();
    var html = '<div class="alert alert-success" role="alert">';
    html += "Hãy chèn các tham số sau vào phần cần chỉnh sửa trong nội dung email: <br/><br/>";
    if (values != null) {
        for (var i = 0; i < values.length; i++) {
            //param_hiden_86
            console.log('#param_hiden_' + values[i]);
            html += "<div class='col-md-4'>" + $('#param_hiden_' + values[i]).attr("value_name") + "</div><div class='col-md-8'>{" + $('#param_hiden_' + values[i]).val() + "}</div>";
        }
    }
    html += "<div class='clearfix'></div></div>";
    $('#' + out_id).html(html);
}


function upCase(obj) {
    var str = obj.value;
    obj.value = str.toUpperCase();
}
;

var valueBefoAddF = "";
function paddF(obj, len) {
//    var sVal = obj.value;
//    valueBefoAddF = sVal;
//    if (sVal.length != len) {
//        for (var i = sVal.length; i < len; i++) {
//            sVal = sVal + "F";
//        }
//    }
//    obj.value = sVal;
}
;
function loadbaseF(obj) {
    if (valueBefoAddF != "") {
        obj.value = valueBefoAddF;
    }
}
;

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateItemEmail() {

//    mailTo
//    mailCc
//    mailBcc
//    mailToEdit
//    mailCcEdit
//    mailBccEdit
    if (!checkValidate('mailTo')) {
        return false;
    }
    if (!checkValidate('mailCc')) {
        return false;
    }
    if (!checkValidate('mailBcc')) {
        return false;
    }
    if (!checkValidate('mailToEdit')) {
        return false;
    }
    if (!checkValidate('mailCcEdit')) {
        return false;
    }
    if (!checkValidate('mailBccEdit')) {
        return false;
    }
    if (!checkValidate('email')) {
        return false;
    }

    $('input[type=email]').each(function () {
        if (!checkValidate2($(this))) {
            return false;
        }
    });

    return true;
}

$('form').submit(function (e) {
    $('input[type=email]').each(function () {
        if (!checkValidate2($(this))) {
            e.preventDefault();
            return;
        }
    });
    $("input[class^='number'], input[class*='number']").each(function () {
        if ($(this).val() != "") {
            var arr = $(this).val().split("");
            for (var i = 0; i < arr.length; i++) {
                if (isNaN(arr[i])) {
                    alert("Dữ liệu đầu vào không phải kiểu số, vui lòng nhập lại");
                    $(this).focus();
                    e.preventDefault();
                    return;
                }
            }
        }
    });
    $("input[class^='date'], input[class*='date']").each(function () {
        if ($(this).val() != "") {
            if (!validateDate($(this).val())) {
                alert("Lỗi định dạng ngày tháng");
                $(this).focus();

//                $("cus-id-msg-danger").text("Lỗi định dạng ngày tháng");
//                $("div-cus-id-msg-danger").show();
                e.preventDefault();
                return false;
            }
        }
    });
});




function checkValidate2(objThis) {
    if (objThis.val() != null && objThis.val() !== "") {
        var msg = objThis.attr('type-message');
        var emaillist = objThis.val();
        var arr = emaillist.split(";");
        var obj = objThis[0];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].trim() != "" && !validateEmail(arr[i].trim())) {
                obj.setCustomValidity(msg);
                obj.reportValidity();
                return false;
            }
        }
        obj.setCustomValidity('');
    }
    return true;
}
function checkValidate(key) {
    if ($("#" + key).val() != null && $("#" + key).val() !== "") {
        var msg = $("#" + key).attr('type-message');
        var emaillist = $("#" + key).val();
        var arr = emaillist.split(";");
        var obj = $("#" + key)[0];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].trim() != "" && !validateEmail(arr[i].trim())) {
                obj.setCustomValidity(msg);
                obj.reportValidity();
                return false;
            }
        }
        obj.setCustomValidity('');
    }
    return true;
}