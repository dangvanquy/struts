<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<header class="header">
    <div class="container">
        <a class="logo" href="#" onclick="gotoHome()">
            <img alt="NAPAS" src="resources/PublishingImages/logo.png" />
        </a>
        <%

            String fullName = (String) request.getSession().getAttribute("fullName");

            String REQUEST_VIEW_INDEX = (String) request.getSession().getAttribute("REQUEST_VIEW_INDEX");
            String REQUEST_VIEW = (String) request.getSession().getAttribute("REQUEST_VIEW");
            String CERTIFICATE_VIEW = (String) request.getSession().getAttribute("CERTIFICATE_VIEW");
            String BANKMEMBERSHIP_VIEW = (String) request.getSession().getAttribute("BANKMEMBERSHIP_VIEW");

            String REPORT_VIEW_ROOTCA = (String) request.getSession().getAttribute("REPORT_VIEW_ROOTCA");
            String REPORT_VIEW_CERT = (String) request.getSession().getAttribute("REPORT_VIEW_CERT");
            String REPORT_VIEW_REQUEST = (String) request.getSession().getAttribute("REPORT_VIEW_REQUEST");
            String REPORT_VIEW_NHTV = (String) request.getSession().getAttribute("REPORT_VIEW_NHTV");

            String ADMIN_VIEW_ACCOUNT = (String) request.getSession().getAttribute("ADMIN_VIEW_ACCOUNT");
            String ADMIN_VIEW_GROUPPERMISION = (String) request.getSession().getAttribute("ADMIN_VIEW_GROUPPERMISION");
            String ADMIN_VIEW_ALERT = (String) request.getSession().getAttribute("ADMIN_VIEW_ALERT");
            String ADMIN_VIEW_KEY = (String) request.getSession().getAttribute("ADMIN_VIEW_KEY");
            String ADMIN_VIEW_CHECKSUM = (String) request.getSession().getAttribute("ADMIN_VIEW_CHECKSUM");
            String ADMIN_VIEW_RQDELETE = (String) request.getSession().getAttribute("ADMIN_VIEW_RQDELETE");
            String ADMIN_VIEW_HISTORY = (String) request.getSession().getAttribute("ADMIN_VIEW_HISTORY");
            String ADMIN_VIEW_PARAM = (String) request.getSession().getAttribute("ADMIN_VIEW_PARAM");
            String ADMIN_VIEW_EMAIL = (String) request.getSession().getAttribute("ADMIN_VIEW_EMAIL");
            String ADMIN_VIEW_CONFIG_HSM = (String) request.getSession().getAttribute("ADMIN_VIEW_CONFIG_HSM");
            String CERTIFICATE_VERIFY = (String) request.getSession().getAttribute("CERTIFICATE_VERIFY");
        %>

        <!--        <div class="header-right">
                    <ul class="menu-top list-inline">
                        <li class="hotline">
                            <span class="glyphicon glyphicon-user"></span>
                        </li>
                        <li>
    </li>
    <li>
        
    </li>
</ul>
</div>-->
        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> ${fullName}</span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="doi-mat-khau.html">Đổi mật khẩu</a></li>
                <li class="divider"></li>
                <li><a style="cursor: pointer" onclick="$('#formLogout').submit();">Logout</a></li>
            </ul>
        </div>
        <!-- user dropdown ends -->
    </div>
    <form id="formLogout" action="logout.do" method="POST">
    </form>
</header>
<nav class="menu ">
    <style type="text/css">
        .menu2
        {
            width:70%; 
            height:30px; 
            line-height:30px;
            margin:0 auto;
            /*float: right;*/
        }
        .menu2 li
        {
            position:relative;
            display:inline-block;
            width:100%;
        }
        .menu2 li ul
        {
            position:absolute; 
            display:none;
        }
        .menu2 li:hover > ul
        {
            display:block;
            background-color: white;
        }
        .menu2 a
        {
            text-decoration: none;
            text-transform: uppercase;
            font-size: 15px;
            color: #1d427e;
            font-family: "Akzidenz-Bold", serif;
        }
        .menu2 ul ul,
        .menu2 li:hover > a,
        .menu2 a:hover
        {
            /*opacity:.8;*/
        }
        /*Level 1*/
        .menu2 > ul > li
        {
            float:left;
            /*width: 20%;*/
        }
        .menu2 > ul > li > a
        {
            width:100%;
            padding-left:3%; 
            padding-right:3%; 
            text-transform:uppercase; 
            font-weight:bold;
            text-align: center;
        }
        /*Level 2*/
        .menu2 > ul > li > ul
        {
            position:absolute; 
            top:100%; 
            left:0; 
            width:100%; 
            width: 135%;
        }
        .menu2 > ul > li > ul > li
        {
            /*width: 135%;*/
            width: 100%;
        }
        .menu2 > ul > li > ul > li > a
        {
            width:100%;
            padding-left:3%;
            padding-right:3%;
            text-align: left;
            margin-top: 10px;
        }
        /* Level +++ */
        .menu2 ul ul ul
        {
            width:100%;
            left:100%;
            top:0;
            /*background:#a1b1d4;*/
        }
        .menu2 ul ul ul li
        {
            width:100%;
        }
        .menu2 ul ul ul li a
        {
            width:94%;
            padding-left:3%;
            padding-right:3%;

        }
        .menu-chiden a:hover {
            color: white;
            background-color: #1d427e;
        }
        .menu-chiden:hover {
            color: white;
            background-color: #1d427e;
        }


        @media (min-width:992px){
            .menu2 {width: 90% !important}
            .menu-c-1 { width: 15% !important}
            .menu-c-2 { width: 20% !important}
            .menu-c-3 { width: 12% !important}
            .menu-c-4 { width: 10% !important}
            .menu-c-5 { width: 20% !important}
        }
        @media (min-width:1200px){
            .menu-c-1 { width: 15% !important}
            .menu-c-2 { width: 18% !important}
            .menu-c-3 { width: 12% !important}
            .menu-c-4 { width: 10% !important}
            .menu-c-5 { width: 15% !important}
        }

        @media (min-width:1600px){
            .menu2 {width: 70% !important}
            .menu-c-1 { width: 15% !important}
            .menu-c-2 { width: 18% !important}
            .menu-c-3 { width: 12% !important}
            .menu-c-4 { width: 10% !important}
            .menu-c-5 { width: 15% !important}
        }

    </style>
    <div class="menu2">
        <ul>
            <li class="menu-c-1">
                <a href="#">QUẢN LÝ YÊU CẦU</a>
                <c:if test="${REQUEST_VIEW == 'true' || REQUEST_VIEW_INDEX == 'true'}">
                    <ul>
                        <c:if test="${REQUEST_VIEW_INDEX == 'true'}">
                            <li class="menu-chiden"><a href="mau-yeu-cau.html">TẠO MẪU YÊU CẦU</a></li>
                            </c:if>
                            <c:if test="${REQUEST_VIEW == 'true'}">
                            <li class="menu-chiden"><a href="quan-ly-yeu-cau.html">DANH SÁCH YÊU CẦU</a></li>
                            </c:if>

                    </ul>
                </c:if>

            </li>
            <li class="menu-c-2">
                <a href="#">QUẢN LÝ CHỨNG THƯ SỐ</a>
                <c:if test="${CERTIFICATE_VIEW == 'true' || CERTIFICATE_VERIFY == 'true'}">
                    <ul>
                        <c:if test="${CERTIFICATE_VIEW == 'true'}">
                            <li class="menu-chiden"><a href="quan-ly-chung-thu-so.html">DANH SÁCH CHỨNG THƯ SỐ</a></li>
                            </c:if>
                            <c:if test="${CERTIFICATE_VERIFY == 'true'}">
                            <li class="menu-chiden"><a href="xac-minh-chung-thu-so.html">XÁC NHẬN CHỨNG THƯ SỐ</a></li>
                            </c:if>
                    </ul>
                </c:if>
            </li>
            <li class="menu-c-3">
                <c:if test="${BANKMEMBERSHIP_VIEW == 'true'}">
                    <a href="quan-ly-nhtv.html">QUẢN LÝ NHTV</a>
                </c:if>
                <c:if test="${BANKMEMBERSHIP_VIEW != 'true'}">
                    <a href="#">QUẢN LÝ NHTV</a>
                </c:if>
            </li>
            <li class="menu-c-4">
                <a href="#" >BÁO CÁO</a>
                    <ul style="width: 200%">
                        <c:if test="${REPORT_VIEW_ROOTCA == 'true'}"><li class="menu-chiden"><a href="bao-cao-root-ca.html">BÁO CÁO ROOT CA</a></li></c:if>
                        <c:if test="${REPORT_VIEW_CERT == 'true'}"><li class="menu-chiden"><a href="bao-cao-chung-thu-so.html">BAO CÁO CHỨNG THƯ SỐ</a></li></c:if>
                        <c:if test="${REPORT_VIEW_REQUEST == 'true'}"><li class="menu-chiden"><a href="bao-cao-yeu-cau-cap-cts.html">BÁO CÁO YÊU CẦU CẤP CHỨNG THƯ SỐ</a></li></c:if>
                        <c:if test="${REPORT_VIEW_NHTV == 'true'}"><li class="menu-chiden"><a href="bao-cao-nhtv.html">BÁO CÁO NGÂN HÀNG THÀNH VIÊN</a></li></c:if>
                    </ul>
                </li>
                <li class="menu-c-5">
                    <a href="#">QUẢN TRỊ HỆ THỐNG</a>

                    <ul>
                    <c:if test="${ADMIN_VIEW_ACCOUNT == 'true'}"><li class="menu-chiden"><a href="quan-ly-nguoi-dung.html">QUẢN LÝ NGƯỜI DÙNG</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_GROUPPERMISION == 'true'}"><li class="menu-chiden"><a href="danh-sach-nhom-quyen.html">QUẢN LÝ QUYỀN</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_KEY == 'true'}"><li class="menu-chiden"><a href="quan-ly-rsa.html">QUẢN LÝ KHÓA RSA</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_CHECKSUM == 'true'}"><li class="menu-chiden"><a href="quan-ly-checksum.html">QUẢN LÝ CHECKSUM VÀ PUBLICKEY</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_ALERT == 'true'}"><li class="menu-chiden"><a href="quan-ly-canh-bao.html">QUẢN LÝ CẢNH BÁO</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_RQDELETE == 'true'}"><li class="menu-chiden"><a href="danh-sach-yeu-cau-da-xoa.html">DANH SÁCH YÊU CẦU ĐÃ XÓA</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_HISTORY == 'true'}"><li class="menu-chiden"><a href="lich-su-truy-cap.html">LỊCH SỬ TRUY CẬP</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_PARAM == 'true'}"><li class="menu-chiden"><a href="cau-hinh-tham-so.html">CẤU HÌNH THAM SỐ</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_EMAIL == 'true'}"><li class="menu-chiden"><a href="cau-hinh-email.html">CẤU HÌNH EMAIL</a></li></c:if>
                    <c:if test="${ADMIN_VIEW_CONFIG_HSM == 'true'}"><li class="menu-chiden"><a href="cau-hinh-hsm.html">CẤU HÌNH HSM</a></li></c:if>
                </ul>

            </li>
        </ul>
    </div>
</nav>
<script>
    function gotoHome() {
        $('#homeForm').submit();
    }
</script>

<form id="homeForm" action="trang-chu.html" method="GET">
</form> 