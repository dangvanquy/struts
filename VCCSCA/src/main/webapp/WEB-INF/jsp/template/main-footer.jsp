<%@page contentType="text/html" pageEncoding="UTF-8"%>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="logo-footer">
                    <a href="/">
                        <img id="ctl00_ctl18_SiteLogoImage92" src="resources/PublishingImages/logo_white.png" alt="napas" style="width: 100%; max-width: 180px" />
                    </a>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12" style="text-align: left;">                
                <div class="company">
                    <h2>
                        CÔNG TY CỔ PHẦN THANH TOÁN QUỐC GIA <br /> VIỆT NAM - NAPAS
                    </h2>
                    <p>
                        National Payment Corporation of Vietnam
                    </p>
                </div>                
                <div class="social">
                    <a id="ctl00_ctl18_lbtFaceBook" href="https://www.facebook.com/NapasVietnam/" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a id="ctl00_ctl18_lbtYoutube" href="https://www.youtube.com/channel/UCc5yR3KB47BSHkuNSzvl8dQ" target="_blank">
                        <i class="fa fa-youtube-square"></i>
                    </a>
                    <a id="ctl00_ctl18_lbtEmail" href="mailto:contact@napas.com.vn">
                        <i class="fa fa-envelope-square"></i>
                    </a>

                </div>
            </div>
        </div>
    </div>
</footer>
<div class="backtotop" style="display: block !important">
    <a href="#">
        <img src="resources/napas_theme/images/backtotop.png" /></a>
</div>
<div class="backtodesktop" style="display: block !important">
    <a href="#" onclick="BackToDesktop()" style="display: block !important">
        <img src="resources/napas_theme/images/backto-desktop.png" /></a>
</div>
<script>
    $(document).ready(function () {
        if (document.documentElement.clientWidth <= 1024) {
            var viewState = localStorage.getItem("viewState");
            if (viewState == "Desktop") {
                document.querySelector("meta[name=viewport]").setAttribute('content', 'width=1024, initial-scale=-1');
            } else {
                document.querySelector("meta[name=viewport]").setAttribute('content', 'width=device-width, initial-scale=1');
            }
            $(".backtodesktop").show()
        } else {
            $(".backtodesktop").hide();
            document.querySelector("meta[name=viewport]").setAttribute('content', 'width=device-width, initial-scale=1');
        }
    });

    function BackToDesktop() {
        var viewState = localStorage.getItem("viewState");
        if (viewState == "Mobile") {
            localStorage.setItem("viewState", "Desktop");
            document.querySelector("meta[name=viewport]").setAttribute('content', 'width=1024, initial-scale=-1');
        } else {
            localStorage.setItem("viewState", "Mobile");
            document.querySelector("meta[name=viewport]").setAttribute('content', 'width=device-width, initial-scale=1');
        }

        location.reload();
    }
</script>