<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<html>
    <body>
        <div class="container-fluid">
            <div>
                <img style="height: 200px; margin: 0px auto;max-width: 300px;" class="img-responsive" alt="NAPAS BACKEND" src="resources/napas_theme/images/big_logo.png" />
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <form id="loginModel" name='loginForm' action="login.do" modelAttribute="LoginForm" method="POST">
                        <!--<fieldset>-->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h1 class="panel-title text-center">Đăng nhập NAPAS VCCS CA</h1>
                            </div>
                            <div class="panel-body">
                                <div class="form-group row col-md-12">
                                    <label class="control-label" for="username" style="text-align:left;">
                                        Tên đăng nhập
                                    </label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                            </span>
                                            <input type="username" name="userName" class="form-control" id="username" placeholder="Username"
                                                   required
                                                   require-message="${MessageUtils.getMessage("login.user.notinput")}"
                                                   >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row col-md-12">
                                    <label class="control-label" for="password" style="text-align:left;">
                                        Mật khẩu
                                    </label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-lock" aria-hidden="true"/></span>
                                            </span>
                                            <input type="password" name="passWord" class="form-control password" id="password" placeholder="Password" required
                                                   require-message="${MessageUtils.getMessage("login.pass.notinput")}"
                                                   type-message="${MessageUtils.getMessage("change.password.validate.lengh")}"
                                                   />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <center>
                                    <c:if test="${errMessage != null}">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-danger" role="alert">
                                                    ${errMessage}
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${successMessage != null}">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-success" role="alert">
                                                    ${successMessage}
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                </center>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success form-control" type="submit">
                                            <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Đăng nhập </button>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <center><a class="text-center text-primary" href="quen-mat-khau.html">Quên mật khẩu ? </a></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</fieldset>-->
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>