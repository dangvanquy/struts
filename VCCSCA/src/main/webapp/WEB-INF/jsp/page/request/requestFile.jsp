<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<script src="resources/gijgo/js/gijgo.min.js" type="text/javascript"></script>
<link href="resources/gijgo/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<%
    String REQUEST_CREATE_INDEX = (String) request.getSession().getAttribute("REQUEST_CREATE_INDEX");
    String REQUEST_EXPORT_INDEX = (String) request.getSession().getAttribute("REQUEST_EXPORT_INDEX");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                        Quản lý yêu cầu
                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                        Mẫu yêu cầu
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='requestForm' action="mau-yeu-cau.html" modelAttribute="RequestForm" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                                        Số đăng ký
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" maxlength="8" class="form-control number" name="registerId" value="${requestForm.registerId}" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Tên ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="searchBankName" class="form-control" data-rel="chosen" name="bin">
                                                                <option value="">Chọn ngân hàng</option>
                                                                <c:forEach items="${bankList}" var="bankLst">
                                                                    <option value="${bankLst.bin}"  ${requestForm.bin == bankLst.bin ? 'selected': ''}>${bankLst.bankFullName}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                                                        Ngày tạo
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control date" name="createDate" id="createDate" placeholder="" value="${requestForm.createDate}"
                                                                   type-message="${MessageUtils.getMessage("date.validate.unformatted2")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${REQUEST_CREATE_INDEX == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalCreate()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách yêu cầu đã tạo</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên ngân hàng</th>
                                                        <th style="text-align: center">Số đăng ký</th>
                                                        <th style="text-align: center">Người tạo</th>
                                                        <th style="text-align: center">Ngày tạo</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${requestFileList}" var="requestBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index + 1}</td>
                                                            <td>${requestBO.bankName}</td>
                                                            <td>${requestBO.registerId}</td>
                                                            <td>${requestBO.createUser}</td>
                                                            <td>${DateTimeUtils.convertDateToDDMMYYYYHHmmss(requestBO.createDate)}</td>
                                                            <td>
                                                    <center>
                                                        <c:if test="${REQUEST_EXPORT_INDEX == 'true'}">
                                                            <a href="#" onclick="openModalExport('${AESUtil.encryption(requestBO.registerId)}');"
                                                               data-toggle="tooltip" data-placement="bottom" title="Tải file"><span class="glyphicon glyphicon-share"/></a>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <c:if test="${regIdTemp != null}">
                                        <input type="hidden" id="regIdTemp" value="${regIdTemp}">
                                    </c:if>
                                    <c:if test="${regIdTemp == null}">
                                        <input type="hidden" id="regIdTemp" value="">
                                    </c:if>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<form style="display: none" id="exportRequestForm" action="exportIndex.do" method='POST' accept-charset="UTF-8">
    <input type="hidden" name="regId" id="regId"/>
</form>

<!-- Create request file -->
<div id="createRequestFile" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="createRequestFileId" class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="createFileRq" name='createRequestForm' action="createIndex.do" modelAttribute="CreateRequestForm" method='POST' accept-charset="UTF-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h1 class="panel-title text-left">Thêm mới</h1>
                    </div>
                    <div class="panel-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <h1 class="panel-title text-left">Thông tin chung</h1>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                    Số đăng ký <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" maxlength="6" onclick="loadreadnum()" onblur="paddnumber()" class="form-control registerId number" name="registerId" id="createRegisterId" placeholder="" required
                                               require-message="${MessageUtils.getMessage("create.request.file.register.id.empty")}"
                                               type-message="${MessageUtils.getMessage("create.request.file.register.id.unformatted")}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="createBankName" style="text-align:right;">
                                    Tên ngân hàng <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="bankNameError" id="bankNameError" style="position: absolute; top: 5px; left: 50px; width: 0px; z-index: -1" required
                                               require-message="${MessageUtils.getMessage("create.request.file.bank.name.empty")}">
                                        <select id="createBankName" class="form-control" onchange="fillDataCreate()" data-rel="chosen" name="bankName">
                                            <option value="">Chọn ngân hàng</option>
                                            <c:forEach items="${bankListActive}" var="bankLst">
                                                <option value="${bankLst.bankId}" ${requestForm.bankName == bankLst.bankFullName ? 'selected': ''}>${bankLst.bankFullName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group row">
                            <div class="form-control-lg">
                                <div class="col-sm-12">
                                    <center>
                                        <button class="btn btn-primary" style="width: 150px;" type="button" onclick="createPopup()">Tải file</button>
                                        <button class="btn btn-default" style="width: 150px;" data-dismiss="modal" type="button" onclick="clearPopup()">Đóng</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="hidden" id="createSubmit" class="btn btn-success" style="width: 150px; visibility: hidden" type="submit"></button>
            </form>
        </div>
    </div>
</div>

<!-- Alert create modal -->
<div class="modal fade" id="alertCreate" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn các thông tin đăng ký là chính xác?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-primary" onclick="submitCreateForm()">Thêm mới</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#createDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy'
        });
        if ($('#regIdTemp').val() !== "") {
            $("#regId").val($("#regIdTemp").val());
            $('#exportRequestForm').submit();
        }
    });

    function clearPopup() {
        $('#createRegisterId').val("");
        $('#createBankName').val("");
        $('#createBankName').trigger("chosen:updated");
    }
    ;

    function fillDataCreate() {
        if ($('#createBankName').val() !== "") {
            $('#bankNameError').attr("type", "hidden");
            $('#bankNameError').val($('#createBankName').val());
        } else {
            $('#bankNameError').attr("type", "text");
            $('#bankNameError').val("");
        }
    }
    ;

    function openModalCreate() {
//        $.get("tao-mau-yeu-cau.html", {}, function (data) {
//            $("#createRequestFileId").html(data);
//            $('#createRequestFile').modal({backdrop: 'static', keyboard: false});//.modal('show')
//        });
        $('#createRequestFile').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#createBankName').next('div').addClass("showfull");
    }
    ;
    function createPopup() {
        $('#alertCreate').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function submitCreateForm() {
        $('#createSubmit').trigger("click");
        $('#alertCreate').modal('hide');
    }
    ;
    function openModalExport(regId) {
        $("#regId").val(regId);
        $('#exportRequestForm').submit();
    }
    ;
    function checkRequestExist() {
        alert(111);
        var registerId = $("#createRegisterId").val();
        $.get("tao-mau-yeu-cau.html", {registerId: registerId}, function (data) {
        });
//        returnData.then(success, error);
    }
    ;
    function success(response) {
        alert(response.d);
        if (response.d === 'success') {
//            alert(123);
            return true;
        } else {
//            alert(456);
            return false;
        }
    }

    function error(response) {
//        alert(789);
        return false;
    }
    function loadreadnum() {
        var sVal = $('#createRegisterId').val();
        if (sVal != "" && sVal != 'NaN') {
            $('#createRegisterId').val(parseInt(sVal))
        }
    }
    function paddnumber() {
        var sVal = $('#createRegisterId').val();
        var len = 6;
        if (sVal.length != len) {
            for (var i = sVal.length; i < len; i++) {
                sVal = "0" + sVal;
            }
        }
        $('#createRegisterId').val(sVal);
    }
</script>