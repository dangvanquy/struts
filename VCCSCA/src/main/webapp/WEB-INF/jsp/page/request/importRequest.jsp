<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<div style="background-color: #f5f5f5">
    <form:form id="idForm" method="POST" action="importRequest.do" enctype="multipart/form-data">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Import</h1>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="fileDatas" style="text-align:right;">
                            Tải file
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <span class="control-fileupload">
                                    <input type="file" required name="file"
                                           require-message="${MessageUtils.getMessage("request.import.upload.validate.empty")}">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <button class="btn btn-success" style="width: 100px;" type="button" onclick="smImport()">
                            <span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Import</button>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Thông tin chứng thư số</h1>
        </div>
        <form name='createRequestForm' modelAttribute="CreateRequestForm" action="createRequest.do" method='POST'
              accept-charset="UTF-8">
            <div class="panel-body">
                <div class="form-group row">
                    <center>
                        <c:if test="${errMessage != null}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger" role="alert">
                                        ${errMessage}
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${successMessage != null}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success" role="alert">
                                        ${successMessage}
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </center>
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                            Số đăng ký
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" name="registerId" id="registerId" readonly="true" value="${hexDataRequest.get('seRegisteredNumber')}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="serial" style="text-align:right;">
                            Serial
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" name="serial" id="serial" readonly="true" value="${hexDataRequest.get('newSerial')}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                            Tên ngân hàng
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" name="bankName" id="bankName" readonly="true" value="${hexDataRequest.get('bankFullName')}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                            Số định danh NH
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" name="bankIdentity" id="bankIdentity" readonly="true" value="${StringUtils.getBankIdentityNoFF(hexDataRequest.get('seIssuerIdentificationNumber'))}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                            IPK
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea rows="3" class="form-control" name="ipk" id="ipk" readonly="true">${hexDataRequest.get('usIPKModulus')}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                            Ngày hết hạn
                        </label>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="month" id="month" readonly="true" value="${hexDataRequest.get('exDate1')}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="year" id="year" readonly="true" value="${hexDataRequest.get('exDate2')}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="form-group row">
                    <div class="form-control-lg">
                        <div class="col-sm-12">
                            <center>
                                <button class="btn btn-success" style="width: 150px;" type="submit">Xác nhận</button>
                                <button class="btn btn-default" style="width: 150px;" data-dismiss="modal" type="button">Đóng</button>
                            </center>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>