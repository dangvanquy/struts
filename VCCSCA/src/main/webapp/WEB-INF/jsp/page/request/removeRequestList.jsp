<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<%
    request.setAttribute("requestError", request.getSession().getAttribute("requestError"));
    request.getSession().removeAttribute("requestError");
    request.setAttribute("bankError", request.getSession().getAttribute("bankError"));
    request.getSession().removeAttribute("bankError");
    request.setAttribute("requestMessageError", request.getSession().getAttribute("requestMessageError"));
    request.getSession().removeAttribute("requestMessageError");
    request.setAttribute("bankMessageError", request.getSession().getAttribute("bankMessageError"));
    request.getSession().removeAttribute("bankMessageError");
    String ADMIN_RECOVER_RQDELETE = (String) request.getSession().getAttribute("ADMIN_RECOVER_RQDELETE");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Danh sách yêu cầu đã xóa
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='requestForm' action="danh-sach-yeu-cau-da-xoa.html" modelAttribute="RequestForm" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                                        Số đăng ký
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="8" type="text" class="form-control number" name="registerId" id="registerId" placeholder="" value="${requestForm.registerId}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Tên ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="bankName" id="bankName" placeholder="" value="${requestForm.bankName}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                                                        IPK
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control rsa" name="ipk" id="ipk" placeholder="" value="${requestForm.ipk}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                                                        Serial
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control character" name="serial" id="serial" placeholder="" value="${requestForm.serial}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                                                        Số định danh ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control character" name="bankIdentity" id="bankIdentity" placeholder="" value="${requestForm.bankIdentity}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <center>
                                                <c:if test="${requestError != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                Số đăng ký yêu cầu: ${requestError} ${requestMessageError}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${bankError != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                Ngân hàng: ${bankError} ${bankMessageError}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${ADMIN_RECOVER_RQDELETE == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="multiRestore(${requestRemovedList.size()})"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Khôi phục</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách yêu cầu</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Số đăng ký</th>
                                                        <th style="text-align: center">Tên ngân hàng</th>
                                                        <th style="text-align: center">IPK</th>
                                                        <th style="text-align: center">Ngày hết hạn</th>
                                                        <th style="text-align: center">Số định danh ngân hàng</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                        <th style="text-align: center">
                                                            <c:if test="${ADMIN_RECOVER_RQDELETE == 'true'}">
                                                    <center>
                                                        #
                                                        <br/>
                                                        <input id="checkAll" type="checkbox" value="">
                                                    </center>
                                                </c:if>
                                                </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${requestRemovedList}" var="requestBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index + 1}</td>
                                                            <td>${requestBO.registerId}</td>
                                                            <td>${requestBO.bankName}</td>
                                                            <td>${StringUtils.displayKey(requestBO.ipk)}</td>
                                                            <td>${requestBO.expDate}</td>
                                                            <td>${requestBO.bankIdentity}</td>
                                                            <td>
                                                                Đã xóa
                                                            </td>
                                                            <td>
                                                                <c:if test="${ADMIN_RECOVER_RQDELETE == 'true'}">
                                                        <center>
                                                            <a href="#" onclick="openRestorePopup('${AESUtil.encryption(requestBO.requestId)}');"
                                                               data-toggle="tooltip" data-placement="bottom" title="Khôi phục"><span class="glyphicon glyphicon-refresh"/></a>
                                                        </center>
                                                    </c:if>
                                                    </td>
                                                    <td>
                                                        <c:if test="${ADMIN_RECOVER_RQDELETE == 'true'}">
                                                        <center>
                                                            <input id="check${itr.index}" type="checkbox" value="${AESUtil.encryption(requestBO.requestId)}" onclick="singleCheck(${itr.index + 1})">
                                                        </center>
                                                    </c:if>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function openRestorePopup(requestId) {
        $('#paramId').val(requestId);
        $('#alertRestore').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    $('#checkAll').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });
    function multiRestore(count) {
        var listChecked = "";
        for (var i = 0; i < count; i++) {
            if ($('#check' + i).is(":checked")) {
                listChecked += $('#check' + i).val();
                listChecked += ",";
            }
        }
        if (listChecked !== null && listChecked !== "") {
            $('#paramId').val(listChecked.substr(0, listChecked.length - 1));
            $('#alertRestore').modal({backdrop: 'static', keyboard: false});//.modal('show')
        } else {
            $('#alertNoneChecked').modal({backdrop: 'static', keyboard: false});//.modal('show')
        }
    }
    ;
</script>

<!-- Alert restore modal -->
<div class="modal fade" id="alertRestore" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn khôi phục yêu cầu đã chọn?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="rqdelete.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="paramId" id="paramId"/>
                        <button type="submit" class="btn btn-primary" >Khôi phục</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert restore modal -->
<div class="modal fade" id="alertNoneChecked" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn chưa chọn yêu cầu cần khôi phục!
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>