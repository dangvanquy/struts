<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>


<div class="panel panel-primary">
    <div class="panel-heading">
        <h1 class="panel-title text-left">Xác minh chứng thư số</h1>
    </div>
    <div class="panel-body">
        <div class="form-group row">
            <div class="col-lg-6">
                <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                    Số đăng ký
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="registerId" id="registerId" placeholder="" disabled="true" value="${requestBO.registerId}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                    Tên ngân hàng
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="bankName" id="bankName" placeholder="" disabled="true" value="${requestBO.bankName}">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                    Số định danh NH
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="bankIdentity" id="bankIdentity" placeholder="" disabled="true" value="${StringUtils.getBankIdentityNoFF(requestBO.bankIdentity)}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                    IPK
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea rows="3" class="form-control" name="ipk" id="ipk" placeholder="" disabled="true" style="resize: none">${requestBO.ipk}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="serial" style="text-align:right;">
                    Số serial (Hex)
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="serial" id="serial" placeholder="" disabled="true" value="${requestBO.serial}">
                    </div>
                </div>
                <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                    Ngày hết hạn
                </label>
                <div class="col-md-4">
                    <div class="form-group">
                        <select id="month" class="form-control" name="month" disabled="true">
                            <option value="">---Chọn tháng--- </option>
                            <option value="01" ${month eq '01' ? 'selected': ''}>Tháng 1</option>
                            <option value="02" ${month eq '02' ? 'selected': ''}>Tháng 2</option>
                            <option value="03" ${month eq '03' ? 'selected': ''}>Tháng 3</option>
                            <option value="04" ${month eq '04' ? 'selected': ''}>Tháng 4</option>
                            <option value="05" ${month eq '05' ? 'selected': ''}>Tháng 5</option>
                            <option value="06" ${month eq '06' ? 'selected': ''}>Tháng 6</option>
                            <option value="07" ${month eq '07' ? 'selected': ''}>Tháng 7</option>
                            <option value="08" ${month eq '08' ? 'selected': ''}>Tháng 8</option>
                            <option value="09" ${month eq '09' ? 'selected': ''}>Tháng 9</option>
                            <option value="10" ${month eq '10' ? 'selected': ''}>Tháng 10</option>
                            <option value="11" ${month eq '11' ? 'selected': ''}>Tháng 11</option>
                            <option value="12" ${month eq '12' ? 'selected': ''}>Tháng 12</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select id="year" class="form-control" name="year" disabled="true">
                            <option value="">Chọn năm</option>
                            <c:forEach var="expYear" begin="2018" end="3000" >
                                <option value="${expYear}" ${year == expYear ? 'selected': ''}>${expYear}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="ipkLengh" style="text-align:right;">
                    Độ dài IPK
                </label>
                <div class="col-md-8">
                    <div class="input-group">
                        <div style="float: left;width: 40px">
                            <p style="color: red;font-weight: bold;" id="createIpkLengh">${requestBO.ipkLengh}</p>
                        </div>
                        <div style="float: left;">
                            <p>(bit)</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="appDate" style="text-align:right;">
                    Ngày xin cấp
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="appDate" id="appDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYY(requestBO.appDate)}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="status" style="text-align:right;">
                    Trạng thái
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:if test="${requestBO.status == 0}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Soạn thảo">
                        </c:if>
                        <c:if test="${requestBO.status == 1}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Trình ký/duyệt">
                        </c:if>
                        <c:if test="${requestBO.status == 2}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Đã ký/duyệt">
                        </c:if>
                        <c:if test="${requestBO.status == 3}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Từ chối">
                        </c:if>
                        <c:if test="${requestBO.status == 4}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Xóa">
                        </c:if>
                        <c:if test="${requestBO.status == 5}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Thu hồi">
                        </c:if>
                        <c:if test="${requestBO.status == 6}">
                            <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Tạo mới">
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <h1 class="panel-title text-left" style="color: black;"><b>Thông tin khác</b></h1>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="createUser" style="text-align:right;">
                    Người tạo
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" id="createUser" placeholder="" disabled="true" value="${requestBO.createUser}">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                    Ngày tạo
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" id="createDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(requestBO.createDate)}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="updateUser" style="text-align:right;">
                    Người cập nhật
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" id="updateUser" placeholder="" disabled="true" value="${requestBO.updateUser}">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                    Ngày cập nhật
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" id="updateDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(requestBO.updateDate)}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <h1 class="panel-title text-center">Chữ ký</h1>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <c:if test="${requestBO.status == 1}">
                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                        Index
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <form id="approveRq" action="approvalRequest.do" method='POST' accept-charset="UTF-8">
                                <input type="hidden" name="paramId" id="paramId" value="${AESUtil.encryption(requestBO.requestId)}"/>
                                <button style="display: none" type="submit" id="btnsm"></button>
                                <select id="index" class="form-control" name="paramIndex"
                                        required require-message="Chưa chọn index">
                                    <option value="">Index</option>
                                    <c:forEach items="${allKeysList}" var="rSAKeysBO" varStatus="itr">
                                        <option value="${StringUtils.paddingIndex(rSAKeysBO.rsaIndex)}">${StringUtils.paddingIndex(rSAKeysBO.rsaIndex)}</option>
                                    </c:forEach>
                                </select>
                            </form>
                        </div>
                    </div>
                </c:if>
            </div>
            <div class="col-lg-6">
                <label class="control-label col-md-4" for="content" style="text-align:right;">
                    Nội dung từ chối
                </label>
                <div class="col-md-8">
                    <div class="form-group">
                        <form id="approveReject" action="rejectRequest.do" method='POST' accept-charset="UTF-8">
                            <input type="hidden" name="paramId" id="paramIdReject" value="${AESUtil.encryption(requestBO.requestId)}"/>
                            <textarea maxlength="2000" rows="3" class="form-control" name="paramContent" 
                                      required="" require-message="Lý do thu hồi không được để trống"></textarea>
                            <button style="display: none" type="submit" id="btnsmReject"></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="form-group row">
            <div class="form-control-lg">
                <div class="col-sm-12">
                    <center>
                        <c:if test="${REQUEST_APPROVAL == 'true' && requestBO.status == 1}">
                            <button class="btn btn-success" style="width: 150px;" type="button" onclick="openApprovePopup();">Ký duyệt</button>
                        </c:if>
                        <c:if test="${REQUEST_APPROVAL == 'true' && requestBO.status != 4 && requestBO.status != 3}">
                            <button class="btn btn-danger" style="width: 150px;" type="button" onclick="openRejectPopup();">Từ chối</button>
                        </c:if>
                        <button class="btn btn-default" style="width: 150px;" data-dismiss="modal" type="button">Đóng</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
