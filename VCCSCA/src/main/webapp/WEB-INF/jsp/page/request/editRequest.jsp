<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<form id="editRequestForm" name='editRequestForm' action="editRequest.do" 
      modelAttribute="EditRequestForm" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Cập nhật</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left">Thông tin chung</h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                        Số đăng ký <font style="color: red">*</font>
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="id" id="id" placeholder="" disabled="true" value="${requestBO.registerId}">
                            <input type="hidden" name="registerId" value="${requestBO.registerId}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                        Tên ngân hàng <font style="color: red">*</font>
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="bankName" id="bankName" placeholder="" disabled="true" value="${requestBO.bankName}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                        Số định danh NH <font style="color: red">*</font>
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control bankIdentity number" name="bankIdentity" id="bankIdentity" placeholder="" value="${StringUtils.getBankIdentityNoFF(requestBO.bankIdentity)}" required
                                   require-message="${MessageUtils.getMessage("edit.request.bank.identity.validate.empty")}"
                                   type-message="${MessageUtils.getMessage("bank.identity.validate.unformatted")}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                        IPK <font style="color: red">*</font>
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea rows="3" class="form-control rsa" name="ipk" id="editIpk" 
                                      onclick="backeditDivision()"
                                      onblur="editDivision()" 
                                      onkeyup="countnum()" 
                                      placeholder="" style="resize: vertical" required
                                      require-message="${MessageUtils.getMessage("edit.request.ipk.validate.empty")}"
                                      type-message="${MessageUtils.getMessage("ipk.validate.unformatted")}">${requestBO.ipk}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                        Số serial (Hex) <font style="color: red">*</font>
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" maxlength="6" class="form-control reqSerial character" name="serial" id="serial" placeholder="" value="${requestBO.serial}" required
                                   require-message="${MessageUtils.getMessage("edit.request.serial.validate.empty")}">
                        </div>
                    </div>
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn <font style="color: red">*</font>
                    </label>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="editMonth" class="form-control" name="month" required
                                    require-message="${MessageUtils.getMessage("edit.request.exp.date.month.validate.empty")}">
                                <option value="">Tháng</option>
                                <option value="01" ${requestForm.month eq '01' ? 'selected': ''}>Tháng 1</option>
                                <option value="02" ${requestForm.month eq '02' ? 'selected': ''}>Tháng 2</option>
                                <option value="03" ${requestForm.month eq '03' ? 'selected': ''}>Tháng 3</option>
                                <option value="04" ${requestForm.month eq '04' ? 'selected': ''}>Tháng 4</option>
                                <option value="05" ${requestForm.month eq '05' ? 'selected': ''}>Tháng 5</option>
                                <option value="06" ${requestForm.month eq '06' ? 'selected': ''}>Tháng 6</option>
                                <option value="07" ${requestForm.month eq '07' ? 'selected': ''}>Tháng 7</option>
                                <option value="08" ${requestForm.month eq '08' ? 'selected': ''}>Tháng 8</option>
                                <option value="09" ${requestForm.month eq '09' ? 'selected': ''}>Tháng 9</option>
                                <option value="10" ${requestForm.month eq '10' ? 'selected': ''}>Tháng 10</option>
                                <option value="11" ${requestForm.month eq '11' ? 'selected': ''}>Tháng 11</option>
                                <option value="12" ${requestForm.month eq '12' ? 'selected': ''}>Tháng 12</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="editYear" class="form-control" name="year" required
                                    require-message="${MessageUtils.getMessage("edit.request.exp.date.year.validate.empty")}">
                                <option value="">Năm</option>
                                <c:forEach var="expYear" begin="2018" end="3000" >
                                    <option value="${expYear}" ${year == expYear ? 'selected': ''}>${expYear}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="ipkLengh" style="text-align:right;">
                        Độ dài IPK
                    </label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <div style="float: left;width: 40px">
                                <p style="color: red;font-weight: bold;" id="editIpkLengh" >${requestBO.ipkLengh}</p>
                            </div>
                            <div style="float: left;">
                                <p>(bit)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="appDate" style="text-align:right;">
                        Ngày xin cấp <font style="color: red">*</font>
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control date" name="appDate" id="editAppDate" placeholder="" value="${DateTimeUtils.convertDateToDDMMYYYY(requestBO.appDate)}" required
                                   require-message="${MessageUtils.getMessage("edit.request.app.date.validate.empty")}"
                                   type-message="${MessageUtils.getMessage("date.validate.unformatted")}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:if test="${requestBO.status == 0}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Soạn thảo">
                            </c:if>
                            <c:if test="${requestBO.status == 1}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Trình ký/duyệt">
                            </c:if>
                            <c:if test="${requestBO.status == 2}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Đã ký/duyệt">
                            </c:if>
                            <c:if test="${requestBO.status == 3}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Từ chối">
                            </c:if>
                            <c:if test="${requestBO.status == 4}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Xóa">
                            </c:if>
                            <c:if test="${requestBO.status == 5}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Thu hồi">
                            </c:if>
                            <c:if test="${requestBO.status == 6}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Tạo mới">
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="button" onclick="updatePopupById('${AESUtil.encryption(requestBO.requestId)}')">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="hidden" id="editSubmit" class="btn btn-success" style="width: 150px; visibility: hidden" type="submit"></button>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#editAppDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });
        var split = '${requestBO.expDate}'.split("/");
        $('#editMonth').val(split[0]);
        $('#editYear').val(split[1]);
    });

    var backvalue = "";
    function editDivision() {
        var ipk = $('#editIpk').val();
        backvalue = ipk;
        var length = $('#editIpk').val().length;
        if (length % 2 === 0) {
            $('#editIpkLengh').text(length * 4);
        } else {
            $('#editIpk').val("0" + ipk);
            $('#editIpkLengh').text((length + 1) * 4);
        }
        ;
    }
    function countnum() {
        var length = $('#editIpk').val().length;
        $('#editIpkLengh').text(length * 4);

        ;
    }
    function backeditDivision() {
        if (backvalue != "") {
            $('#editIpk').val(backvalue);
        }
    }
    ;
    $('#updateRegisterId').next('div').addClass("showfull");
</script>