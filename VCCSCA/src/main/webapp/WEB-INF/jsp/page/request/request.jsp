<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<script src="resources/gijgo/js/gijgo.min.js" type="text/javascript"></script>
<link href="resources/gijgo/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<%
    String REQUEST_ADD = (String) request.getSession().getAttribute("REQUEST_ADD");
    String REQUEST_EDIT = (String) request.getSession().getAttribute("REQUEST_EDIT");
    String REQUEST_DELETE = (String) request.getSession().getAttribute("REQUEST_DELETE");
    String REQUEST_IMPORT = (String) request.getSession().getAttribute("REQUEST_IMPORT");
    String REQUEST_SUBMISSION = (String) request.getSession().getAttribute("REQUEST_SUBMISSION");
    String REQUEST_APPROVAL = (String) request.getSession().getAttribute("REQUEST_APPROVAL");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý yêu cầu
                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                        Danh sách yêu cầu
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='requestForm' action="quan-ly-yeu-cau.html" modelAttribute="RequestForm" method='POST'>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                                        Số đăng ký
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" maxlength="8" class="form-control number" name="registerId" value="${requestForm.registerId}" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Tên ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="bankName" class="form-control" data-rel="chosen" name="bin">
                                                                <option value="">Chọn ngân hàng</option>
                                                                <c:forEach items="${listBanks}" var="bankLst">
                                                                    <option value="${bankLst.bin}"  ${requestForm.bin == bankLst.bin ? 'selected': ''}>${bankLst.bankFullName}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                                                        IPK
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control rsa" name="ipk" id="ipk" placeholder="" value="${requestForm.ipk}"
                                                                   type-message="${MessageUtils.getMessage("ipk.validate.unformatted")}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                                                        Số serial
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control reqSerial" maxlength="6" name="serial" id="serial" placeholder="" value="${requestForm.serial}"
                                                                   type-message="${MessageUtils.getMessage("serial.validate.unformatted")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                                                        Số định danh ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="registerId" class="form-control" data-rel="chosen" name="bankIdentity">
                                                            <option value="">Chọn số định danh ngân hàng</option>
                                                            <c:forEach items="${listBanks}" var="bankbo">
                                                                <option value="${bankbo.bin}" ${requestForm.bankIdentity == bankbo.bin ? 'selected': ''}>${bankbo.bin}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                                                        Ngày hết hạn
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="hidden" name="searchMonthHidden" id="searchMonthHidden" style="position: absolute; top: 5px; left: 15px; width: 0px; z-index: -1" required
                                                                   require-message="${MessageUtils.getMessage("search.request.month")}">
                                                            <select id="searchMonth" class="form-control" name="month" onchange="selectExpDate()">
                                                                <option value="">Tháng</option>
                                                                <option value="01" ${requestForm.month eq '01' ? 'selected': ''}>Tháng 1</option>
                                                                <option value="02" ${requestForm.month eq '02' ? 'selected': ''}>Tháng 2</option>
                                                                <option value="03" ${requestForm.month eq '03' ? 'selected': ''}>Tháng 3</option>
                                                                <option value="04" ${requestForm.month eq '04' ? 'selected': ''}>Tháng 4</option>
                                                                <option value="05" ${requestForm.month eq '05' ? 'selected': ''}>Tháng 5</option>
                                                                <option value="06" ${requestForm.month eq '06' ? 'selected': ''}>Tháng 6</option>
                                                                <option value="07" ${requestForm.month eq '07' ? 'selected': ''}>Tháng 7</option>
                                                                <option value="08" ${requestForm.month eq '08' ? 'selected': ''}>Tháng 8</option>
                                                                <option value="09" ${requestForm.month eq '09' ? 'selected': ''}>Tháng 9</option>
                                                                <option value="10" ${requestForm.month eq '10' ? 'selected': ''}>Tháng 10</option>
                                                                <option value="11" ${requestForm.month eq '11' ? 'selected': ''}>Tháng 11</option>
                                                                <option value="12" ${requestForm.month eq '12' ? 'selected': ''}>Tháng 12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="hidden" name="searchYearHidden" id="searchYearHidden" style="position: absolute; top: 5px; left: 15px; width: 0px; z-index: -1" required
                                                                   require-message="${MessageUtils.getMessage("search.request.year")}">
                                                            <select id="searchYear" class="form-control" name="year" onchange="selectExpDate()">
                                                                <option value="">Năm</option>
                                                                <c:forEach var="expYear" begin="2018" end="3000" >
                                                                    <option value="${expYear}" ${requestForm.year == expYear ? 'selected': ''}>${expYear}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="status" class="form-control" name="status">
                                                                <option value="">---Tất cả--- </option>
                                                                <option value="0" ${requestForm.status == 0 ? 'selected': ''}>Soạn thảo</option>
                                                                <option value="1" ${requestForm.status == 1 ? 'selected': ''}>Trình ký/duyệt</option>
                                                                <option value="2" ${requestForm.status == 2 ? 'selected': ''}>Đã ký/duyệt</option>
                                                                <option value="3" ${requestForm.status == 3 ? 'selected': ''}>Từ chối</option>
                                                                <!--<option value="4" ${requestForm.status == 4 ? 'selected': ''}>Xóa</option>-->
                                                                <option value="5" ${requestForm.status == 5 ? 'selected': ''}>Thu hồi</option>
                                                                <!--<option value="6" ${requestForm.status == 6 ? 'selected': ''}>Tạo mới</option>-->
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                                                        Ngày tạo
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control date validate_date_before" name="fromDate" id="fromDate" placeholder="Từ ngày" value="${requestForm.fromDate}"
                                                                   type-message="${MessageUtils.getMessage("date.validate.unformatted")}"
                                                                   type-message-before="${MessageUtils.getMessage("date.validate.before")}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control date validate_date_after" name="toDate" id="toDate" placeholder="Đến ngày" value="${requestForm.toDate}"
                                                                   type-message="${MessageUtils.getMessage("date.validate.unformatted")}"
                                                                   type-message-after="${MessageUtils.getMessage("date.validate.after")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${REQUEST_ADD == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalCreate()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới</button>
                                                            </c:if>
                                                            <c:if test="${REQUEST_IMPORT == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalImport()"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Import</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách yêu cầu đã tạo</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table id="testtable" class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center;width: 10%">Số đăng ký</th>
                                                        <th style="text-align: center">Tên ngân hàng</th>
                                                        <th style="text-align: center">IPK</th>
                                                        <th style="text-align: center">Serial</th>
                                                        <th style="text-align: center">Số định danh ngân hàng</th>
                                                        <th style="text-align: center">Ngày hết hạn</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${requestList}" var="requestBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index + 1}</td>
                                                            <td>${StringUtils.paddingLeft(requestBO.registerId,6)}</td>
                                                            <td>${requestBO.bankName}</td>
                                                            <td><div style = "width:250px; word-wrap: break-word">${StringUtils.displayKey(requestBO.ipk)}</div></td>
                                                            <td>${requestBO.serial}</td>
                                                            <td>${StringUtils.getBankIdentityNoFF(requestBO.bankIdentity)}</td>
                                                            <td>${requestBO.expDate}</td>
                                                            <td>
                                                                <c:if test="${requestBO.status == 0}">
                                                                    Soạn thảo
                                                                </c:if>
                                                                <c:if test="${requestBO.status == 1}">
                                                                    Trình ký/duyệt
                                                                </c:if>
                                                                <c:if test="${requestBO.status == 2}">
                                                                    Đã ký/duyệt
                                                                </c:if>
                                                                <c:if test="${requestBO.status == 3}">
                                                                    Từ chối
                                                                </c:if>
                                                                <c:if test="${requestBO.status == 4}">
                                                                    Xóa
                                                                </c:if>
                                                                <c:if test="${requestBO.status == 5}">
                                                                    Thu hồi
                                                                </c:if>
                                                                <c:if test="${requestBO.status == 6}">
                                                                    Tạo mới
                                                                </c:if>
                                                            </td>
                                                            <td>
                                                    <center>
                                                        <c:if test="${requestBO.status != 4}">
                                                            <c:if test="${requestBO.status != 1}">
                                                                <a href="#" onclick="openModalDetail('${AESUtil.encryption(requestBO.requestId)}');"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                            </c:if>
                                                            <c:if test="${requestBO.status == 1 && REQUEST_APPROVAL == 'true'}">
                                                                <a href="#" onclick="openModalApprove('${AESUtil.encryption(requestBO.requestId)}');"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                            </c:if>
                                                            <c:if test="${requestBO.status != 1 && requestBO.status != 2}">
                                                                <c:if test="${requestBO.status != 5 && REQUEST_EDIT == 'true'}">
                                                                    <a href="#" onclick="openModalEdit('${AESUtil.encryption(requestBO.requestId)}');"
                                                                       data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                                </c:if>
                                                                <c:if test="${requestBO.status != 5 && REQUEST_DELETE == 'true'}">
                                                                    <a href="#" onclick="openDeletePopup('${AESUtil.encryption(requestBO.requestId)}');"
                                                                       data-toggle="tooltip" data-placement="bottom" title="Xóa"><span class="glyphicon glyphicon-remove-sign"/></a>
                                                                </c:if>
                                                            </c:if>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Create request -->
<div id="createRequest" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="createRequestId" class="modal-dialog modal-lg">
        <form id="createRq" name='createRequestForm' action="createRequest.do" modelAttribute="CreateRequestForm" method='POST' accept-charset="UTF-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="panel-title text-left">Thêm mới</h1>
                </div>
                <div class="panel-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <h1 class="panel-title text-left">Thông tin chung</h1>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                Số đăng ký <font style="color: red">*</font>
                            </label>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="registerIdError" id="registerIdError" style="position: absolute; top: 5px; left: 50px; width: 0px; z-index: -1" required
                                           require-message="${MessageUtils.getMessage("create.request.register.id.validate.empty")}">
                                    <select id="createRegisterId" class="form-control" onchange="fillCreateData()" data-rel="chosen" name="registerId">
                                        <option value="">Chọn số đăng ký</option>
                                        <c:forEach items="${requestFileList}" var="regLst">
                                            <option value="${regLst.registerId}" ${requestForm.registerId == regLst.registerId ? 'selected': ''}>${regLst.registerId}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="showHide" hidden="true">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                    Tên ngân hàng <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select id="createBankName" class="form-control" name="bankName" disabled="true" required
                                                require-message="${MessageUtils.getMessage("create.request.bank.name.validate.empty")}">
                                            <option value=""></option>
                                            <c:forEach items="${requestFileList}" var="regLst">
                                                <option value="${regLst.registerId}"  ${requestForm.registerId == regLst.registerId ? 'selected': ''}>${regLst.bankName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                                    Số định danh NH <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" maxlength="8" class="form-control bankIdentity number" name="bankIdentity" id="createBankIdentity" placeholder="" required
                                               onblur="upCase(this);paddF(this, 8)" onclick="loadbaseF(this)"
                                               require-message="${MessageUtils.getMessage("create.request.bank.identity.validate.empty")}"
                                               >

                                        <c:forEach items="${requestFileList}" var="regLst">
                                            <input type="hidden" id="bintoiif_${regLst.registerId}" value="${regLst.bin}" />
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                                    IPK <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <textarea rows="3" maxlength="2000" class="form-control rsa" name="ipk" id="createIpk" placeholder="" onkeyup="createDivision()" onblur="createDivision()" style="resize: none" required
                                                  require-message="${MessageUtils.getMessage("create.request.ipk.validate.empty")}"
                                                  type-message="${MessageUtils.getMessage("ipk.validate.unformatted")}"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="serial" style="text-align:right;">
                                    Số serial (Hex) <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" maxlength="6" class="form-control reqSerial character" name="serial" id="serial" value="${serialHex}" placeholder="" required
                                               onblur="upCase(this)" pattern="[A-Za-z0-9]+"
                                               require-message="${MessageUtils.getMessage("create.request.serial.validate.empty")}">
                                    </div>
                                </div>
                                <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                                    Ngày hết hạn <font style="color: red">*</font>
                                </label>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select id="createMonth" class="form-control" name="month" required
                                                require-message="${MessageUtils.getMessage("create.request.exp.date.month.validate.empty")}">
                                            <option value="">Tháng</option>
                                            <option value="01">Tháng 1</option>
                                            <option value="02">Tháng 2</option>
                                            <option value="03">Tháng 3</option>
                                            <option value="04">Tháng 4</option>
                                            <option value="05">Tháng 5</option>
                                            <option value="06">Tháng 6</option>
                                            <option value="07">Tháng 7</option>
                                            <option value="08">Tháng 8</option>
                                            <option value="09">Tháng 9</option>
                                            <option value="10">Tháng 10</option>
                                            <option value="11">Tháng 11</option>
                                            <option value="12">Tháng 12</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select id="createYear" class="form-control" name="year" required
                                                require-message="${MessageUtils.getMessage("create.request.exp.date.year.validate.empty")}">
                                            <option value="">Năm</option>
                                            <c:forEach var="expYear" begin="2018" end="3000" >
                                                <option value="${expYear}">${expYear}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="ipkLengh" style="text-align:right;">
                                    Độ dài IPK
                                </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div style="float: left;width: 40px">
                                            <p style="color: red;font-weight: bold;" id="createIpkLengh"/>
                                        </div>
                                        <div style="float: left;">
                                            <p>(bit)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="appDate" style="text-align:right;">
                                    Ngày xin cấp <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control date" name="appDate" id="createAppDate" placeholder="dd/MM/yyyy" required
                                               require-message="${MessageUtils.getMessage("create.request.app.date.validate.empty")}"
                                               type-message="${MessageUtils.getMessage("date.validate.unformatted")}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="control-label col-md-4" for="status" style="text-align:right;">
                                    Trạng thái
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select id="status" class="form-control" name="status" disabled="true">
                                            <option value="0">Soạn thảo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="form-group row">
                        <div class="form-control-lg">
                            <div class="col-sm-12">
                                <center>
                                    <button class="btn btn-success" style="width: 150px;" type="button" onclick="createPopup()">Lưu</button>
                                    <button class="btn btn-default" style="width: 150px;" data-dismiss="modal" type="button" onclick="clearPopup()">Đóng</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="hidden" id="createSubmit" class="btn btn-success" style="width: 150px; visibility: hidden" type="submit"></button>
        </form>
        <script>
            $(document).ready(function () {
                $('#createAppDate').datepicker({
                    uiLibrary: 'bootstrap',
                    format: 'dd/mm/yyyy',
                    maxDate: function () {
                        return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                    }
                });
            });
            function fillCreateData() {
                if ($('#createRegisterId').val() !== "") {
                    $('#showHide').prop("hidden", false);
                    $('#registerIdError').attr("type", "hidden");
                    $('#createBankName').val($('#createRegisterId').val());
                    var id = $('#createRegisterId').val();
                    $('#createBankIdentity').val($("#bintoiif_" + id).val());
                    $('#registerIdError').val($('#createRegisterId').val());
                    $('#createBankIdentity').focus();
                } else {
                    $('#showHide').prop("hidden", true);
                    $('#registerIdError').attr("type", "text");
                    $('#createBankName').val("");
                    $('#registerIdError').val("");
                }
            }
            ;
            function createDivision() {
                var length = $('#createIpk').val().length;
                if (length % 2 === 0) {
                    $('#createIpkLengh').text(length * 4);
                } else {
                    $('#createIpkLengh').text((length + 1) * 4);
                }
                ;
            }
            ;
        </script>
    </div>
</div>

<!-- View detail request -->
<div id="viewDetail" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="viewDetailId" class="modal-dialog modal-lg">
        <!--<div class="modal-content">-->

        <!--</div>-->
    </div>
</div>

<!-- Edit request -->
<div id="editRequest" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="editRequestId" class="modal-dialog modal-lg">
        <!--<div class="modal-content">-->

        <!--</div>-->
    </div>
</div>

<!-- Import request -->
<div id="importRequest" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="importRequestId" class="modal-dialog modal-lg">
        <!--<div class="modal-content">-->

        <!--</div>-->
    </div>
</div>

<!-- Approve request -->
<div id="approveRequest" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="approveRequestId" class="modal-dialog modal-lg">
        <!--<div class="modal-content">-->

        <!--</div>-->
    </div>
</div>

<!-- Alert create modal -->
<div class="modal fade" id="alertCreate" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn các thông tin đăng ký là chính xác?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-primary" onclick="submitCreateForm()">Thêm mới</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert update modal -->
<div class="modal fade" id="alertUpdateById" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn các thông tin cập nhật là chính xác?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" id="buttonUpdateById" class="btn btn-primary" onclick="submitUpdateForm()">Cập nhật</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert delete modal -->
<div class="modal fade" id="alertDelete" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn xóa yêu cầu này?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteRequest.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="paramId" id="paramId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert submission modal -->
<div class="modal fade" id="alertSubmission" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn trình ký yêu cầu này?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="submissionRequest.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="paramId" id="paramId"/>
                        <button type="submit" class="btn btn-primary" >Trình ký</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert approve modal -->
<div class="modal fade" id="alertApprove" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn duyệt yêu cầu này?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <!--<form action="approvalRequest.do" method='POST' accept-charset="UTF-8">-->

                    <button type="button" class="btn btn-primary" onclick="smformduyet()" >Duyệt</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <!--</form>-->
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert reject modal -->
<div class="modal fade" id="alertReject" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn từ chối yêu cầu này?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <!--<form action="rejectRequest.do" method='POST' accept-charset="UTF-8">-->

                    <button type="button" class="btn btn-primary" onclick="smformtuchoi()" >Từ chối</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <!--</form>-->
                </center>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    // this is the id of the form
    function smImport() {
        var form = $("#idForm");
        var url = form.attr('action');
        jQuery.ajax({
            type: 'POST',
            url: url,
            data: new FormData($("#idForm")[0]),
            processData: false,
            contentType: false,
            success: function (data) {
                $("#importRequestId").html(data);
            }
        });
    }
    ;

    $(document).ready(function () {

        $('#fromDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });
        $('#toDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });

    });

    function clearPopup() {
        $('#registerIdError').val("");
         $('#registerIdError').attr("type", "text");
        $('#createRegisterId').val("");
        $('#createRegisterId').trigger("chosen:updated");
        $('#createBankIdentity').val("");
        $('#createIpk').val("");
        $('#createMonth').val("");
        $('#createYear').val("");
        $('#createIpkLengh').val("");
        $('#createAppDate').val("");
    }
    ;

    function selectExpDate() {
        if ($('#searchMonth').val() !== "" && $('#searchYear').val() === "") {
            $('#searchMonthHidden').attr("type", "hidden");
            $('#searchYearHidden').attr("type", "text");
        }
        if ($('#searchYear').val() !== "" && $('#searchMonth').val() === "") {
            $('#searchYearHidden').attr("type", "hidden");
            $('#searchMonthHidden').attr("type", "text");
        }
        if (($('#searchMonth').val() === "" && $('#searchYear').val() === "")
                || ($('#searchMonth').val() !== "" && $('#searchYear').val() !== "")) {
            $('#searchMonthHidden').attr("type", "hidden");
            $('#searchYearHidden').attr("type", "hidden");
        }
    }
    ;

    function createPopup() {
        $('#alertCreate').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function submitCreateForm() {
        $('#alertCreate').modal('hide');
        $('#createSubmit').trigger("click");
    }
    ;

    function updatePopupById(requestId) {
        $('#alertUpdateById').modal({backdrop: 'static', keyboard: false});//.modal('show')

    }
    ;
//    function submitUpdateFormById() {
//        $('#alertUpdateById').modal('hide');
//        $('#editSubmit').trigger("click");
//    }
//    ;

    function submitUpdateForm() {
        $('#alertUpdate').modal('hide');
        $('#editSubmit').trigger("click");
    }
    ;

    function openModalCreate() {
        if ($('#createRegisterId').val() !== "") {
            $('#showHide').prop("hidden", false);
        } else {
            $('#showHide').prop("hidden", true);
        }
        $('#createRequest').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#registerIdError').val("");
        $('#registerIdError').attr("type", "text");
        $('#createRegisterId').next('div').addClass("showfull");
    }
    ;
    function openModalDetail(requestId) {
        $.get("xem-chi-tiet-yeu-cau.html", {requestId: requestId}, function (data) {
            $("#viewDetailId").html(data);
            $('#viewDetail').modal({backdrop: 'static', keyboard: false});//.modal('show')
//            $('#btnEditFView').attr("offsetE", idbtnEdit);
        });
    }
    ;
//    function openModalEdit(offset) {
//        $('#viewDetail').modal('hide');
//
//        $("form").each(function () {
//            if ($(this).attr("def-class") == "form-hidden") {
//                $(this).css("display", "none");
//            }
//        });
//
//        $('#editRequest').modal({backdrop: 'static', keyboard: false});//.modal('show')
//        $('#editRq_' + offset).css("display", "block");
//
//        $("select").each(function () {
//            if ($(this).attr("data-rel") == "chosen") {
//                $(this).next('div').addClass("showfull");
//            }
//        });
//    }
//    ;

    function openModalEdit(requestId) {
        $('#viewDetail').modal('hide');
        $.get("sua-yeu-cau.html", {requestId: requestId}, function (data) {
            $("#editRequestId").html(data);
            $('#editRequest').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalEditFV() {
        $('#viewDetail').modal('hide');

        $("form").each(function () {
            if ($(this).attr("def-class") == "form-hidden") {
                $(this).css("display", "none");
            }
        });

        $('#editRequest').modal({backdrop: 'static', keyboard: false});//.modal('show')
        var offsetFV = $("#btnEditFView").attr("offsetE");
        $('#editRq_' + offsetFV).css("display", "block");

        $("select").each(function () {
            if ($(this).attr("data-rel") == "chosen") {
                $(this).next('div').addClass("showfull");
            }
        });
    }
    ;

    function openModalImport() {
        $.get("tai-yeu-cau.html", {requestId: "requestId"}, function (data) {
            $("#importRequestId").html(data);
            $('#importRequest').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalApprove(requestId) {
        $('#viewDetail').modal('hide');
        $.get("ky-duyet-yeu-cau.html", {requestId: requestId}, function (data) {
            $("#approveRequestId").html(data);
            $('#approveRequest').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openDeletePopup(requestId) {
        $('#viewDetail').modal('hide');
        $('#alertDelete').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#paramId').val(requestId);
        $('#paramContent').val($('#content').val());
    }
    ;
    function openSubmissionPopup(requestId) {
        $('#viewDetail').modal('hide');
        $('#alertSubmission').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#paramId').val(requestId);
    }
    ;
    function openApprovePopup() {
        $('#viewDetail').modal('hide');
        $('#alertApprove').modal({backdrop: 'static', keyboard: false});//.modal('show')
//        $('#paramId').val(requestId);
//        $('#paramIndex').val($('#index').val());
    }
    ;
    function smformduyet() {
        $('#alertApprove').modal('hide');
        $('#btnsm').trigger("click");

    }
    ;
    function smformtuchoi() {
        $('#alertReject').modal('hide');
        $('#btnsmReject').trigger("click");
    }
    ;
    function openRejectPopup() {
        $('#viewDetail').modal('hide');
        $('#alertReject').modal({backdrop: 'static', keyboard: false});//.modal('show')

    }
    ;


</script>