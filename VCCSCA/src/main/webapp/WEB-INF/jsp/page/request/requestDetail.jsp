<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<form name='requestDetailForm' action="xem-chi-tiet-yeu-cau.html" modelAttribute="RequestDetailForm" method='POST'>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Xem</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                        Số đăng ký
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="registerId" placeholder="" disabled="true" value="${requestBO.registerId}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                        Tên ngân hàng
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="bankName" placeholder="" disabled="true" value="${requestBO.bankName}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                        Số định danh NH
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="bankIdentity" placeholder="" disabled="true" value="${StringUtils.getBankIdentityNoFF(requestBO.bankIdentity)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                        IPK
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea rows="3" class="form-control" id="ipk" placeholder="" disabled="true" style="resize: none">${requestBO.ipk}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                        Số serial (Hex)
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="serial" placeholder="" disabled="true" value="${requestBO.serial}">
                        </div>
                    </div>
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="expDate" placeholder="" disabled="true" value="${requestBO.expDate}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="ipkLengh" style="text-align:right;">
                        Độ dài IPK
                    </label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <div style="float: left;width: 40px">
                                <p style="color: red;font-weight: bold;" id="createIpkLengh">${requestBO.ipkLengh}</p>
                            </div>
                            <div style="float: left;">
                                <p>(bit)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="appDate" style="text-align:right;">
                        Ngày xin cấp
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="appDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYY(requestBO.appDate)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:if test="${requestBO.status == 0}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Soạn thảo">
                            </c:if>
                            <c:if test="${requestBO.status == 1}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Trình ký/duyệt">
                            </c:if>
                            <c:if test="${requestBO.status == 2}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Đã ký/duyệt">
                            </c:if>
                            <c:if test="${requestBO.status == 3}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Từ chối">
                            </c:if>
                            <c:if test="${requestBO.status == 4}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Xóa">
                            </c:if>
                            <c:if test="${requestBO.status == 5}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Thu hồi">
                            </c:if>
                            <c:if test="${requestBO.status == 6}">
                                <input type="text" class="form-control" id="status" placeholder="" disabled="true" value="Tạo mới">
                            </c:if>
                        </div>
                    </div>
                </div>
                <c:if test="${requestBO.status == 3}">
                    <div class="col-lg-6">
                        <label class="control-label col-md-4" for="status" style="text-align:right;">
                            Lý do từ chối
                        </label>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea readonly="true" class="form-control" 
                                          >${requestBO.content}</textarea>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin khác</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createUser" style="text-align:right;">
                        Người tạo
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="createUser" placeholder="" disabled="true" value="${requestBO.createUser}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                        Ngày tạo
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="createDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(requestBO.createDate)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateUser" style="text-align:right;">
                        Người cập nhật
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="updateUser" placeholder="" disabled="true" value="${requestBO.updateUser}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                        Ngày cập nhật
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="updateDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(requestBO.updateDate)}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <c:if test="${requestBO.status != 4 && requestBO.status != 2}">
                                <c:if test="${REQUEST_SUBMISSION == 'true' && requestBO.status == 0}">
                                    <button class="btn btn-success" style="width: 150px;" type="button" onclick="openSubmissionPopup('${AESUtil.encryption(requestBO.requestId)}');">Trình ký</button>
                                </c:if>
                                <c:if test="${requestBO.status != 1 && requestBO.status != 2}">
                                    <c:if test="${requestBO.status != 5 && REQUEST_EDIT == 'true'}">
                                        <button id="btnEditFView" class="btn btn-success" style="width: 150px;" type="button" offsetE onclick="openModalEdit('${AESUtil.encryption(requestBO.requestId)}');">Sửa</button>
                                    </c:if>
                                    <c:if test="${requestBO.status != 5 && REQUEST_DELETE == 'true'}">
                                        <button class="btn btn-danger" style="width: 150px;" type="button" onclick="openDeletePopup('${AESUtil.encryption(requestBO.requestId)}');">Xóa</button>
                                    </c:if>
                                </c:if>
                            </c:if>
                            <button class="btn btn-default" style="width: 150px;" data-dismiss="modal" type="button">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>