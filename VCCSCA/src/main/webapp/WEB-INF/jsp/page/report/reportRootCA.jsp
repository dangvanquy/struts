<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>

<%
    String REPORT_EXPORT_ROOTCA = (String) request.getSession().getAttribute("REPORT_EXPORT_ROOTCA");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo Root CA
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="rootCAForm" name='rootCAForm' modelAttribute="rootCAForm" action="bao-cao-root-ca.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-5">
                                                    <label class="control-label col-md-3" for="rsaIndex" style="text-align:right;">
                                                        Index
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="rsaIndex" class="form-control" name="rsaIndex">
                                                            <option value="">Tất cả</option>
                                                            <c:forEach items="${allKeysList}" var="allKeyBO">
                                                                <option value="${allKeyBO}" ${rootCAForm.rsaIndex==allKeyBO ? "selected" : ""} >${StringUtils.paddingIndex(allKeyBO)}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-7">
                                                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                                                        Ngày hết hạn
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <select id="month" class="form-control" name="month">
                                                            <option value="">---Chọn tháng--- </option>
                                                            <option value="01" ${rootCAForm.month eq '01' ? 'selected': ''}>Tháng 1</option>
                                                            <option value="02" ${rootCAForm.month eq '02' ? 'selected': ''}>Tháng 2</option>
                                                            <option value="03" ${rootCAForm.month eq '03' ? 'selected': ''}>Tháng 3</option>
                                                            <option value="04" ${rootCAForm.month eq '04' ? 'selected': ''}>Tháng 4</option>
                                                            <option value="05" ${rootCAForm.month eq '05' ? 'selected': ''}>Tháng 5</option>
                                                            <option value="06" ${rootCAForm.month eq '06' ? 'selected': ''}>Tháng 6</option>
                                                            <option value="07" ${rootCAForm.month eq '07' ? 'selected': ''}>Tháng 7</option>
                                                            <option value="08" ${rootCAForm.month eq '08' ? 'selected': ''}>Tháng 8</option>
                                                            <option value="09" ${rootCAForm.month eq '09' ? 'selected': ''}>Tháng 9</option>
                                                            <option value="10" ${rootCAForm.month eq '10' ? 'selected': ''}>Tháng 10</option>
                                                            <option value="11" ${rootCAForm.month eq '11' ? 'selected': ''}>Tháng 11</option>
                                                            <option value="12" ${rootCAForm.month eq '12' ? 'selected': ''}>Tháng 12</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <select id="year" class="form-control" name="year">
                                                            <option value="">---Chọn năm--- </option>
                                                            <c:forEach var="expYear" begin="2018" end="3000" >
                                                                <option value="${expYear}" ${rootCAForm.year eq expYear ? 'selected' : ''}>${expYear}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-5">
                                                    <label class="control-label col-md-3" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="rsaStatus">
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${rootCAForm.rsaStatus == 0 ? 'selected' : ''} >Hoạt động</option>
                                                            <option value="1" ${rootCAForm.rsaStatus == 1 ? 'selected' : ''} >Ngừng hoạt động</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit" formaction="bao-cao-root-ca.html"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${REPORT_EXPORT_ROOTCA == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="submitExport('rootCAForm', 'exportRootCAReport.do')"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null && errMessage != ''}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách Root CA</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">CA IPK Index</th>
                                                        <th style="text-align: center">Ngày hết hạn</th>
                                                        <th style="text-align: center">Tình trạng hoạt động</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${keysList}" var="rsaKeyBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${itr.index + 1}</td>
                                                            <td>${StringUtils.paddingIndex(rsaKeyBO.rsaIndex)}</td>
                                                            <td>${rsaKeyBO.expirationDate==null?"":StringUtils.padding(rsaKeyBO.expirationDate, 7)}</td>
                                                            <td>${rsaKeyBO.rsaStatus==0?'Hoạt động':'Ngừng hoạt động'}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>