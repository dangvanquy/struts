<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String REPORT_EXPORT_NHTV = (String) request.getSession().getAttribute("REPORT_EXPORT_NHTV");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo ngân hàng thành viên
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="reportMembershipBankingForm" name='reportMembershipBankingForm' modelAttribute="reportMembershipBankingForm" action="bao-cao-nhtv.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankId" style="text-align:right;">
                                                        Ngân hàng
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="bin" class="form-control mdb-select md-form colorful-select dropdown-primary" name="arrBin" data-rel="chosen" multiple="multiple">
                                                            <c:forEach items="${allBanks}" var="bankMembershipBO">
                                                                <option style="height: 38px" value="${bankMembershipBO.bin}" ${fn:contains(lstSelectedBin,  bankMembershipBO.getBin2())?'selected':''} >${bankMembershipBO.bankFullName}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bin" style="text-align:right;">
                                                        Số BIN
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input id="bin" name="bin" type="text" class="form-control number"  placeholder="" value="${bankMembershipForm.bin}" maxlength="9">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="status">
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${reportMembershipBankingForm.status == 0 ? 'selected' : ''} >Hoạt động</option>
                                                            <option value="1" ${reportMembershipBankingForm.status == 1 ? 'selected' : ''}>Không hoạt động</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit" formaction="bao-cao-nhtv.html"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${REPORT_EXPORT_NHTV == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="submitExport('reportMembershipBankingForm', 'exportNhtvReport.do');"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách ngân hàng thành viên</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên ngân hàng</th>
                                                        <th style="text-align: center">Số BIN</th>
                                                        <th style="text-align: center">Tình trạng hoạt động</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${banksList}" var="bankMembershipBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${itr.index + 1}</td>
                                                            <td>${bankMembershipBO.bankFullName }</td>
                                                            <td>${bankMembershipBO.bin }</td>
                                                            <td>${bankMembershipBO.status==0?'Hoạt động':'Không hoạt động'}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("keypress", "#bin", function (e) {
        var a = [];
        var k = e.which;
        a.push(8);
        a.push(46);
        for (i = 48; i < 58; i++) {
            a.push(i);
        }
        if (!(a.indexOf(k) >= 0)) {
            e.preventDefault();
        }
    });
</script>