<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<%
    String REPORT_EXPORT_CERT = (String) request.getSession().getAttribute("REPORT_EXPORT_CERT");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo chứng thư số
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="certificateForm" name='certificateForm' modelAttribute="certificateForm" action="bao-cao-chung-thu-so.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-5">
                                                    <label class="control-label col-md-4" for="rsaIndex" style="text-align:right;">
                                                        Index
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="rsaIndex" class="form-control" name="rsaIndex">
                                                            <option value="">Tất cả</option>
                                                            <c:forEach items="${indexLst}" var="index">
                                                                <option value="${index}" ${certificateForm.rsaIndex==index ? "selected" : ""} >${StringUtils.paddingIndex(index)}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7">
                                                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                                                        Ngày hết hạn
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <select id="monthCategory" class="form-control" name="month">
                                                            <option value="">---Chọn tháng--- </option>
                                                            <option value="01" ${certificateForm.month eq '01' ? 'selected': ''}>Tháng 1</option>
                                                            <option value="02" ${certificateForm.month eq '02' ? 'selected': ''}>Tháng 2</option>
                                                            <option value="03" ${certificateForm.month eq '03' ? 'selected': ''}>Tháng 3</option>
                                                            <option value="04" ${certificateForm.month eq '04' ? 'selected': ''}>Tháng 4</option>
                                                            <option value="05" ${certificateForm.month eq '05' ? 'selected': ''}>Tháng 5</option>
                                                            <option value="06" ${certificateForm.month eq '06' ? 'selected': ''}>Tháng 6</option>
                                                            <option value="07" ${certificateForm.month eq '07' ? 'selected': ''}>Tháng 7</option>
                                                            <option value="08" ${certificateForm.month eq '08' ? 'selected': ''}>Tháng 8</option>
                                                            <option value="09" ${certificateForm.month eq '09' ? 'selected': ''}>Tháng 9</option>
                                                            <option value="10" ${certificateForm.month eq '10' ? 'selected': ''}>Tháng 10</option>
                                                            <option value="11" ${certificateForm.month eq '11' ? 'selected': ''}>Tháng 11</option>
                                                            <option value="12" ${certificateForm.month eq '12' ? 'selected': ''}>Tháng 12</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <select id="yearCategory" class="form-control" name="year">
                                                            <option value="">---Chọn năm--- </option>
                                                            <c:forEach var="expYear" begin="2018" end="3000" >
                                                                <option value="${expYear}" ${certificateForm.year eq expYear ? 'selected' : ''}>${expYear}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-5">
                                                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                                                        Số serial
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input type="text" maxlength="6" class="form-control rsa" onblur="upCase(this)" id="serial" name="serial" value="${certificateForm.serial}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-7">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Ngân hàng
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="bankName" class="form-control mdb-select md-form colorful-select dropdown-primary" name="arrBin" data-rel="chosen" multiple="multiple">
                                                            <option value="">---Chọn ngân hàng--- </option>
                                                            <c:forEach items="${banksList}" var="bankMembershipBO">
                                                                <!--<option value="${bankMembershipBO.bin}" ${certificateForm.bin==bankMembershipBO.bin ? "selected" : ""}>${bankMembershipBO.bankFullName}</option>-->
                                                                <option style="height: 38px" value="${bankMembershipBO.bin}" ${fn:contains(lstSelectedBin,  bankMembershipBO.getBin2())?'selected':''} >${bankMembershipBO.bankFullName}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-5">
                                                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                                        Số yêu cầu
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input class="form-control number" maxlength="8" id="registerId" name="registerId" value="${certificateForm.registerId}" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-lg-7">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="status">
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${certificateForm.status==0 ? "selected" : ""}>Đã ký/duyệt</option>
                                                            <option value="1" ${certificateForm.status==1 ? "selected" : ""}>Thu hồi</option>
                                                            <option value="2" ${certificateForm.status==2 ? "selected" : ""}>Hết hạn</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit" formaction="bao-cao-chung-thu-so.html"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${REPORT_EXPORT_CERT == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="submitExport('certificateForm', 'exportCertReport.do')"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null && errMessage != ''}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách chứng thư số</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">CA Index</th>
                                                        <th style="text-align: center">Số serial</th>
                                                        <th style="text-align: center">Ngày hết hạn</th>
                                                        <th style="text-align: center">Cấp cho ngân hàng</th>
                                                        <th style="text-align: center">Số yêu cầu cấp</th>
                                                        <th style="text-align: center">Tình trạng hoạt động</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${certificatesList}" var="certificateBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${itr.index + 1}</td>
                                                            <td>${StringUtils.paddingIndex(certificateBO.rsaIndex)}</td>
                                                            <td>${certificateBO.serial }</td>
                                                            <td>${certificateBO.expDate }</td>
                                                            <td>${certificateBO.bankName }</td>
                                                            <td>${certificateBO.registerId }</td>
                                                            <td>
                                                                <c:if test="${certificateBO.status == 0}">
                                                                    Đã ký/duyệt
                                                                </c:if>
                                                                <c:if test="${certificateBO.status == 1}">
                                                                    Thu hồi
                                                                </c:if>
                                                                <c:if test="${certificateBO.status == 2}">
                                                                    Hết hạn
                                                                </c:if>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>