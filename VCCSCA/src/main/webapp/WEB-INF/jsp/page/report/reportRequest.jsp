<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<script src="resources/gijgo/js/gijgo.min.js" type="text/javascript"></script>
<link href="resources/gijgo/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<%
    String REPORT_EXPORT_REQUEST = (String) request.getSession().getAttribute("REPORT_EXPORT_REQUEST");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Báo cáo yêu cầu cấp Chứng thư số
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="requestForm" name='requestForm' modelAttribute="requestForm" action="bao-cao-yeu-cau-cap-cts.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                                                        Số đăng ký
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input type="text"  maxlength="8" class="form-control number" name="registerId" id="registerId" value="${requestForm.registerId}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Ngân hàng
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="bin" class="form-control mdb-select md-form colorful-select dropdown-primary" name="arrBin" data-rel="chosen" multiple="multiple">
                                                            <c:forEach items="${banksList}" var="bankMembershipBO">
                                                                <option style="height: 38px" value="${bankMembershipBO.bin}" ${fn:contains(lstSelectedBin,  bankMembershipBO.getBin2())?'selected':''} >${bankMembershipBO.bankFullName}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="appDate" style="text-align:right;">
                                                        Ngày xin cấp
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input id="appDate" type="text" class="form-control date" name="appDateSearch" value="${requestForm.appDateSearch}"
                                                               type-message="${MessageUtils.getMessage("date.validate.unformatted")}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit" formaction="bao-cao-yeu-cau-cap-cts.html"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${REPORT_EXPORT_REQUEST == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="submitExport('requestForm', 'exportRequestReport.do')"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách yêu cầu cấp chứng thư số</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Số đăng ký</th>
                                                        <th style="text-align: center">Số BIN</th>
                                                        <th style="text-align: center">Ngân hàng thành viên</th>
                                                        <th style="text-align: center">Ngày xin cấp</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${requestsList}" var="requestBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${itr.index + 1}</td>
                                                            <td style="text-align: right">${requestBO.registerId }</td>
                                                            <td style="text-align: right">${requestBO.bin }</td>
                                                            <td>${requestBO.bankName }</td>
                                                            <td style="text-align: center"><fmt:formatDate pattern = "dd/MM/yyyy" value = "${requestBO.appDate}" /></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#appDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy'
        });
    });
</script>