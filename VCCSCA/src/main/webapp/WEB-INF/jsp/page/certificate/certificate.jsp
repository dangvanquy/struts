<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<script src="resources/gijgo/js/gijgo.min.js" type="text/javascript"></script>
<link href="resources/gijgo/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<%
    String CERTIFICATE_EXPORT = (String) request.getSession().getAttribute("CERTIFICATE_EXPORT");
    String CERTIFICATE_RECALL = (String) request.getSession().getAttribute("CERTIFICATE_RECALL");
    String CERTIFICATE_RESTORE = (String) request.getSession().getAttribute("CERTIFICATE_RESTORE");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý chứng thư số
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Danh sách chứng thư số
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="certificateForm" name='certificateForm' action="quan-ly-chung-thu-so.html" modelAttribute="CertificateForm" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Tên ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="bankName" class="form-control" data-rel="chosen" name="bin">
                                                                <option value="">---Chọn ngân hàng---</option>
                                                                <c:forEach items="${bankList}" var="bankList">
                                                                    <option value="${bankList.bin}" ${certificateForm.bin == bankList.bin ? 'selected': ''}>${bankList.bankFullName}</option>
                                                                </c:forEach>
                                                            </select>
                                                            <input id="binSearch" type="hidden" name="binSearch">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                                                        Số serial
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control cerSerial" name="serial" id="serial" placeholder="" value="${certificateForm.serial}"
                                                                   type-message="${MessageUtils.getMessage("serial.validate.unformatted")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                                                        Số định danh ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select id="registerId" class="form-control" data-rel="chosen" name="bankIdentity">
                                                            <option value="">Chọn số định danh ngân hàng</option>
                                                            <c:forEach items="${bankList}" var="bankbo">
                                                                <option value="${StringUtils.getBankIdentity(bankbo.bin)}" ${StringUtils.getBankIdentity(certificateForm.bankIdentity) == StringUtils.getBankIdentity(bankbo.bin) ? 'selected': ''}>${bankbo.bin}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                                                        Ngày hết hạn
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="hidden" name="searchMonthHidden" id="searchMonthHidden" style="position: absolute; top: 5px; left: 15px; width: 0px; z-index: -1" required
                                                                   require-message="${MessageUtils.getMessage("search.request.month")}">
                                                            <select id="searchMonth" class="form-control" onchange="selectExpDate()" name="month">
                                                                <option value="">Chọn tháng</option>
                                                                <option value="01" ${certificateForm.month eq '01' ? 'selected': ''}>Tháng 1</option>
                                                                <option value="02" ${certificateForm.month eq '02' ? 'selected': ''}>Tháng 2</option>
                                                                <option value="03" ${certificateForm.month eq '03' ? 'selected': ''}>Tháng 3</option>
                                                                <option value="04" ${certificateForm.month eq '04' ? 'selected': ''}>Tháng 4</option>
                                                                <option value="05" ${certificateForm.month eq '05' ? 'selected': ''}>Tháng 5</option>
                                                                <option value="06" ${certificateForm.month eq '06' ? 'selected': ''}>Tháng 6</option>
                                                                <option value="07" ${certificateForm.month eq '07' ? 'selected': ''}>Tháng 7</option>
                                                                <option value="08" ${certificateForm.month eq '08' ? 'selected': ''}>Tháng 8</option>
                                                                <option value="09" ${certificateForm.month eq '09' ? 'selected': ''}>Tháng 9</option>
                                                                <option value="10" ${certificateForm.month eq '10' ? 'selected': ''}>Tháng 10</option>
                                                                <option value="11" ${certificateForm.month eq '11' ? 'selected': ''}>Tháng 11</option>
                                                                <option value="12" ${certificateForm.month eq '12' ? 'selected': ''}>Tháng 12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="hidden" name="searchYearHidden" id="searchYearHidden" style="position: absolute; top: 5px; left: 15px; width: 0px; z-index: -1" required
                                                                   require-message="${MessageUtils.getMessage("search.request.year")}">
                                                            <select id="searchYear" class="form-control" onchange="selectExpDate()" name="year">
                                                                <option value="">Chọn năm</option>
                                                                <c:forEach var="expYear" begin="2018" end="3000" >
                                                                    <option value="${expYear}" ${certificateForm.year == expYear ? 'selected': ''}>${expYear}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="status" class="form-control" name="status">
                                                                <option value="">---Tất cả---</option>
                                                                <option value="0" ${certificateForm.status eq '0' ? 'selected': ''}>Đã ký/duyệt</option>
                                                                <option value="1" ${certificateForm.status eq '1' ? 'selected': ''}>Thu hồi</option>
                                                                <option value="2" ${certificateForm.status eq '2' ? 'selected': ''}>Hết hạn</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                                                        Ngày tạo
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control date validate_date_before" name="fromDate" id="fromDate" placeholder="Từ ngày" value="${certificateForm.fromDate}"
                                                                   type-message="${MessageUtils.getMessage("date.validate.unformatted")}"
                                                                   type-message-before="${MessageUtils.getMessage("date.validate.before")}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control date validate_date_after" name="toDate" id="toDate" placeholder="Đến ngày" value="${certificateForm.toDate}"
                                                                   type-message="${MessageUtils.getMessage("date.validate.unformatted")}"
                                                                   type-message-after="${MessageUtils.getMessage("date.validate.after")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách chứng thư số</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Số đăng ký</th>
                                                        <th style="text-align: center">Index</th>
                                                        <th style="text-align: center">Tên ngân hàng</th>
                                                        <th style="text-align: center">IPK</th>
                                                        <th style="text-align: center">Serial</th>
                                                        <th style="text-align: center">Số định danh ngân hàng</th>
                                                        <th style="text-align: center">Ngày hết hạn</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${certificateList}" var="certificateBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index + 1}</td>
                                                            <td>${StringUtils.paddingLeft(certificateBO.registerId,6)}</td>
                                                            <td>${StringUtils.paddingLeft(certificateBO.rsaId,2)}</td>
                                                            <td>${certificateBO.bankName}</td>
                                                            <td>${StringUtils.displayCut(certificateBO.ipk,10)}</td>
                                                            <td>${certificateBO.serial}</td>
                                                            <td>${StringUtils.getBankIdentityNoFF(certificateBO.bankIdentity)}</td>
                                                            <td>${certificateBO.expDate}</td>
                                                            <td>
                                                                <c:if test="${certificateBO.status == 0}">
                                                                    Đã ký/duyệt
                                                                </c:if>
                                                                <c:if test="${certificateBO.status == 1}">
                                                                    Thu hồi
                                                                </c:if>
                                                                <c:if test="${certificateBO.status == 2}">
                                                                    Hết hạn
                                                                </c:if>
                                                            </td>
                                                            <td>
                                                    <center>
                                                        <a href="#" onclick="openModalDetail('${AESUtil.encryption(certificateBO.certificateId)}');"
                                                           data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                        <c:if test="${CERTIFICATE_EXPORT == 'true'}">
                                                            <a href="#" onclick="openModalExport('${AESUtil.encryption(certificateBO.certificateId)}');"
                                                               data-toggle="tooltip" data-placement="bottom" title="Tải file"><span class="glyphicon glyphicon-download-alt"/></a>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- View detail certificate -->
<div id="viewDetail" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="viewDetailId" class="modal-dialog modal-lg">
        <!--<div class="modal-content">-->

        <!--</div>-->
    </div>
</div>

<!-- Alert popup -->
<div class="modal fade" id="recallPopup" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn thu hồi chứng thư này?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="recallCertificate.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="cerIdRecall" id="cerIdRecall"/>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="control-label col-md-4" for="recallContent" style="text-align:left;">
                                    Lý do thu hồi <font style="color: red">*</font>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" id="recallContent" name="recallContent" style="resize: vertical" placeholder="" required
                                                  require-message="${MessageUtils.getMessage("certificate.recall.content.validate.empty")}"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success" style="width: 150px;">Thu hồi</button>
                        <button type="button" class="btn btn-default" style="width: 150px;" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<!-- Alert popup -->
<div class="modal fade" id="restorePopup" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn khôi phục chứng thư này?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="restoreCertificate.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="cerIdRes" id="cerIdRes"/>
                        <button type="submit" class="btn btn-success" style="width: 150px;">Khôi phục</button>
                        <button type="button" class="btn btn-default" style="width: 150px;" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<form style="display: none" id="exportCertificateForm" action="exportCertificate.do" method='POST' accept-charset="UTF-8">
    <input type="hidden" name="certId" id="certId"/>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('#fromDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });
        $('#toDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });
    });

    function selectExpDate() {
        if ($('#searchMonth').val() !== "" && $('#searchYear').val() === "") {
            $('#searchMonthHidden').attr("type", "hidden");
            $('#searchYearHidden').attr("type", "text");
        }
        if ($('#searchYear').val() !== "" && $('#searchMonth').val() === "") {
            $('#searchYearHidden').attr("type", "hidden");
            $('#searchMonthHidden').attr("type", "text");
        }
        if (($('#searchMonth').val() === "" && $('#searchYear').val() === "")
                || ($('#searchMonth').val() !== "" && $('#searchYear').val() !== "")) {
            $('#searchMonthHidden').attr("type", "hidden");
            $('#searchYearHidden').attr("type", "hidden");
        }
    }
    ;

    function openModalDetail(certificateId) {
        $.get("chi-tiet-chung-thu-so.html", {certificateId: certificateId}, function (data) {
            $("#viewDetailId").html(data);
            $('#viewDetail').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalExport(certId) {
        $("#certId").val(certId);
        $('#exportCertificateForm').submit();
    }
    ;
    function openRecallPopup(cerId) {
        $('#recallPopup').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#cerIdRecall').val(cerId);
    }
    ;
    function openRestorePopup(cerId) {
        $('#restorePopup').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#cerIdRes').val(cerId);
    }
    ;
</script>