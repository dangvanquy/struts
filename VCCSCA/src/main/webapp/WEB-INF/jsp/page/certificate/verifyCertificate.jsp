<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<%
    String CERTIFICATE_VERIFY_CREATE = (String) request.getSession().getAttribute("CERTIFICATE_VERIFY_CREATE");
    String CERTIFICATE_IMPORT = (String) request.getSession().getAttribute("CERTIFICATE_IMPORT");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý chứng thư số
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Xác nhận chứng thư số
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form:form method="POST" action="importCertificate.do" enctype="multipart/form-data">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Xác nhận chứng thư số</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="fileDatas" style="text-align:right;">
                                                        Tải file<span style="color: red">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <span class="control-fileupload">
                                                                <input type="file" required name="file"
                                                                       require-message="${MessageUtils.getMessage("certificate.import.upload.validate.empty")}">
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                                                        Index<span style="color: red">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="index" class="form-control" name="index" required
                                                                    require-message="${MessageUtils.getMessage("certificate.import.index.validate.empty")}">
                                                                <option value="">Index</option>
                                                                <c:forEach items="${allKeysList}" var="rSAKeysBO" varStatus="itr">
                                                                    <option value="${StringUtils.paddingIndex(rSAKeysBO.rsaIndex)}">${StringUtils.paddingIndex(rSAKeysBO.rsaIndex)}</option>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <c:if test="${CERTIFICATE_IMPORT == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="submit">Kiểm tra</button>
                                                            </c:if>

                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                        </div>
                                    </div>
                                </form:form>
                                <form name='certificateForm' modelAttribute="CertificateForm" action="verifyCertificate.do" method='POST'
                                      accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <h1 class="panel-title text-left">Thông tin chứng thư số</h1>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                                                        Tên ngân hàng
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="bankName" id="bankName" value="${hexDataCert.get('bankFullName')}" placeholder="" readonly="true">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                                                        Serial
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="serial" id="serial" value="${hexDataCert.get('certificateSerialNumber')}" placeholder="" readonly="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                                                        IPK
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <textarea rows="3" class="form-control" name="ipk" id="ipk" placeholder="" readonly="true">${hexDataCert.get('iPKModulus')}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                                                        Số định danh NH
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="bankIdentity" id="bankIdentity" value="${StringUtils.getBankIdentityNoFF(hexDataCert.get('issuerIdentificationNumber'))}" placeholder="" readonly="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                                                        Ngày hết hạn
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control " name="expDate" id="expDate" value="${hexDataCert.get('exDate1')}" placeholder="" readonly="true">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">

                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <c:if test="${CERTIFICATE_VERIFY_CREATE == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="submit">Lưu vào hệ thống</button>
                                                            </c:if>

                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('input[type=file]').change(function () {
            var t = $(this).val();
            var labelText = 'File : ' + t.substr(12, t.length);
            $(this).prev('label').text(labelText);
        })
    });
</script>