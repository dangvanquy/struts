<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<form id="certificateDetailForm" name='certificateDetailForm' modelAttribute="certificateDetailForm" action="chi-tiet-chung-thu-so.html" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Xem</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left">Thông tin chung</h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                        Số đăng ký
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="hidden" name="certId" id="certId" value="${AESUtil.encryption(certificateBO.certificateId)}">
                            <input type="text" class="form-control" id="registerId" placeholder="" readonly="true" value="${certificateBO.registerId}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="registerId" style="text-align:right;">
                        Index
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="registerId" placeholder="" readonly="true" value="${StringUtils.paddingIndex(rSAKeysBO.rsaIndex)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="bankName" style="text-align:right;">
                        Tên ngân hàng
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="bankName" placeholder="" readonly="true" value="${certificateBO.bankName}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="bankIdentity" style="text-align:right;">
                        Số định danh NH
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="bankIdentity" placeholder="" readonly="true" value="${StringUtils.getBankIdentityNoFF(certificateBO.bankIdentity)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="ipk" style="text-align:right;">
                        IPK
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea rows="3" class="form-control" id="ipk" placeholder="" readonly="true">${certificateBO.ipk}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="serial" style="text-align:right;">
                        Số serial (Hex)
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="serial" placeholder="" readonly="true" value="${certificateBO.serial}">
                        </div>
                    </div>
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="expDate" placeholder="" readonly="true" value="${certificateBO.expDate}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="ipkLengh" style="text-align:right;">
                        Độ dài IPK
                    </label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <div style="float: left;width: 40px">
                                <p style="color: red;font-weight: bold;" id="createIpkLengh">${certificateBO.ipkLengh}</p>
                            </div>
                            <div style="float: left;">
                                <p>(bit)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:if test="${certificateBO.status == 0}">
                                <input type="text" class="form-control" id="status" placeholder="" readonly="true" value="Đã ký/duyệt">
                            </c:if>
                            <c:if test="${certificateBO.status == 1}">
                                <input type="text" class="form-control" id="status" placeholder="" readonly="true" value="Đã thu hồi">
                            </c:if>
                            <c:if test="${certificateBO.status == 2}">
                                <input type="text" class="form-control" id="status" placeholder="" readonly="true" value="Hết hạn">
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <c:if test="${certificateBO.status ==1}">
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label class="control-label col-md-2" for="content" style="text-align:right;">
                            Lý do thu hồi
                        </label>
                        <div class="col-md-10">
                            <div class="form-group">
                                <textarea class="form-control" id="content" style="resize: vertical" placeholder="" readonly="true">${certificateBO.content}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-center">Thông tin khác</h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createCSUser" style="text-align:right;">
                        Người tạo
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="createUser" placeholder="" readonly="true" value="${certificateBO.createUser}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createCSDate" style="text-align:right;">
                        Ngày tạo
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="createDate" placeholder="" readonly="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(certificateBO.createDate)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateCSUser" style="text-align:right;">
                        Người ký
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="updateUser" placeholder="" readonly="true" value="${certificateBO.signUser}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateCSDate" style="text-align:right;">
                        Ngày ký
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="updateDate" placeholder="" readonly="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(certificateBO.signDate)}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateUser" style="text-align:right;">
                        Người thu hồi
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="updateUser" placeholder="" readonly="true" value="${certificateBO.evictionUser}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                        Ngày thu hồi
                    </label>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="updateDate" placeholder="" readonly="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(certificateBO.evictionDate)}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>

                            <c:if test="${CERTIFICATE_EXPORT == 'true'}">
                                <button class="btn btn-success" style="width: 150px;" type="button" data-dismiss="modal" onclick="submitExport('certificateDetailForm', 'exportCertificate.do');">Export</button>
                            </c:if>
                            <c:if test="${certificateBO.status != 2}">
                                <c:if test="${CERTIFICATE_RECALL == 'true' && certificateBO.status != 1}">
                                    <button class="btn btn-danger" style="width: 150px;" type="button" onclick="openRecallPopup('${AESUtil.encryption(certificateBO.certificateId)}');">Thu hồi</button>
                                </c:if>
                                <c:if test="${CERTIFICATE_RESTORE == 'true' && certificateBO.status == 1}">
                                    <button class="btn btn-danger" style="width: 150px;" type="button" onclick="openRestorePopup('${AESUtil.encryption(certificateBO.certificateId)}');">Khôi phục</button>
                                </c:if>
                            </c:if>
                            <button class="btn btn-default" style="width: 150px;" data-dismiss="modal" type="button">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>