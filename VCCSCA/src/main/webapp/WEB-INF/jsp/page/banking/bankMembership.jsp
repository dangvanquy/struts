<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>

<%
    String BANKMEMBERSHIP_EDIT = (String) request.getSession().getAttribute("BANKMEMBERSHIP_EDIT");
    String BANKMEMBERSHIP_DELETE = (String) request.getSession().getAttribute("BANKMEMBERSHIP_DELETE");
    String BANKMEMBERSHIP_ADD = (String) request.getSession().getAttribute("BANKMEMBERSHIP_ADD");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý ngân hàng thành viên
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='bankMembershipForm' action="quan-ly-nhtv.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bin" style="text-align:right;">
                                                        Số BIN
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input id="bin" name="bin" type="text" class="form-control number"  placeholder="" value="${bankMembershipForm.bin}" maxlength="8">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankFullName" style="text-align:right;">
                                                        Tên ngân hàng
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input id="bankFullName" name="bankFullName" type="text" class="form-control"  placeholder="" value="${bankMembershipForm.bankFullName}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="bankShortName" style="text-align:right;">
                                                        Tên viết tắt
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input id="bankShortName" name="bankShortName" type="text" class="form-control"  placeholder="" value="${bankMembershipForm.bankShortName}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit" ><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${BANKMEMBERSHIP_ADD == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalCreate();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null && errMessage != ''}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h1 class="panel-title text-left">Danh sách ngân hàng thành viên</h1>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center">STT</th>
                                                            <th style="text-align: center">Số BIN</th>
                                                            <th style="text-align: center">Tên ngân hàng</th>
                                                            <th style="text-align: center">Tên viết tắt</th>
                                                            <th style="text-align: center">Đầu mối liên hệ</th>
                                                            <th style="text-align: center">Email</th>
                                                            <th style="text-align: center">Số điện thoại</th>
                                                            <th style="text-align: center">Trạng thái</th>
                                                            <th style="text-align: center">Thao tác</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="myTable">
                                                        <c:forEach items="${banksList}" var="bankMembershipBO" varStatus="itr">
                                                            <tr>
                                                                <td style="text-align: center">${offset + itr.index +1 }</td>
                                                                <td>${bankMembershipBO.bin}</td>
                                                                <td>${bankMembershipBO.bankFullName}</td>
                                                                <td>${bankMembershipBO.bankShortName}</td>
                                                                <td>${bankMembershipBO.represent1}</td>
                                                                <td>${bankMembershipBO.email1}</td>
                                                                <td>${bankMembershipBO.phoneNumber1}</td>
                                                                <td>${bankMembershipBO.status==0?'Hoạt động':'Không hoạt động'}</td>
                                                                <td>
                                                        <center>
                                                            <a href="#" onclick="openModalDetail('${AESUtil.encryption(bankMembershipBO.bankId)}');"
                                                               data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                            <c:if test="${BANKMEMBERSHIP_EDIT == 'true'}">
                                                                <a href="#" onclick="openModalEdit('${AESUtil.encryption(bankMembershipBO.bankId)}');"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                            </c:if>
                                                            <c:if test="${BANKMEMBERSHIP_DELETE == 'true'}">
                                                                <a href="#" onclick="openModalDelete('${AESUtil.encryption(bankMembershipBO.bankId)}');"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Xóa"><span class="glyphicon glyphicon-remove-sign"/></a>
                                                            </c:if>
                                                            <a href="#" onclick="gotoCert('${bankMembershipBO.bin}')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Xem chứng thư"><span class="glyphicon glyphicon-link"/></a>
                                                        </center>
                                                        </td>
                                                        </tr>
                                                    </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="createBankMembershipModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="createBankMembershipId" >
    </div>
</div>
<div id="editBankMembershipModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="editBankMembershipId" >
    </div>
</div>
<div id="bankMembershipDetailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="bankMembershipDetailId" >
    </div>
</div>
<!-- Alert modal -->
<div class="modal fade" id="alertDeleteBankingModal" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-danger">
                    <center>
                        <strong>Chú ý:</strong> 
                        Bạn có chắc chắn muốn xóa NHTV ?
                    </center>
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteBankMembership.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="deleteBankId" id="deleteBankId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<form id="formToCert" action="quan-ly-chung-thu-so.html" method="POST">
    <input type="hidden" name="binSearch" id="binSearch"/>
</form>

<script>
//    $(document).on("keypress", "#bin", function (e) {
//        var a = [];
//        var k = e.which;
//        a.push(8);
//        a.push(46);
//        for (i = 48; i < 58; i++) {
//            a.push(i);
//        }
//        if (!(a.indexOf(k) >= 0)) {
//            e.preventDefault();
//        }
//    });

    function openModalCreate() {
        $.get("tao-moi-nhtv.html", function (data) {
            $("#createBankMembershipId").html(data);
            $('#createBankMembershipModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalEdit(bankId) {
        $('#bankMembershipDetailModal').modal('hide');
        $.get("chinh-sua-nhtv.html", {bankId: bankId}, function (data) {
            $("#editBankMembershipId").html(data);
            $('#editBankMembershipModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalDetail(bankId) {
        $.get("xem-chi-tiet-nhtv.html", {bankId: bankId}, function (data) {
            $("#bankMembershipDetailId").html(data);
            $('#bankMembershipDetailModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalDelete(bankId) {
        $('#deleteBankId').val(bankId);
        $('#bankMembershipDetailModal').modal('hide');
        $('#alertDeleteBankingModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function gotoCert(bin) {
        $('#binSearch').val(bin);
        $('#formToCert').submit();
    }
    ;

</script>