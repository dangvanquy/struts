<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form name="bankMembershipDetailForm" method='POST' modelAttribute="bankMembershipForm" action="banking/create" accept-charset="UTF-8">
    <div class="Container">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary" style="margin-top: 100px">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1 class="panel-title text-left">Xem</h1>
                    </div>
                </div>
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#viewBankDetail" aria-controls="uploadTab" role="tab" data-toggle="tab">Thông tin ngân hàng</a>

                        </li>
                        <li role="presentation"><a href="#viewCerfiticateDetail" aria-controls="browseTab" role="tab" data-toggle="tab">Danh sách chứng thư</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="viewBankDetail">
                            <div class="panel-body">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <label class="control-label col-md-3" for="bin" style="text-align:right;">
                                            BIN
                                        </label>
                                        <div class="col-lg-9">
                                            <input disabled="true" type="text " name="bin" class="form-control" id="bin" placeholder="" value="${membershipBO.bin}">
                                            <input type="hidden" name="bankId" value="${membershipBO.bankId}" class="form-control" id="bankId" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="control-label col-md-4" for="bankFullName" style="text-align:right;">
                                            Tên ngân hàng
                                        </label>
                                        <div class="col-lg-8">
                                            <input disabled="true" type="text " name="bankFullName" class="form-control" id="bankFullName" placeholder="" value="${membershipBO.bankFullName}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="control-label col-md-4" for="bankShortName" style="text-align:right;">
                                            Tên viết tắt
                                        </label>
                                        <div class="col-lg-8">
                                            <input disabled="true" type="text" name="bankShortName" class="form-control" id="bankShortName" placeholder="" value="${membershipBO.bankShortName}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <label class="control-label col-md-3" for="status" style="text-align:right;">
                                            Trạng thái
                                        </label>
                                        <div class="col-lg-9">
                                            <select id="status" class="form-control" name="status" disabled="true">
                                                <option value="0" ${membershipBO.status==0?"selected":""}>Hoạt động</option>
                                                <option value="1" ${membershipBO.status==1?"selected":""}>Không hoạt động</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>   
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <h1 class="panel-title text-left" style="color: black;"><b>Thông tin đầu mối</b></h1>
                                    </div>
                                </div>

                                <c:forEach var = "idx" begin = "1" end = "5">    
                                    <div class="form-group row" style="${(StringUtils.getrep(membershipBO,idx) != '' || StringUtils.getEmail(membershipBO,idx) != '' || StringUtils.getphone(membershipBO,idx) != '') ? '':'display: none'}">
                                        <div class="col-lg-4">
                                            <label  class="control-label col-md-3" for="represent1" style="text-align:right;">
                                                Họ tên
                                            </label>
                                            <div class="col-lg-3">
                                                <input disabled="true" type="text " class="form-control" placeholder="" value="${StringUtils.getApp(membershipBO,idx)==0?'Ông':'Bà'}">
                                            </div>
                                            <div class="col-lg-6">
                                                <input disabled="true" type="text " class="form-control" placeholder="" value="${StringUtils.getrep(membershipBO,idx)}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="control-label col-md-4" for="email" style="text-align:right;">
                                                Email
                                            </label>
                                            <div class="col-lg-8">
                                                <input disabled="true" type="text " class="form-control" placeholder="" value="${StringUtils.getEmail(membershipBO,idx)}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="control-label col-md-6" for="phoneNumber1" style="text-align:right;">
                                                Số điện thoại
                                            </label>
                                            <div class="col-lg-6">
                                                <input disabled="true" type="text " class="form-control" placeholder="" value="${StringUtils.getphone(membershipBO,idx)}">
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>   

                                
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <h1 class="panel-title text-left" style="color: black;"><b>Thông tin khác</b></h1>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="control-label col-md-3" for="createUser" style="text-align:right;">
                                            Người tạo
                                        </label>
                                        <div class="col-lg-9">
                                            <input value="${membershipBO.createUser}" disabled="true" type="text" class="form-control" id="createUser" placeholder="" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                                            Ngày tạo
                                        </label>
                                        <div class="col-lg-8">
                                            <input value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(membershipBO.createDate)}" disabled="true" type="text" class="form-control" id="createDate" placeholder="" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="control-label col-md-3" for="updateUser" style="text-align:right;">
                                            Người cập nhật
                                        </label>
                                        <div class="col-lg-9">
                                            <input value="${membershipBO.updateUser}" disabled="true" type="text" class="form-control" id="updateUser" placeholder="" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                                            Ngày cập nhật
                                        </label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" id="updateDate" placeholder="" disabled="true" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(membershipBO.updateDate)}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group row">
                                    <div class="form-control-lg">
                                        <div class="col-sm-12">
                                            <center>
                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalEdit('${AESUtil.encryption(membershipBO.bankId)}');">Sửa</button>
                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalDelete('${AESUtil.encryption(membershipBO.bankId)}');">Xóa</button>
                                                <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="viewCerfiticateDetail">
                            <div class="panel-body">
                                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">STT</th>
                                            <th style="text-align: center">Số đăng ký</th>
                                            <th style="text-align: center">Tên ngân hàng</th>
                                            <th style="text-align: center">IPK</th>
                                            <th style="text-align: center">Serial</th>
                                            <th style="text-align: center">Số định danh ngân hàng</th>
                                            <th style="text-align: center">Ngày hết hạn</th>
                                            <th style="text-align: center">Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${certificateList}" var="certificateBO" varStatus="itr">
                                            <tr>
                                                <td style="text-align: center">${offset + itr.index + 1}</td>
                                                <td>${certificateBO.registerId}</td>
                                                <td>${certificateBO.bankName}</td>
                                                <td>${StringUtils.displayKey(certificateBO.ipk)}</td>
                                                <td>${certificateBO.serial}</td>
                                                <td>${certificateBO.bankIdentity}</td>
                                                <td>${certificateBO.expDate}</td>
                                                <td>
                                                    <c:if test="${certificateBO.status == 0}">
                                                        Đã ký/duyệt
                                                    </c:if>
                                                    <c:if test="${certificateBO.status == 1}">
                                                        Thu hồi
                                                    </c:if>
                                                    <c:if test="${certificateBO.status == 2}">
                                                        Hết hạn
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group row">
                                    <div class="form-control-lg">
                                        <div class="col-sm-12">
                                            <center>
                                                <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>