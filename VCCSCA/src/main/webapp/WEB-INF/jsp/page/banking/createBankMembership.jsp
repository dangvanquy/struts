<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form method='POST' modelAttribute="bankMembershipForm" action="createBankMembership.do" accept-charset="UTF-8"
      onsubmit="return validateItemEmail();"
      >
    <div class="Container">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary" style="margin-top: 100px">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1 class="panel-title text-left">Thêm mới</h1>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="control-label col-md-3" for="bin" style="text-align:right;">
                                BIN <span class="required" style="color: red">*</span>
                            </label>
                            <div class="col-lg-9">
                                <input type="text" maxlength="8" name="bin" class="form-control number" id="bin" placeholder=""
                                       required="" require-message="${MessageUtils.getMessage("bin.not.empty")}"
                                       type-message="${MessageUtils.getMessage("banking.bin.exist")}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label col-md-4" for="bankFullName" style="text-align:right;">
                                Tên ngân hàng <span class="required" style="color: red">*</span>
                            </label>
                            <div class="col-lg-8">
                                <input maxlength="50" type="text" name="bankFullName" class="form-control" id="bankFullName" placeholder=""
                                       required="" require-message="${MessageUtils.getMessage("bank.full.name.not.empty")}"
                                       type-message="${MessageUtils.getMessage("banking.bankName.exist")}" onchange="">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label col-md-4" for="bankShortName" style="text-align:right;">
                                Tên viết tắt<span class="required" style="color: red">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input maxlength="30" type="text" name="bankShortName" class="form-control character" id="bankShortName" placeholder=""
                                       required="" require-message="${MessageUtils.getMessage("bank.short.name.not.empty")}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="control-label col-md-3" for="status" style="text-align:right;">
                                Trạng thái
                            </label>
                            <div class="col-lg-9">
                                <select id="status" class="form-control" name="status">
                                    <option value="0">Hoạt động</option>
                                    <option value="1">Không hoạt động</option>
                                </select>
                            </div>
                        </div>
                    </div>   
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <h1 class="panel-title text-left" style="color: black;"><b>Thông tin đầu mối</b></h1>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label  class="control-label col-md-3" for="represent" style="text-align:right;">
                                Họ tên
                            </label>
                            <div class="col-lg-4">
                                <select id="appellation" name="appellation1" class="form-control">
                                    <option value="0">Ông</option>
                                    <option value="1">Bà</option>
                                </select>
                            </div>
                            <div class="col-lg-5">
                                <input type="text" maxlength="100" class="form-control" id="represent" placeholder="" name="represent1" >
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label col-md-4" for="email" style="text-align:right;">
                                Email
                            </label>
                            <div class="col-lg-8">
                                <input name="email1" maxlength="100" type="email" class="form-control" id="email" placeholder=""
                                       type-message="${MessageUtils.getMessage("email.config.validate.unformatted")}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label col-md-4" for="phoneNumber" style="text-align:right;">
                                Số điện thoại
                            </label>
                            <div class="col-lg-6">
                                <input id="phoneNumber" maxlength="15" name="phoneNumber1" type="text" class="form-control phone-number">
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-success form-control" class="" style="height: 25px;width: 25px;margin-top: 6px" type="button" onclick="showItem()"><i style="margin: -6px;" class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </div>
                    </div>

                    <c:forEach var = "idx" begin = "2" end = "5">
                        <div id="item${idx}" class="form-group row" style="display: none">
                            <div class="col-lg-4">
                                <label  class="control-label col-md-3" for="represent" style="text-align:right;">
                                    Họ tên
                                </label>
                                <div class="col-lg-4">
                                    <select  id="appellation" name="appellation${idx}" class="form-control" >
                                        <option value="0">Ông</option>
                                        <option value="1">Bà</option>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <input maxlength="100" type="text" class="form-control" id="represent" placeholder="" name="represent${idx}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label col-md-4" for="email" style="text-align:right;">
                                    Email
                                </label>
                                <div class="col-lg-8">
                                    <input  maxlength="100" name="email${idx}" type="email" class="form-control" id="email" placeholder=""
                                            type-message="${MessageUtils.getMessage("email.config.validate.unformatted")}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label class="control-label col-md-4" for="phoneNumber" style="text-align:right;">
                                    Số điện thoại
                                </label>
                                <div class="col-lg-6">
                                    <input maxlength="15" id="phoneNumber" name="phoneNumber${idx}" type="text" class="form-control phone-number"   placeholder="">
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-danger form-control" class="" style="height: 25px;width: 25px;margin-top: 6px" type="button" onclick="deleteItem(${idx})"><i style="margin: -6px;" class="glyphicon glyphicon-remove"></i></button>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
                <div class="panel-footer">
                    <div class="form-group row">
                        <div class="form-control-lg">
                            <div class="col-sm-12">
                                <center>
                                    <button class="btn btn-success" style="width: 100px;" type="submit">Lưu</button>
                                    <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <center>
                            <c:if test="${errMessage != null && errMessage != ''}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger" role="alert">
                                            ${errMessage}
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${successMessage != null}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-success" role="alert">
                                            ${successMessage}
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Alert modal -->
<div class="modal fade" id="alertUpdate" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn các thông tin đăng ký là chính xác?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-primary" onclick="submitForm()">Thêm mới</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function showItem() {
        for (var i = 2; i < 6; i++) {
            if ($('#item' + i).css('display') == 'none')
            {
                $('#item' + i).show('slow');
                break;
            }
        }
    }
    ;

    function deleteItem(objId) {
        $('#item' + objId).hide('slow');
    }
    ;

</script>