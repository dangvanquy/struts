<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form name='editChecksumForm' modelAttribute="editChecksumForm" action="editChecksum.do" method='POST' accept-charset="UTF-8"
      onsubmit="return validateItemEdit()">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Cập nhật</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                        Index <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <select id="index" class="form-control" name="rsaIndex" disabled="true">
                            <option value="${keysBO.rsaIndex}">${StringUtils.paddingIndex(keysBO.rsaIndex)}</option>
                        </select>
                        <input type="hidden" class="form-control" id="rsaIndex" placeholder="" name="rsaIndex" value="${keysBO.rsaIndex}">
                        <input type="hidden" class="form-control" id="rsaId" placeholder="" name="rsaId" value="${keysBO.rsaId}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-4">
                        <select id="month" class="form-control" name="month"
                                required="" require-message="${MessageUtils.getMessage("common.require.select.month")}">
                            <option value="">---Tháng---</option>
                            <option value="01" ${editChecksumForm.month == '01' ? 'selected': ''}>Tháng 1</option>
                            <option value="02" ${editChecksumForm.month == '02' ? 'selected': ''}>Tháng 2</option>
                            <option value="03" ${editChecksumForm.month == '03' ? 'selected': ''}>Tháng 3</option>
                            <option value="04" ${editChecksumForm.month == '04' ? 'selected': ''}>Tháng 4</option>
                            <option value="05" ${editChecksumForm.month == '05' ? 'selected': ''}>Tháng 5</option>
                            <option value="06" ${editChecksumForm.month == '06' ? 'selected': ''}>Tháng 6</option>
                            <option value="07" ${editChecksumForm.month == '07' ? 'selected': ''}>Tháng 7</option>
                            <option value="08" ${editChecksumForm.month == '08' ? 'selected': ''}>Tháng 8</option>
                            <option value="09" ${editChecksumForm.month == '09' ? 'selected': ''}>Tháng 9</option>
                            <option value="10" ${editChecksumForm.month == '10' ? 'selected': ''}>Tháng 10</option>
                            <option value="11" ${editChecksumForm.month == '11' ? 'selected': ''}>Tháng 11</option>
                            <option value="12" ${editChecksumForm.month == '12' ? 'selected': ''}>Tháng 12</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <select id="year" class="form-control" name="year"
                                required="" require-message="${MessageUtils.getMessage("common.require.select.year")}">
                            <option value="">---Năm---</option>
                            <c:forEach var="expYear" begin="2018" end="3000" >
                                <option value="${expYear}" ${editChecksumForm.year eq expYear ? 'selected' : ''}>${expYear}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <div></div>
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Public key exponent (Hex)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <select id="year" class="form-control" name="publicKeyExponent">
                            <option value="03" ${keysBO.publicKeyExponent == '03' ? 'selected':''}>03</option>
                            <option value="010001" ${keysBO.publicKeyExponent == '010001' ? 'selected':''}>010001</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div></div>
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Độ dài khóa
                    </label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input  type="text" id="rsaLengthEdit"  class="form-control " name="rsaLength" value="${keysBO.rsaLength}" disabled="true"/>
                            <div class="input-group-addon">bit</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">

                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyModulus" style="text-align:right;">
                        Public key modulus (Hex)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <textarea maxlength="500" rows="3" class="form-control rsa" id="publicKeyModulus" name="publicKeyModulus" required="" onkeyup="loadlenEdit()"
                                  require-message="${MessageUtils.getMessage("checksum.public.key.modulus.not.empty")}">${keysBO.publicKeyModulus}</textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="checksumNameEdit" style="text-align:right;">
                        Privatekey Checksum (SHA256)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <textarea maxlength="64" rows="3" class="form-control rsa" id="checksumNameEdit" name="checksumName"
                                  required="" 
                                  require-message="${MessageUtils.getMessage("checksum.name.not.empty")}"
                                  checksum-message="Privatekey Checksum (SHA256) không đủ độ dài"
                                  >${keysBO.checksumName}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">

                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="checksumStatus">
                            <option value="0" ${keysBO.checksumStatus==0 ? "selected" : ""} >Hoạt động</option>
                            <option value="1" ${keysBO.checksumStatus==1 ? "selected" : ""} >Ngừng hoạt động</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-2" style="text-align:right;">
                    <label class="control-label" for="checksumContent" >
                        Mô tả
                    </label>
                </div>
                <div class="col-lg-10" style="margin-left: 5px;width: 81.333%;">
                    <textarea maxlength="200" class="form-control" id="checksumContent" name="checksumContent" placeholder="">${keysBO.checksumContent}</textarea>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
//    $(document).on("keypress", "#publicKeyExponent", function (e) {
//        var a = [];
//        var k = e.which;
//        a.push(8);
//        a.push(46);
//        for (i = 48; i < 58; i++) {
//            a.push(i);
//        }
//        if (!(a.indexOf(k) >= 0)) {
//            e.preventDefault();
//        }
//    });
    function loadlenEdit() {
        $("#rsaLengthEdit").val($("#publicKeyModulus").val().length * 4);
    }

    function validateItemEdit() {
        var msg = $("#checksumNameEdit").attr("checksum-message");
        if ($("#checksumNameEdit").val().length !== 64) {
            $("#checksumNameEdit")[0].setCustomValidity(msg);
            $("#checksumNameEdit")[0].reportValidity();
            return false;
        } else {
            $("#checksumNameEdit")[0].setCustomValidity("");
            return true;
        }
    }
</script>
