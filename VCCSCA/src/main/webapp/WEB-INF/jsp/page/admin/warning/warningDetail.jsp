<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.StringUtils"%>

<%
    String ADMIN_EDIT_ALERT = (String) request.getSession().getAttribute("ADMIN_EDIT_ALERT");
    String ADMIN_DELETE_ALERT = (String) request.getSession().getAttribute("ADMIN_DELETE_ALERT");
%>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h1 class="panel-title text-left">Xem</h1>
    </div>
    <div class="panel-body">
        <div class="form-group row">
            <div class="col-lg-12">
                <div class=" col-md-2 text-right">
                    <label class="control-label" for="param" >Loại cảnh báo</label>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><input disabled="true" type="radio" name="type" ${configWarningSystemForm.type == 0 ?'checked':''} value="0">&nbsp;Chứng thư số</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><input disabled="true" type="radio" name="type" ${configWarningSystemForm.type == 1 ?'checked':''} value="1">&nbsp;Root CA</label>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >Tên cảnh báo</label>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="warningName" readonly="true"
                               value="${configWarningSystemForm.warningName}"
                               />
                        <input type="hidden" class="form-control" name="warningId" value="${configWarningSystemForm.warningId}"
                               />
                    </div>
                </div>
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label " for="param" >
                        Tham số cảnh báo
                    </label>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="number" class="form-control" name="dayConfig" placeholder="Số ngày trước khi cảnh báo" readonly="true"
                                   value="${configWarningSystemForm.dayConfig}" />
                            <div class="input-group-addon">Ngày</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >To</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="email" class="form-control" name="mailTo" readonly="true" 
                               value="${configWarningSystemForm.mailTo}" 
                               />
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >CC</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="email" class="form-control" name="mailCc" readonly="true"
                               value="${configWarningSystemForm.mailCc}" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >BCC</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="email" class="form-control" name="mailBcc" readonly="true"
                               value="${configWarningSystemForm.mailBcc}" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >Thời gian gửi</label>
                </div>
                <div class="col-md-4">
                    <div class="input-group" style="width: 45% !important;float: left" >
                        <select id="status" name="sendTime1"  class="form-control" disabled="true" >
                            <c:forEach var = "hh" begin = "0" end = "23">
                                <option value="${StringUtils.paddingIndex(hh)}" ${StringUtils.paddingIndex(hh) == configWarningSystemForm.sendTime1 ? 'selected':''} >${StringUtils.paddingIndex(hh)}</option>
                            </c:forEach>
                        </select>
                        <div class="input-group-addon">Giờ</div>
                    </div>

                    <div style="width: 10% !important;float: left" ><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>

                    <div class="input-group" style="width: 45% !important;float: left" >
                        <select id="status" name="sendTime2" class="form-control" disabled="true">
                            <c:forEach var = "mm" begin = "0" end = "59">
                                <option value="${StringUtils.paddingIndex(mm)}" ${StringUtils.paddingIndex(mm) == configWarningSystemForm.sendTime2 ? 'selected':''}>${StringUtils.paddingIndex(mm)}</option>
                            </c:forEach>
                        </select>
                        <div class="input-group-addon">Phút</div>
                    </div>
                </div>


                <!--                <div class="col-md-4">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input type="text" id="timeEdit" placeholder="Time" class="form-control input-small" name="sendTime" readonly="true" 
                                               value="${configWarningSystemForm.sendTime}" />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>-->
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label " for="param" >Số lần cảnh báo</label>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="number" class="form-control" id="" placeholder="" name="sendNumber" readonly="true" 
                                   value="${configWarningSystemForm.sendNumber}" />
                            <div class="input-group-addon">Lần</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >Trạng thái</label>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" readonly="true" 
                                   value="${configWarningSystemForm.status == 0 ?'Hoạt động':'Không hoạt động'}" />
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >Tiêu đề</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" readonly="true" 
                               value="${configWarningSystemForm.subject}" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >Nội dung</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <textarea rows="5" class="form-control" id="content" placeholder="" name="content" readonly="true" 
                                  >${configWarningSystemForm.content}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class=" col-md-2 text-right label-text">
                    <label class="control-label" for="param" >Tham số nội dung</label>
                </div>
                <div class="col-md-10">
                    <div id="help_edit_param" >
                        <c:if test="${configWarningSystemForm.params != null}">
                            <div class="alert alert-success" role="alert">
                                <c:forEach items="${configWarningSystemForm.params}" var="paramBo">
                                    <c:if test="${fn:contains(configWarningSystemForm.param, paramBo.paramId)}">
                                        <div class='col-md-4'>${paramBo.paramName}</div>
                                        <div class='col-md-8'>{${paramBo.columnValue}}</div>
                                    </c:if>
                                </c:forEach>
                                <div class='clearfix'></div>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <c:if test="${ADMIN_EDIT_ALERT == 'true'}">
                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalEdit('${configWarningSystemForm.warningId}')">Sửa</button>
                            </c:if>
                            <c:if test="${ADMIN_DELETE_ALERT == 'true'}">
                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalDelete('${configWarningSystemForm.warningId}')">Xóa</button>
                            </c:if>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
