<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form name='editRsaForm' modelAttribute="editRsaForm" action="editKey.do" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Cập nhật</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                        Index
                    </label>
                    <div class="col-lg-4">
                        <select disabled="true" id="index" class="form-control" name="rsaIndex"
                                required="" require-message="${MessageUtils.getMessage("rsa.require.select.index")}">
                            <c:if test="${keysBO != null}">
                                <option value="${StringUtils.paddingIndex(keysBO.rsaIndex)}">${StringUtils.paddingIndex(keysBO.rsaIndex)}</option>
                            </c:if>
                        </select>
                        <input type="hidden" class="form-control" id="rsaId" placeholder="" name="rsaId" value="${keysBO.rsaId}">
                    </div>
                    <div class="col-lg-4">
                        <select id="index" class="form-control" name="slotHsm" 
                                required require-message="${MessageUtils.getMessage("rsa.require.select.slot")}">
                            <option value="">Slot HSM</option>
                            <c:forEach items="${slotLst}" var="configHsmBO">
                                <option value="${configHsmBO.slot}" ${keysBO.slotHsm==configHsmBO.slot ? "selected":""} >${configHsmBO.slot}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="rsaStatus">
                            <option value="0" ${keysBO.rsaStatus==0 ? "selected":""}>Hoạt động</option>
                            <option value="1" ${keysBO.rsaStatus==1 ? "selected":""}>Ngừng hoạt động</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Public key exponent (Hex)
                    </label>
                    <div class="col-lg-8">
                        <textarea maxlength="500" disabled="true" rows="3" class="form-control" id="publicKeyExponent" name="publicKeyExponent" 
                                  required="" require-message="${MessageUtils.getMessage("checksum.public.key.exponent.not.empty")}">${keysBO.publicKeyExponent}</textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyModulus" style="text-align:right;">
                        Public key modulus (Hex)
                    </label>
                    <div class="col-lg-8">
                        <textarea maxlength="500" disabled="true" rows="3" class="form-control" id="publicKeyModulus" name="publicKeyModulus"
                                  required="" require-message="${MessageUtils.getMessage("checksum.public.key.modulus.not.empty")}">${keysBO.publicKeyModulus}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="rsaLength" style="text-align:right;">
                        Độ dài khóa
                    </label>
                    <div class="col-lg-8">

                        <div class="form-group">
                            <div class="input-group">
                                <input maxlength="5"  type="text" id="rsaLength"  class="form-control number" name="rsaLength" value=" ${keysBO.rsaLength}" disabled="true"/>
                                <div class="input-group-addon">bit</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn
                    </label>
                    <div class="col-md-8">
                        <input class="form-control" disabled="true" id="expirationDate" name="expirationDate" value="${keysBO.expirationDate}">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="content" style="text-align:right;">
                        Mô tả
                    </label>
                    <div class="col-lg-10">
                        <textarea maxlength="200" style="margin-left: -5px; width: 101%;" class="form-control" id="content" name="checksumContent" placeholder="">${keysBO.checksumContent}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <center>
                <c:if test="${errMessage != null && errMessage != ''}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                ${errMessage}
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${successMessage != null}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="alert">
                                ${successMessage}
                            </div>
                        </div>
                    </div>
                </c:if>
            </center>
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <!--<button class="btn btn-success" style="width: 150px;" type="submit">Export</button>-->
                            <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
