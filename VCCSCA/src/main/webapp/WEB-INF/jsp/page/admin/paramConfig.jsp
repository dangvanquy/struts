<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String ADMIN_ADD_PARAM = (String) request.getSession().getAttribute("ADMIN_ADD_PARAM");
    String ADMIN_EDIT_PARAM = (String) request.getSession().getAttribute("ADMIN_EDIT_PARAM");
    String ADMIN_DELETE_PARAM = (String) request.getSession().getAttribute("ADMIN_DELETE_PARAM");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Cấu hình tham số
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="paramForm" name='paramForm' action="createParam.do" method='POST' modelAttribute="paramForm"  accept-charset="UTF-8" >

                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách Tham Số</h1>
                                        </div>
                                        <div class="panel-body">
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr style="text-align: center">
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên tham số</th>
                                                        <th style="text-align: center">Bảng</th>
                                                        <th style="text-align: center">Trường</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <input maxlength="20" type="text" value="${paramForm.paramName}" name="paramName" class="form-control-lg col-md-12 warningName removespecchar" required=""
                                                                   require-message="${MessageUtils.getMessage("param.not.paramName")}"
                                                                   placeholder="Nhập tên tham số"
                                                                   />
                                                        </td>
                                                        <td>
                                                            <select id="tableNameId" style="height: 28px;" class="form-control-lg col-md-12" name="tableName" >
                                                                <option value="0">---Chọn Bảng---</option>
                                                                <option value="TBL_USERS">TBL_USERS</option>
                                                                <option value="TBL_BANK_MEMBERSHIP">TBL_BANK_MEMBERSHIP</option>
                                                                <option value="TBL_DIGITAL_CERTIFICATE">TBL_DIGITAL_CERTIFICATE</option>
                                                                <option value="TBL_RSA_KEYS">TBL_RSA_KEYS</option>
                                                                <option value="TBL_REQUEST">TBL_REQUEST</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="ColumeNameId" style="height: 28px;" class="form-control-lg col-md-12" name="columnValue" >
                                                                <option value="0">---Chọn Trường---</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                <center>
                                                    <c:if test="${ADMIN_ADD_PARAM == 'true'}">
                                                        <input type="submit" id="btnsmparam" style="display: none"/>
                                                        <a href="#" onclick="smm();"
                                                           data-toggle="tooltip" data-placement="bottom" title="Lưu"><span class="glyphicon glyphicon-floppy-save"/></a>
                                                    </c:if>
                                                </center>
                                                </td>
                                                </tr>


                                                <c:forEach items="${listParam}" var="paramBO" varStatus="itr">
                                                    <tr>
                                                        <td style="text-align: center">${offset + itr.index +1 }</td>
                                                        <td>${paramBO.paramName }</td>
                                                        <td>${paramBO.tableName }</td>
                                                        <td>${paramBO.columnValue }</td>
                                                        <td>
                                                    <center>
                                                        <c:if test="${ADMIN_EDIT_PARAM == 'true'}">
                                                            <a href="#" onclick="openModalEdit('${AESUtil.encryption(paramBO.paramId)}', '${paramBO.paramName }', '${paramBO.tableName }', '${paramBO.columnValue }')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                        </c:if>
                                                        <c:if test="${ADMIN_DELETE_PARAM == 'true'}">
                                                            <a href="#" onclick="openModalDelete('${AESUtil.encryption(paramBO.paramId)}')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Xóa"><span class="glyphicon glyphicon-trash"/></a>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Alert modal -->
<div class="modal fade bs-example-modal-lg" id="alertUpdateParam" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="editParam.do" modelAttribute="paramForm"  method='POST' accept-charset="UTF-8">
                <div class="modal-header" style="background-color: #1f417d; color: white">
                    <h1 class="panel-title text-left">Cập nhật</h1>
                </div>
                <input type="hidden" name="paramId" id="txtparamId"/>
                <div class="modal-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr style="text-align: center">
                                <th style="text-align: center">Tên tham số</th>
                                <th style="text-align: center">Bảng</th>
                                <th style="text-align: center">Trường</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="paramNameEditId" maxlength="20" type="text" value="" name="paramName" class="form-control-lg col-md-12 removespecchar" required="true"
                                           require-message="${MessageUtils.getMessage("param.not.paramName")}" />
                                </td>
                                <td>
                                    <select id="tableEditNameId" style="height: 28px;" class="form-control-lg col-md-12" name="tableName" >
                                        <option value="TBL_USERS">TBL_USERS</option>
                                        <option value="TBL_BANK_MEMBERSHIP">TBL_BANK_MEMBERSHIP</option>
                                        <option value="TBL_DIGITAL_CERTIFICATE">TBL_DIGITAL_CERTIFICATE</option>
                                        <option value="TBL_RSA_KEYS">TBL_RSA_KEYS</option>
                                        <option value="TBL_REQUEST">TBL_REQUEST</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="ColumeEditNameId" style="height: 28px;" class="form-control-lg col-md-12" name="columnValue">
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-success" style="width: 150px">Cập nhật</button>
                        <button type="button" class="btn btn-default" style="width: 150px" data-dismiss="modal">Đóng</button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="alertDeleteParam" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn xóa Tham Số
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteParam.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="txtparamId" id="txtDeleteParamId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function openModalEdit(idGp, name, table, col) {
        $('#txtparamId').val(idGp);
        $('#paramNameEditId').val(name);
        $("#tableEditNameId").val(table);
        loadHtml('ColumeEditNameId', table);
        $("#ColumeEditNameId").val(col);

        $('#alertUpdateParam').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function openModalDelete(idGp) {
        $('#txtDeleteParamId').val(idGp);
        $('#alertDeleteParam').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;

    function loadHtml(id, option) {
        if (option == "TBL_USERS") {
            $("#" + id).html(TBL_USERS);
        } else if (option == "TBL_BANK_MEMBERSHIP") {
            $("#" + id).html(TBL_BANK_MEMBERSHIP);
        } else if (option == "TBL_REQUEST") {
            $("#" + id).html(TBL_REQUEST);
        } else if (option == "TBL_RSA_KEYS") {
            $("#" + id).html(TBL_RSA_KEYS);
        } else if (option == "TBL_DIGITAL_CERTIFICATE") {
            $("#" + id).html(TBL_DIGITAL_CERTIFICATE);
        } else {
            $("#" + id).html("");
        }
    }

    $("#tableNameId").change(function () {
        var option = $(this).prop("value");
        loadHtml('ColumeNameId', option)
    });

    $("#tableEditNameId").change(function () {
        var option = $(this).prop("value");
        loadHtml('ColumeEditNameId', option);
    });


    var TBL_USERS = "<option value='0'>---Chọn trường---</option><option value='userId'>USER_ID</option><option value='groupId'>GROUP_ID</option><option value='username'>USER_NAME</option><option value='password'>PASSWORD</option><option value='fullName'>FULL_NAME</option><option value='email'>EMAIL</option><option value='address'>ADDRESS</option><option value='phoneNumber'>PHONE_NUMBER</option><option value='createUser'>CREATE_USER</option><option value='updateUser'>UPDATE_USER</option><option value='createDate'>CREATE_DATE</option><option value='updateDate'>UPDATE_DATE</option><option value='status'>STATUS</option>";
    var TBL_BANK_MEMBERSHIP = "<option value='0'>---Chọn trường---</option><option value='bin'>BIN</option><option value='bankFullName'>BANK_FULL_NAME</option><option value='bankShortName'>BANK_SHORT_NAME</option><option value='appellation1'>APPELLATION1</option><option value='represent1'>REPRESENT1</option><option value='email1'>EMAIL1</option><option value='phoneNumber1'>PHONE_NUMBER1</option><option value='appellation2'>APPELLATION2</option><option value='represent2'>REPRESENT2</option><option value='email2'>EMAIL2</option><option value='phoneNumber2'>PHONE_NUMBER2</option><option value='appellation3'>APPELLATION3</option><option value='represent3'>REPRESENT3</option><option value='email3'>EMAIL3</option><option value='phoneNumber3'>PHONE_NUMBER3</option><option value='appellation4'>APPELLATION4</option><option value='represent4'>REPRESENT4</option><option value='email4'>EMAIL4</option><option value='phoneNumber4'>PHONE_NUMBER4</option><option value='appellation5'>APPELLATION5</option><option value='represent5'>REPRESENT5</option><option value='email5'>EMAIL5</option><option value='phoneNumber5'>PHONE_NUMBER5</option><option value='createUser'>CREATE_USER</option><option value='updateUser'>UPDATE_USER</option><option value='createDate'>CREATE_DATE</option><option value='updateDate'>UPDATE_DATE</option><option value='status'>STATUS</option>";
    var TBL_REQUEST = "<option value='0'>---Chọn trường---</option><option value='registerId'>REGISTER_ID</option><option value='bin'>BIN</option><option value='bankName'>BANK_NAME</option><option value='ipk'>IPK</option><option value='serial'>SERIAL</option><option value='bankIdentity'>BANK_IDENTITY</option><option value='ipkLengh'>IPK_LENGH</option><option value='createUser'>CREATE_USER</option><option value='acceptUser'>ACCEPT_USER</option><option value='createDate'>CREATE_DATE</option><option value='signUser'>SIGN_USER</option><option value='signDate'>SIGN_DATE</option><option value='updateUser'>UPDATE_USER</option><option value='updateDate'>UPDATE_DATE</option><option value='acceptDate'>ACCEPT_DATE</option><option value='expDate'>EXP_DATE</option><option value='appDate'>APP_DATE</option><option value='content'>CONTENT</option><option value='status'>STATUS</option>";
    var TBL_RSA_KEYS = "<option value='0'>---Chọn trường---</option><option value='rsaId'>RSA_ID</option><option value='rsaIndex'>RSA_INDEX</option><option value='typeKey'>TYPE_KEY</option><option value='publicKeyModulus'>PUBLIC_KEY_MODULUS</option><option value='publicKeyExponent'>PUBLIC_KEY_EXPONENT</option><option value='rsaLength'>RSA_LENGTH</option><option value='checksumName'>CHECKSUM_NAME</option><option value='checksumContent'>CHECKSUM_CONTENT</option><option value='expirationDate'>EXPIRATION_DATE</option><option value='createUser'>CREATE_USER</option><option value='updateUser'>UPDATE_USER</option><option value='createDate'>CREATE_DATE</option><option value='updateDate'>UPDATE_DATE</option><option value='createCsUser'>CREATE_CS_USER</option><option value='updateCsUser'>UPDATE_CS_USER</option><option value='createCsDate'>CREATE_CS_DATE</option><option value='updateCsDate'>UPDATE_CS_DATE</option><option value='checksumStatus'>CHECKSUM_STATUS</option><option value='rsaStatus'>RSA_STATUS</option>";
    var TBL_DIGITAL_CERTIFICATE = "<option value='0'>---Chọn trường---</option><option value='certificateId'>CERTIFICATE_ID</option><option value='rsaId'>RSA_ID</option><option value='registerId'>REGISTER_ID</option><option value='certificate'>CERTIFICATE</option><option value='bin'>BIN</option><option value='bankName'>BANK_NAME</option><option value='ipk'>IPK</option><option value='ipkLengh'>IPK_LENGH</option><option value='expDate'>EXP_DATE</option><option value='serial'>SERIAL</option><option value='bankIdentity'>BANK_IDENTITY</option><option value='rsaLengh'>RSA_LENGH</option><option value='evictionUser'>EVICTION_USER</option><option value='evictionDate'>EVICTION_DATE</option><option value='signUser'>SIGN_USER</option><option value='signDate'>SIGN_DATE</option><option value='createUser'>CREATE_USER</option><option value='createDate'>CREATE_DATE</option><option value='content'>CONTENT</option><option value='status'>STATUS</option>";

    function smm() {
        $('#btnsmparam').trigger("click")
    }
</script>