<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form id="editUserForm" modelAttribute="usersForm" action="editAccount.do" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Cập nhật</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="fullname" style="text-align:right;">
                        Tên người dùng <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.fullName}" name="fullName" type="text" class="form-control" id="fullname"
                               required=""
                               require-message="${MessageUtils.getMessage("user.fullname.not.empty")}" maxlength="30">
                        <input name="userId" value="${userBO.userId}" type="hidden" class="form-control" id="userId" placeholder="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="phoneNumber" style="text-align:right;">
                        Số điện thoại
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.phoneNumber}" name="phoneNumber" type="text" class="form-control phone-number" id="phoneNumber" placeholder="" maxlength="15">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="username" style="text-align:right;">
                        Tài khoản <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.username}" name="username" type="text" class="form-control character" id="username"
                               required="" require-message="${MessageUtils.getMessage("user.username.not.empty")}" maxlength="30">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="email" style="text-align:right;">
                        Email <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.email}" name="email" type="email" class="form-control" id="email"
                               required="" require-message="${MessageUtils.getMessage("user.email.not.empty")}" maxlength="100"
                               type-message="${MessageUtils.getMessage("email.config.validate.unformatted")}">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="password" style="text-align:right;">
                        Mật khẩu <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-6">
                        <input value="${userBO.password}" name="password" type="password" class="form-control password" id="password" disabled="true"
                               required="" require-message="${MessageUtils.getMessage("user.password.not.empty")}" maxlength="30"
                               type-message="${MessageUtils.getMessage("change.password.validate.lengh")}">
                    </div>
                    <div class="col-lg-2">
                        <div class="checkbox">
                            <label><input type="checkbox" onclick="openPass(this)" value="repass"></label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="permission" style="text-align:right;">
                        Nhóm quyền <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <select id="permission" class="form-control" name="groupId" data-rel="chosen"
                                required="" require-message="${MessageUtils.getMessage("user.require.select.group.permission")}">
                            <option value="" style="text-align: left">---Chọn nhóm quyền--- </option>
                            <c:forEach items="${groupsListEdit}" var="groupPermissionBO" varStatus="itr">
                                <option value="${groupPermissionBO.groupId}" ${userBO.groupId==groupPermissionBO.groupId?"selected":""}>${groupPermissionBO.groupName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="status">
                            <option value="0" ${userBO.status==0?"selected":""}>Hoạt động</option>
                            <option value="1" ${userBO.status==1?"selected":""}>Khóa</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="content" style="text-align:right;">
                        Ghi chú
                    </label>
                    <div class="col-lg-10">
                        <textarea name="note" style="margin-left: -5px; width: 101%;" class="form-control" id="content" placeholder="" maxlength="200">${userBO.note}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <center>
                <c:if test="${errMessage != null && errMessage != ''}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                ${errMessage}
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${successMessage != null}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="alert">
                                ${successMessage}
                            </div>
                        </div>
                    </div>
                </c:if>
            </center>
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function openPass(obj) {
        if (obj.checked) {
            $("#password").prop('disabled', false);
        } else {
            $("#password").prop('disabled', true);
        }
    }
</script>