<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>

<%
    String ADMIN_EDIT_KEY = (String) request.getSession().getAttribute("ADMIN_EDIT_KEY");
    String ADMIN_EXPORT_KEY = (String) request.getSession().getAttribute("ADMIN_EXPORT_KEY");
%>

<form id="keyDetailForm" name='keyDetailForm' action="exportKey.do" method='POST' accept-charset="UTF-8">
    <input type="hidden" value="${AESUtil.encryption(keysBO.rsaId)}" name="idKeyRsaEx"/>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Xem</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                        Index
                    </label>
                    <div class="col-lg-4">
                        <select disabled="true" id="index" class="form-control" name="rsaIndex"
                                required="" require-message="${MessageUtils.getMessage("rsa.require.select.index")}">
                            <c:if test="${keysBO != null}">
                                <option value="${StringUtils.paddingIndex(keysBO.rsaIndex)}">${StringUtils.paddingIndex(keysBO.rsaIndex)}</option>
                            </c:if>
                        </select>
                        <input type="hidden" class="form-control" id="rsaId" placeholder="" name="rsaId" value="${keysBO.rsaId}">
                    </div>
                    <div class="col-lg-4">
                        <select id="index" class="form-control" name="slotHsm" disabled="true" 
                                required require-message="${MessageUtils.getMessage("rsa.require.select.slot")}">
                            <option value="">Slot HSM</option>
                            <c:forEach items="${slotLst}" var="configHsmBO">
                                <option value="${configHsmBO.slot}" ${keysBO.slotHsm==configHsmBO.slot ? "selected":""} >${configHsmBO.slot}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="rsaStatus" disabled="true">
                            <option value="${keysBO.rsaStatus}" ${keysBO.rsaStatus==0 ? 'selected' : ''}>Hoạt động</option>
                            <option value="${keysBO.rsaStatus}" ${keysBO.rsaStatus==1 ? 'selected' : ''}>Ngừng hoạt động</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Public key exponent (Hex)
                    </label>
                    <div class="col-lg-8">
                        <textarea rows="3" class="form-control" id="publicKeyExponent" placeholder="" disabled="true">${keysBO.publicKeyExponent}</textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyModulus" style="text-align:right;">
                        Public key modulus (Hex)
                    </label>
                    <div class="col-lg-8">
                        <textarea rows="3" class="form-control" id="publicKeyModulus" placeholder="" disabled="true">${keysBO.publicKeyModulus}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="length" style="text-align:right;">
                        Độ dài khóa
                    </label>
                    <div class="col-lg-8">
                        <div class="input-group">
                            <input  type="text" id="rsaLength"  class="form-control " name="rsaLength" value=" ${keysBO.rsaLength}" disabled="true"/>
                            <div class="input-group-addon">bit</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn
                    </label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" readonly="true"  value="${keysBO.expirationDate}" placeholder="dd/MM/yyyy" disabled="true">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="content" style="text-align:right;">
                        Mô tả
                    </label>
                    <div class="col-lg-10">
                        <textarea style="margin-left: -5px; width: 101%;" class="form-control" id="content" placeholder="" disabled="true">${keysBO.checksumContent}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin khác</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label  class="control-label col-md-4" for="createUser" style="text-align:right;">
                        Người tạo
                    </label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="createUser" value="${keysBO.createUser}" placeholder="" name="createUser" disabled="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                        Ngày tạo
                    </label>
                    <div class="col-lg-8">
                        <input name="createDate" type="text" class="form-control" id="createDate" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(keysBO.createDate)}" placeholder="" disabled="true">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label  class="control-label col-md-4" for="updateUser" style="text-align:right;">
                        Người cập nhật
                    </label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="updateUser" value="${keysBO.updateUser}" placeholder="" name="updateUser" disabled="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                        Ngày cập nhật
                    </label>
                    <div class="col-lg-8">
                        <input name="updateDate" type="text" class="form-control" id="updateDate" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(keysBO.updateDate)}" placeholder="" disabled="true">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <c:if test="${ADMIN_EXPORT_KEY == 'true'}">
                                <button class="btn btn-success" style="width: 150px;" type="submit">Export</button>
                            </c:if>
                            <c:if test="${ADMIN_EDIT_KEY == 'true'}">
                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalEdit('${AESUtil.encryption(keysBO.rsaId)}');">Sửa</button>
                            </c:if>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('#expDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy'
        });
    });
</script>