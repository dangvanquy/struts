<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form modelAttribute="userForm" action="createAccount.do" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Thêm mới</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="fullname" style="text-align:right;">
                        Tên người dùng <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input maxlength="100" type="text" name="fullName" class="form-control" id="fullname" placeholder="" required=""
                               require-message="${MessageUtils.getMessage("user.fullname.not.empty")}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="phoneNumber" style="text-align:right;">
                        Số điện thoại
                    </label>
                    <div class="col-lg-8">
                        <input maxlength="15" type="text" name="phoneNumber" class="form-control phone-number" id="phoneNumber" placeholder="">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="username" style="text-align:right;">
                        Tài khoản <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input maxlength="100" type="text" name="username" class="form-control character" id="username" placeholder=""
                               required="" require-message="${MessageUtils.getMessage("user.username.not.empty")}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="email" style="text-align:right;">
                        Email <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input maxlength="100" type="email" name="email" class="form-control" id="email" placeholder=""
                               required="" require-message="${MessageUtils.getMessage("user.email.not.empty")}"
                               type-message="${MessageUtils.getMessage("email.config.validate.unformatted")}">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="password" style="text-align:right;">
                        Mật khẩu <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <input maxlength="30" type="password" name="password" class="form-control password" id="password" placeholder=""
                               required="" require-message="${MessageUtils.getMessage("user.password.not.empty")}"
                               type-message="${MessageUtils.getMessage("change.password.validate.lengh")}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="permission" style="text-align:right;">
                        Nhóm quyền <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <select id="permission" class="form-control" name="groupId" style="text-align: left" 
                               data-rel="chosen" required="" require-message="${MessageUtils.getMessage("user.require.select.group.permission")}">
                            <option value="">---Chọn nhóm quyền--- </option>
                            <c:forEach items="${groupsList}" var="groupsPermissionBO" varStatus="itr">
                                <option value="${groupsPermissionBO.groupId}">${groupsPermissionBO.groupName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="status">
                            <option value="0">Hoạt động</option>
                            <option value="1">Khóa</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="note" style="text-align:right;">
                        Ghi chú
                    </label>
                    <div class="col-lg-10">
                        <textarea style="margin-left: -5px; width: 101%;" maxlength="200" class="form-control" name="note" id="note" placeholder=""></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <center>
                <c:if test="${errMessage != null && errMessage != ''}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                ${errMessage}
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${successMessage != null}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-success" role="alert">
                                ${successMessage}
                            </div>
                        </div>
                    </div>
                </c:if>
            </center>
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
