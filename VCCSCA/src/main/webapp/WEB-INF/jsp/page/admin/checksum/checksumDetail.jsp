<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%
    String ADMIN_EDIT_CHECKSUM = (String) request.getSession().getAttribute("ADMIN_EDIT_CHECKSUM");
%>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Xem</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                        Index
                    </label>
                    <div class="col-lg-8">
                        <select id="index" class="form-control" name="index" disabled="true">
                            <option value="${StringUtils.paddingIndex(keysBO.rsaIndex)}">${StringUtils.paddingIndex(keysBO.rsaIndex)}</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn
                    </label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control " id="expDate" value="${keysBO.expirationDate}" disabled="true">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Public key exponent (Hex)
                    </label>
                    <div class="col-lg-8">
                        <input class="form-control" id="rsaLength"  type="text" name="" disabled="true" value="${keysBO.publicKeyExponent}" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Độ dài khóa
                    </label>
                    <div class="col-lg-8">
                        <div class="input-group">
                            <div style="float: left;width: 40px">
                                <p style="color: red;font-weight: bold;" id="createIpkLengh">${keysBO.rsaLength}</p>
                            </div>
                            <div style="float: left;">
                                <p>(bit)</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyModulus" style="text-align:right;">
                        Public key modulus (Hex)
                    </label>
                    <div class="col-lg-8">
                        <textarea rows="3" class="form-control" id="publicKeyModulus" disabled="true">${keysBO.publicKeyModulus}</textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="checksum" style="text-align:right;">
                        Privatekey Checksum (SHA256)
                    </label>
                    <div class="col-lg-8">
                        <textarea rows="3" class="form-control" id="checksum" disabled="true">${keysBO.checksumName}</textarea>
                    </div>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <input class="form-control"  type="text" disabled="true" value="${keysBO.checksumStatus==0?'Hoạt động':'Ngừng hoạt động'}" />

                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="content" style="text-align:right;">
                        Mô tả
                    </label>
                    <div class="col-lg-10">
                        <textarea style="margin-left: -5px; width: 101%;" class="form-control" id="content" disabled="true">${keysBO.checksumContent}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin khác</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label  class="control-label col-md-4" for="createUser" style="text-align:right;">
                        Người tạo
                    </label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="createUser" value="${keysBO.createCsUser}" placeholder="" name="createUser" disabled="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                        Ngày tạo
                    </label>
                    <div class="col-lg-8">
                        <input disabled="true" name="createDate" type="text" pattern="dd/mm/yyyy hh:mm:ss.s" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(keysBO.createCsDate)}" class="form-control" id="createDate" >
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label  class="control-label col-md-4" for="updateUser" style="text-align:right;">
                        Người cập nhật
                    </label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="updateUser" value="${keysBO.updateCsUser}" placeholder="" name="updateUser" disabled="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                        Ngày cập nhật
                    </label>
                    <div class="col-lg-8">
                        <input name="updateDate" type="text" class="form-control" id="updateDate" value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(keysBO.updateCsDate)}" disabled="true">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <c:if test="${ADMIN_EDIT_CHECKSUM == 'true'}">
                                <c:if test="${keysBO.rsaStatus != 0 && keysBO.rsaStatus != 1}">
                                    <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalEdit('${AESUtil.encryption(keysBO.rsaId)}');">Sửa</button>
                                </c:if>
                            </c:if>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
//    $(document).ready(function () {
//        $('#expDate').datepicker({
//            uiLibrary: 'bootstrap',
//            format: 'dd/mm/yyyy'
//        });
//    });
</script>