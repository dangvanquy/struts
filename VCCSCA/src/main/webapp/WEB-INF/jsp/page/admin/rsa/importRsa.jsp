<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>

<form name='rsaKeyForm' modelAttribute="rsaKeyForm" action="importKey.do" method='POST' accept-charset="UTF-8"
      onsubmit="return checkPrivateKeyExponent();">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Import</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="rsaIndex" style="text-align:left;">
                        Index <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-4">
                        <select id="rsaIndex" class="form-control" name="rsaIndex" data-rel="chosen" onchange="loadContent()"
                                required require-message="${MessageUtils.getMessage("rsa.require.select.index")}">
                            <option value="">---Chọn---</option>
                            <c:forEach items="${listKeyBo}" var="listKeyBo">
                                <option value="${listKeyBo.rsaIndex}" ${rsaKeyForm.rsaIndex == listKeyBo.rsaIndex ? 'selected':''}>${StringUtils.paddingIndex(listKeyBo.rsaIndex)}</option>
                            </c:forEach>
                        </select>
                        <input type="hidden" id="rsaId" name="rsaId" value=""/>
                        <c:forEach items="${listKeyBo}" var="listKeyBo">
                            <input type="hidden" id="publicKeyE_${listKeyBo.rsaIndex}" value="${listKeyBo.publicKeyExponent}"/>
                            <input type="hidden" id="publicKeyM_${listKeyBo.rsaIndex}" value="${listKeyBo.publicKeyModulus}"/>
                            <input type="hidden" id="publicMonth_${listKeyBo.rsaIndex}" value="${listKeyBo.expirationDate.substring(0, 2)}"/>
                            <input type="hidden" id="publicYeah_${listKeyBo.rsaIndex}" value="${listKeyBo.expirationDate.substring(3, 7)}"/>
                            <input type="hidden" id="r_id_${listKeyBo.rsaIndex}" value="${listKeyBo.rsaId}"/>
                        </c:forEach>
                    </div>
                    <div class="col-lg-4">
                        <select id="index" class="form-control" name="slotHsm" 
                                required require-message="${MessageUtils.getMessage("rsa.require.select.slot")}">
                            <option value="">Slot HSM</option>
                            <c:forEach items="${slotLst}" var="configHsmBO">
                                <option value="${configHsmBO.slot}">${configHsmBO.slot}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn
                    </label>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control" name="month" id="monthID" disabled="true"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control" name="year" id="yearID" disabled="true" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:left;">
                        Public key Exponent (Hex)
                    </label>
                    <div class="col-md-8">
                        <textarea
                            rows="3" 
                            readonly="true" 
                            class="form-control" 
                            style="width: 100%;" 
                            name="publicKeyExponent" 
                            id="publicKeyExponent" 
                            value="${rsaKeyForm.publicKeyExponent}"></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyModulus" style="text-align:right;">
                        Public key modulus (Hex)
                    </label>
                    <div class="col-md-8">
                        <textarea rows="3" readonly="true" class="form-control" style="width: 100%;" name="publicKeyModulus" id="publicKeyModulus" value="${rsaKeyForm.publicKeyModulus}"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="privateKeyExponent1" style="text-align:left;">
                        Private key Exponent 1 (Hex)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <textarea  pattern="[A-Fa-f0-9]+" rows="3" class="form-control rsa" style="width: 100%;" 
                                   name="privateKeyExponent1" id="privateKeyExponent1" onkeyup="getlen(1)" onblur="getlen(1)"
                                   required require-message="${MessageUtils.getMessage("rsa.validate.prie.null")}"
                                   ></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="privateKeyLength1" style="text-align:right;">
                        Độ dài khóa 1
                    </label>
                    <div class="col-md-8">
                        <!--<input type="text" class="form-control" name="privateKeyLength1" id="privateKeyLength1">-->
                        <div style="float: left;width: 20%">
                            <p style="color: red;font-weight: bold;" name="privateKeyLength1" id="privateKeyLength1"/>
                        </div>
                        <div style="float: left;width: 10%">
                            <p>(bit)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="privateKeyExponent2" style="text-align:left;">
                        Private key Exponent 2 (Hex)<span class="required rsa" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <textarea rows="3" class="form-control rsa" style="width: 100%;" name="privateKeyExponent2" id="privateKeyExponent2" onkeyup="getlen(2)" onblur="getlen(2)"
                                  required require-message="${MessageUtils.getMessage("rsa.validate.prie.null")}"
                                  ></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="privateKeyLength2" style="text-align:right;">
                        Độ dài khóa 2
                    </label>
                    <div class="col-md-8">
                        <!--<input type="text" class="form-control" name="privateKeyLength2" id="privateKeyLength2">-->
                        <div style="float: left;width: 20%">
                            <p style="color: red;font-weight: bold;" name="privateKeyLength2" id="privateKeyLength2"/>
                        </div>
                        <div style="float: left;width: 10%">
                            <p>(bit)</p>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="privateKeyLength2" style="text-align:right;">
                        Tổng độ dài
                    </label>
                    <div class="col-md-8">
                        <div style="float: left;width: 20%">
                            <p style="color: red;font-weight: bold;" name="privateKeyLength" id="privateKeyLength"/>
                        </div>
                        <div style="float: left;width: 10%">
                            <p>(bit)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Import</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

<form style="display: none" id="rsaKeyForm" action="import-rsa.html" method='POST'>
    <input type="hidden" name="index" id="index"/>
</form>

<script>
    $(document).ready(function () {
        $('#expirationDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy'
        });
    });

    function getlen(check) {
        if (check == 1) {
            $('#privateKeyLength1').text($('#privateKeyExponent1').val().length * 4);
        } else {
            $('#privateKeyLength2').text($('#privateKeyExponent2').val().length * 4);
        }
        $('#privateKeyLength').text(($('#privateKeyExponent1').val().length * 4) + ($('#privateKeyExponent2').val().length * 4));

    }
    ;

    function loadContent() {
        var index = $('#rsaIndex').val();
        if (index != "") {
            $('#publicKeyExponent').val($('#publicKeyE_' + index).val());
            $('#publicKeyModulus').val($('#publicKeyM_' + index).val());
            $('#monthID').val($('#publicMonth_' + index).val());
            $('#yearID').val($('#publicYeah_' + index).val());

            $('#rsaId').val($('#r_id_' + index).val());
        }
    }
    ;

//    function checkPrivateKeyExponent() {
//
//        var key1 = $("#privateKeyExponent1").val() + $("#privateKeyExponent2").val();
//
//        if (key1.length % 2 !== 0) {
//            if ($("#privateKeyExponent1").val() !== "") {
//                var key = $("#privateKeyExponent1").val();
//                if (key.length % 2 !== 0) {
//                    $("#privateKeyExponent1").attr("spec-message", "Mã Private key exponent không chính xác");
//                    $("#privateKeyExponent1")[0].setCustomValidity("Mã Private key exponent không chính xác");
//                     $("#privateKeyExponent1")[0].reportValidity();
//                    return false;
//                } else {
//                    $("#privateKeyExponent1").removeAttr("spec-message");
//                }
//            }
//            if ($("#privateKeyExponent2").val() !== "") {
//                var key = $("#privateKeyExponent2").val();
//                if (key.length % 2 !== 0) {
//                    $("#privateKeyExponent2").attr("spec-message", "Mã Private key exponent không chính xác");
//                    $("#privateKeyExponent2")[0].setCustomValidity("Mã Private key exponent không chính xác");
//                            $("#privateKeyExponent2")[0].reportValidity();
//                    return false;
//                } else {
//                    $("#privateKeyExponent2").removeAttr("spec-message");
//                }
//            }
//        }
//        return true;
//    }
</script>