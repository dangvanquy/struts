<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>


<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <form id="changePass" name='changePasswordForm' action="changePass.do" modelAttribute="ChangePasswordForm" method='POST'>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h1 class="panel-title text-left">Đổi mật khẩu</h1>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-9" for="oldPassword" style="text-align:left;">
                                                                Mật khẩu cũ <font style="color: red">*</font>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="password" class="form-control col-md-5" name="oldPassword" id="oldPassword" placeholder="Old password" required
                                                                       require-message="${MessageUtils.getMessage("change.password.old.validate.empty")}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-9" for="newPassword" style="text-align:left;">
                                                                Mật khẩu mới <font style="color: red">*</font>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="password" class="form-control col-md-5 password" name="newPassword" id="newPassword" placeholder="New password" required
                                                                       require-message="${MessageUtils.getMessage("change.password.new.validate.empty")}"
                                                                       type-message="${MessageUtils.getMessage("change.password.validate.lengh")}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-9" for="confirmPassword" style="text-align:left;">
                                                                Nhập lại mật khẩu <font style="color: red">*</font>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="password" class="form-control col-md-5 password" name="confirmPassword" id="confirmPassword" placeholder="Confirm password" required
                                                                       require-message="${MessageUtils.getMessage("change.password.confirm.validate.empty")}"
                                                                       type-message="${MessageUtils.getMessage("change.password.confirm.validate.not.equal")}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <center>
                                                            <c:if test="${errMessage != null}">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="alert alert-danger" role="alert">
                                                                            ${errMessage}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </c:if>
                                                            <c:if test="${successMessage != null}">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="alert alert-success" role="alert">
                                                                            ${successMessage}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </c:if>
                                                        </center>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-success form-control" type="button" onclick="alertPopup()">
                                                                    <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Thay đổi</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="hidden" id="chgPassSubmit" style="visibility: hidden" type="submit"></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function submitForm() {
        $('#alertUpdate').modal('hide');
        $('#chgPassSubmit').trigger("click");
    }
    function alertPopup() {
        $('#alertUpdate').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
</script>
<!-- Modal -->
<div class="modal fade" id="alertUpdate" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn thay đổi Mật khẩu?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-primary" onclick="submitForm()">Cập nhật</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>