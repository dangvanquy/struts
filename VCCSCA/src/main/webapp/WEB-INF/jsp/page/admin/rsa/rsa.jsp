<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<script src="resources/gijgo/js/gijgo.min.js" type="text/javascript"></script>
<link href="resources/gijgo/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<%
    String ADMIN_ADD_KEY = (String) request.getSession().getAttribute("ADMIN_ADD_KEY");
    String ADMIN_EDIT_KEY = (String) request.getSession().getAttribute("ADMIN_EDIT_KEY");
    String ADMIN_IMPORT_KEY = (String) request.getSession().getAttribute("ADMIN_IMPORT_KEY");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý khóa RSA
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='rsaForm' modelAttribute="keysForm" action="quan-ly-rsa.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                                                        Index
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="index" class="form-control" name="rsaIndex">
                                                            <option value="">---Chọn Index---</option>
                                                            <c:forEach items="${allKeysList}" var="allKeyBO">
                                                                <option value="${allKeyBO}" ${keysForm.rsaIndex==allKeyBO ? "selected" : ""} >${StringUtils.paddingIndex(allKeyBO)}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="rsaStatus" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="rsaStatus" class="form-control" name="rsaStatus">
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${keysForm.rsaStatus==0 ? "selected" : ""}>Hoạt động</option>
                                                            <option value="1" ${keysForm.rsaStatus==1 ? "selected" : ""}>Ngừng hoạt động</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${ADMIN_ADD_KEY == 'true'}">
                                                                <button class="btn btn-success" style="width: 160px;" type="button" onclick="openModalCreate();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới từ HSM</button>
                                                            </c:if>
                                                            <c:if test="${ADMIN_IMPORT_KEY == 'true'}">
                                                                <button class="btn btn-success" style="width: 200px;" type="button" onclick="openModalImport();"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Nhập thành phần bí mật</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null && errMessage != ''}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách khóa RSA</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Index</th>
                                                        <th style="text-align: center">Public key exponent</th>
                                                        <th style="text-align: center">Public key modulus</th>
                                                        <th style="text-align: center">Độ dài khóa</th>
                                                        <th style="text-align: center">SLOT HSM</th>
                                                        <th style="text-align: center">Hình thức tạo khóa</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${keysList}" var="keysBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index +1 }</td>
                                                            <td>${StringUtils.paddingIndex(keysBO.rsaIndex)}</td>
                                                            <td><input type="text" id="e1_${StringUtils.paddingIndex(keysBO.rsaIndex)}" value="${keysBO.publicKeyExponent}" style="border: 0px;    background: #ffffff00;" readonly="true" /> </td>
                                                            <td><input type="text" id="m1_${StringUtils.paddingIndex(keysBO.rsaIndex)}" value="${keysBO.publicKeyModulus}" style="border: 0px;    background: #ffffff00;" readonly="true" /> </td>
                                                            <td>${keysBO.rsaLength}</td>
                                                            <td style="text-align: center">${keysBO.slotHsm}</td>
                                                            <td>${keysBO.typeKey}</td>
                                                            <td>${keysBO.rsaStatus==0?'Hoạt động':'Ngừng hoạt động'}</td>
                                                            <td>
                                                    <center>
                                                        <a href="#" onclick="openModalDetail('${AESUtil.encryption(keysBO.rsaId)}');"
                                                           data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                        <c:if test="${ADMIN_EDIT_KEY == 'true'}">
                                                            <a href="#" onclick="openModalEdit('${AESUtil.encryption(keysBO.rsaId)}');"
                                                               data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                            <c:if test="${keysBO.rsaStatus==0}">
                                                                <a href="#" onclick="lock('${AESUtil.encryption(keysBO.rsaId)}', '${AESUtil.encryption(keysBO.rsaIndex)}')"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Khóa"><img border=3 style="margin-bottom: 9px" height="20" width="20" src="resources/napas_theme/images/icon-unlock.png" alt=""/></a>
                                                                </c:if>
                                                                <c:if test="${keysBO.rsaStatus==1}">
                                                                <a href="#" onclick="unlock('${AESUtil.encryption(keysBO.rsaId)}', '${AESUtil.encryption(keysBO.rsaIndex)}')"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Mở khóa"><img border=3 style="margin-bottom: 9px" height="20" width="20" src="resources/napas_theme/images/icon-lock.png" alt=""/></a>
                                                                </c:if>
                                                            </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="createKeyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="createKeyId">
        </div>
    </div>
</div>

<div id="editKeyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
    <div id="editKeyId" class="modal-dialog modal-lg">
    </div>
</div>
<div id="keyDetailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
    <div id="keyDetailId" class="modal-dialog modal-lg">
    </div>
</div>
<div id="importKeyModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
    <div id="importKeyModalId" class="modal-dialog modal-lg">
    </div>
</div>

<div class="modal fade" id="alertLock" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn các Dừng hoạt động của khóa RSA
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form id="formLockRSAKey" action="lockRsaKey.do" method="POST">
                        <input type="hidden" name="rsaId" id="rsaLockId"/>
                        <input type="hidden" name="rsaIndex" id="rsaLockIndex"/>
                        <button type="submit" class="btn btn-primary" >Khóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>

                </center>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alertUnLock" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn các mở khóa khóa RSA
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form id="formUnlockRSAKey" action="unlockRsaKey.do" method="POST">
                        <input type="hidden" name="rsaId" id="rsaUnlockId"/>
                        <input type="hidden" name="rsaIndex" id="rsaUnlockIndex"/>
                        <button type="submit" class="btn btn-primary" >Mở Khóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>




<script>
    function openModalCreate() {
        $.get("tao-moi-rsa.html", function (data) {
            $("#createKeyId").html(data);
            $('#createKeyModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalDetail(rsaId) {
        $('#keyDetailModal').modal('hide');
        $.get("xem-chi-tiet-rsa.html", {rsaId: rsaId}, function (data) {
            $("#keyDetailId").html(data);
            $('#keyDetailModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalEdit(rsaId) {
        $('#keyDetailModal').modal('hide');
        $.get("chinh-sua-rsa.html", {rsaId: rsaId}, function (data) {
            $("#editKeyId").html(data);
            $('#editKeyModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalImport() {
        $.get("import-rsa.html", function (data) {
            $("#importKeyModalId").html(data);
            $('#importKeyModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
        $('#importKeyModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#rsaIndex').next('div').addClass("showfull");
    }
    ;

    function lock(rsaId, rsaIndex) {
        $("#rsaLockId").val(rsaId);
        $("#rsaLockIndex").val(rsaIndex);
         $('#alertLock').modal({backdrop: 'static', keyboard: false});//.modal('show')
//        $("#formLockRSAKey").submit();
    }
    ;
    function unlock(rsaId, rsaIndex) {
        $("#rsaUnlockId").val(rsaId);
        $("#rsaUnlockIndex").val(rsaIndex);
         $('#alertUnLock').modal({backdrop: 'static', keyboard: false});//.modal('show')
//        $("#formUnlockRSAKey").submit();
    }
    ;

    function loadpbk() {
        var idrsa = $("#indexRsa").val();
        $("#publicKeyExponent").val($("#e1_" + idrsa).val())
        $("#publicKeyModulus").val($("#m1_" + idrsa).val())
    }

</script>
