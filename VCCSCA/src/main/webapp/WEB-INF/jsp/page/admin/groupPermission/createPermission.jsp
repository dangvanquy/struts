<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form id="saveConfigEmailId" name='groupPermissionForm' action="createGroupPermission.do" modelAttribute="groupPermissionForm" method='POST'
      accept-charset="UTF-8" style="margin: 10px;"
      onsubmit="return checkPermisionList()"
      >
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Thêm mới nhóm quyền</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left">Thông tin chung</h1>
                </div>
            </div>
            <div class="form-group row">

                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        Tên nhóm quyền<font style="color: red">*</font>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control removespecchar" id="permissionName" placeholder="" name="groupName"
                                   maxlength="100" required="" require-message="${MessageUtils.getMessage("permission.name.not.empty")}">
                        </div>
                    </div>
                    <div class=" col-md-2 text-right label-text">
                        Trạng thái
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="status" class="form-control" name="status" >
                                <option value="0" selected>Hoạt động</option>
                                <option value="1" >Không hoạt động</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        Ghi chú
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <textarea class="form-control" id="content" placeholder="" name="note" maxlength="200"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Danh sách quyền</h1>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        <th style="text-align: center"></th>
                        <th style="text-align: center">#</th>
                        <th style="text-align: center">Xem</th>
                        <th style="text-align: center">Thêm</th>
                        <th style="text-align: center">Sửa</th>
                        <th style="text-align: center">Xóa</th>
                        <th style="text-align: center">Trình Ký/Duyệt</th>
                        <th style="text-align: center">Ký/Duyệt/Từ chối</th>
                        <th style="text-align: center">Thu hồi</th>
                        <th style="text-align: center">Khôi phục</th>
                        <!--<th style="text-align: center">Verify</th>-->
                        <th style="text-align: center">Import</th>
                        <th style="text-align: center">Export</th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    <tr style="text-align: center">
                        <td style="text-align: left">#</td>
                        <td></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 1)" onclick="selectAllColume(this, 1)" id="ck_r_1_c_1" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 2)" id="ck_r_1_c_2" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 3)" id="ck_r_1_c_3" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 4)" id="ck_r_1_c_4" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 5)" id="ck_r_1_c_5" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 6)" id="ck_r_1_c_6" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 7)" id="ck_r_1_c_7" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 8)" id="ck_r_1_c_8" type="checkbox" /></td>
                        <!--<td style="text-align: center"><input onclick="selectAllColume(this, 9)" id="ck_r_1_c_9" type="checkbox" /></td>-->
                        <td style="text-align: center"><input onclick="selectAllColume(this, 10)" id="ck_r_1_c_10" type="checkbox" /></td>
                        <td style="text-align: center"><input onclick="selectAllColume(this, 11)" id="ck_r_1_c_11" type="checkbox" /></td>

                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý yêu cầu</td>
                        <td style="text-align: center"><input id="ck_r_2_c_0" onclick="selectAllRow(this, 2)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_2_c_1" type="checkbox" name="grPermissions" value="request_view"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_2" type="checkbox" name="grPermissions" value="request_add"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_3" type="checkbox" name="grPermissions" value="request_edit"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_4" type="checkbox" name="grPermissions" value="request_delete"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_5" type="checkbox" name="grPermissions" value="request_submission"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_6" type="checkbox" name="grPermissions" value="request_approval"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_2_c_8" type="checkbox" name="grPermissions" value="request_restore"/></td>-->
                        <!--<td style="text-align: center"><input id="ck_r_2_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_2_c_10" type="checkbox" name="grPermissions" value="request_import"/></td>
                        <td style="text-align: center"><input id="ck_r_2_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Tạo file mẫu cấp CTS</td>
                        <td style="text-align: center"><input id="ck_r_3_c_0" onclick="selectAllRow(this, 3)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_3_c_1" type="checkbox" name="grPermissions" value="request_view_index"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_2" type="checkbox" name="grPermissions" value="request_create_index"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_3_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_3_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_3_c_11" type="checkbox" name="grPermissions" value="request_export_index"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý chứng thư số</td>
                        <td style="text-align: center"><input id="ck_r_4_c_0" onclick="selectAllRow(this, 4)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_4_c_1" type="checkbox" name="grPermissions" value="certificate_view"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_7" type="checkbox" name="grPermissions" value="certificate_recall"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_8" type="checkbox" name="grPermissions" value="certificate_restore"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_4_c_9" type="checkbox" name="grPermissions" value="certificate_verify"/></td>-->
                        <td style="text-align: center"><input id="ck_r_4_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_4_c_11" type="checkbox" name="grPermissions" value="certificate_export"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Xác nhận chứng thư số</td>
                        <td style="text-align: center"><input id="ck_r_20_c_0" onclick="selectAllRow(this, 20)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_20_c_1" type="checkbox" name="grPermissions" value="certificate_verify"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_2" type="checkbox" name="grPermissions" value="certificate_verify_create"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_4_c_9" type="checkbox" name="grPermissions" value="certificate_verify"/></td>-->
                        <td style="text-align: center"><input id="ck_r_20_c_10" type="checkbox" name="grPermissions" value="certificate_import"/></td>
                        <td style="text-align: center"><input id="ck_r_20_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý ngân hàng thành viên</td>
                        <td style="text-align: center"><input id="ck_r_5_c_0" onclick="selectAllRow(this, 5)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_5_c_1" type="checkbox" name="grPermissions" value="bankmembership_view"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_2" type="checkbox" name="grPermissions" value="bankmembership_add"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_3" type="checkbox" name="grPermissions" value="bankmembership_edit"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_4" type="checkbox" name="grPermissions" value="bankmembership_delete"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_5_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_5_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_5_c_11" type="checkbox" disabled="true"/></td>
                    </tr>

                    <!--=======================================================================================-->
                    <tr style="text-align: center">
                        <td style="text-align: left">Báo cáo Root CA</td>
                        <td style="text-align: center"><input id="ck_r_6_c_0" onclick="selectAllRow(this, 6)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_6_c_1" type="checkbox" name="grPermissions" value="report_view_rootca" ${groupPermissionForm.getPermission("report_view_rootca")} /></td>
                        <td style="text-align: center"><input id="ck_r_6_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_6_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_6_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_6_c_11" type="checkbox" name="grPermissions"  value="report_export_rootca" ${groupPermissionForm.getPermission("report_export_rootca")} /></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Báo cáo chứng thư số</td>
                        <td style="text-align: center"><input id="ck_r_7_c_0" onclick="selectAllRow(this, 7)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_7_c_1" type="checkbox" name="grPermissions" value="report_view_cert" ${groupPermissionForm.getPermission("report_view_cert")} /></td>
                        <td style="text-align: center"><input id="ck_r_7_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_7_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_7_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_7_c_11" type="checkbox" name="grPermissions" value="report_export_cert" ${groupPermissionForm.getPermission("report_export_cert")} /></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Báo cáo yêu cầu cấp CTS</td>
                        <td style="text-align: center"><input id="ck_r_8_c_0" onclick="selectAllRow(this, 8)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_8_c_1" type="checkbox" name="grPermissions" value="report_view_request" ${groupPermissionForm.getPermission("report_view_request")} /></td>
                        <td style="text-align: center"><input id="ck_r_8_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_8_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_8_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_8_c_11" type="checkbox" name="grPermissions" value="report_export_request" ${groupPermissionForm.getPermission("report_export_request")} /></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Báo cáo NHTV</td>
                        <td style="text-align: center"><input id="ck_r_9_c_0" onclick="selectAllRow(this, 9)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_9_c_1" type="checkbox" name="grPermissions" value="report_view_nhtv" ${groupPermissionForm.getPermission("report_view_nhtv")} /></td>
                        <td style="text-align: center"><input id="ck_r_9_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_9_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_9_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_9_c_11" type="checkbox" name="grPermissions"  value="report_export_nhtv" ${groupPermissionForm.getPermission("report_export_nhtv")} /></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý tài khoản</td>
                        <td style="text-align: center"><input id="ck_r_10_c_0" onclick="selectAllRow(this, 10)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_10_c_1" type="checkbox" name="grPermissions"  value="admin_view_account" ${groupPermissionForm.getPermission("admin_view_account")} /></td>
                        <td style="text-align: center"><input id="ck_r_10_c_2" type="checkbox" name="grPermissions"  value="admin_add_account" ${groupPermissionForm.getPermission("admin_add_account")} /></td>
                        <td style="text-align: center"><input id="ck_r_10_c_3" type="checkbox" name="grPermissions"  value="admin_edit_account" ${groupPermissionForm.getPermission("admin_edit_account")} /></td>
                        <td style="text-align: center"><input id="ck_r_10_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_10_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_10_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_10_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_10_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_10_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_10_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_10_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý nhóm quyền</td>
                        <td style="text-align: center"><input id="ck_r_11_c_0" onclick="selectAllRow(this, 11)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_11_c_1" type="checkbox" name="grPermissions"  value="admin_view_grouppermision" ${groupPermissionForm.getPermission("admin_view_grouppermision")} /></td>
                        <td style="text-align: center"><input id="ck_r_11_c_2" type="checkbox" name="grPermissions"  value="admin_add_grouppermision" ${groupPermissionForm.getPermission("admin_add_grouppermision")} /></td>
                        <td style="text-align: center"><input id="ck_r_11_c_3" type="checkbox" name="grPermissions"  value="admin_edit_grouppermision" ${groupPermissionForm.getPermission("admin_edit_grouppermision")} /></td>
                        <td style="text-align: center"><input id="ck_r_11_c_4" type="checkbox" name="grPermissions"  value="admin_delete_grouppermision" ${groupPermissionForm.getPermission("admin_delete_grouppermision")} /></td>
                        <td style="text-align: center"><input id="ck_r_11_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_11_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_11_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_11_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_11_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_11_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_11_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Cấu hình cảnh báo</td>
                        <td style="text-align: center"><input id="ck_r_12_c_0" onclick="selectAllRow(this, 12)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_12_c_1" type="checkbox" name="grPermissions"  value="admin_view_alert" ${groupPermissionForm.getPermission("admin_view_alert")} /></td>
                        <td style="text-align: center"><input id="ck_r_12_c_2" type="checkbox" name="grPermissions"  value="admin_add_alert" ${groupPermissionForm.getPermission("admin_add_alert")} /></td>
                        <td style="text-align: center"><input id="ck_r_12_c_3" type="checkbox" name="grPermissions"  value="admin_edit_alert" ${groupPermissionForm.getPermission("admin_edit_alert")} /></td>
                        <td style="text-align: center"><input id="ck_r_12_c_4" type="checkbox" name="grPermissions"  value="admin_delete_alert" ${groupPermissionForm.getPermission("admin_delete_alert")} /></td>
                        <td style="text-align: center"><input id="ck_r_12_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_12_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_12_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_12_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_12_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_12_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_12_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý khóa RSA</td>
                        <td style="text-align: center"><input id="ck_r_13_c_0" onclick="selectAllRow(this, 13)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_13_c_1" type="checkbox" name="grPermissions"  value="admin_view_key" ${groupPermissionForm.getPermission("admin_view_key")} /></td>
                        <td style="text-align: center"><input id="ck_r_13_c_2" type="checkbox" name="grPermissions"  value="admin_add_key" ${groupPermissionForm.getPermission("admin_add_key")} /></td>
                        <td style="text-align: center"><input id="ck_r_13_c_3" type="checkbox" name="grPermissions"  value="admin_edit_key" ${groupPermissionForm.getPermission("admin_edit_key")} /></td>
                        <td style="text-align: center"><input id="ck_r_13_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_13_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_13_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_13_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_13_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_13_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_13_c_10" type="checkbox" name="grPermissions"  value="admin_import_key" ${groupPermissionForm.getPermission("admin_import_key")} /></td>
                        <td style="text-align: center"><input id="ck_r_13_c_11" type="checkbox" name="grPermissions"  value="admin_export_key" ${groupPermissionForm.getPermission("admin_export_key")} /></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Quản lý CheckSum và Public key</td>
                        <td style="text-align: center"><input id="ck_r_14_c_0" onclick="selectAllRow(this, 14)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_14_c_1" type="checkbox" name="grPermissions"  value="admin_view_checksum" ${groupPermissionForm.getPermission("admin_view_checksum")} /></td>
                        <td style="text-align: center"><input id="ck_r_14_c_2" type="checkbox" name="grPermissions"  value="admin_add_checksum" ${groupPermissionForm.getPermission("admin_add_checksum")} /></td>
                        <td style="text-align: center"><input id="ck_r_14_c_3" type="checkbox" name="grPermissions"  value="admin_edit_checksum" ${groupPermissionForm.getPermission("admin_edit_checksum")} /></td>
                        <td style="text-align: center"><input id="ck_r_14_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_14_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_14_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_14_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_14_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_14_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_14_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_14_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Danh sách yêu cầu đã xóa</td>
                        <td style="text-align: center"><input id="ck_r_15_c_0" onclick="selectAllRow(this, 15)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_15_c_1" type="checkbox" name="grPermissions"  value="admin_view_rqdelete" ${groupPermissionForm.getPermission("admin_view_rqdelete")} /></td>
                        <td style="text-align: center"><input id="ck_r_15_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_8" type="checkbox" name="grPermissions"  value="admin_recover_rqdelete" ${groupPermissionForm.getPermission("admin_recover_rqdelete")} /></td>
                        <!--<td style="text-align: center"><input id="ck_r_15_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_15_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_15_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Lịch sử truy cập</td>
                        <td style="text-align: center"><input id="ck_r_16_c_0" onclick="selectAllRow(this, 16)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_16_c_1" type="checkbox" name="grPermissions"  value="admin_view_history" ${groupPermissionForm.getPermission("admin_view_history")} /></td>
                        <td style="text-align: center"><input id="ck_r_16_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_3" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_16_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_16_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_16_c_11" type="checkbox" name="grPermissions"  value="admin_export_history" ${groupPermissionForm.getPermission("admin_export_history")} /></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Cấu hình tham số cảnh báo</td>
                        <td style="text-align: center"><input id="ck_r_17_c_0" onclick="selectAllRow(this, 17)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_17_c_1" type="checkbox" name="grPermissions"  value="admin_view_param" ${groupPermissionForm.getPermission("admin_view_param")} /></td>
                        <td style="text-align: center"><input id="ck_r_17_c_2" type="checkbox" name="grPermissions"  value="admin_add_param" ${groupPermissionForm.getPermission("admin_add_param")} /></td>
                        <td style="text-align: center"><input id="ck_r_17_c_3" type="checkbox" name="grPermissions"  value="admin_edit_param" ${groupPermissionForm.getPermission("admin_edit_param")} /></td>
                        <td style="text-align: center"><input id="ck_r_17_c_4" type="checkbox" name="grPermissions"  value="admin_delete_param" ${groupPermissionForm.getPermission("admin_delete_param")} /></td>
                        <td style="text-align: center"><input id="ck_r_17_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_17_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_17_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_17_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_17_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_17_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_17_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <tr style="text-align: center">
                        <td style="text-align: left">Cấu hình Email</td>
                        <td style="text-align: center"><input id="ck_r_18_c_0" onclick="selectAllRow(this, 18)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_18_c_1" type="checkbox" name="grPermissions"  value="admin_view_email" ${groupPermissionForm.getPermission("admin_view_email")} /></td>
                        <td style="text-align: center"><input id="ck_r_18_c_2" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_18_c_3" type="checkbox" name="grPermissions"  value="admin_edit_email" ${groupPermissionForm.getPermission("admin_edit_email")} /></td>
                        <td style="text-align: center"><input id="ck_r_18_c_4" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_18_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_18_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_18_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_18_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_18_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_18_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_18_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                    <!--=======================================================================================-->
                    <tr style="text-align: center">
                        <td style="text-align: left">Cấu hình HSM</td>
                        <td style="text-align: center"><input id="ck_r_19_c_0" onclick="selectAllRow(this, 19)" type="checkbox" /></td>
                        <td style="text-align: center"><input id="ck_r_19_c_1" type="checkbox" name="grPermissions"  value="admin_view_config_hsm" ${groupPermissionForm.getPermission("admin_view_config_hsm")} /></td>
                        <td style="text-align: center"><input id="ck_r_19_c_2" type="checkbox" name="grPermissions"  value="admin_add_config_hsm" ${groupPermissionForm.getPermission("admin_add_config_hsm")} /></td>
                        <td style="text-align: center"><input id="ck_r_19_c_3" type="checkbox" name="grPermissions"  value="admin_edit_config_hsm" ${groupPermissionForm.getPermission("admin_edit_config_hsm")} /></td>
                        <td style="text-align: center"><input id="ck_r_19_c_4" type="checkbox" name="grPermissions"  value="admin_delete_config_hsm" ${groupPermissionForm.getPermission("admin_delete_config_hsm")} /></td>
                        <td style="text-align: center"><input id="ck_r_19_c_5" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_19_c_6" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_19_c_7" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_19_c_8" type="checkbox" disabled="true"/></td>
                        <!--<td style="text-align: center"><input id="ck_r_19_c_9" type="checkbox" disabled="true"/></td>-->
                        <td style="text-align: center"><input id="ck_r_19_c_10" type="checkbox" disabled="true"/></td>
                        <td style="text-align: center"><input id="ck_r_19_c_11" type="checkbox" disabled="true"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <br/>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
            <center>
                <div id="divMsg"  class="row"  style="display: none">
                    <div class="col-sm-12">
                        <div class="alert alert-danger" role="alert">
                            ${MessageUtils.getMessage("permission.notpermission")}
                        </div>
                    </div>
                </div>
            </center>
        </div>
    </div>
</form>
<script type="text/javascript">

    var row = 20;
    var colume = 11;
    function selectAllColume(obj, idColume) {
        for (var idx = 0; idx <= row; idx++) {
            if (obj.checked) {
                if ($("#ck_r_" + idx + "_c_" + idColume).is('[disabled=true]')) {
                    continue;
                }
                $("#ck_r_" + idx + "_c_" + idColume).prop('checked', true);
            } else {
                $("#ck_r_" + idx + "_c_0").prop('checked', false);
                $("#ck_r_" + idx + "_c_" + idColume).prop('checked', false);
            }
        }
    }
    function selectAllRow(obj, idrow) {
        for (var idx = 0; idx <= colume; idx++) {
            if (obj.checked) {
                if ($("#ck_r_" + idrow + "_c_" + idx).is('[disabled=true]')) {
                    continue;
                }
                $("#ck_r_" + idrow + "_c_" + idx).prop('checked', true);
            } else {
                $("#ck_r_1_c_" + idx).prop('checked', false);
                $("#ck_r_" + idrow + "_c_" + idx).prop('checked', false);
            }

        }
    }

    function checkPermisionList() {

        for (var idxColume = 1; idxColume <= colume; idxColume++) {
            for (var idxRow = 2; idxRow <= row; idxRow++) {
                if ($("#ck_r_" + idxRow + "_c_" + idxColume).prop('checked') == true) {
                    return true;
                }
            }
        }
        $("#divMsg").show("slow");
        return false;
    }
</script>