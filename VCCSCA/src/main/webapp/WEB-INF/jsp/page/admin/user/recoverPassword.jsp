<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>

<html>
    <body>
        <div class="container-fluid">
            <div>
                <img style="height: 200px; margin: 0px auto;max-width: 300px;" class="img-responsive" alt="NAPAS BACKEND" src="resources/napas_theme/images/big_logo.png" />
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <form id="recoverPasswordModel" name='recoverPasswordForm' action="recoverPassword.do" modelAttribute="RecoverPasswordForm" method='POST'>
                        <!--<fieldset>-->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h1 class="panel-title text-left"><b>Quên mật khẩu</b></h1>
                            </div>
                            <div class="panel-body">
                                <div class="form-group row">
                                    <label class="control-label col-md-9" for="username" style="text-align:left;">
                                        Tên đăng nhập
                                    </label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="username" id="username" placeholder="" value="${username}" required 
                                                   require-message="${MessageUtils.getMessage("recover.password.username.validate.empty")}">
                                        </div>
                                    </div>
                                    <label class="control-label col-md-9" for="email" style="text-align:left;">
                                        Nhập địa chỉ email để lấy lại mật khẩu
                                    </label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="" value="${email}" required 
                                                   require-message="${MessageUtils.getMessage("recover.password.email.validate.empty")}"
                                                   type-message="${MessageUtils.getMessage("recover.password.email.validate.not.exist")}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <center>
                                    <c:if test="${errMessage != null}">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-danger" role="alert">
                                                    ${errMessage}
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${successMessage != null}">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-success" role="alert">
                                                    ${successMessage}
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                </center>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success form-control" type="submit">
                                            <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Lấy lại mật khẩu</button>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <center><a class="text-center text-primary" href="login">Đăng nhập</a></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</fieldset>-->
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>