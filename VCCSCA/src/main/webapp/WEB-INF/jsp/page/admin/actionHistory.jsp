<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/paginTaglib.tld" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<script src="resources/gijgo/js/gijgo.min.js" type="text/javascript"></script>
<link href="resources/gijgo/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<%
    String ADMIN_EXPORT_HISTORY = (String) request.getSession().getAttribute("ADMIN_EXPORT_HISTORY");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Lịch sử truy cập
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="actionHistoryForm" name='actionHistoryForm' modelAttribute="actionHistoryForm"
                                      action="lich-su-truy-cap.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="fullName" style="text-align:right;">
                                                        Tên người dùng
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input type="text" maxlength="100" class="form-control character" id="fullName" name="fullName" value="${actionHistoryForm.fullName}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="processTime" style="text-align:right;">
                                                        Thời gian
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <input type="text"  class="form-control date " id="fromDate" name="fromDate" placeholder="Từ ngày" value="${actionHistoryForm.fromDate}"
                                                               type-message="${MessageUtils.getMessage("date.validate.unformatted")}"

                                                               >
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control date " id="toDate" name="toDate" placeholder="Đến ngày" value="${actionHistoryForm.toDate}"
                                                               type-message="${MessageUtils.getMessage("date.validate.unformatted")}"
                                                               >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="status">
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${actionHistoryForm.status==0?"selected":""}>Thành công</option>
                                                            <option value="1" ${actionHistoryForm.status==1?"selected":""}>Thất bại</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="button" onclick="submitExport('actionHistoryForm', 'lich-su-truy-cap.html')"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${ADMIN_EXPORT_HISTORY == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="submitExport('actionHistoryForm', 'exportHistory.do')"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null && errMessage != ''}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách lịch sử truy cập</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên người dùng</th>
                                                        <th style="text-align: center">Địa chỉ IP</th>
                                                        <th style="text-align: center">Hành động</th>
                                                        <th style="text-align: center">Chức năng</th>
                                                        <th style="text-align: center">Thời gian thực hiện</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${actionsList}" var="actionBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index +1 }</td>
                                                            <td>${actionBO.userName}</td>
                                                            <td>${actionBO.ip}</td>
                                                            <td>${actionBO.actionName}</td>
                                                            <td>${actionBO.function}</td>
                                                            <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value = "${actionBO.dateTime}" /></td>
                                                            <td>${actionBO.status==0?'Thành công':'Thất bại'}</td>
                                                        </tr>
                                                    </c:forEach>
                                                    <c:if test="${actionsList == null || actionsList.size() ==0}">
                                                        <tr>
                                                            <td colspan="7">Không tìm thấy bản ghi nào</td>
                                                        </tr>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                            <div class="col-md-12"><div class="dataTables_info" id="DataTables_Table_0_info">Tổng : ${count} </div></div>
                                        </div>

                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <center>
                                                        <tag:paginate max="10" offset="${offset}" count="${count}" uri="lich-su-truy-cap.html" next="Sau → " previous="← Trước"/>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#fromDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });
        $('#toDate').datepicker({
            uiLibrary: 'bootstrap',
            format: 'dd/mm/yyyy',
            maxDate: function () {
                return new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            }
        });
    });
</script>