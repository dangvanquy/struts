<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--<script src="resources/timepicker/latest/timepicker.min.js"></script>
<link href="resources/timepicker/latest/timepicker.min.css" rel="stylesheet"/>-->


<form data-toggle="validator" role="form" id="createWarningSystem" 
      name='configWarningSystemForm' action="editAlert.do" 
      modelAttribute="configWarningSystemForm" 
      method='POST'
      accept-charset="UTF-8" 
      onsubmit="return validateItemEmail();"
      style="margin: 10px;margin-bottom: -10px;">
    <input type="hidden" name="warningId"  value="${configWarningSystemForm.warningId}"/>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Cập nhật</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right">
                        <label class="control-label" >Loại cảnh báo <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label><input type="radio" name="type" ${configWarningSystemForm.type == 0 ?'checked':''} value="0">&nbsp;Chứng thư số</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label><input type="radio" name="type" ${configWarningSystemForm.type == 1 ?'checked':''} value="1">&nbsp;Root CA</label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" >Tên cảnh báo <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input maxlength="100" type="text" class="form-control warningName removespecchar" name="warningName" required
                                   require-message="${MessageUtils.getMessage("configWarningSystem.not.warningName")}"
                                   value="${configWarningSystemForm.warningName}"
                                   />
                        </div>
                    </div>
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label "  >
                            Tham số cảnh báo <span class="required" style="color: red">*</span>
                        </label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <input maxlength="2" type="text" class="form-control number" name="dayConfig" placeholder="Số ngày trước khi cảnh báo" required
                                       require-message="${MessageUtils.getMessage("configWarningSystem.not.dayConfig")}"
                                       value="${configWarningSystemForm.dayConfig}" />
                                <div class="input-group-addon">Ngày</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >To <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="200" type="text" class="form-control" name="mailTo" required id="mailToEdit" placeholder="Gửi nhiều email cách nhau bằng dấu ; "
                                   require-message="${MessageUtils.getMessage("configWarningSystem.not.mailTo")}"
                                   type-message="${MessageUtils.getMessage("configWarningSystem.validate.mailTo")}"
                                   value="${configWarningSystemForm.mailTo}" 
                                   />
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >CC</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="200" type="text" class="form-control" name="mailCc" id="mailCcEdit" placeholder="Gửi nhiều email cách nhau bằng dấu ; "
                                   type-message="${MessageUtils.getMessage("configWarningSystem.validate.mailCc")}"
                                   value="${configWarningSystemForm.mailCc}" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >BCC</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="200" type="text" class="form-control" name="mailBcc" id="mailBccEdit" placeholder="Gửi nhiều email cách nhau bằng dấu ; "
                                   type-message="${MessageUtils.getMessage("configWarningSystem.validate.mailBcc")}"
                                   value="${configWarningSystemForm.mailBcc}" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >Thời gian gửi <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group" style="width: 45% !important;float: left" >
                            <select id="status" name="sendTime1"  class="form-control" require-message="${MessageUtils.getMessage("configWarningSystem.not.sendTime")}">
                                <c:forEach var = "hh" begin = "0" end = "23">
                                    <option value="${StringUtils.paddingIndex(hh)}" ${StringUtils.paddingIndex(hh) == configWarningSystemForm.sendTime1 ? 'selected':''} >${StringUtils.paddingIndex(hh)}</option>
                                </c:forEach>
                            </select>
                            <div class="input-group-addon">Giờ</div>
                        </div>

                        <div style="width: 10% !important;float: left" ><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>

                        <div class="input-group" style="width: 45% !important;float: left" >
                            <select id="status" name="sendTime2" class="form-control" require-message="${MessageUtils.getMessage("configWarningSystem.not.sendTime")}">
                                <c:forEach var = "mm" begin = "0" end = "59">
                                    <option value="${StringUtils.paddingIndex(mm)}" ${StringUtils.paddingIndex(mm) == configWarningSystemForm.sendTime2 ? 'selected':''}>${StringUtils.paddingIndex(mm)}</option>
                                </c:forEach>
                            </select>
                            <div class="input-group-addon">Phút</div>
                        </div>
                    </div>
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label "  >Số lần cảnh báo <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <input maxlength="2" type="text" class="form-control number" id="" placeholder="" name="sendNumber" required 
                                       require-message="${MessageUtils.getMessage("configWarningSystem.not.sendNumber")}"
                                       value="${configWarningSystemForm.sendNumber}" />
                                <div class="input-group-addon">Lần</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >Trạng thái</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="status" class="form-control" name="status">
                                <option value="0" ${configWarningSystemForm.status == 0 ?'selected':''}>Hoạt động</option>
                                <option value="1" ${configWarningSystemForm.status == 1 ?'selected':''}>Không hoạt động</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >Tiêu đề <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="100" type="text" class="form-control" name="subject" required 
                                   require-message="${MessageUtils.getMessage("configWarningSystem.not.subject")}"
                                   value="${configWarningSystemForm.subject}" />
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label"  >Nội dung <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <textarea maxlength="300" rows="5" class="form-control" id="edit_content" placeholder="" name="content" required 
                                      require-message="${MessageUtils.getMessage("configWarningSystem.not.content")}">${configWarningSystemForm.content}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-righ label-textt">
                        <label class="control-label"  >Tham số nội dung </label>
                    </div>
                    <div class="col-md-10">
                        <div class="controls">

                            <select id="selectParamIdEdit" multiple 
                                    class="form-control" 
                                    onchange="fillParamToConten('selectParamIdEdit', 'help_edit_param')" data-rel="chosen" 
                                    name="param"
                                    >
                                <c:forEach items="${configWarningSystemForm.params}" var="paramBo">
                                    <option value="${paramBo.paramId}" ${fn:contains(configWarningSystemForm.param, paramBo.paramId)?'selected':''} >${paramBo.paramName}</option>
                                </c:forEach>
                            </select>
                            <c:forEach items="${configWarningSystemForm.params}" var="paramBo">
                                <input type="hidden" id="param_hiden_${paramBo.paramId}" value_name="${paramBo.paramName}" value="${paramBo.columnValue}" />
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-righ label-textt">
                    </div>
                    <div class="col-md-10">
                        <div id="help_edit_param" >
                            <c:if test="${configWarningSystemForm.params != null}">
                                <div class="alert alert-success" role="alert">
                                    Hãy chèn các tham số sau vào phần cần chỉnh sửa trong nội dung email: <br/><br/>
                                    <c:forEach items="${configWarningSystemForm.params}" var="paramBo">
                                        <c:if test="${fn:contains(configWarningSystemForm.param, paramBo.paramId)}">
                                            <div class='col-md-4'>${paramBo.paramName}</div>
                                            <div class='col-md-8'>{${paramBo.columnValue}}</div>
                                        </c:if>
                                    </c:forEach>
                                    <div class='clearfix'></div>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <div class="form-group row">
                    <div class="form-control-lg">
                        <div class="col-sm-12">
                            <center>
                                <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                                <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>

</script>