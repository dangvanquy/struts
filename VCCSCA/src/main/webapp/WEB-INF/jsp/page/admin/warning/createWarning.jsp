<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="resources/timepicker/latest/timepicker.min.js"></script>
<link href="resources/timepicker/latest/timepicker.min.css" rel="stylesheet"/>

<form data-toggle="validator" role="form" id="createWarningSystem" 
      name='configWarningSystemForm' action="createAlert.do" 
      modelAttribute="configWarningSystemForm" method='POST'
      accept-charset="UTF-8" style="margin: 10px;margin-bottom: -10px;"
      onsubmit="return validateItemEmail();"
      >
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Thêm mới</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right">
                        <label class="control-label" for="param" >Loại cảnh báo <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label><input type="radio" name="type" checked value="0">&nbsp;Chứng thư số</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label><input type="radio" name="type"  value="1">&nbsp;Root CA</label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >Tên cảnh báo <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input maxlength="100" type="text" class="form-control warningName removespecchar" name="warningName" required
                                   require-message="${MessageUtils.getMessage("configWarningSystem.not.warningName")}"
                                   
                                   />
                        </div>
                    </div>
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label " for="param" >
                            Tham số cảnh báo <span class="required" style="color: red">*</span>
                        </label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <input  maxlength="2" type="text" class="form-control number" name="dayConfig" placeholder="Số ngày trước khi cảnh báo" value="1" required
                                        require-message="${MessageUtils.getMessage("configWarningSystem.not.dayConfig")}"/>
                                <div class="input-group-addon">Ngày</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >To <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="200" type="text" class="form-control" name="mailTo" required id="mailTo" placeholder="Gửi nhiều email cách nhau bằng dấu ; "
                                   require-message="${MessageUtils.getMessage("configWarningSystem.not.mailTo")}"
                                   type-message="${MessageUtils.getMessage("configWarningSystem.validate.mailTo")}"
                                   />
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >CC</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="200" type="text" class="form-control" name="mailCc" id="mailCc" placeholder="Gửi nhiều email cách nhau bằng dấu ; "
                                   type-message="${MessageUtils.getMessage("configWarningSystem.validate.mailCc")}"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >BCC</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="200" type="text" class="form-control" name="mailBcc" id="mailBcc" placeholder="Gửi nhiều email cách nhau bằng dấu ; "
                                   type-message="${MessageUtils.getMessage("configWarningSystem.validate.mailBcc")}"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >Thời gian gửi <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group" style="width: 45% !important;float: left" >
                            <select id="" name="sendTime1" class="form-control" require-message="${MessageUtils.getMessage("configWarningSystem.not.sendTime")}">
                                <c:forEach var = "hh" begin = "0" end = "23">
                                    <option value="${hh}" ${hh == 8? 'selected':''} >${hh}</option>
                                </c:forEach>
                            </select>
                            <div class="input-group-addon">Giờ</div>
                        </div>

                        <div style="width: 10% !important;float: left" ><p>&nbsp;&nbsp;&nbsp;&nbsp;</p></div>

                        <div class="input-group" style="width: 45% !important;float: left" >
                            <select id="status" name="sendTime2" class="form-control" require-message="${MessageUtils.getMessage("configWarningSystem.not.sendTime")}">
                                <c:forEach var = "mm" begin = "0" end = "59">
                                    <option value="${mm}" >${mm}</option>
                                </c:forEach>
                            </select>
                            <div class="input-group-addon">Phút</div>
                        </div>
                    </div>
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label " for="param" >Số lần cảnh báo <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <input maxlength="2" type="text" class="form-control number" id="" placeholder="" value="3" name="sendNumber" required 
                                       require-message="${MessageUtils.getMessage("configWarningSystem.not.sendNumber")}"/>
                                <div class="input-group-addon">Lần</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >Trạng thái</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="status" class="form-control" name="status">
                                <option value="0" selected>Hoạt động</option>
                                <option value="1">Không hoạt động</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >Tiêu đề <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <input maxlength="100" type="text" class="form-control" name="subject" required 
                                   require-message="${MessageUtils.getMessage("configWarningSystem.not.subject")}"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-right label-text">
                        <label class="control-label" for="param" >Nội dung <span class="required" style="color: red">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <textarea maxlength="300" rows="5" class="form-control" id="content" placeholder="" name="content" required 
                                      require-message="${MessageUtils.getMessage("configWarningSystem.not.content")}"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-righ label-textt">
                        <label class="control-label" for="param" >Tham số nội dung </label>
                    </div>
                    <div class="col-md-10">
                        <div class="controls">
                            <select id="selectParamId" multiple class="form-control" 
                                    onchange="fillParamToConten('selectParamId', 'help_create_param')" data-rel="chosen" 
                                    name="param"
                                    placeholder="---Chọn Tham số nội dung---" 
                                    >
                                <c:forEach items="${configWarningSystemForm.params}" var="paramBo">
                                    <option value="${paramBo.paramId}" >${paramBo.paramName}</option>
                                </c:forEach>
                            </select>
                            <c:forEach items="${configWarningSystemForm.params}" var="paramBo">
                                <input type="hidden" id="param_hiden_${paramBo.paramId}" value_name="${paramBo.paramName}" value="${paramBo.columnValue}" />
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-lg-12">
                    <div class=" col-md-2 text-righ label-textt">
                    </div>
                    <div class="col-md-10">
                        <div id="help_create_param" >
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <div class="form-group row">
                    <div class="form-control-lg">
                        <div class="col-sm-12">
                            <center>
                                <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                                <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
//khai bao msg

//    var timepicker = new TimePicker('time', {
//        lang: 'en',
//        theme: 'dark'
//    });
//    timepicker.on('change', function (evt) {
//        var value = (evt.hour || '00') + ':' + (evt.minute || '00');
//        evt.element.value = value;
//        $("#time").val(value);
//        $("#time")[0].setCustomValidity("");
//    });

</script>