<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>

<%
    String ADMIN_ADD_ACCOUNT = (String) request.getSession().getAttribute("ADMIN_ADD_ACCOUNT");
    String ADMIN_EDIT_ACCOUNT = (String) request.getSession().getAttribute("ADMIN_EDIT_ACCOUNT");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right" style="font-size: 15px"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý người dùng
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form modelAttribute="userForm" action="quan-ly-nguoi-dung.html" method='POST' accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="fullName" style="text-align:right;">
                                                        Tên người dùng
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input maxlength="100" type="text" name="fullName" class="form-control" id="fullname" value="${userForm.fullName}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="phoneNumber" style="text-align:right;">
                                                        Số điện thoại
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input maxlength="13" type="text" name="phoneNumber" class="form-control" id="phoneNumber" value="${userForm.phoneNumber}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="email" style="text-align:right;">
                                                        Email
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input maxlength="100" type="text" name="email" class="form-control" id="email" value="${userForm.email}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="status" >
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${userForm.status==0 ? "selected" : ""} >Hoạt động</option>
                                                            <option value="1" ${userForm.status==1 ? "selected" : ""} >Khóa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${ADMIN_ADD_ACCOUNT == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalCreate();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null && errMessage != ''}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách tài khoản người dùng</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên người dùng</th>
                                                        <th style="text-align: center">Tài khoản</th>
                                                        <th style="text-align: center">Số điện thoại</th>
                                                        <th style="text-align: center">Email</th>
                                                        <th style="text-align: center">Nhóm quyền</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${usersList}" var="userBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index +1 }</td>
                                                            <td>${userBO.fullName}</td>
                                                            <td>${userBO.username}</td>
                                                            <td>${userBO.phoneNumber}</td>
                                                            <td>${userBO.email}</td>
                                                            <td>
                                                                <c:if test="${groupsListMap[userBO.groupId] != null}">
                                                                    <c:out value="${groupsListMap[userBO.groupId]}"/>
                                                                </c:if>
                                                            </td>
                                                            <td>
                                                                ${userBO.status==0?'Hoạt động':'Khóa'}
                                                            </td>
                                                            <td>
                                                    <center>
                                                        <a href="#" onclick="openModalDetail('${AESUtil.encryption(userBO.userId)}');"
                                                           data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>

                                                        <c:if test="${ADMIN_EDIT_ACCOUNT == 'true' }">
                                                            <a href="#" onclick="openModalEdit('${AESUtil.encryption(userBO.userId)}');"
                                                               data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                            <!--<a href="#" title="Khóa"onclick="openModalDelete('${AESUtil.encryption(userBO.userId)}');"><span class="glyphicon glyphicon-remove-sign"/></a>-->

                                                            <c:if test="${userBO.status==0}">
                                                                <a href="#" onclick="lock('${AESUtil.encryption(userBO.userId)}')"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Khóa"><img border=3 style="margin-bottom: 9px" height="20" width="20" src="resources/napas_theme/images/icon-unlock.png" alt=""/></a>
                                                                </c:if>
                                                                <c:if test="${userBO.status==1}">
                                                                <a href="#" title="Mở khóa" onclick="unlock('${AESUtil.encryption(userBO.userId)}');"
                                                                   data-toggle="tooltip" data-placement="bottom" title="Mở khóa"><img border=3 style="margin-bottom: 9px" height="20" width="20" src="resources/napas_theme/images/icon-lock.png" alt=""/></a>
                                                                </c:if>
                                                            </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="createUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="createUserId" class="modal-dialog modal-lg">
    </div>
</div>
<div id="editUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="editUserId" class="modal-dialog modal-lg">
    </div>
</div>
<div id="userDetailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="userDetailId" class="modal-dialog modal-lg">
    </div>
</div>
<!-- Alert modal -->
<div class="modal fade" id="alertDeleteUserForm" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn xóa tài khoản ?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteAccount.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="deleteUserId" id="deleteUserId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>
<form id="formLockUser" action="lockUser.do" method="POST" style="display: none">
    <input type="hidden" name="userId" id="lockUserId"/>
</form>

<form id="formUnlockUser" action="unlockUser.do" method="POST" style="display: none">
    <input type="hidden" name="userId" id="unlockUserId"/>
</form>

<script>
    function openModalCreate() {
        $.get("tao-moi-nguoi-dung.html", function (data) {
            $("#createUserId").html(data);
            $('#createUserModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalDetail(userId) {
        $.get("xem-chi-tiet-nguoi-dung.html", {userId: userId}, function (data) {
            $("#userDetailId").html(data);
            $('#userDetailModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;

    function openModalEdit(userId) {
        $.ajax({url: "chinh-sua-nguoi-dung.html",data: {userId: userId},cache: false, success: function (data) {
                $("#editUserId").html(data);
                $('#editUserModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
            }});


//        $.get("chinh-sua-nguoi-dung.html", {userId: userId}, function (data) {
//            $("#editUserId").html(data);
//            $('#editUserModal').modal({backdrop: 'static', keyboard: false});//.modal('show')
//        });
        $('#userDetailModal').modal('hide');
    }
    ;

    function openModalDelete(userId) {
        $('#deleteUserId').val(userId);
        $('#alertDeleteUserForm').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function lock(rsaId, rsaIndex) {
        $("#lockUserId").val(rsaId);
        $("#formLockUser").submit();
    }
    ;
    function unlock(rsaId, rsaIndex) {
        $("#unlockUserId").val(rsaId);
        $("#formUnlockUser").submit();
    }
    ;
</script>