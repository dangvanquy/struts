<%--<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>--%>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="resources/timepicker/latest/timepicker.min.js"></script>
<link href="resources/timepicker/latest/timepicker.min.css" rel="stylesheet"/>


<%
    String ADMIN_ADD_ALERT = (String) request.getSession().getAttribute("ADMIN_ADD_ALERT");
    String ADMIN_EDIT_ALERT = (String) request.getSession().getAttribute("ADMIN_EDIT_ALERT");
    String ADMIN_DELETE_ALERT = (String) request.getSession().getAttribute("ADMIN_DELETE_ALERT");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản lý cảnh báo
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='configWarningSystemForm' action="quan-ly-canh-bao.html" method='POST' modelAttribute="configWarningSystemForm"  accept-charset="UTF-8" >
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="warning" style="text-align:right;">
                                                        Tên cảnh báo
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input maxlength="100" type="text" class="form-control" id="warning" placeholder="" name="warningName" value="${configWarningSystemForm.warningName}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="status">
                                                            <option value="">Tất cả</option>
                                                            <option value="0" ${configWarningSystemForm.status == 0 ? 'selected' : ''} >Hoạt động</option>
                                                            <option value="1" ${configWarningSystemForm.status == 1 ? 'selected' : ''} >Không hoạt động</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${ADMIN_ADD_ALERT == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalCreate();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách cảnh báo</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên cảnh báo</th>
                                                        <th style="text-align: center">Người tạo</th>
                                                        <th style="text-align: center">Ngày tạo</th>
                                                        <th style="text-align: center">Ngày cập nhật</th>
                                                        <th style="text-align: center">Người cập nhật</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">

                                                    <c:forEach items="${listConfigWarningSystem}" var="configWarningSystem" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index +1 }</td>
                                                            <td>${configWarningSystem.warningName }</td>
                                                            <td>${configWarningSystem.createUser }</td>
                                                            <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value = "${configWarningSystem.createDate}" /></td>
                                                            <td>${configWarningSystem.updateUser }</td>
                                                            <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value = "${configWarningSystem.updateDate }"/></td>
                                                            <td>${configWarningSystem.status==0?'Hoạt động':'Không hoạt động' }</td>
                                                            <td>
                                                    <center>
                                                        <a  onclick="openModalView('${AESUtil.encryption(configWarningSystem.warningId)}')"
                                                            data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                        <c:if test="${ADMIN_EDIT_ALERT == 'true'}">
                                                            <a  onclick="openModalEdit('${AESUtil.encryption(configWarningSystem.warningId)}')"
                                                                data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                        </c:if>
                                                        <c:if test="${ADMIN_DELETE_ALERT == 'true'}">
                                                            <a  onclick="openModalDelete('${AESUtil.encryption(configWarningSystem.warningId)}')"
                                                                data-toggle="tooltip" data-placement="bottom" title="Xóa"><span class="glyphicon glyphicon-trash"/></a>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Approve request -->
<div id="createConfigWarningSystemId" class="modal fade bs-example-modal-lg"tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="createPermissionMainId" class="modal-dialog modal-lg" style="width: 1180px;" >
        <div class="modal-content">
            <jsp:include page="createWarning.jsp" />
        </div>
    </div>
</div>

<div id="editConfigWarningSystemId" class="modal fade bs-example-modal-lg"tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="editModalAlert" class="modal-dialog modal-lg" style="width: 1180px;" >
        <div class="modal-content">
            <jsp:include page="editWarning.jsp" />
        </div>
    </div>
</div>

<!-- Alert modal -->
<div class="modal fade" id="alertDeleteConfigWarningSystem" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn xóa Cảnh báo
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteAlert.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="txtconfigWarningSystemId" id="txtconfigWarningSystemId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    function openModalCreate() {
        $('#createConfigWarningSystemId').modal({backdrop: 'static', keyboard: false});//.modal('show')
        $('#selectParamId').next('div').addClass("showfull");
        if ($('.default').attr('autocomplete') === "off") {
            $('.default').val("---Chọn Tham số nội dung---");
            $('.default').addClass("showfull");
            $('.default').css( "width", "100%");
        }

    }
    ;

    function openModalEdit(idGp) {
        $.get("chinh-sua-canh-bao.html", {idRequest: idGp}, function (data) {
            $("#editModalAlert").html(data);
            $('[data-rel="chosen"],[rel="chosen"]').chosen({width: "100%"});
            if ($('.default').attr('autocomplete') === "off") {
                $('.default').val("---Chọn Tham số nội dung---");
                $('.default').addClass("showfull");
                $('.default').css( "width", "100%");
            }

            $('#editConfigWarningSystemId').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalView(idGp) {
        $.get("chi-tiet-canh-bao.html", {idRequest: idGp}, function (data) {
//            console.log("okkk");
            $("#editModalAlert").html(data);
            $('#editConfigWarningSystemId').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalDelete(idGp) {
        $('#txtconfigWarningSystemId').val(idGp);
        $('#alertDeleteConfigWarningSystem').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
//    var timepicker = new TimePicker('timeEdit', {
//        lang: 'en',
//        theme: 'dark'
//    });
//    timepicker.on('change', function (evt) {
//
//        var value = (evt.hour || '00') + ':' + (evt.minute || '00');
//        evt.element.value = value;
//    });
</script>