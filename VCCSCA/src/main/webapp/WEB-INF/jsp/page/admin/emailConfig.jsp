<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>

<%
    String ADMIN_ADD_EMAIL = (String) request.getSession().getAttribute("ADMIN_ADD_EMAIL");
    String ADMIN_EDIT_EMAIL = (String) request.getSession().getAttribute("ADMIN_EDIT_EMAIL");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Cấu hình email
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="saveConfigEmailId" name='emailConfigForm' action="cau-hinh-email.html" modelAttribute="EmailConfigForm" method='POST'
                                      accept-charset="UTF-8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Cập nhật</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="username" style="text-align:right;">
                                                        Tên đăng nhập <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="50" type="text" class="form-control" name="username" id="username" placeholder="" value="${dataObject.username}" required
                                                                   require-message="${MessageUtils.getMessage("username.config.validate.empty")}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="email" style="text-align:right;">
                                                        Email <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="50" type="email" class="form-control" name="email" id="email" placeholder="" value="${dataObject.email}" required 
                                                                   require-message="${MessageUtils.getMessage("email.config.validate.empty")}"
                                                                   type-message="${MessageUtils.getMessage("email.config.validate.unformatted")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="password" style="text-align:right;">
                                                        Mật khẩu <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="100" type="password" class="form-control cfPass password" name="password" id="password" placeholder="" required
                                                                   require-message="${MessageUtils.getMessage("password.config.validate.empty")}"
                                                                   type-message="${MessageUtils.getMessage("change.password.validate.lengh")}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="mailServer" style="text-align:right;">
                                                        Mail server <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="50" type="text" class="form-control" name="mailServer" id="mailServer" placeholder="" value="${dataObject.mailServer}" required
                                                                   require-message="${MessageUtils.getMessage("mail.server.config.validate.empty")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="confirmPassword" style="text-align:right;">
                                                        Nhập lại mật khẩu <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="100" type="password" class="form-control cfConfirmPass" name="confirmPassword" id="confirmPassword" placeholder="" required
                                                                   require-message="${MessageUtils.getMessage("confirm.password.config.validate.empty")}"
                                                                   type-message="${MessageUtils.getMessage("confirm.password.config.validate.not.equal")}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="securityType" style="text-align:right;">
                                                        Loại bảo mật <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="status" class="form-control" name="securityType" value="${dataObject.securityType}" >
                                                                <option value="0" ${dataObject.securityType == 0 ? 'selected': ''}>SSL/TLS</option>
                                                                <option value="1" ${dataObject.securityType == 1 ? 'selected': ''}>SSL</option>
                                                                <option value="2" ${dataObject.securityType == 2 ? 'selected': ''}>TLS</option>
                                                                <option value="3" ${dataObject.securityType == 3 ? 'selected': ''}>Không dùng</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <select id="status" class="form-control" name="status">
                                                                <option value="0" ${dataObject.status == 0 ? 'selected': ''}>Hoạt động</option>
                                                                <option value="1" ${dataObject.status == 1 ? 'selected': ''}>Không hoạt động</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="port" style="text-align:right;">
                                                        Cổng <font style="color: red">*</font>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input maxlength="9" type="text" class="form-control port" name="port" id="port" placeholder="" value="${dataObject.port}" required
                                                                   require-message="${MessageUtils.getMessage("port.config.validate.empty")}"
                                                                   type-message="${MessageUtils.getMessage("port.config.validate.unformatted")}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <label class="control-label col-md-2" for="description" style="text-align:right;">
                                                        Ghi chú
                                                    </label>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <textarea class="form-control" maxlength="200" style="margin-left: -5px;resize: none"  name="description" id="description" placeholder="">${dataObject.description}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <c:if test="${ADMIN_EDIT_EMAIL == 'true'}">
                                                                <button class="btn btn-success" type="button" style="width: 150px;" onclick="alertPopup()">Lưu</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="hidden" style="visibility: hidden" id="configConfirm" type="submit">Lưu</button>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function submitForm() {
        $('#alertUpdate').modal('hide')
        $('#configConfirm').trigger("click");
    }
    ;
    function alertPopup() {
        $('#alertUpdate').modal('show')
    }
    ;
</script>

<!-- Alert modal -->
<div class="modal fade" id="alertUpdate" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn thay đổi cấu hình email hệ thống?
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-primary" onclick="submitForm()">Cập nhật</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </center>
            </div>
        </div>
    </div>
</div>