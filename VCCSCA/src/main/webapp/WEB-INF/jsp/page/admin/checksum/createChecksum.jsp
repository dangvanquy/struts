<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form name='createChecksumForm' modelAttribute="createChecksumForm" action="createChecksum.do" method='POST' accept-charset="UTF-8"
      onsubmit="return validateItem();"
      >
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title text-left">Thêm mới</h1>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="rsaIndex" style="text-align:right;">
                        Index <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <select id="rsaIndex" class="form-control" name="rsaIndex"
                                required="" require-message="${MessageUtils.getMessage("checksum.require.select.index")}">
                            <option value="">---Chọn Index---</option>
                            <c:forEach items="${indexLst}" var="index">
                                <option value="${StringUtils.paddingIndex(index)}">${StringUtils.paddingIndex(index)}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expirationDate" style="text-align:right;">
                        Ngày hết hạn<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-4">
                        <select id="month" class="form-control" name="month"
                                required="" require-message="${MessageUtils.getMessage("common.require.select.month")}">
                            <option value="">---Tháng---</option>
                            <option value="01">Tháng 1</option>
                            <option value="02">Tháng 2</option>
                            <option value="03">Tháng 3</option>
                            <option value="04">Tháng 4</option>
                            <option value="05">Tháng 5</option>
                            <option value="06">Tháng 6</option>
                            <option value="07">Tháng 7</option>
                            <option value="08">Tháng 8</option>
                            <option value="09">Tháng 9</option>
                            <option value="10">Tháng 10</option>
                            <option value="11">Tháng 11</option>
                            <option value="12">Tháng 12</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <select id="year" class="form-control" name="year"
                                required="" require-message="${MessageUtils.getMessage("common.require.select.year")}">
                            <option value="">---Năm---</option>
                            <c:forEach var="expYear" begin="2018" end="3000" >
                                <option value="${expYear}">${expYear}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <div></div>
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Public key exponent (Hex)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <select id="year" class="form-control" name="publicKeyExponent">
                            <option value="03" selected>03</option>
                            <option value="010001">010001</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div></div>
                    <label class="control-label col-md-4" for="publicKeyExponent" style="text-align:right;">
                        Độ dài khóa
                    </label>
                    <div class="col-md-8">
                         <div class="input-group">
                            <input  type="text" id="rsaLength"  class="form-control " name="rsaLength" value=" ${keysBO.rsaLength}" disabled="true"/>
                            <div class="input-group-addon">bit</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="publicKeyModulus" style="text-align:right;">
                        Public key modulus (Hex)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <textarea maxlength="2000" rows="3" class="form-control rsa" id="publicKeyModulus" name="publicKeyModulus" onkeyup="loadlen()" onblur="loadlen()"
                                  required="" require-message="${MessageUtils.getMessage("checksum.public.key.modulus.not.empty")}"></textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="checksumName" style="text-align:right;">
                        Privatekey Checksum (SHA256)<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-8">
                        <textarea maxlength="64" rows="3" class="form-control rsa" id="checksumName" name="checksumName"
                                  required="" 
                                  require-message="${MessageUtils.getMessage("checksum.name.not.empty")}"
                                  checksum-message="Privatekey Checksum (SHA256) không đủ độ dài"
                                  ></textarea>
                    </div>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-md-8">
                        <select id="status" class="form-control" name="checksumStatus">
                            <option value="0">Hoạt động</option>
                            <option value="1">Ngừng hoạt động</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="content" style="text-align:right;">
                        Mô tả
                    </label>
                    <div class="col-md-10">
                        <textarea maxlength="200" style="margin-left: -5px; width: 101%;" class="form-control" id="content" name="checksumContent" placeholder=""></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Lưu</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    function loadlen() {
        $("#rsaLength").val($("#publicKeyModulus").val().length * 4);
    }

    function validateItem() {
        var msg = $("#checksumName").attr("checksum-message");
        if ($("#checksumName").val().length !== 64) {
            $("#checksumName")[0].setCustomValidity(msg);
            $("#checksumName")[0].reportValidity();
            return false;
        } else {
            $("#checksumName")[0].setCustomValidity("");
            return true;
        }
    }
</script>