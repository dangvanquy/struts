<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form name='createKeyForm' modelAttribute="createKeyForm" action="createKey.do" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Thêm mới</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="index" style="text-align:right;">
                        Index <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-4">
                        <select id="index" class="form-control" name="rsaIndex" 
                                required require-message="${MessageUtils.getMessage("rsa.require.select.index")}">
                            <option value="">Index</option>
                            <c:forEach items="${indexLst}" var="index">
                                <option value="${StringUtils.paddingIndex(index)}">${StringUtils.paddingIndex(index)}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <select id="index" class="form-control" name="slotHsm" 
                                required require-message="${MessageUtils.getMessage("rsa.require.select.slot")}">
                            <option value="">Slot HSM</option>
                            <c:forEach items="${slotLst}" var="configHsmBO">
                                <option value="${configHsmBO.slot}">${configHsmBO.slot}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="rsaLength" style="text-align:right;">
                        Độ dài khóa <span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <div class="input-group">
                                <input maxlength="5" type="text" id="rsaLength"  class="form-control number" name="rsaLength"  required
                                        require-message="${MessageUtils.getMessage("rsa.require.select.key.length")}"/>
                                <div class="input-group-addon">bit</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="rsaStatus"  style="text-align:left;">
                            <option value="0">Hoạt động</option>
                            <option value="1">Ngừng hoạt động</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="expDate" style="text-align:right;">
                        Ngày hết hạn<span class="required" style="color: red">*</span>
                    </label>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="month" class="form-control" name="month" required
                                    require-message="${MessageUtils.getMessage("common.require.select.month")}">
                                <option value="">---Tháng---</option>
                                <option value="01">Tháng 1</option>
                                <option value="02">Tháng 2</option>
                                <option value="03">Tháng 3</option>
                                <option value="04">Tháng 4</option>
                                <option value="05">Tháng 5</option>
                                <option value="06">Tháng 6</option>
                                <option value="07">Tháng 7</option>
                                <option value="08">Tháng 8</option>
                                <option value="09">Tháng 9</option>
                                <option value="10">Tháng 10</option>
                                <option value="11">Tháng 11</option>
                                <option value="12">Tháng 12</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <select id="year" class="form-control" name="year" required
                                    require-message="${MessageUtils.getMessage("common.require.select.year")}">
                                <option value="">---Năm---</option>
                                <c:forEach var="expYear" begin="2018" end="3000" >
                                    <option value="${expYear}">${expYear}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="checksumContent" style="text-align:right;">
                        Mô tả
                    </label>
                    <div class="col-lg-10">
                        <textarea style="margin-left: -5px; width: 101%;" maxlength="200" rows="3" class="form-control" id="checksumContent" name="checksumContent" placeholder=""></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="submit">Tạo</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
