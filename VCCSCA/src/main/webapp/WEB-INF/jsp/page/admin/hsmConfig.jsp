<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.MessageUtils"%>
<%@page import="com.napas.vccsca.utils.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%
    String ADMIN_ADD_CONFIG_HSM = (String) request.getSession().getAttribute("ADMIN_ADD_CONFIG_HSM");
    String ADMIN_EDIT_CONFIG_HSM = (String) request.getSession().getAttribute("ADMIN_EDIT_CONFIG_HSM");
    String ADMIN_DELETE_CONFIG_HSM = (String) request.getSession().getAttribute("ADMIN_DELETE_CONFIG_HSM");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Cấu hình HSM
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form id="configHsmForm" name='configHsmForm' action="createConfigHsm.do" method='POST' modelAttribute="configHsmForm"  accept-charset="UTF-8" >

                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách Slot HSM</h1>
                                        </div>
                                        <div class="panel-body">
                                            <center>
                                                <c:if test="${errMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-danger" role="alert">
                                                                ${errMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                                <c:if test="${successMessage != null}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success" role="alert">
                                                                ${successMessage}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </center>
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr style="text-align: center">
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">SLOT HSM</th>
                                                        <th style="text-align: center">Mật khẩu</th>
                                                        <th style="text-align: center">Ngày tạo</th>
                                                        <th style="text-align: center">Ngày cập nhật</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <input maxlength="10" type="text" value="${configHsmForm.slot}" name="slot" class="form-control-lg col-md-12 number" required
                                                                   require-message="${MessageUtils.getMessage("email.config.validate.empty")}"
                                                                   placeholder="Nhập slot"
                                                                   />
                                                        </td>
                                                        <td>
                                                            <input maxlength="200" type="password" value="${configHsmForm.password}" name="password" class="form-control-lg col-md-12" required
                                                                   require-message="${MessageUtils.getMessage("email.config.validate.empty")}"
                                                                   placeholder="Nhập mật khẩu slot HSM"
                                                                   />
                                                        </td>
                                                        <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value="${StringUtils.getnow()}"/></td>
                                                        <td></td>
                                                        <td>
                                                <center>
                                                    <c:if test="${ADMIN_ADD_CONFIG_HSM == 'true'}">
                                                        <button style="display: none" type="submit" id="smhsmf"></button>
                                                        <a href="#" onclick="smhsmf();"
                                                           data-toggle="tooltip" data-placement="bottom" title="Lưu"><span class="glyphicon glyphicon-floppy-save"/></a>
                                                    </c:if>
                                                </center>
                                                </td>
                                                </tr>


                                                <c:forEach items="${listConfigHsm}" var="configHsmBO" varStatus="itr">
                                                    <tr>
                                                        <td style="text-align: center">${offset + itr.index +1 }</td>
                                                        <td>${StringUtils.paddingIndex(configHsmBO.slot)}</td>
                                                        <td>************************</td>
                                                        <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value="${configHsmBO.createDate}"/></td>
                                                        <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value="${configHsmBO.updateDate}"/></td>
                                                        <td>
                                                    <center>
                                                        <c:if test="${ADMIN_EDIT_CONFIG_HSM == 'true'}">
                                                            <a href="#" onclick="openModalEdit('${AESUtil.encryption(configHsmBO.hsmId)}', '${configHsmBO.slot }', '')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                        </c:if>
                                                        <c:if test="${ADMIN_DELETE_CONFIG_HSM == 'true'}">
                                                            <a href="#" onclick="openModalDelete('${AESUtil.encryption(configHsmBO.hsmId)}')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Xóa"><span class="glyphicon glyphicon-trash"/></a>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Alert modal -->
<div class="modal fade bs-example-modal-lg" id="alertUpdateConfigHsm" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="editConfigHsm.do" modelAttribute="configHsmForm"  method='POST' accept-charset="UTF-8">
                <div class="modal-header" style="background-color: #1f417d; color: white">
                    <h1 class="panel-title text-left">Cập nhật</h1>
                </div>
                <input type="hidden" name="hsmId" id="txthsmId"/>
                <div class="modal-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr style="text-align: center">
                                <th style="text-align: center">SLOT HSM</th>
                                <th style="text-align: center">Mật khẩu</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="txtHsmId" type="hidden" value="" name="hsmId" />
                                    <input id="txtSlot" type="hidden" value="" name="slot"/>
                                    <input id="txtSlot1" type="text" value="" name="" disabled="true" class="form-control-lg col-md-12" required="true"/>
                                </td>
                                <td>
                                    <input id="txtPassword" type="password" value="" name="password" class="form-control-lg col-md-12" required="true"
                                           require-message="${MessageUtils.getMessage("email.config.validate.empty")}"
                                           />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-success" style="width: 150px">Cập nhật</button>
                        <button type="button" class="btn btn-default" style="width: 150px" data-dismiss="modal">Đóng</button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="alertDeleteConfigHsm" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn xóa Slot HSM
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteConfigHsm.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="txtHsmId" id="txtDeleteConfigHsmId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function openModalEdit(hsmId, slot, password) {
        if (slot.length !== 2) {
            for (var i = slot.length; i < 2; i++) {
                slot = "0" + slot;
            }
        }
        $('#txtHsmId').val(hsmId);
        $('#txtSlot').val(slot);
        $('#txtSlot1').val(slot);
        $("#txtPassword").val(password);
        $('#alertUpdateConfigHsm').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function openModalDelete(hsmId) {
        $('#txtDeleteConfigHsmId').val(hsmId);
        $('#alertDeleteConfigHsm').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
    function smhsmf() {
        $('#smhsmf').trigger("click");
    }
    ;

</script>