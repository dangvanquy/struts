<%--<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>--%>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String ADMIN_ADD_GROUPPERMISION = (String) request.getSession().getAttribute("ADMIN_ADD_GROUPPERMISION");
    String ADMIN_EDIT_GROUPPERMISION = (String) request.getSession().getAttribute("ADMIN_EDIT_GROUPPERMISION");
    String ADMIN_DELETE_GROUPPERMISION = (String) request.getSession().getAttribute("ADMIN_DELETE_GROUPPERMISION");
%>

<div class="ms-webpart-zone ms-fullWidth">
    <div id="MSOZoneCell_WebPartctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032" 
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div width="100%" class="ms-WPBody noindex ">
                <div id="ctl00_ctl16_g_96477866_d470_4e7a_8864_8e89fc35d032">
                    <section class="middle middle-body">
                        <div class="middle-inner">
                            <br/>
                            <div class="container">
                                <!--site-map-->
                                <div class="panel panel-default site-map" >
                                    <h5>&nbsp;
                                        <i class="glyphicon glyphicon-home"></i> 
                                        <i class="glyphicon glyphicon-chevron-right"></i> 
                                        Quản trị hệ thống
                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                        Quản lý quyền
                                    </h5>
                                </div>
                                <!--site-map-->
                                <form name='permissionListForm' action="danh-sach-nhom-quyen.html" method='POST' modelAttribute="groupPermissionForm"  accept-charset="UTF-8" >
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Thông tin tìm kiếm</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="groupName" style="text-align:right;">
                                                        Tên nhóm quyền
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <input maxlength="50" type="text" class="form-control" id="groupName" name="groupName" value="${groupPermissionForm.groupName}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                                                        Trạng thái
                                                    </label>
                                                    <div class="col-lg-8">
                                                        <select id="status" class="form-control" name="status">
                                                            <option value="-1">Tất cả</option>
                                                            <option value="0" ${groupPermissionForm.status == 0 ? 'selected': ''}>Hoạt động</option>
                                                            <option value="1" ${groupPermissionForm.status == 1 ? 'selected': ''}>Không hoạt động</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group row">
                                                <div class="form-control-lg">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <button class="btn btn-success" style="width: 150px;" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Tìm kiếm</button>
                                                            <c:if test="${ADMIN_ADD_GROUPPERMISION == 'true'}">
                                                                <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalCreate()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm mới</button>
                                                            </c:if>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <center>
                                                    <c:if test="${errMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-danger" role="alert">
                                                                    ${errMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${successMessage != null}">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="alert alert-success" role="alert">
                                                                    ${successMessage}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h1 class="panel-title text-left">Danh sách nhóm quyền</h1>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center">STT</th>
                                                        <th style="text-align: center">Tên nhóm quyền</th>
                                                        <th style="text-align: center">Người tạo</th>
                                                        <th style="text-align: center">Ngày tạo</th>
                                                        <th style="text-align: center">Người cập nhật</th>
                                                        <th style="text-align: center">Ngày cập nhật</th>
                                                        <th style="text-align: center">Trạng thái</th>
                                                        <th style="text-align: center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <c:forEach items="${listPersons}" var="groupsPermissionBO" varStatus="itr">
                                                        <tr>
                                                            <td style="text-align: center">${offset + itr.index +1 }</td>
                                                            <td>${groupsPermissionBO.groupName }</td>
                                                            <td>${groupsPermissionBO.createUser }</td>
                                                            <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value="${groupsPermissionBO.createDate}"/></td>
                                                            <td>${groupsPermissionBO.updateUser }</td>
                                                            <td><fmt:formatDate pattern = "dd/MM/yyyy HH:mm:ss" value="${groupsPermissionBO.updateDate}"/></td>
                                                            <td>${groupsPermissionBO.status==0?'Hoạt động':'Không hoạt động' }</td>
                                                            <td>
                                                    <center>
                                                        <a href="#" onclick="openModalView('${AESUtil.encryption(groupsPermissionBO.groupId)}')"
                                                           data-toggle="tooltip" data-placement="bottom" title="Xem chi tiết"><span class="glyphicon glyphicon-eye-open"/></a>
                                                        <c:if test="${ADMIN_EDIT_GROUPPERMISION == 'true'}">
                                                            <a href="#" onclick="openModalEdit('${AESUtil.encryption(groupsPermissionBO.groupId)}')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Cập nhật"><span class="glyphicon glyphicon-edit"/></a>
                                                        </c:if>
                                                        <c:if test="${ADMIN_DELETE_GROUPPERMISION == 'true'}">
                                                            <a href="#" onclick="openModalDelete('${AESUtil.encryption(groupsPermissionBO.groupId)}')"
                                                               data-toggle="tooltip" data-placement="bottom" title="Xóa"><span class="glyphicon glyphicon-trash"/></a>
                                                        </c:if>
                                                    </center>
                                                    </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Approve request -->
<div id="createPermissionGroupId" class="modal fade bs-example-modal-lg"tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div id="createPermissionMainId" class="modal-dialog modal-lg" style="width: 1180px;" >
        <div class="modal-content">
            <jsp:include page="createPermission.jsp" />
        </div>
    </div>
</div>

<div id="editPermissionGroupId" class="modal fade bs-example-modal-lg"tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" style="width: 1180px;" >
        <div class="modal-content" id="editPermissionGroupMainId" >

        </div>
    </div>
</div>
        
<div id="viewPermissionGroupId" class="modal fade bs-example-modal-lg"tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" style="width: 1180px;" >
        <div class="modal-content" id="viewPermissionGroupMainId" >

        </div>
    </div>
</div>

<!-- Alert modal -->
<div class="modal fade" id="alertDeletePer" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning">
                    <strong>Chú ý:</strong> Bạn có chắc chắn muốn xóa Nhóm Quyền
                </div>
            </div>
            <div class="modal-footer">
                <center>
                    <form action="deleteGroupPermission.do" method='POST' accept-charset="UTF-8">
                        <input type="hidden" name="txtparamId" id="txtparamId"/>
                        <button type="submit" class="btn btn-primary" >Xóa</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </form>
                </center>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    function openModalCreate() {
        $('#createPermissionGroupId').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;

    function openModalEdit(idGp) {
        $.get("chinh-sua-nhom-quyen.html", {idRequest: idGp}, function (data) {
            $("#editPermissionGroupMainId").html(data);
            $('#editPermissionGroupId').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalView(idGp) {
        $.get("chi-tiet-nhom-quyen.html", {idRequest: idGp}, function (data) {
            $("#viewPermissionGroupMainId").html(data);
            $('#viewPermissionGroupId').modal({backdrop: 'static', keyboard: false});//.modal('show')
        });
    }
    ;
    function openModalDelete(idGp) {
        $('#txtparamId').val(idGp);
        $('#alertDeletePer').modal({backdrop: 'static', keyboard: false});//.modal('show')
    }
    ;
</script>