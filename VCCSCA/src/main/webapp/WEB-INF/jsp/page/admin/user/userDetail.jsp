<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.napas.vccsca.utils.AESUtil"%>
<%@page import="com.napas.vccsca.utils.DateTimeUtils"%>

<form id="userDetailForm" name='userDetailForm' action="userDetail" method='POST' accept-charset="UTF-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading">
                <h1 class="panel-title text-left">Xem</h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin chung</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="fullname" style="text-align:right;">
                        Tên người dùng
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.fullName}" disabled="true" type="text" class="form-control" id="fullname" placeholder="" readonly="true">
                        <input id="userId" name="userId" value="${userBO.userId}" type="hidden" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="phoneNumber" style="text-align:right;">
                        Số điện thoại
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.phoneNumber}" disabled="true" type="text" class="form-control" id="phoneNumber" placeholder="" readonly="true">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="username" style="text-align:right;">
                        Tài khoản
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.username}" disabled="true" type="text" class="form-control" id="username" placeholder="" readonly="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="email" style="text-align:right;">
                        Email
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.email}" disabled="true" type="text" class="form-control" id="email" placeholder="" readonly="true">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="status" style="text-align:right;">
                        Trạng thái
                    </label>
                    <div class="col-lg-8">
                        <select id="status" class="form-control" name="status" disabled="true">
                            <option value="0" ${userBO.status==0?'selected':''}>Hoạt động</option>
                            <option value="1" ${userBO.status==1?'selected':''}>Khóa</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="permission" style="text-align:right;">
                        Nhóm quyền
                    </label>
                    <div class="col-lg-8">
                        <select id="permission" class="form-control" name="permission" disabled="true">
                            <c:forEach items="${groupsList}" var="groupPermissionBO" varStatus="itr">
                                <option value="${groupPermissionBO.groupId}" ${userBO.groupId==groupPermissionBO.groupId?"selected":""}>${userBO.groupId==groupPermissionBO.groupId?groupPermissionBO.groupName:''}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label class="control-label col-md-2" for="note" style="text-align:right;">
                        Ghi chú
                    </label>
                    <div class="col-lg-10">
                        <textarea style="margin-left: -5px; width: 101%;" disabled="true" class="form-control" id="note" placeholder="">${userBO.note}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <h1 class="panel-title text-left" style="color: black;"><b>Thông tin khác</b></h1>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createUser" style="text-align:right;">
                        Người tạo
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.createUser}" disabled="true" type="text" class="form-control" id="createUser" placeholder="" readonly="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="createDate" style="text-align:right;">
                        Ngày tạo
                    </label>
                    <div class="col-lg-8">
                        <input value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(userBO.createDate)}" disabled="true" type="text" class="form-control" id="createDate" placeholder="" readonly="true">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateUser" style="text-align:right;">
                        Người cập nhật
                    </label>
                    <div class="col-lg-8">
                        <input value="${userBO.updateUser}" disabled="true" type="text" class="form-control" id="updateUser" placeholder="" readonly="true">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="control-label col-md-4" for="updateDate" style="text-align:right;">
                        Ngày cập nhật
                    </label>
                    <div class="col-lg-8">
                        <input value="${DateTimeUtils.convertDateToDDMMYYYYHHmmss(userBO.updateDate)}" disabled="true" type="text" class="form-control" id="updateDate" placeholder="" readonly="true">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="form-group row">
                <div class="form-control-lg">
                    <div class="col-sm-12">
                        <center>
                            <button class="btn btn-success" style="width: 150px;" type="button" onclick="openModalEdit('${AESUtil.encryption(userBO.userId)}');">Sửa</button>
                            <button class="btn btn-default" style="width: 150px;" type="button" data-dismiss="modal">Đóng</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>



