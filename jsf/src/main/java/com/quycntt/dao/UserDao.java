package com.quycntt.dao;

import com.quycntt.bean.User;

public interface UserDao {
	User checkLogin(String email, String password);
}
