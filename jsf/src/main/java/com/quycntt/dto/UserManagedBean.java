package com.quycntt.dto;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.quycntt.bean.User;
import com.quycntt.daoimp.UserDaoImp;
import com.quycntt.service.UserService;

@ManagedBean(name="userManagedBean")
@SessionScoped
public class UserManagedBean {
	private UserService userService = new UserService();
	private User user = new User();
	private String mess;
	
	public String getMess() {
		return mess;
	}

	public void setMess(String mess) {
		this.mess = mess;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String checkLogin() {
		if (userService.checkLogin(this.user.getEmail(), this.user.getPassword()) != null) {
			mess = "Login success ! Welcome";
			return "index?faces-redirect=true";
		} else {
			mess = "Login error";
			return "login";
		}
	}
}
