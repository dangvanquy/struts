package com.quycntt.service;

import com.quycntt.bean.User;
import com.quycntt.dao.UserDao;
import com.quycntt.daoimp.UserDaoImp;

public class UserService implements UserDao{
	private UserDaoImp userDaoImp = new UserDaoImp();
	
	@Override
	public User checkLogin(String email, String password) {
		return userDaoImp.checkLogin(email, password);
	}
}
