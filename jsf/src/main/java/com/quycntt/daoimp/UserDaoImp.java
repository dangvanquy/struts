package com.quycntt.daoimp;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.quycntt.bean.User;
import com.quycntt.dao.UserDao;

public class UserDaoImp implements UserDao{
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public User checkLogin(String email, String password) {
		Session session = null;
		Transaction transaction = null;
		User user = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from User where email='"+email+"' and password='"+password+"'";
			user = (User) session.createQuery(sql).uniqueResult();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return user;
	}
}
