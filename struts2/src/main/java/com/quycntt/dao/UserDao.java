package com.quycntt.dao;

import com.quycntt.entity.User;

public interface UserDao {
    User getUser(String email, String password);
    boolean register(User user);
}