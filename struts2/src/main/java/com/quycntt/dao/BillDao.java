package com.quycntt.dao;

import com.quycntt.entity.Bill;

public interface BillDao {
    boolean insertBill(Bill bill);
}