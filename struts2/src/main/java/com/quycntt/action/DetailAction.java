package com.quycntt.action;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.Cart;
import com.quycntt.entity.Food;
import com.quycntt.service.FoodService;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DetailAction extends ActionSupport implements SessionAware,ServletRequestAware{
    private FoodService foodService;
    private int id;
    private String name;
    private double price;
    private int quantity;
    private Food food;
    private List<Cart> listCart = new ArrayList<Cart>();
    private HttpServletRequest request;
    private String sumstring;
    String dataJson;
    private Map<String, Object> session;

    public String getDataJson() {
        return dataJson;
    }

    public void setDataJson(String dataJson) {
        this.dataJson = dataJson;
    }

    public void setFoodService(FoodService foodService) {
        this.foodService = foodService;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public List<Cart> getListCart() {
        return listCart;
    }

    public void setListCart(List<Cart> listCart) {
        this.listCart = listCart;
    }

    public String getSumstring() {
        return sumstring;
    }

    public void setSumstring(String sumstring) {
        this.sumstring = sumstring;
    }

    @Override
    public String execute() throws Exception {
        this.food = foodService.getFood1(id);
        return SUCCESS;
    }

    public String addCart() {
        Cart cart = new Cart();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            cart = objectMapper.readValue(dataJson, Cart.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (session.size() == 0) {
            listCart.add(cart);
        } else {
                int idCheck = check(id);
                if (idCheck == -1) {
                    listCart.add(cart);
                } else {
                    int quantity1 = listCart.get(idCheck).getQuantity()+1;
                    listCart.get(idCheck).setQuantity(quantity1);
                }
        }
        session.put("listcart", listCart);
        int sum = sum();
        sumstring = "" + sum;
        session.put("sum",sumstring);
        return SUCCESS;
    }

    public String deleteCart() {
        int idCheck = check(id);
        listCart.remove(idCheck);
        int sum = sum();
        sumstring = "" + sum;
        session.put("sum",sumstring);
        return SUCCESS;
    }

    private int check(int id) {
        for (int i=0; i<listCart.size();i++) {
            if (listCart.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    private int sum() {
        int sum1 = 0;
        for (int i=0;i<listCart.size();i++) {
            sum1 = (int) (sum1 + listCart.get(i).getQuantity() * listCart.get(i).getPrice());
        }
        return sum1;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }
}