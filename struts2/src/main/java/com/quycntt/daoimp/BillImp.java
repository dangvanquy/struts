package com.quycntt.daoimp;

import com.quycntt.dao.BillDao;
import com.quycntt.entity.Bill;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class BillImp implements BillDao{
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public boolean insertBill(Bill bill) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        boolean check = false;
        try {
            transaction = session.beginTransaction();
            int idCheck = (Integer) session.save(bill);
            if (idCheck > 0) {
                check = true;
            } else {
                check = false;
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return check;
    }
}