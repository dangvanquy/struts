package com.quycntt.daoimp;

import com.quycntt.dao.MenuDao;
import com.quycntt.entity.Menu;
import org.hibernate.*;

import java.util.List;

public class MenuImp implements MenuDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Menu> getListMenu() {
        Session sessin = sessionFactory.openSession();
        Transaction transaction = null;
        List<Menu> listMenu = null;

        try {
            transaction = sessin.beginTransaction();
            Query query = sessin.createQuery("FROM menu");
            listMenu = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            sessin.close();
        }
        return listMenu;
    }
}