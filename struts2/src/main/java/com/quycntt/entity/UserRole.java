package com.quycntt.entity;

import javax.persistence.*;

@Entity(name = "userrole")
public class UserRole {
    @Id
    private int id;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="userid")
    private User userid;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="roleid")
    private Role roleid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    public Role getRoleid() {
        return roleid;
    }

    public void setRoleid(Role roleid) {
        this.roleid = roleid;
    }
}