package com.quycntt.service;

import com.quycntt.dao.BillDao;
import com.quycntt.daoimp.BillImp;
import com.quycntt.entity.Bill;

public class BillService implements BillDao {
    private BillImp billImp;

    public void setBillImp(BillImp billImp) {
        this.billImp = billImp;
    }
    public boolean insertBill(Bill bill) {
        return billImp.insertBill(bill);
    }
}