<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"  language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href='<c:url value="../../resources/css/bootstrap.css"/> '/>
    <link rel="stylesheet" href='<c:url value ="../../resources/css/freshfood.css"/>'/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
</head>
<body>

<s:form action="login" method="POST">
    <s:textfield type="text" name="email" label="Email"/>
    <s:password name="password" label="Password"/>
    <s:submit label="Login"/>
</s:form>

</body>
</html>