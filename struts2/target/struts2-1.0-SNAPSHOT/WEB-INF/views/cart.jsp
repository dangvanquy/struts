<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"  language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>cart</title>
    <link rel="stylesheet" href='<c:url value="../../resources/css/bootstrap.css"/> '/>
    <link rel="stylesheet" href='<c:url value ="../../resources/css/freshfood.css"/>'/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="title">
        <h3>Danh Sách Giỏ Hàng</h3>
    </div>

    <table class="table">
        <thead>
        <tr>
            <td>Tên</td>
            <td>Giá</td>
            <td>Số lượng</td>
            <td>Mã</td>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${listcart}" var="cart">
                <tr>
                    <td class="name1"><c:out value="${cart.getName()}"/></td>
                    <td class="price" data-price="<c:out value="${cart.getPrice()}" />"><c:out value="${cart.getPrice()}" />VNĐ</td>
                    <td class="quantity" data-quantity="${cart.getQuantity()}">${cart.getQuantity()}</td>
                    <td class="id" data-id="<c:out value="${cart.getId()}" />"><c:out value="${cart.getId()}" /></td>
                    <c:if test="${users != null}">
                        <td><button class="btn-dathang btn btn-success">Đặt</button></td>
                    </c:if>
                    <td><button class="btn-delete btn btn-danger">Xóa</button></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div class="tongtien">
        <p>Tổng Tiền : ${sum} VNĐ</p>
    </div>
    <c:choose>
        <c:when test="${users != null }">
            <p class="userid" data-userid="${users.getId()}"></p>
        </c:when>
        <c:otherwise>
            <p>Đăng nhập để đặt hàng</p>
            <div class="menu2">
                <ul>
                    <li><a href="loginexe">ĐĂNG NHẬP</a></li>
                    <li><a href="registerexe">ĐĂNG KÍ</a></li>
                </ul>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<script src="../../resources/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $(".btn-delete").click(function () {
        var result = confirm("Xóa khỏi giỏ hàng?")
        if (result) {
            var del = $(this);
            var id = $(this).closest("tr").find(".id").attr("data-id");

            $.ajax({
                url: 'delete',
                type: 'GET',
                data: {id: id},
                success: function () {
                    del.closest("tr").remove();
                    location.reload();
                },
                error: function () {
                }
            });
        }
    });

    $(".btn-dathang").click(function () {
        var name = $(this).closest("tr").find(".name1").text();
        var price = $(this).closest("tr").find(".price").attr("data-price");
        var quantity = $(this).closest("tr").find(".quantity").attr("data-quantity");
        alert("Đặt Hàng");

        $.ajax({
            url: 'cart',
            type: 'GET',
            data: {
                name: name,
                price:price,
                quantity: quantity
            },
            success: function () {
                alert("Đặt Hàng");
            },
            error: function () {
            }
        });
    });
</script>
</body>
</html>
