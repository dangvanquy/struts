package com.quycntt.dto;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.quycntt.bean.Category;
import com.quycntt.bean.Product;
import com.quycntt.service.CategoryService;
import com.quycntt.service.ProductService;

@ManagedBean(name="indexManagedBean")
@SessionScoped
public class IndexManagedBean {
	private CategoryService categoryService = new CategoryService();
	private ProductService productService = new ProductService();
	public static final int START = 0;
	public static final int COUNT = 8;
	
	public List<Category> findAll() {
		return categoryService.findAll();
	}
	
	public List<Product> findAllLimit() {
		return productService.findAllLimit(START, COUNT);
	}
}
