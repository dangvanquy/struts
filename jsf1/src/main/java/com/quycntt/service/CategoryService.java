package com.quycntt.service;

import java.util.List;

import com.quycntt.bean.Category;
import com.quycntt.dao.CategoryDao;
import com.quycntt.daoimp.CategoryDaoImp;

public class CategoryService implements CategoryDao{

	private CategoryDaoImp categoryDaoImp = new CategoryDaoImp();
	
	public List<Category> findAll() {
		return categoryDaoImp.findAll();
	}
}
