package com.quycntt.service;

import java.util.List;

import com.quycntt.bean.Product;
import com.quycntt.dao.ProductDao;
import com.quycntt.daoimp.ProductDaoImp;

public class ProductService implements ProductDao{

	private ProductDaoImp productDaoImp = new ProductDaoImp();
	
	public List<Product> findAllLimit(int start, int count) {
		return productDaoImp.findAllLimit(start, count);
	}
}
