package com.quycntt.daoimp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.quycntt.bean.HibernateUtil;
import com.quycntt.bean.Product;
import com.quycntt.dao.ProductDao;

public class ProductDaoImp implements ProductDao{

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public List<Product> findAllLimit(int start, int count) {
		Session session = null;
		List<Product> listProductLimit = null;
		Transaction transaction = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String sql = "from Product order by joindate desc";
			listProductLimit = session.createQuery(sql).setFirstResult(start).setMaxResults(count).getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			session.close();
		}
		return listProductLimit;
	}
}
