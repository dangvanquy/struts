package com.quycntt.dao;

import java.util.List;

import com.quycntt.bean.Category;

public interface CategoryDao {
	List<Category> findAll();
}
