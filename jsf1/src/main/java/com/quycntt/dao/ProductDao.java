package com.quycntt.dao;

import java.util.List;

import com.quycntt.bean.Product;

public interface ProductDao {
	List<Product> findAllLimit(int start, int count);
}
