package com.quycntt.dao;

import java.util.List;

import com.quycntt.entity.Product;

public interface ProductDao {
	List<Product> findLimit(int start, int count);
	Product findOne(int id);
	List<Product> findByCategory(int idcategory);
}
