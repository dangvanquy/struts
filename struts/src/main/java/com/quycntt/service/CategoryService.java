package com.quycntt.service;

import java.util.List;

import com.quycntt.dao.CategoryDao;
import com.quycntt.daoimp.CategoryDaoImp;
import com.quycntt.entity.Category;

public class CategoryService implements CategoryDao{
	private CategoryDaoImp categoryDaoImp = new CategoryDaoImp();
	
	public List<Category> findAll() {
		return categoryDaoImp.findAll();
	}
}
