package com.quycntt.service;

import java.util.List;

import com.quycntt.dao.ProductDao;
import com.quycntt.daoimp.ProductDaoImp;
import com.quycntt.entity.Product;

public class ProductService implements ProductDao{
	private ProductDaoImp productDaoImp = new ProductDaoImp();

	public List<Product> findLimit(int start, int count) {
		return productDaoImp.findLimit(start, count);
	}

	public Product findOne(int id) {
		return productDaoImp.findOne(id);
	}

	public List<Product> findByCategory(int idcategory) {
		return productDaoImp.findByCategory(idcategory);
	}
}
