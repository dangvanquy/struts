package com.quycntt.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.Cart;
import com.quycntt.entity.Category;
import com.quycntt.entity.Product;
import com.quycntt.service.CategoryService;
import com.quycntt.service.ProductService;

public class DetailAction extends ActionSupport{
	private CategoryService categoryService = new CategoryService();
	private ProductService productService = new ProductService();
	private List<Category> listCategory;
	private List<Product> listProduct;
	private Product product = new Product();
	private int id;
	private String dataJson;
	
	public List<Category> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<Category> listCategory) {
		this.listCategory = listCategory;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getDataJson() {
		return dataJson;
	}

	public void setDataJson(String dataJson) {
		this.dataJson = dataJson;
	}
	
	public List<Product> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<Product> listProduct) {
		this.listProduct = listProduct;
	}

	@Override
	public String execute() throws Exception {
		listCategory = categoryService.findAll();
		product = productService.findOne(id);
		System.out.println(product.getCategory().getId());
		listProduct = productService.findByCategory(product.getCategory().getId());
		return SUCCESS;
	}
	
	public String addCart() {
		Cart cart = new Cart();
        ObjectMapper objectMapper = new ObjectMapper();
        int id = 0;
        
        try {
        	JsonNode jsonNode = objectMapper.readTree(dataJson);
        	 id = jsonNode.get("id").asInt();
        	 cart = objectMapper.readValue(dataJson, Cart.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        if (session.get("cart") == null) {
        	List<Cart> listCart = new ArrayList<Cart>();
            listCart.add(cart);
            session.put("cart",listCart);
        } else {
        	List<Cart> listCart = (List<Cart>) session.get("cart");
        	int idCheck = check(id);
            if (idCheck == -1) {
                listCart.add(cart);
            } else {
                int quantity1 = listCart.get(idCheck).getQuantity()+1;
                listCart.get(idCheck).setQuantity(quantity1);
            }
                session.put("cart",listCart);
        }
		return SUCCESS;
	}
	
	public String deleteCart() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		List<Cart> listCart = (List<Cart>) session.get("cart");
		int check = check(id);
		System.out.println(id);
		listCart.remove(check);
		return SUCCESS;
	}
	
	private int check(int id) {
		Map<String, Object> session = ActionContext.getContext().getSession();
		List<Cart> listCart = (List<Cart>) session.get("cart");
		for (int i = 0; i < listCart.size(); i++) {
			if (listCart.get(i).getId() == id) {
				return i;
			}
		}
		return -1;
	}
}
